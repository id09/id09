# general physics functions

import numpy as np
from scipy.constants import e


def photoelectric_cross_section(energy):
    """Photoelectric cross section of SILICON (cm2/g)."""

    loge = np.log10(energy/1e3)  # energy in keV

    # empirical formula from Owen et al, J. Synchrotron Rad. 2009
    logApe = 4.158 - 2.238*loge - 0.477*loge**2 + 0.0789*loge**3

    Ape = 10**logApe

    return Ape*1e-1


def photoconversion_ratio(energy, thickness):
    """Photoconversion ration of SILICON."""

    Ape = photoelectric_cross_section(energy)

    rho = 2329.0  # kg/m3

    Neh = energy/3.6  # Number of electron-hole pairs

    Q = e*Neh  # Charge created by the absorption of one X-ray photon

    pr = 1/Q/(1 - np.exp(-Ape*rho*thickness))

    return pr


def photon_flux(current, energy, thickness=300e-6, front_cover=None):
    """Incident photon flux from Silicon PIN diode current."""

    pr = photoconversion_ratio(energy, thickness=thickness)

    phi = current*pr

    if front_cover is not None:
        phi = phi / np.squeeze(front_cover.transmission(energy))

    return phi


def photons_per_pulse(current, energy, thickness=300e-6, front_cover=None,
                      rep_rate=1e3):
    """Number of incident photons per pulse from Silicon PIN diode current."""

    phi = photon_flux(current=current, energy=energy, thickness=thickness,
                      front_cover=front_cover)

    phi /= rep_rate

    return phi
