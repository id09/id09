class Diode:

    """
    Class for diodes (PIN diodes, diamond detector, GaAs detector, ...).

    The goal is to have an objects that allows:
    - moving the diode IN/OUT of the beam
    - reading the diode CURRENT or Analog Output through a P201
    - adjusting the current RANGE (or put it into AUTO)
    - converting diode output to photon flux

    ...
    """

    def __init__(self, keithley=None, motor=None, motor_in = None, motor_out = None, wago=None, wago_ch=None,
                 wago_in=0, wago_out=1, tolerance=1e-3, v2f=False,
                 v2f_ch=None):

        from bliss import config

        if wago is not None and wago_ch is None:
            raise ValueError("'wago_ch' must be str if 'wago' is not None.")
        
        if v2f and v2f_ch is None:
            raise ValueError("'v2f_ch' must be str if 'v2f' is True.")
        
        if keithley is not None:
            self.cur = config.static.get_config().get(keithley)
            # self.cur = self.cur._counter_controller  # optional
            rngs = []
            for rng in self.cur.possible_ranges:
                rngs.append(convert_unit(rng, unit="A", precision=0,
                            strip_spaces=True))
            self.possible_ranges = rngs
            self.auto_range = self.cur.auto_range
        else:
            self.cur = None

        if motor is not None:
            self.motor = config.static.get_config().get(motor)
        else:
            self.motor = None
        
        self.motor_in = motor_in
        self.motor_out = motor_out
        self.wago = wago
        self.wago_ch = wago_ch
        self._wago_in = wago_in
        self._wago_out = wago_out
        self._tolerance = tolerance

        self.v2f = v2f
        self.v2f_ct = v2f_ch

        if wago is not None:
            self._wago = config.static.get_config().get(wago)

        if v2f is True:
            p201 = config.static.get_config().get("p201")
            self.v2f = p201.counters[self.v2f_ct]

    def __info__(self):
        info = ["ID09 Diode obj. Diode is %s"%self.get_pos()]
        return "\n".join(info)

    @property
    def counters(self):
        # Not working yet...
        counters_list = []
        if self.cur is not None:
            counters_list.append(self.cur)
        if self.v2f is not None:
            counters_list.append(self.v2f)
        return counters_list

    @property
    def range(self):
        """Get the keithley current range (A)."""
        if self.cur is not None:
            cur_range = self.cur.range
            if cur_range == "AUTO":
                return "AUTO"
            cur_range_str = convert_unit(cur_range, unit="A", precision=0,
                                         strip_spaces=True)
            # print("Current range: %s" % cur_range_str)
            return cur_range_str

    @range.setter
    def range(self, cur_range):
        """Set keithley current range (A).
        
        Parameters
        ----------
        cur_range: float or str
            Keithley current range (e.g. 2e-9 or "2nA").
            It must be one of the available ranges for the keithley.

        """

        if not isinstance(cur_range, float) and not isinstance(cur_range, str):
            raise TypeError("'cur_range' must be float or str.")

        if isinstance(cur_range, float):
            cur_range = convert_unit(cur_range, unit="A", precision=0,
                                     strip_spaces=True)

        if cur_range not in self.possible_ranges:
            print("'cur_range' must be one of : ", self.possible_ranges)
        else:
            unit = cur_range[-2:]
            value = float(cur_range[:-2])
            self.cur.range = value*inv_si[unit[0]]

    def IN(self):
        """Move the diode IN the beam."""
        if self.motor is not None:
            umv(self.motor,self.motor_in)
        elif self.wago is not None:
            self._wago.set(self.wago_ch, self._wago_in)

    def OUT(self):
        """Move the diode OUT of the beam."""

        if self.motor is not None:
            umv(self.motor,self.motor_out)
        elif self.wago is not None:
            self._wago.set(self.wago_ch, self._wago_out)
        else:
            print("WARNING: no OUT position for diode %s." % self.__name__)

    def get_pos(self):
        """Get the position (IN/OUT) of the deiode."""

        if self.motor is not None:
            if np.isclose(self.motor.position, self.motor_in, rtol=self._tolerance):
                return "IN"
            if np.isclose(self.motor.position, self.motor_out, rtol=self._tolerance):
                return "OUT"        
        elif self.wago is not None:
            pos = self._wago.get(self.wago_ch)
            if np.isclose(pos, self._wago_in, rtol=self._tolerance):
                return "IN"
            elif np.isclose(pos, self._wago_out, rtol=self._tolerance):
                return "OUT"
            else:
                print("WARNING: could not determine diode position.")
        else:
            return "IN"

#pd0 = Diode(keithley="pd0_keith_sensor", motor = "pd0z", motor_in = 0, motor_out = 32, 
#            tolerance = 0.01)

#pd1 = Diode(keithley="pd1_keith_sensor", wago="wcid09e", wago_ch="pd1",
#            wago_in=0, wago_out=1, v2f=True, v2f_ch="pd1ic")

#pd2 = Diode(keithley="pd2_keith_sensor", v2f=True, v2f_ch="pd2ic")            

#pd3 = Diode(keithley="pd3_keith_sensor", motor = "mpd3", motor_in = 90, motor_out = 0, 
#            tolerance = 3, v2f=True, v2f_ch="pd3ic")
            
#pd4 = Diode(keithley="pd4_keith_sensor", motor = "mpd4", motor_in = 90, motor_out = 180, 
#            tolerance = 3, v2f=True, v2f_ch="pd4ic") #not tested
            
