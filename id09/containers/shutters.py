# -*- coding: utf-8 -*-
"""Module for handling different kind of shutters available at ID09."""

__author__ = "Celine Mariette, Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "01/09/2022"
__version__ = "0.0.1"


from time import sleep
from bliss.common.logtools import disable_print


class Shutter:
    """
    Class to handle X-ray and laser shutters.

    This is just a "backbone" class. It must be inherited and
    most methods must be redefined.

    Attributes
    ----------
    - state
        - open
        - closed
    - scan_mode:
        - "open" (always open during a scan)
        - "closed" (always closed during a scan)
        - "gated" (gated during a scan)
        - "pulsed" (pulsed during a scan)

    Methods
    -------
    - open
    - close
    - ext_trig_on
    - ext_trig_off
    - fast_motion_on
    - fast_motion_off
    - get_state
    - get_scan_mode, set_scan_mode

    """

    def __init__(self, config):

        self._config = config
        self._name = config.get("name")
        self._description = ""
        self._scan_modes = ["gated", "pulsed", "open", "closed"]
        scan_mode = config.get("scan_mode", default="gated")
        if scan_mode not in self._scan_modes:
            raise ValueError("'scan_mode' should be one of: %s"
                             % self._scan_modes)
        else:
            self._scan_mode = scan_mode
        self._state = self.get_state()
        # self.close()  # ML: Sep 10, 2024

    def ext_trig_off(self):
        pass

    def ext_trig_on(self):
        pass

    def is_ext_trig(self):
        pass

    def fast_motion_on(self):
        pass

    def fast_motion_off(self):
        pass

    def is_fast_motion(self):
        pass

    def is_open(self):
        pass

    def is_open_disabled(self):
        pass

    def get_state(self):
        # should contain:
        # - self._state = get state from hdw
        # - return self._state
        pass

    def get_scan_mode(self):
        return self._scan_mode

    def set_scan_mode(self, scan_mode, verbose=True):
        """Set the way the shutter is externally controlled."""
        if scan_mode not in self._scan_modes:
            out = "'%s' not in available modes: " % self._name
            out += ','.join(self._scan_modes)
            out += "\nShutter mode is still: '%s'" % self._scan_mode
            print(out)
        else:
            self._scan_mode = scan_mode
            if verbose:
                print("'%s' scan mode is now '%s'." % (self._name, scan_mode))

    def set_mode(self, mode):
        # to be deleted; currently used for back compatibility
        print("DEPRECATED: you should use set_scan_mode() !!!")
        self.set_scan_mode(scan_mode=mode)

    def open(self):
        # should contain:
        # - ext_trig_off()
        # - hdw open
        # - self._state = "open"
        pass

    def close(self):
        # should contain:
        # - ext_trig_off()
        # - hdw close
        # - self._state = "closed"
        pass

    def gated(self):
        self.set_scan_mode("gated", verbose=False)

    def pulsed(self):
        self.set_scan_mode("pulsed", verbose=False)

    def __info__(self):
        if self._description != "":
            info = "%s: " % self._description
        else:
            info = "'%s': " % self._name
        info += "state='%s', " % self.get_state()
        info += "scan_mode='%s'" % self.get_scan_mode()
        return info


class TunnelShutter(Shutter):
    """Class to handle tunnel shutters made of a rotating copper block.

    For synchronized operation (pulsed or gated):
     - the shutter is externally triggered through the IcePAP controller
       (ext_trig_on)
     - the speed and acceleration are increased with respect to normal mode
       (fast_motion_on)

    In pulsed mode (opening time ~1.5 ms and/or rep rate <= 40 Hz):
     - the shutter is moved within a small angular range around the tunnel

    """

    def __init__(self, config):
        self._axis = config.get("axis")
        self._open_pos = config.get("open_pos")
        self._closed_pos = config.get("closed_pos")
        self._ext_trig_vel = config.get("ext_trig_vel")
        self._ext_trig_acc = config.get("ext_trig_acc")
        self._ext_trig_closed_pos = config.get("ext_trig_closed_pos")
        self._open_disabled = config.get("open_disabled", False)
        self._ext_trig = False
        self._fast_motion = False
        super().__init__(config)
        self._description = "x-ray shutter"

    def ext_trig_on(self):
        """Enable X-ray ms-shutter external triggering.

        There are two different possibilities for external triggering:
        1) 'gated' mode
           The ms-shutter will open and close once during the whole burst.
           This mode should be used when N354-xshut rep rate is high (1 kHz).
        2) 'pulsed' mode
           The ms-shutter will rotate back and forth between -3 and +3 deg
           every time the shutr IcePAP controller receives an external
           triggering pulse. The idea is that only one X-ray pulse can go
           through every time shutr crosses the 0.
           This mode should be used when the N354-xshut rep rate is low
           (<=40 Hz)

        NOTE: in gated mode, the shutter could be open after a CTRL-C
              (this makes sense in the context of a scan)

        """

        self._axis.activate_tracking(False)

        if self._axis.position < self._open_pos:
            self._axis.move(self._ext_trig_closed_pos[0])
            closed_pos = self._ext_trig_closed_pos[0]  # useful in gated mode
            first_pos = closed_pos  # useful in pulsed mode
            last_pos = self._ext_trig_closed_pos[1]  # useful in pulsed mode
        else:
            self._axis.move(self._ext_trig_closed_pos[1])
            closed_pos = self._ext_trig_closed_pos[1]  # useful in gated mode
            first_pos = closed_pos  # useful in pulsed mode
            last_pos = self._ext_trig_closed_pos[0]  # useful in pulsed mode

        if self.get_scan_mode() == "gated":
            pos_list = [closed_pos, self._open_pos]
        else:
            pos_list = [first_pos, last_pos]

        self._axis.set_tracking_positions(pos_list, cyclic=True)

        self._axis.activate_tracking(True)
        self._ext_trig = True

    def ext_trig_off(self):
        """Stop the tracking mode on the ms-shutter IcePAP axis (shutr)."""
        self._axis.activate_tracking(False)
        sleep(0.05)
        self._axis.activate_tracking(False)
        self._ext_trig = False

    def is_ext_trig(self):
        # There is currently no way to check directly if the IcePAP is in
        # tracking mode.
        # So we just check if the axis is moving or not
        if not self._axis.state.READY:
            if not self._ext_trig:
                print("WARNING: self._ext_trig was False even if" +
                      " axis state was NOT READY.")
                # self._ext_trig = True  # alternative 2
            return True  # alternative 1
        else:
            if self._ext_trig:
                print("WARNING: self._ext_trig was True even if" +
                      " axis state is READY.")
                self._ext_trig = False
            return False  # alternative 1
        # return self._ext_trig  # alternative 2

    def fast_motion_on(self):
        with disable_print():
            self._axis.velocity = self._ext_trig_vel
            self._axis.acceleration = self._ext_trig_acc
        self._fast_motion = True

    def fast_motion_off(self):
        with disable_print():
            self._axis.velocity = self._axis.config.get("velocity")
            self._axis.acceleration = self._axis.config.get("acceleration")
        self._fast_motion = False

    def is_fast_motion(self):
        return self._fast_motion

    def sync_scan_prepare(self):
        """Prepare the ms-shutter synchronized mode for scans.

        NOTE: in gated mode, the shutter could be open after a ctrl-c
        """
        self.ext_trig_on()
        self.fast_motion_on()

    def sync_scan_disable(self):
        """Stop the ms-shutter synchronized mode for scans.

        NOTE: if this function is not called through self.open() or
              self.close() the shutter might remain open!
        """
        self.ext_trig_off()
        self.fast_motion_off()

    def prepare_for_ext_trig(self):
        # to be deleted; currently used for back compatibility
        self.sync_scan_prepare()

    def _inhibit_ext_trig(self):
        # to be deleted; currently used for back compatibility
        self.sync_scan_disable()

    def is_open(self):
        pos = self._axis.position
        if (pos > -0.5) and (pos < 0.5):
            return True
        else:
            return False

    def is_open_disabled(self):
        return self._open_disabled

    def get_state(self):
        if self.is_open():
            self._state = "open"
        else:
            self._state = "closed"
        return self._state

    def close(self):
        self.sync_scan_disable()
        if self._axis.position < self._open_pos:
            self._axis.move(self._closed_pos[0])
        else:
            self._axis.move(self._closed_pos[1])
        self._state = "closed"

    def open(self):
        if self.is_open_disabled():
            print("ERROR: manual opening of the shutter is disabled.")
        else:
            self.sync_scan_disable()
            self._axis.move(self._open_pos)
            self._state = "open"


class LaserShutter(Shutter):
    """
    Class to handle laser shutters.

    These are typically shutters where an electromagnetic actuator is used
    to drive a thin blade in/out the beam path.

    """

    def __init__(self, config):
        self.core = config.get("core")
        # self._is_timeout = self.core._is_timeout
        super().__init__(config)
        self._open_disabled = config.get("open_disabled", False)
        self._description = "laser shutter"

    def ext_trig_on(self):
        self.core.ext_trig_on()
        self._ext_trig = True

    def ext_trig_off(self):
        self.core.ext_trig_off()
        self._ext_trig = False

    def is_ext_trig(self):
        """Check if external trigger on TTL high is enabled."""
        # ext_trig = self.core.get_ext_trig()
        if self.core.ext_trig_mode == "trigger on high":
            if not self._ext_trig:
                print("WARNING: self._ext_trig was False even if" +
                      " controller external trigger is on.")
                self._ext_trig = True
            return True
        else:
            if self._ext_trig:
                print("WARNING: self._ext_trig was True even if" +
                      " controller external trigger is off.")
                self._ext_trig = False
            return False

    def fast_motion_on(self):
        self.core.fast_motion_on()
        self._fast_motion = True

    def fast_motion_off(self):
        self.core.fast_motion_off()
        self._fast_motion = False

    def is_fast_motion(self):
        """Check if fast motion is enabled."""
        # hdw_mode = self.core.get_hdw_mode()
        if self.core.hdw_mode == "fast":
            self._fast_motion = True
        else:
            self._fast_motion = False
        return self._fast_motion

    def is_open(self):
        return self.core.is_open

    def is_open_disabled(self):
        return self._open_disabled

    def get_state(self):
        # self._is_timeout = self.core.check_timeout()
        # if self._is_timeout:
        #     return "unknown"
        # self._state = self.core.get_state()
        return self.core.state

    def open(self):
        if self.is_open_disabled():
            print("Manual opening of the laser shutter is disabled.")
        else:
            # self._is_timeout = self.core.check_timeout()
            # if self._is_timeout:
            #     print("Cannot open the laser shutter because" +
            #           " of a communication timeout...\n")
            #     return
            # else:
            #     self.ext_trig_off()
            #     self.core.open()
            #     self._state = "open"
            self.ext_trig_off()
            self.core.open()
            self._state = 'open'

    def close(self):
        # self._is_timeout = self.core.check_timeout()
        # if self._is_timeout:
        #     print("Cannot close the laser shutter because" +
        #           " of a communication timeout...\n")
        #     return
        # else:
        self.ext_trig_off()
        self.core.close()
        self._state = "close"

    def apply_config(self):
        """re-initialize core and obj + apply beacon settings."""
        self.core.__init__()
        self.__init__(self._config)
