# -*- coding: utf-8 -*-
"""Description and properties of ID09 attenuators."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "04/03/2020"
__version__ = "0.0.1"


import numpy as np
import itertools
import joblib
import functools
from collections import OrderedDict
from copy import copy
import time

from bliss.controllers.wago.wago import Wago
from bliss.shell.standard._motion import move

from id09.materials import get_density, transmission
from id09.controllers.wagoaxis_ctrl import WagoAxis

memory = joblib.Memory("/tmp/.id09_att_cache", verbose=False)


# @memory.cache
def transmission_multi(materials, energies, thicknesses, densities=None,
                       angle=None, bw=None):
    """Calc X-ray transmisssion of attenuators at given photon energy.

    Parameters
    ----------
    materials : list of str
        List of materials (chemical formulas, e.g. ['Si3N4', 'Al']).
    energies : array-like
        List of X-ray photon energies (eV).
    thicknesses : list
        List of thicknesses (m).
    densities : list or None, optional
        List of densities (kg/m3).
        If None (default), density is guessed from material composition.
    angle : float or None, optional
        Effective angle between the attenuator normal and the
        incoming beam (deg).
        Default (None): normal incidence.
    bw : float or None, optional
        Convolution spectrum (Gaussian) relative bandwidth.
        If energies spaced by more than one-tenth of 'bw' are present,
        each of these energies is convoluted with a Gaussian having
        nregrid=11 points between -3*sigma and +3*sigma.

    Returns
    -------
    T : ndarray
        Attenuator transmission.

    To Do
    -----
    - merge 'transmission()' and 'transmission_multi()'

    """

    if densities is None:
        densities = []
        for material in materials:
            densities.append(get_density(material))

    for rho, cmpnd in zip(densities, materials):
        if rho is None:
            raise ValueError("Density of materials is required to compute"
                             + " transmission.\n --> Density of " +
                             "'%s' is unknown." % cmpnd)

    T = 1.

    for m, d, rho in zip(materials, thicknesses, densities):
        T *= transmission(material=m, energy=energies, thickness=d,
                          density=rho, angle=angle, bw=bw)

    return T


class Attenuator:

    """
    An Attenuator is a solid target (or a set of solid targets stacked
    together) having known properties (composition, density, thickness).

    It is used to calculate the transmission of an X-ray beam through
    different objects (windows, filters, supports, ...).

    If is the elementary object used to build:
    - AttenuatorAxis
    - AttenuatorMultiAxis
    - AttenuatorWagoAxis
    - AttenuatorWagoSet

    Attributes
    ----------
    material : list
        Composition (chemical formula, e.g. 'Zr').
    thickness : list
        Thickness (m).
    density : list
        Density (kg/m3).
    contact : list or None
        List of the materials that are in direct contact.
        This attribute is useful for heatload calculations.
    angle : float or None
        Effective angle between the attenuator normal and the
        incoming beam (deg).
    diameter : float or None
        Diameter of a round attenuator (clear aperture) (m).
    width : float or None
        Width of a rectangular attenuator (m).
    height : float or None
        Height of a rectangular attenuator (m).
    css_distance : float or None
        Distance from CSS.
    name : str or None
        Attenuator label name.
    description : str or None
        Attenuator verbose description.

    Methods
    -------
    transmission(E, bw=None)
        Calc attenuator transmission at given photon energy.

    Notes
    -----
    - For a limited number of materials, it is possible to provide the
      material name (polymide, pmma, ...) in place of the chemical formula
      in the 'material' input parameter.

    Examples
    --------

    >>> att = Attenuator(config=None, material=['C', 'Al'],
    >>>                  thickness=(0.35e-3, 1e-3])

    >>> att.transmission(15e3)
    0.10567583930846362

    """

    def __init__(self, config, material=None, thickness=None, density=None,
                 contact=None, angle=None, diameter=None, width=None,
                 height=None, css_distance=None, name=None, description=None):
        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the session.
        material : str or list or None, optional
            Composition (chemical formula, e.g. 'Zr' or ['Si3N4', 'Al']).
            If None (default), 'material' is taken from the YAML file.
        thickness : float or list or None, optional
            Thickness (m).
            If None (default), 'thickness' is taken from the YAML file.
        density : float or list or None, optional
            Density (kg/m3). Default is None.
        contact : list of str or list of list or None, optional
            List of the materials that are in direct contact. The order in the
            list corresponds to the order the beam hits the different
            materials. It must be a sublist of 'material'. Default is None.
        ...

        Notes
        -----
        - If 'material' is a list and 'thickness' is float, the same
          thickness will be assumed for all materials in the attenuator.
        - If 'material' is a list and 'density' is not None, then 'density'
          must be a list as well.
        - The variable 'contact' is useful for thernal load calculations.

        """

        self._config = config

        if config is not None:
            # load from YAML
            self.name = config.get("name")
            self.description = config.get("description")
            material = config.get("material")
            thickness = config.get("thickness")
            density = config.get("density", default=None)
            self.contact = config.get("contact", default=None)
            self.angle = float(config.get("angle", default=0))
            self.diameter = config.get("diameter", default=None)
            self.width = config.get("width", default=None)
            self.height = config.get("height", default=None)
            self.css_distance = config.get("css_distance", default=None)
        else:
            if material is None or thickness is None:
                raise ValueError("'material' and 'thickness' cannot be " +
                                 "None if 'config' is None.")
            self.name = name
            self.description = description
            self.contact = contact
            self.angle = angle
            self.diameter = diameter
            self.width = width
            self.height = height
            self.css_distance = css_distance

        if np.isscalar(material):
            self.material = [material]
        else:
            self.material = material

        if np.isscalar(thickness):
            if len(self.material) > 1:
                self.thickness = [float(thickness)]*len(self.material)
            else:
                self.thickness = [float(thickness)]
        elif len(thickness) < len(material) and len(thickness) > 1:
            raise ValueError("length of 'thickness' should be equal" +
                             " to the length of 'material'")
        else:
            self.thickness = [float(t) for t in thickness]

        if density is None:
            self.density = []
            for m in self.material:
                self.density.append(get_density(m))
        elif len(self.material) == 1:
            if np.isscalar(material):
                self.density = [float(density)]
            else:
                raise TypeError("'density' must be scalar if 'material' is " +
                                "scalar.")
        else:
            if not isinstance(density, list):
                raise TypeError("'density' must be list if 'material' is " +
                                "list.")
            if len(density) != len(self.material):
                raise ValueError("'density' must have same len of 'material'.")
            self.density = density
            for k, m in enumerate(self.material):
                if self.density[k] is None:
                    self.density[k] = get_density(m)
                else:
                    self.density[k] = float(self.density[k])

        if self.diameter is not None:
            self.diameter = float(self.diameter)
        if self.width is not None:
            self.width = float(self.width)
        if self.height is not None:
            self.height = float(self.height)
        if self.css_distance is not None:
            self.css_distance = float(self.css_distance)

    def __str__(self):
        return "Attenuator obj"

    def __info__(self):
        out = []
        for m, t in zip(self.material, self.thickness):
            if m != '':
                if t >= 0.4e-3:
                    out.append("%s_%gmm" % (m, t*1e3))
                else:
                    out.append("%s_%gum" % (m, t*1e6))
            else:
                out.append('empty')
        return ', '.join(out)

    def transmission(self, E, bw=None):
        """Calc attenuator transmission at given photon energy.

        Parameters
        ----------
        E : array-like
            Photon energy (eV).

        Returns
        -------
        T : ndarray
            Attenuator transmission.

        """

        T = transmission_multi(self.material, E, self.thickness, self.density,
                               self.angle, bw=bw)

        return T


class AttenuatorEmpty:

    def __init__(self):
        self.material = ['']
        self.thickness = [0]
        self.contact = None

    def transmission(self, E, bw=None):
        return 1

    def __str__(self):
        return "AttenuatorEmpty obj"

    def __info__(self):
        return 'empty attenuator'


class AttenuatorAxis:

    """
    Object containing several attenuators mounted along a movable axis.
    Only one attenuator at a time can be selected.

    Attributes
    ----------
    atts : OrderedDict of Attenuator
        Dictionary of Attenuator objects.
    labels : list
        Labels of available attenuators. Same as atts.keys().
    position : str
        Label specifiying which attenuator is IN the beam.
        It has to be one of atts.keys() or '?'.
    axis : bliss.common.axis.Axis or None
        BLISS Axis controlling the physical position of attenuators.
    axis_positions : OrderedDict
        List of the axis positions corresponding to an attenuator IN status.
    axis_tolerances : OrderedDict
        Tolerances on the attenuators IN positions.
    descriptions : list
        Verbose description of each attenuator.
    diameter : float or None
        Diameter of clear aperture (m).
    width : float or None
        Width of rectangular attenuator (m).
    height : float or None
        Height of rectangular attenuator (m).
    css_distance : float or None
        Distance from CSS (m).
    name : str or None
        AttnuatorAxis obj name.

    Methods
    -------
    move(label)
        Move in the beam the attenuator corresponding to 'label'.
    transmission(E)
        Calc the transmission of the currently selected attenuator at the
        photone energy 'E'.

    Examples
    --------
    >>> atts = [AttenuatorEmpty(),
    >>>         Attenuator(config=None, material="C", thickness=10e-6)]

    >>> hpda = AttenuatorAxis(config=None, atts=atts, labels=["OUT", "A1"])

    >>> hpda.move(1)

    >>> hpda.move("A1")

    """

    def __init__(self, config, atts=None, labels=None, descriptions=None,
                 axis=None, axis_positions=None, axis_tolerances=None,
                 diameter=None, width=None, height=None, css_distance=None,
                 frontend=None, name=None):

        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the session.
            If 'config' is not None (default), all other parameters are taken
            from the YAML file.`
            If 'config' is None, 'atts' and 'labels' cannot be None.
        atts : list or None, optional
            List of Attenuator objects.
        labels : list or None, optional
            List of attenuator labels.
        descriptions : list or None, optional
            List of attenuator verbose descriptions.
        axis : bliss.common.axis.Axis or None, optional
            Axis controlling the attenuators position. Default is None.
        axis_positions : list or None, optional
            List of axis position corresponding to one of the attenuators.
        axis_tolerances : list or None, optional
            List of axis tolerances (to evaluate if an attenuator is IN).
        ...

        """

        self._config = config

        self.atts = OrderedDict()
        self.descriptions = OrderedDict()
        self.axis_positions = OrderedDict()
        self.axis_tolerances = OrderedDict()

        if config is not None:

            # load from YAML
            self.axis = config.get("axis")
            self.diameter = config.get("diameter", default=None)
            self.width = config.get("width", default=None)
            self.height = config.get("height", default=None)
            self.css_distance = config.get("css_distance", default=None)
            self.frontend = config.get("frontend", default=None)
            self.name = config.get("name")

            positions = self._config['positions']

            for position in positions:
                if 'material' not in position.keys():
                    position['material'] = None
                if 'thickness' not in position.keys():
                    position['thickness'] = None
                if 'density' not in position.keys():
                    position['density'] = None

            self.labels = []

            for position in positions:
                label = position['label']
                self.labels.append(label)
                if position['material'] is None:
                    self.atts[label] = AttenuatorEmpty()
                else:
                    self.atts[label] = Attenuator(
                        config=None,
                        material=position['material'],
                        thickness=position['thickness'],
                        density=position['density'])
                self.descriptions[label] = position['description']
                self.axis_positions[label] = position['axis_position']
                self.axis_tolerances[label] = position['tolerance']

        else:

            if atts is None or labels is None:
                raise ValueError("'atts' and 'labels' and cannot " +
                                 "be None if 'config' is None.")

            self.axis = axis
            self.diameter = diameter
            self.width = width
            self.height = height
            self.css_distance = css_distance
            self.frontend = frontend
            self.name = name

            self.labels = labels

            for k, n in enumerate(labels):
                self.atts[n] = atts[k]
                if self.axis is not None:
                    self.axis_positions[n] = axis_positions[k]
                    self.axis_tolerances[n] = axis_tolerances[k]
                if descriptions is not None:
                    self.descriptions[n] = descriptions[k]

            if self.axis is None:
                self._position = labels[0]

        # Add a method named after each position label
        for label in self.labels:
            self._add_pos_move_method(label)

    def __str__(self):
        return "AttenuatorAxis obj"

    def __info__(self, position=None):

        if position is None:
            position = copy(self.position)

        out1 = []
        max_len = len(max(self.labels, key=len))
        for n, att in self.atts.items():
            name = "'%s'" % n
            if n == position:
                out2 = ["[x] %s" % (name.ljust(max_len+2, ' '))]
            else:
                out2 = ["[ ] %s" % (name.ljust(max_len+2, ' '))]
            out3 = []
            for m, t in zip(att.material, att.thickness):
                if m != '':
                    if t > 0.5e-3:
                        out3.append("%s_%gmm" % (m, t*1e3))
                    else:
                        out3.append("%s_%gum" % (m, t*1e6))
                else:
                    out3.append('no filter')
            out2.append(", ".join(out3))
            out1.append(" ".join(out2))
        return "\n".join(out1)

    def _add_pos_move_method(self, pos):
        """Add a moving method named after a position label."""

        def label_move_func(label):
            self.move(label)

        setattr(self, pos, functools.partial(label_move_func, pos))

    def _convert_pos(self, pos):
        """Convert pos index to pos label."""
        if pos not in self.labels and pos not in range(len(self.labels)):
            raise ValueError("'pos' must be an integer or one of 'labels'.")

        if pos in self.labels:
            return pos
        else:
            return self.labels[pos]

    def move(self, pos, wait=True):
        """Move axis to a position corresponding to an attenuator.

        Parameters
        ----------
        pos : str or int
            Can be either an attenuator label or the attenuator position
            index (0, 1, ...).

        """

        label = self._convert_pos(pos)

        if self.axis is None:
            self._position = label
        else:
            in_pos = True
            if self.position != label:
                in_pos = False
            # print("in pos? ", in_pos)

            destination = self.axis_positions[label]

            frontend_was_open = False
            if self.frontend is not None and self.frontend.is_open:
                frontend_was_open = True
                if not in_pos and not isinstance(self.axis, WagoAxis):
                    self.frontend.close()
                    print()

            # self.axis.move(destination, wait=wait)
            if isinstance(self.axis, WagoAxis):
                if not in_pos:
                    self.axis.move(destination, wait=wait)
            else:
                if not in_pos:
                    # move([(self.axis, destination)], relative=False,
                    #      wait=wait)
                    move(self.axis, destination, relative=False,
                         wait=wait)

            if wait and self.frontend is not None and frontend_was_open:
                if not in_pos and not isinstance(self.axis, WagoAxis):
                    self.frontend.open()
                    self.frontend.mode = 'AUTOMATIC'
                    print()

    @property
    def position(self):
        """AttenuatorAxis position (label)."""
        pos = None

        if self.axis is None:
            return self._position

        for label in self.labels:
            destination = self.axis_positions[label]
            tolerance = self.axis_tolerances[label]
            if np.isclose(self.axis.position, destination, atol=tolerance):
                pos = label

        return pos

    def transmission(self, E, position=None, bw=None):
        """
        Calc transmission at given energy and currently selected attenuator.

        Parameters
        ----------
        E : array-like of float
           X-ray photon energy (eV).
        position : str or int or None, optional
           Label or integer index corresponding to an attenuator.
           If None, the tranmission is calculated for the currently selected
           attenuator.

        Returns
        -------
        T : ndarray of float
           Transmission at given energy and currently selected attenuator.

        """

        if position is None:
            position = copy(self.position)
        else:
            position = self._convert_pos(position)

        if bw is None:
            T = self.atts[position].transmission(E)
        else:
            T = self.atts[position].convolve_transmission(E, bw=bw)

        return T


class AttenuatorMultiAxis:

    """
    Object with multiple attenuator axes. Each instance can have several
    independent axes with several attenuators per axis (NOTE: only one
    attenuator at a time can be selected in each axis).

    Attributes
    ----------
    axes : list of AttenuatorAxis
        List of attenuator axes.

    axes_len : list of int
        Number of attenuators per axis.

    configs : list of tuple
        List of all possible combinations of attenuators position.

    exclude : list of tuple or None
        List of configs to be excluded. If None or [] no config is excluded.

    Methods
    -------
    move_axis(axis_id, pos)
        Select the attenuator corresponding to 'pos' in the given axis.
    move(config)
        Select the attenuators corresponding to 'config'.
    get_config()
        Return the list of the attenuators position.
    transmission(E, position=None)
        Get transmission at a given photon energy and at current selected
        attenuators (default) or at input attenuators position.
    get_best_config(E, T)
        Get attenuator configuration that corresponds to the closest
        transmission at given energy.
    get_att_comb(T)
        Get combination of attenuators giving the transmission closest to the
        target value.

    """

    def __init__(self, config, att_axes=None, att_axes_names=None,
                 exclude=None, name=None):

        """
        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the session.
        att_axes : list or None, optional
            ...
        att_axes_names : list or None, optional
            ...

        """

        self._config = config

        self.att_axes = OrderedDict()
        self._att_axes_len = []

        self._virtual = False

        if config is not None:

            # load from YAML
            name = config.get("name")
            att_axes = config.get("attenuator_axes")
            exclude = config.get("exclude", default=None)

            if any(att_axis.name is None for att_axis in att_axes):
                raise ValueError("'name' attribute cannot be None for any " +
                                 "'att_axes' element.")

        else:

            if any(att_axis.axis is None for att_axis in att_axes):
                self._virtual = True

            if att_axes is None:
                raise ValueError("'att_axes' cannot be None if 'config' is" +
                                 " None.")

            if any(att_axis.name is None for att_axis in att_axes):

                if att_axes_names is None:
                    raise ValueError("'name' attribute cannot be None for " +
                                     "any 'att_axes' element unless " +
                                     "'att_axes_name' is not None.")
                else:
                    # copy 'name' to each element in 'att_axes'
                    for att_axis, n in zip(att_axes, att_axes_names):
                        att_axis.name = n

        self._att_axes = att_axes

        for att_axis in att_axes:
            self.att_axes[att_axis.name] = att_axis
            self._att_axes_len.append(len(att_axis.labels))
            # Add an attribute for each AttenuatorAxis
            setattr(self, att_axis.name, att_axis)

        self.name = name
        self.exclude = exclude

        att_axes_frontend = [att_axis.frontend for att_axis in att_axes]
        self.frontend = next(filter(None, att_axes_frontend), None)

        if exclude is None:
            self.exclude = []

        self.combs = itertools.product(*[range(n) for n in self._att_axes_len])
        self.combs = [c for c in self.combs if c not in self.exclude]

    def __str__(self):
        return "AttenuatorMultiAxes obj"

    def __info__(self, position=None):

        att_axis_names = [att_axis.name+":" for att_axis in self._att_axes]
        max_len = len(max(att_axis_names, key=len))

        if position is None:
            position = copy(self.position)

        out1 = []
        for k, att_axis in enumerate(self._att_axes):
            att_axis_lbl = att_axis_names[k].ljust(max_len+1, " ")
            att_lbls = att_axis.__info__(position=position[k]).split("\n")
            out2 = [att_axis_lbl + att_lbls[0]]
            for att_lbl in att_lbls[1:]:
                out2.append("".ljust(max_len+1) + att_lbl)
            out1.append("\n".join(out2))

        return "\n\n".join(out1)

        # out1 = []
        # for att_axis, pos in zip(self._att_axes, position):
        #     out2 = []
        #     out2.append(att_axis.name)
        #     out2.append("%s" % att_axis.__info__(position=pos))
        #     out1.append("\n".join(out2))

        # return "\n"+"\n\n".join(out1)

    @property
    def position(self):
        """AttenuatorMultAxis position (list of labels)."""
        return [att_axis.position for att_axis in self._att_axes]

    def move(self, *args):
        """
        Move AttenuatorMultiAxis obj.

        Examples
        --------
        >>> ma.move([0, 1])

        >>> ma.move(["OUT", "P1"])

        >>> ma.move("att1", 0, "att2", 1)

        >>> ma.move("att1", "OUT", "att2", "P1")

        """
        if len(args) == 1:
            if not isinstance(args[0], (list, tuple)):
                raise TypeError("'pos' must be None or list or tuple.")
            else:
                pos = args[0]
        else:
            naxes = len(self.att_axes)
            if len(args) != 2*naxes:
                raise Exception("If 'pos' is None, the arguments must be" +
                                " the name and the target position of" +
                                " each axis.")
            else:
                pos = []
                for k in range(0, 2*naxes, 2):
                    pos.append(args[k+1])

        if tuple(pos) in self.exclude:
            print("Cannot move to position: ", pos)
            print("Input position is in the list of excluded positions.")
        else:
            all_in_pos = True
            hpda_in_pos = True
            for v, p in zip(self.att_axes.values(), pos):
                if v.position != v._convert_pos(p):
                    all_in_pos = False
                    if not isinstance(v.axis, WagoAxis):
                        hpda_in_pos = False
            # print("All in pos? ", all_in_pos)
            # print("HPDA axes all in pos? ", hpda_in_pos)

            frontend_was_open = False
            if self.frontend is not None and self.frontend.is_open:
                frontend_was_open = True
                if not all_in_pos and not hpda_in_pos:
                    self.frontend.close()

            # for n, p in zip(self.att_axes.keys(), pos):
            #     self.att_axes[n].move(p, wait=False)

            axis_pos_list = []
            for v, p in zip(self.att_axes.values(), pos):
                if isinstance(v.axis, WagoAxis):
                    v.move(p)
                    # if p == "OUT":
                    #     print("%s OUT" % v.name)
                    # elif p == "IN":
                    #     print("%s IN" % v.name)
                else:
                    pos = v.axis_positions[v._convert_pos(p)]
                    # axis_pos_list.append((v.axis, pos))
                    axis_pos_list.append(v.axis)
                    axis_pos_list.append(pos)
            if not hpda_in_pos:
                print("\nWaiting for HPDA axes to reach their " +
                      "target position...")
                # _umove(axis_pos_list, relative=False, wait=False)
                move(*axis_pos_list, display_mode="auto")
            else:
                print("\nAll attenuator axes in position\n")

            if not self._virtual:
                for n in list(self.att_axes.keys()):
                    self.att_axes[n].axis.wait_move()
                if self.frontend is not None and frontend_was_open:
                    if not all_in_pos and not hpda_in_pos:
                        # to be tested with frontend enabled (no MDT)
                        self.frontend.open()
                        self.frontend.mode = 'AUTOMATIC'
                        print()

    def OUT(self):
        """Move all axes OUT."""
        self.move(len(self.att_axes)*["OUT"])

    def transmission(self, E, position=None, bw=None):
        """
        Get transmission at a given photon energy and at current attenuators
        configuration (default) or at input configuration.

        Parameters
        ----------
        E : float
           X-ray photon energy (eV).
        position : list or None, optional
           List of AttenuatorAxes positions or integer indeces.
           If None (default), the transmission is calculated at the current
           attenuators position.

        Return
        ------
        T : ndarray of float
           Transmission at given energy and currently selected attenuator.

        Examples
        --------
        >>> ma.transmission(15e3)

        >>> ma.transmission(15e3, [0, 1])

        >>> ma.transmission(15e3, ["OUT", "P1"])

        """

        if position is None:
            position = [att_axis.position for att_axis in self._att_axes]

        T = 1
        for att_axis, pos in zip(self._att_axes, position):
            T *= att_axis.transmission(E, position=pos, bw=bw)

        return T

    def _calc_all_transmissions(self, E, bw=None):
        combs = self.combs
        exclude = self.exclude
        t = self.transmission
        res = np.array([t(E, c, bw) for c in combs if c not in exclude])
        print("Found %d different combinations." % len(res))
        return res

    def get_best_combination(self, E, T, bw=None, move=False):
        """
        Get combination of attenuators giving the closest transmission to the
        the target value at given energy.

        Parameters
        ----------
        E : float
           X-ray photon energy (eV).
        T : float
           Target transmission.

        """

        if not np.isscalar(E):
            raise ValueError("Input energy 'E' must be float")
        if not np.isscalar(T):
            raise ValueError("Target transmission 'T' must be float")

        E = float(E)
        t = self._calc_all_transmissions(E, bw=bw)
        best = np.argmin(np.abs(t - T))
        best_comb = list(self.combs[best])

        print("\nBest match is:")
        position = []
        for att_axis, index in zip(self._att_axes, best_comb):
            position.append(att_axis.labels[index])
        print(self.__info__(position=position))
        print("\nTransmission = %.3g" % t[best])

        time.sleep(0.1)

        if move:
            reply = input("\nMove to this position (Y/n)? ")
            print()
            if reply is None or reply.lower() in ['y', 'yes', '']:
                self.move(best_comb)

        return best_comb


class AttenuatorWagoSet(AttenuatorMultiAxis):
    """Class to control a set of attenuators through a Wago box."""

    def __init__(self, config, name=None, wago=None, wago_motion=None,
                 wago_status=None, wago_inverted=True, tolerance=None,
                 material=None, thickness=None, density=None, indices=None,
                 diameter=None, width=None, height=None, css_distance=None):
        """
        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the session.

        """

        att_axes = []
        indices = []

        if config is not None:

            # load from YAML
            name = config.get("name")
            wago = config.get("wago")
            wago_motion = config.get("wago_motion")
            wago_status = config.get("wago_status")
            wago_inverted = config.get("wago_inverted")
            tolerance = config.get("tolerance")
            diameter = config.get("diameter", default=None)
            width = config.get("width", default=None)
            height = config.get("height", default=None)
            css_distance = config.get("css_distance", default=None)

            attenuators = config['attenuators']

            for attenuator in attenuators:

                if "name" not in attenuator.keys():
                    if float.is_integer(attenuator["thickness"]*1e6):
                        name = "_%gum" % (attenuator["thickness"]*1e6)
                    else:
                        intpart = np.floor(attenuator["thickness"]*1e6)
                        decimal = (attenuator["thickness"]*1e6-intpart)*10
                        name = "_%gp%gum" % (intpart, decimal)
                    name = attenuator["material"] + name
                else:
                    name = attenuator['name']

                att = Attenuator(config=None, material=attenuator["material"],
                                 thickness=attenuator["thickness"])

                atts = [AttenuatorEmpty(), att]

                wago_axis = WagoAxis(config=None, wago=wago,
                                     wago_ch=attenuator["wago_ch"],
                                     wago_motion=wago_motion,
                                     wago_status=wago_status,
                                     wago_inverted=wago_inverted,
                                     tolerance=tolerance)

                att_axis = AttenuatorAxis(config=None, atts=atts,
                                          labels=["OUT", "IN"],
                                          name=name, axis=wago_axis,
                                          axis_positions=[0, 1],
                                          axis_tolerances=[tolerance]*2)

                att_axes.append(att_axis)
                indices.append(attenuator["index"])

        else:

            if wago is None:
                raise ValueError("'wago' cannot be None " +
                                 "if 'config' is None.")

            if not isinstance(wago, Wago):
                raise TypeError("'wago' must be a bliss Wago instance.")

            for k, m in enumerate(material):

                name = "%s_%gum" % (m, thickness[k]*1e6)

                att = Attenuator(config=None, material=m,
                                 thickness=thickness[m], density=density[m])

                atts = [AttenuatorEmpty(), att]

                wago_axis = WagoAxis(config=None, wago=wago,
                                     wago_ch=k,
                                     wago_motion=wago_motion,
                                     wago_status=wago_status,
                                     wago_inverted=wago_inverted,
                                     tolerance=tolerance)

                att_axis = AttenuatorAxis(config=None, atts=atts,
                                          labels=["OUT", "IN"],
                                          name=name, axis=wago_axis,
                                          axis_positions=[0, 1],
                                          axis_tolerances=[tolerance]*2)

                att_axes.append(att_axis)
                indices.append(attenuator["index"])

        # make sure that the order of elements in the 'att_axes' list
        # is that of increasing 'indices'.
        att_axes = [att_axis for _, att_axis in sorted(zip(indices, att_axes))]

        super().__init__(config=None, att_axes=att_axes)

        self._config = config
        self._wago = wago
        self.wago = wago.name
        self._wago_motion = wago_motion
        self._wago_status = wago_status
        self._wago_inverted = wago_inverted
        self.tolerance = tolerance
        self.diameter = diameter
        self.width = width
        self.height = height
        self.css_distance = css_distance


def merge_attenuator_multi_axis(att_multi_axis):
    """
    Merge AttenuaatorMultiAxis objects in a single one.

    Parameters
    ----------
    att_multi_axis : list
        List of AttenuatorMultiAxes instances.

    Returns
    -------
    att : AttenuatorMultiAxes
        AttenuatorMultiAxes containing all the AttenuatorAxis available
        in the input objects.

    Example
    -------
    >>> att = merge_attenuator_multi_axis([hpda, wago_att])

    """
    att_axes = []
    for ma in att_multi_axis:
        for att_axis in ma.att_axes.values():
            att_axes.append(att_axis)
    att = AttenuatorMultiAxis(config=None, att_axes=att_axes)
    return att
