
import functools
from tabulate import tabulate

from bliss import global_map


class WagoMultiplePositions:
    """Handle multiple positions for wago axis."""

    def __init__(self, config):
        self.simultaneous = True
        self.targets_dict = {}  # dict of all the targets (to be used by GUI)
        self._config = config
        self._name = config.get("name")
        self._read_config()

        # Add label-named method for all positions.
        for position in self.positions_list:
            self._add_label_move_method(position["label"])

        global_map.register(self, tag=self._name)

    def _read_config(self):
        """Read the configuration."""
        self.targets_dict = {}
        self.positions_list = []
        try:
            for pos in self._config.get("positions"):
                self.positions_list.append(pos)
                self.targets_dict[pos.get("label")] = pos.get("target")
            self.simultaneous = self._config.get("move_simultaneous", True)
            _label = self.position
            if "unknown" not in self.position:
                self._current_label = _label
            if not (self._last_label and self._current_label):
                self._last_label = self.positions_list[0]["label"]
        except TypeError:
            print("No position configured")

    def _add_label_move_method(self, pos_label):
        """Add a method named after the position label to move to the
        corresponding position.
        """

        def label_move_func(mp_obj, pos):
            mp_obj.move(pos)

        # ACHTUNG: cannot start with a number...
        if pos_label.isidentifier():
            setattr(
                self,
                pos_label,
                functools.partial(label_move_func, mp_obj=self, pos=pos_label),
            )
        else:
            print(
                f"{self._name}: '{pos_label}' is not a valid python identifier."
            )

    def __info__(self):
        """Adapted from MultiplePosition Class from Bliss
        Standard method called by BLISS Shell info helper.
        Return the exhaustive status of the object.
        Returns:
            (str): tabulated string
        """
        # HEADER
        table = [("", "LABEL", "WAGO:CHANNEL", "POSITION(S)")]

        curr_pos = self._get_position()
        motpos_str = ""
        for pos in self.positions_list:
            if pos["label"] == curr_pos:
                mystr = "* "
            else:
                mystr = ""

            axisstr, motstr = "", ""
            for target in pos["target"]:
                axisstr += "%s:%s" % (target["axis"].name, target["channel"])
                motstr += "%.1f\n" % target["destination"]

            table.append((mystr, pos["label"], axisstr, motstr))
        # POSITIONS
        pos_str = tabulate(tuple(table), numalign="right", tablefmt="plain")

        return f"{pos_str}\n {motpos_str}"

    def _get_position(self):
        """Read the postion.
        Returns:
            (str): The position label having all axes at destination.
                   Or 'unknown' if no valid position found.
        """
        # for all positions,
        for label, motor_destinations in self.targets_dict.items():
            in_position = []
            # check all destinations of this position.
            for motor_destination in motor_destinations:
                pos = motor_destination['axis'].get(motor_destination['channel'])
                if pos == motor_destination.get("destination"):
                    in_position.append(True)
                else:
                    in_position.append(False)
            if all(in_position):
                self._last_label = label
                return label
        return "unknown"

    @property
    def position(self):
        """Get the position of the object.
        Returns:
            (str): The position as defined in the label configuration parameter.
        """
        pos = self._get_position()
        # if pos == self._current_label:
        self._last_label = pos
        return pos

    def move(self, pos):
        if self._get_position() == pos:
            print(f"{self._name} position is already: {pos}.\n")
        else:
            print(f"moving {self._name} to: {pos}.\n")
            for label, targets in self.targets_dict.items():
                if label == pos:
                    for target in targets:
                        target['axis'].set(target['channel'], target['destination'])
