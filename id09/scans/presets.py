# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import gevent
import numpy as np
from bliss import config
from bliss.scanning.chain import ChainPreset, ChainIterationPreset
# from bliss.scanning.scan import ScanPreset
from bliss.scanning import scan_meta
# from bliss.shell.getval import getval_yes_no

config = config.static.get_config()


class FastShutterPreset(ChainPreset):
    """Preset used in 'soft' mode to handle shutters opening during scans."""

    def __init__(self, chain_name, use_lshut=False):
        self.chain_name = chain_name
        self.use_lshut = use_lshut
        self.xshut = config.get("xshut")
        if self.use_lshut:
            self.lshut = config.get("lshut")
            if self.lshut is None:
                print("ERROR: lshut not available.")
                self.use_lshut = False

    # def prepare(self, chain):  # changet by ML and CM on Sep 17, 2024
    # double check if the shutters are fully open while acquiring the 1st
    # scan point (might add a sleep if that is the case)
    def start(self, chain):
        if self.xshut._scan_mode != 'closed':
            self.xshut.open()
        if self.use_lshut:
            if self.lshut._scan_mode != 'closed':
                self.lshut.open()
        # add sleep ?

    def stop(self, chain):
        if self.xshut._scan_mode != 'open':
            self.xshut.close()
        if self.use_lshut:
            if self.lshut._scan_mode != 'open':
                self.lshut.close()


class SafetyShutterPreset(ChainPreset):
    """Make sure that FE, SH1 and SH2 are open."""

    def __init__(self):
        self.fe = config.get("fe")
        self.sh1 = config.get("sh1")
        self.sh2 = config.get("sh2")

    def prepare(self, acq_chain):
        while not (self.fe.is_open and self.sh1.is_open and self.sh2.is_open):
            if self.fe.is_closed:
                self.fe.open()
                self.fe.mode = "AUTOMATIC"
            if self.sh1.is_closed:
                self.sh1.open()
            if self.sh2.is_closed:
                self.sh2.open()
            gevent.sleep(1)


class AxisTrackingPreset(ChainPreset):
    """
    Preset for hardware triggering of an axis during a scan.

    This preset is typically used to accumulate signal on a 2D detector
    while "gony" is scanned.

    It can be used with any motor tracked by the OPIOM2 card ("gony", "gonz",
    "hphi").

    """
    def __init__(self, chain_name, axis, start, stop, velocity=None,
                 acceleration=None):
        self.chain_name = chain_name
        self._axis = axis
        self._start = start
        self._stop = stop
        if velocity is None:
            self._velocity = axis.velocity
        if acceleration is None:
            self._acceleration = axis.acceleration
        self._trigger_ctrl = config.get("trigger_ctrl")
        self._n354 = config.get("n354")

    class Iterator(ChainIterationPreset):

        def __init__(self, iteration_nb, acq_chain, chain_name, axis, start,
                     stop, intervals, velocity, acceleration):

            self.iteration = iteration_nb
            self.acq_chain = acq_chain
            self.chain_name = chain_name
            self._axis = axis
            self._start = start
            self._stop = stop
            self._intervals = intervals
            self._velocity = velocity
            self._acceleration = acceleration
            self._init_position = axis.position
            self._start += axis.position
            self._stop += axis.position
            self._trigger_ctrl = config.get("trigger_ctrl")
            self._n354 = config.get("n354")

        def prepare(self):
            self._trigger_ctrl.prepare_sample_axis_tracking(
                self._axis, self._start, self._stop, self._intervals)

        def start(self):
            self._trigger_ctrl.start_sample_axis_tracking(
                self._axis, velocity=self._velocity,
                acceleration=self._acceleration)

        def stop(self):
            self._trigger_ctrl.stop_sample_axis_tracking(self._axis)
            self._axis.move(self._init_position)

    def get_iterator(self, acq_chain):
        scan_info = acq_chain.scan.scan_info
        count_time = scan_info['count_time']
        freq = self._n354.core.xshut.read_frequency()

        npoints = int(count_time * freq)

        init_pos = self._axis.position
        positions = np.linspace(self._start + init_pos, self._stop + init_pos,
                                npoints)

        scan_meta_obj = scan_meta.get_user_scan_meta()
        scan_meta_obj.instrument.set(
            self._motor, {f"{self._axis.name}_tracking": positions})

        iteration_nb = 0
        while True:
            yield AxisTrackingPreset.Iterator(
                    iteration_nb, acq_chain, self.chain_name, self._motor,
                    self._start, self._stop, self._npoints-1, self._velocity,
                    self._acceleration)
            iteration_nb += 1


class IrradiationPreset(ChainPreset):

    def __init__(self, irrad_opiom, gony, gonz):
        self.irrad_opiom = irrad_opiom
        self.gony = gony
        self.gonz = gonz
        self.debug = False

    class Iterator(ChainIterationPreset):

        def __init__(self, iteration_nb, irrad_opiom, debug=False):
            self.iteration = iteration_nb
            self.irrad_opiom = irrad_opiom
            self.debug = debug

        def prepare(self):
            if self.debug:
                print(f"Preparing iteration #{self.iteration:02d}")
            # gevent.sleep(1)
            self.irrad_opiom.reset()

        def start(self):
            if self.debug:
                print(f"Starting iteration #{self.iteration:02d}")

        def stop(self):
            if self.debug:
                print(f"Stopping iteration  #{self.iteration:02d}")

    def get_iterator(self, acq_chain):
        iteration_nb = 0
        while True:
            yield IrradiationPreset.Iterator(iteration_nb, self.irrad_opiom,
                                             debug=self.debug)
            if self.irrad_opiom.event() == 1 and self.debug:
                print("Irradiation induced error detected!")
                # resp = getval_yes_no("Irradiation induced error detected" +
                #                      ", continue?", default='yes')
            iteration_nb += 1

    def prepare(self, acq_chain):
        pass

    def start(self, acq_chain):
        pass

    def stop(self, acq_chain):
        self.last_gony = self.gony.position
        self.last_gonz = self.gonz.position


class ChemyxPreset(ChainPreset):
    """Preset for running the Chemyx pump(s) continuously during a scan."""

    def __init__(self, both=False, sleep_time=0.5, verbose=True):
        self.name = 'chemyx'
        self._chemyx = config.get("chemyx")
        self._both = both
        self._sleep_time = sleep_time
        self._verbose = verbose

    def prepare(self, acq_chain):
        scan_info = acq_chain.scan.scan_info
        npoints = scan_info['npoints']

        vol = self._chemyx.volume
        vol_unit = self._chemyx.volume_unit
        # time_unit = self._chemyx.rate_unit
        dt = self._chemyx.time

        if self._verbose:
            if self._both:
                print(f" pump 1 will run for {dt[0]} min and" +
                      f" consume {vol[0]:g} {vol_unit[0]}" +
                      f" ({dt[0]*60/npoints:.2f} sec per scan point)")
                print(f" pump 2 will run for {dt[1]} min and" +
                      f" consume {vol[1]:g} {vol_unit[1]}" +
                      f" ({dt[1]*60/npoints:.2f} sec per scan point)")
            else:
                idx = self._chemyx.default_pump
                print(f" pump {idx} will run for {dt[idx-1]} min and" +
                      f" consume {vol[idx-1]:g} {vol_unit[idx-1]}" +
                      f" ({dt[idx-1]*60/npoints:.2f} sec per scan point)")

    def start(self, acq_chain):
        self._chemyx.start(both=self._both)
        gevent.sleep(self._sleep_time)

    def stop(self, acq_chain):
        # if self._chemyx._status != ('stopped', 'stopeed'):  # typo Oct 30/24
        if self._chemyx._status != ('stopped', 'stopped'):
            print('WARNING: Chemyx status =', self._chemyx._status)
            self._chemyx.stop()


class ChemyxStepsPreset(ChainPreset):
    """Preset for starting the Chemyx pump(s) at eash step of a scan."""

    def __init__(self, both=False, sleep_time=0.5, verbose=True):
        self._trigger_ctrl = config.get("trigger_ctrl")
        self._n354 = config.get("n354")
        self._chemyx = config.get("chemyx")
        self._both = both
        self._sleep_time = sleep_time
        self._verbose = verbose

    class Iterator(ChainIterationPreset):

        def __init__(self, acq_chain, both, sleep_time, verbose):
            self.acq_chain = acq_chain
            self._trigger_ctrl = config.get("trigger_ctrl")
            self._n354 = config.get("n354")
            self._chemyx = config.get("chemyx")
            self._both = both
            self._sleep_time = sleep_time
            self._verbose = verbose

        def prepare(self):
            self._chemyx.start(both=self._both)
            gevent.sleep(self._sleep_time)

        def start(self):
            pass

        def stop(self):
            if self._chemyx._status != ('stopped', 'stopeed'):
                print('WARNING: Chemyx status =', self._chemyx._status)
                self._chemyx.stop()

    def get_iterator(self, acq_chain):
        scan_info = acq_chain.scan.scan_info
        npoints = scan_info['npoints']

        vol = self._chemyx.volume
        vol_unit = self._chemyx.volume_unit

        if self._verbose:
            print("\nWith the current parameters:\n")
            self._chemyx.show_pars()
            print(f"The scan (npoints={npoints}) will consume a total " +
                  "volume of:")
            if self._both:
                print(f" {vol[0]*npoints:g} {vol_unit[0]} for pump 1")
                print(f" {vol[1]*npoints:g} {vol_unit[0]} for pump 2\n")
            else:
                idx = self._chemyx.default_pump
                print(f" {vol[idx-1]*npoints:g} {vol_unit[idx-1]} for pump" +
                      f" {idx}")

        iteration_nb = 0
        while True:
            yield ChemyxStepsPreset.Iterator(acq_chain, both=self._both,
                                             sleep_time=self._sleep_time,
                                             verbose=self._verbose)
            iteration_nb += 1
