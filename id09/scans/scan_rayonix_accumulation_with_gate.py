import time

from bliss import setup_globals
from bliss.config.static import get_config


def dscan_capillary(motor, start, stop, npoints, *counters, velocity=5, 
                    acceleration=80, save=True, run=True):
    """Relative scan of a motor with a single pulse per position.

    Each image is the result of an accumulation of npoints X-ray pulses.

    """
    n354 = get_config().get("n354")
    print(n354)
    mg_bliss_laue = get_config().get("mg_bliss_laue")

    count_time = 1 / n354.core.xshut.read_frequency()

    if len(counters) == 0:
        counters = [mg_bliss_laue]

    scan_info = {}
    scan_params = {}

    title = f"dscan_accu_gate {count_time} on motor {motor.name}"
    name = "dscan_accu_gate"

    init_position = motor.position
    start += motor.position
    stop += motor.position

    trigger_ctrl = get_config().get("trigger_ctrl")
    trigger_ctrl.prepare_sample_axis_tracking(motor, start, npoints, stop - start)
    trigger_ctrl.start_sample_axis_tracking(motor, velocity=velocity, acceleration=acceleration)

    scan = sct(count_time * npoints, *counters)

    trigger_ctrl.stop_sample_axis_tracking(motor)
    motor.move(init_position)

    return scan
