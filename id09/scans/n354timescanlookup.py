

import numpy
import tabulate

from bliss.scanning.scan import Scan
from bliss.controllers.ct2.device import AcqMode as CT2AcqMode
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from id09.controllers.n354.acquisition import N354AcquisitionMaster

from fscan.fscantools import (
    FScanParamBase,
    FTimeScanMode,
    FScanCamSignal,
    FScanParamStruct,
    FScanDisplay,
)
from fscan.mcatools import get_fscan_mca_params
from fscan.chaintools import ChainTool
from fscan.fscanrunner import FScanRunner
from fscan.fscaninfo import FScanInfo
from fscan.limatools import LimaCalib

from fscan.mussttools import MusstProgITrigRecord


class MotorLookupPars:
    def __init__(self, motpos=None):
        self.set(motpos)

    def reset(self):
        self._lookup = dict()

    def set(self, motpos=None):
        self.reset()
        if motpos is not None:
            axes = set()
            npts = None
            for (mot, pos) in motpos:
                if mot in axes:
                    raise ValueError(
                        f"Duplicated axis {mot.name} in motor_lookup parameter"
                    )
                axes.add(mot)
                if npts is None:
                    npts = len(pos)
                else:
                    if len(pos) != npts:
                        raise ValueError(
                            f"Invalid number of positions for {mot.name}. Should be {npts}."
                        )
            self._lookup = motpos

    @property
    def nmot(self):
        return len(self._lookup)

    @property
    def npos(self):
        if len(self._lookup):
            return len(self._lookup[0][1])
        else:
            return 0

    @property
    def motors(self):
        return [mot for (mot, _) in self._lookup]

    def __info__(self):
        if not self.nmot:
            return "No motor lookup defined !!"
        vals = [[f"- {mot.name} :"] + pos for (mot, pos) in self._lookup]
        return tabulate.tabulate(vals, tablefmt="plain")

    def __str__(self):
        infos = self.__info__()
        if self.nmot:
            infos = "\n    " + infos.replace("\n", "\n    ")
        return infos

    def as_list(self):
        motor_positions = list()
        for (mot, pos) in self._lookup:
            motor_positions.extend((mot, pos))
        return motor_positions


class N354TimeScanLookupPars(FScanParamBase):
    DEFAULT = {
        "acq_time": 1.0,
        "npoints": 1,
        "motor_lookup": MotorLookupPars(),
        "latency_time": 0.,
        "sampling_time": 0.5,
        "scan_mode": FTimeScanMode.TIME,
        "camera_signal": FScanCamSignal.EDGE,
        "save_flag": True,
        "display_flag": True,
    }
    OBJKEYS = list()
    LISTVAL = {
        "scan_mode": FTimeScanMode.values,
        "camera_signal": FScanCamSignal.values,
    }
    NOSETTINGS = ["save_flag", "motor_lookup"]

    def __init__(self, name):
        FScanParamBase.__init__(
            self,
            name,
            N354TimeScanLookupPars.DEFAULT,
            N354TimeScanLookupPars.OBJKEYS,
            N354TimeScanLookupPars.LISTVAL,
            N354TimeScanLookupPars.NOSETTINGS,
        )

    def _validate_npoints(self, value):
        return int(value)

    def _validate_scan_mode(self, value):
        return FTimeScanMode.get(value, "scan_mode")

    def _validate_camera_signal(self, value):
        return FScanCamSignal.get(value, "camera_signal")

    def _validate_save_flag(self, value):
        return value and True or False

    def _validate_display_flag(self, value):
        return value and True or False

    def _validate_motor_lookup(self, value):
        lookup = MotorLookupPars()
        lookup.set(value)
        return lookup


class N354TimeScanLookupMaster(object):
    def __init__(self, name, config):
        self.name = name
        self.config = config

        self.musst = config["devices"]["musst"][0]
        self.meas_group = config["devices"]["measgroup"]
        self.chain_config = config["chain_config"]
        self.lima_calib = LimaCalib(config["lima_calib"])
        self.musst_data_rate_factor = config.get("musst_data_rate_factor", 0)

        self.n354 = config["devices"]["n354"][0]
        self.diag_motor_list = config["devices"]["diag_motor_list"]

        self.pars = N354TimeScanLookupPars(self.name)

        self.inpars = None
        self.mgchain = None

    def validate(self):
        self.inpars = FScanParamStruct(self.pars.to_dict())
        pars = self.inpars

        limas = self.get_controllers_found("lima")
        self.lima_calib.validate(limas, pars, use_acc=False)

        if pars.lima_ndev:
            if pars.period < pars.lima_min_period:
                pars.period = pars.lima_min_period
        else:
            if pars.period < pars.acq_time:
                pars.period = pars.acq_time + pars.latency_time

        if not pars.motor_lookup.nmot:
            raise ValueError("Need to specify motor_lookup !!")

        pars.nsteps = pars.motor_lookup.npos

        # Ask the N354 in which mode xshut signal will be generated and if pulsed how many pulses
        # n. pulses is depending of the choosen frequency, but xshut can just be a gate for
        # burst at high frequency
        freq, npulses, mode, acqtime = self.n354.get_burst_params(pars.acq_time)
        if mode is self.n354.PulseMode.Pulsed:
            pars.npoints = npulses
            pars.n354_mode = "PULSED"
        else:
            pars.npoints = 1
            pars.n354_mode = "GATED"
        pars.n354_freq = freq

        pars.total_npoints = pars.nsteps * pars.npoints
        pars.total_time = pars.nsteps * pars.acq_time

    def show(self):
        if self.inpars is None:
            raise RuntimeError(
                f"N354TimeScanMaster[{self.name}]: Parameters not validated yet !!"
            )

        txt = """
Acquisiton  = {acq_time:g} sec
Scan mode       = {scan_mode}
"""
        if self.inpars.lima_ndev:
            txt += "Camera mode     = {lima_acq_mode}\n"
            if self.inpars.scan_mode == FTimeScanMode.CAMERA:
                txt += "Camera signal   = {camera_signal}\n"
        if self.inpars.scan_mode != FTimeScanMode.EXTSTART:
            txt += "Start delay     = {start_delay:.3f} sec\n"

        txt += """
Motor Lookup = {motor_lookup}

Total NPoints = {total_npoints}
Total Time    = {total_time:.3f} sec
N354 Mode     = {n354_mode}
N354 Freq.    = {n354_freq:.5f} Hz
"""
        print(txt.format(**self.inpars.to_dict()))

    def init_scan(self):
        self.mgchain = ChainTool(self.meas_group, self.chain_config)
        self.lima_used = list()
        self.mca_used = list()

    def get_controllers_found(self, ctrl_type):
        mgchain = ChainTool(self.meas_group)
        return mgchain.get_controllers_not_done(ctrl_type)

    def get_controllers_used(self, ctrl_type):
        if self.mgchain is None:
            return []
        else:
            return self.mgchain.get_controllers_done(ctrl_type)

    def setup_acq_chain(self, chain):
        pars = self.inpars

        # --- motor master
        motor_positions = pars.motor_lookup.as_list()
        motor_master = VariableStepTriggerMaster(
            *motor_positions, broadcast_len=pars.npoints
        )
        # --- N354 master
        n354_master = N354AcquisitionMaster(
            self.n354._counter_controller,
            self.n354._counter_countroller.name,
            count_time=pars.acq_time,
        )

        chain.add(motor_master, n354_master)

        # --- musst prog
        # just use a musst to record some diagnostic data (motor positions, adc ...)
        musstprog = MusstProgITrigRecord(self.musst, *self.diag_motor_list)
        musstprog.check_max_timer(pars.total_time)
        musstprog.set_max_data_rate(self.musst_data_rate_factor * (2. * pars.n354_freq))
        # MODE = 0 : no trigger sent
        # MODE = 1 : send one trigger at start

        # RECORD = 0 : record both rise and fall itrig
        # RECORD = 1 : record only rise itrig
        # RECORD = 2 : record only fall itrig
        # RECORD = 3 : record high and low transitions

        musstprog.set_params(pars.npoints, mode=0, record=0)

        musstprog.setup(chain, n354_master)
        musstprog.add_default_calc(chain)
        # musstprog.set_timer_external_channel()

        # acq_master = musstprog.musst_master

        # --- counters
        ct2pars = {
            "npoints": pars.total_npoints,
            "acq_expo_time": pars.acq_time,
            "acq_mode": CT2AcqMode.ExtTrigMulti,
        }
        self.mgchain.setup("ct2", chain, n354_master, ct2pars)

        # --- lima devs
        if pars.scan_mode == FTimeScanMode.CAMERA:
            trig_mode = "EXTERNAL_TRIGGER"
        else:
            trig_mode = "EXTERNAL_TRIGGER_MULTI"

        wait_frame_id = range(
            pars.npoints - 1, pars.npoints * pars.nsteps, pars.npoints
        )
        pars.wait_frame_id = numpy.array(wait_frame_id)
        limapars = {
            "acq_mode": pars.lima_acq_mode,
            "acq_trigger_mode": trig_mode,
            "acq_expo_time": pars.acq_time,
            "acq_nb_frames": pars.total_npoints,
            "wait_frame_id": wait_frame_id,
            "prepare_once": True,
            "start_once": True,
        }
        if pars.scan_mode == FTimeScanMode.CAMERA:
            if pars.period > pars.lima_min_period:
                lat_time = pars.period - pars.acq_time
                limapars["latency_time"] = lat_time
        self.mgchain.setup("lima", chain, acq_master, limapars)
        self.lima_used = self.mgchain.get_controllers_done("lima")

        # --- mca devices
        for mca_ctrl in self.mgchain.get_controllers_not_done("mca"):
            mca_params = get_fscan_mca_params(mca_ctrl, pars.total_npoints, pars.period)
            self.mgchain.setup("mca", chain, acq_master, mca_params, mca_ctrl.name)
            if "trigger_mode" in mca_params:
                # --- in spectrum reading mode
                self.mca_used.append(mca_ctrl)

        # --- sampling
        self.mgchain.setup_sampling(chain, pars.sampling_time)

    def setup_scan(self, chain, scan_name, user_info=None):
        pars = self.inpars

        if pars.display_flag:
            self.show()

        scan_info = FScanInfo(scan_name, "n354timescanlookup", self.inpars.npoints)
        scan_info.set_fscan_title("{acq_time:g} {npoints:d} {period:g}", self.pars)
        scan_info.add_curve_plot(x="timer_trig:timer_trig")
        scan_info.set_fscan_pars(self.pars)
        scan_info.set_fscan_info(user_info)

        motors = pars.motor_lookup.motors
        channames = [f"axis:{mot.name}" for mot in motors]
        scan = Scan(
            chain,
            name=scan_name,
            save=pars.save_flag,
            scan_info=scan_info,
            data_watch_callback=FScanDisplay(
                self.musst,
                trigger_name="timer",
                motors=motors,
                motor_channels=channames,
                limas=self.lima_used,
                mcas=self.mca_used,
            ),
        )

        return scan

    def get_runner_class(self):
        return FTimeScanLookupCustomRunner


class FTimeScanLookupCustomRunner(FScanRunner):
    def __call__(self, motor_lookup, acq_time, **kwargs):
        pars = dict(motor_lookup=motor_lookup, acq_time=acq_time)
        scan_info = kwargs.pop("scan_info", None)
        pars.update(kwargs)
        self.pars.set(**pars)
        self.run(scan_info)
