# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.scanning.chain import ChainPreset
from bliss.scanning.toolbox import ChainBuilder
# for bliss 1.11
#from bliss.scanning.scan import Scan, get_default_scan_progress
from bliss.scanning.scan import Scan, StepScanDataWatch
from bliss.scanning.acquisition.motor import LinearStepTriggerMaster
from bliss.common import event

from bliss import setup_globals
import time

def ascan_accu(motor,start,stop,npoints,count_time,*counters, save=True, run=True):

    scan_info = {}
    scan_params = {}

    title = f"ascan_accu {count_time} on motor {motor.name}"
    name = "ascan_accu"
    
    scan_info.update(
        {
            "npoints": npoints,
            "count_time": count_time,
            "save": save,
            "title": title,
            #"sleep_time": sleep_time,
            #"stab_time": stab_time,
        }
    )

    scan_params.update(
        {
            "npoints": npoints,
            "count_time": count_time,
            #"sleep_time": sleep_time,
            #"stab_time": stab_time,
        }
    )   
    lima_dev = setup_globals.rayonix
    trigger_ctrl = setup_globals.trigger_ctrl
    
    # use this context manager to disable the camera from the MG
    # to not get it triggered by the n345 acq. master, the ctxt manager restore the MG when it exits
    with ActivateDetector(lima_dev, setup_globals.ACTIVE_MG):
    
        top_master=LinearStepTriggerMaster(npoints, motor, start, stop)
        top_master.terminator = False
        chain = setup_globals.DEFAULT_CHAIN.get(
            scan_params,
            counters,
            top_master=top_master,
        )

        #Creating the timescan part
        #First create the monitor timer
        #top_timer = SoftwareTimerMaster(count_time=count_time*(npoints+1))

        builder = ChainBuilder([lima_dev])
        lima_node = builder.nodes[0]
        lima_acq_params={"acq_nb_frames": 1,
                         "acq_mode": "SINGLE",
                         "acq_expo_time": count_time * npoints*1.25, # useless in gate mode
                         "acq_trigger_mode": "INTERNAL_TRIGGER",
                         "prepare_once": False,
                         "start_once": False}
    
        lima_node.set_parameters(acq_params=lima_acq_params)
        # by using the chain and the node, here the chain add the lima as a top master AcqMaster
        # and all defined slaves counters like roicnt... will be chained as well0
        chain.add(lima_node)

        # add a chain preset to trig in gate the camera via the opiom mux
        trig_preset = TrigPreset(trigger_ctrl)        
        chain.add_preset(trig_preset, top_master)

        # Now we can create the scan runner
        # for bliss 1.11
        #scan_progress = get_default_scan_progress()
        scan = Scan(
            chain,
            scan_info=scan_info,
            name=name,
            save=save,
            data_watch_callback=StepScanDataWatch(),
            # for bliss 1.11
            #scan_progress=scan_progress,
        )

        # get the step scan master added below to add an even callback to stop the lima master at the end of the scan
        #event.connect(top_master, "end", )
        scan.run()
        return scan


class ActivateDetector:
    def __init__(self, lima_dev, mg):
        self.lima_name = lima_dev.name
        self.mg = mg
        self.enabled =[]
        for cnt in self.mg.enabled:
            if cnt.find(self.lima_name) == 0:
                self.enabled.append(cnt)

    def __enter__(self):

        for cnt in self.enabled:
            self.mg.disable(cnt)
            
    def __exit__(self, *args):

        for cnt in self.enabled:
            self.mg.enable(cnt)


class TrigPreset(ChainPreset):

    def __init__(self, trig_ctrl):

        self.trig_ctrl = trig_ctrl
        
    def prepare(self, chain):
        self.trig_ctrl.gate_on_camera()

    def start(self, chain):
        pass

    def before_stop(self, chain):
        # if scan is aborted swith off the gate to get a chance to save the image
        # before the camers is stopped with an acqStop()
        self.trig_ctrl.gate_off_camera()
        print("in before_stop")
        time.sleep(1)
        
    def stop(self, chain):
        # called at the end of the scan or if scan is aborted
        self.trig_ctrl.gate_off_camera()        
