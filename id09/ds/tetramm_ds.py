# -*- coding: utf-8 -*-
"""Device server: CAENels TetrAMM picoammeter."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "25/06/2024"
__version__ = "0.0.1"


import numpy as np

from tango import DevState, GreenMode
from tango.server import Device, attribute, command, run

from id09.core.tetramm import Tetramm


def switch_state(tg_dev, state=None, status=None):
    """Helper to switch state and/or status and send event"""
    if state is not None:
        tg_dev.set_state(state)
        if state in (DevState.ALARM, DevState.UNKNOWN, DevState.FAULT):
            msg = "State changed to " + str(state)
            if status is not None:
                msg += ": " + status
    if status is not None:
        tg_dev.set_status(status)


class TetrammServer(Device):

    def init_device(self):
        Device.init_device(self)
        try:
            self.device = Tetramm(url='id09tetramm1', port=10001, debug=False)
            switch_state(self, DevState.ON, "Ready!")
        except Exception as e:
            msg = "Exception initializing device: {0}".format(e)
            self.error_stream(msg)
            switch_state(self, DevState.FAULT, msg)

        self.debug = False  # changed as this was resulting in timescan crashes
        self._in_scan = False
        self._busy_outside_scan = False
        self._busy_in_scan_readout = False
        self._last_data = np.array([], dtype=float)
        self.data = None  # scan data buffer (list of readouts))
        self.ndata = 0  # number of readouts

    def delete_device(self):
        if self.device:
            self.device.abort()

    @command(dtype_in=float, dtype_out=str)
    def raw_read(self, timeout):
        # use only for debugging purpouses
        ans = self.device.comm.read(size=8, timeout=timeout)
        return ans

    @attribute(dtype=bool, doc='busy in scan')
    def in_scan(self):
        return self._in_scan

    @in_scan.setter
    def in_scan(self, value):
        self._in_scan = value
        if self.debug:
            if self._in_scan:
                print(f"\n===== DS IN_SCAN={value} =====")
            else:
                print(f"===== DS IN_SCAN={value} =====\n")

    @attribute(dtype=bool, doc='busy in scan point readout')
    def busy_in_scan_readout(self):
        return self._busy_in_scan_readout

    @busy_in_scan_readout.setter
    def busy_in_scan_readout(self, value):
        self._busy_in_scan_readout = value

    @attribute(dtype=bool, doc='busy outside scan')
    def busy_outside_scan(self):
        return self._busy_outside_scan

    @busy_outside_scan.setter
    def busy_outside_scan(self, value):
        self._busy_outside_scan = value

    @attribute(dtype=bool)
    def is_trg(self):
        return self.device.is_trg

    @attribute(dtype=bool)
    def is_bias(self):
        return self.device.is_bias

    @attribute(dtype=int, doc='Last programmed nb of active channels')
    def last_nch(self):
        return self.device._nch

    @attribute(dtype=int, doc='Last programmed nb of acquisitions')
    def last_naq(self):
        return self.device._naq

    @attribute(dtype=float, doc='Last programmed transfer rate', unit='Hz')
    def last_data_rate(self):
        return self.device._data_rate

    @attribute(dtype=int, doc='Last programmed nb of triggers')
    def last_ntrg(self):
        return self.device._ntrg

    @attribute(dtype=str, doc='Last programmed data termination')
    def last_data_eol(self):
        return self.device.data_eol

    @command
    def trg_off(self):
        if self.debug:
            print("DS trg_off()")
        self.device.trg_off()

    @command
    def trg_on(self):
        if self.debug:
            print("DS trg_on()")
        self.device.trg_on()

    # --- SCAN COMMANDS/ATTRIBUTES (allowed during a scan) ---

    @command(dtype_in=(int,))
    def prepare_acq_once(self, pars):
        """Should be called by _prepare_device() in acquisition slave."""
        naq, ntrg, nch = pars
        if nch != self.device.nch:
            self.device.nch = nch
        if naq != self.device.naq:
            self.device.naq = naq
        if ntrg != self.device.ntrg:
            self.device.ntrg = ntrg
        self.device.data_eol = b'\xff\xf4\x00\x03\xff\xff\xff\xff'*(nch+1)
        self.data = None
        self.ndata = 0
        if self.debug:
            print(f"DS prepare_acq_once: NRSAMP={self.device.nrsamp}, " +
                  f"NAQ={self.device.naq}, NTRG={self.device.ntrg}, " +
                  f"NCH={self.device.nch}, IS_TRIG={self.device.is_trg}, ")

    @command
    def acq_on(self):
        """Should be called by start_device() in acquisition slave."""
        if self.debug:
            print(f"DS acq_on: NRSAMP={self.device.nrsamp}, " +
                  f"NAQ={self.device.naq}, NTRG={self.device.ntrg}, " +
                  f"NCH={self.device.nch}, IS_TRIG={self.device.is_trg}, ")
        self.device.acq_on()

    @command
    def trigger_readout(self):
        """Should be called by trigger() in acquisition slave."""
        self.busy_in_scan_readout = True
        binary_buffer = self.device.comm.readline(eol=self.device.data_eol)
        self.busy_in_scan_readout = False
        parsed_buffer = self.device.parse_binary_buffer(binary_buffer)
        averaged_parsed_buffer = np.mean(parsed_buffer, axis=0)
        if self.data is None:
            self.data = [averaged_parsed_buffer]  # list
            self.ndata += 1
        else:
            self.data.append(averaged_parsed_buffer)
        if self.debug:
            print(f"DS trigger_readout: NRSAMP={self.device._nrsamp}, " +
                  f"NAQ={self.device._naq}, NTRG={self.device._ntrg}, " +
                  f"NCH={self.device._nch}, IS_TRIG={self.device.is_trg}, ")

    @command(dtype_in=int, dtype_out=(float,))
    def get_data(self, from_index):
        """Should be called by get_values() in counter controller."""
        if self.data is None:
            data = np.array([], dtype=float)
        else:
            data = np.array(self.data[from_index:]).flatten()
            self._last_data = np.squeeze(self.data[-1])
        return data

    @command
    def acq_off(self):
        if self.debug:
            print(f"DS acq_off: IN_SCAN={self._in_scan}, " +
                  f"IS_TRIG={self.device.is_trg}")
        self.device.acq_off()

    @command
    def empty_buffer(self):
        if self.debug:
            print(f"DS empty_buffer: IN_SCAN={self._in_scan}, " +
                  f"IS_TRIG={self.device.is_trg}")
        self.device.empty_buffer()
        self.data = None
        self.ndata = 0
        self._in_scan = False

    # --- OTHER COMMANDS/ATTRIBUTES (banned during a scan) ---

    @command(dtype_in=(int,), dtype_out=(float,))
    def get_current(self, pars):
        naq, ntrg, nch = pars
        self._busy_outside_scan = True
        if not self._in_scan:
            try:
                data = self.device.get_current(naq=naq, ntrg=ntrg, nch=nch)
            except Exception as e:
                print(f"===== DS exception: {e} ====")
            finally:
                self._busy_outside_scan = False
        else:
            data = np.copy(self._last_data)
        self._busy_outside_scan = False
        return data

    @attribute(dtype=float, doc='Data transfer rate', unit='Hz')
    def data_rate(self):
        if not self._in_scan:
            self._busy_outside_scan = True
            ans = float(self.device.data_rate)
            self._busy_outside_scan = False
            return ans

    @data_rate.setter
    def data_rate(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.data_rate = float(value)
            self._busy_outside_scan = False

    @attribute(dtype=float, doc='Bias voltage set point', unit='V')
    def bias(self):
        # return -1 if OFF
        if not self._in_scan:
            self._busy_outside_scan = True
            ans = self.device.bias
            self._busy_outside_scan = False
            return ans

    @bias.setter
    def bias(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.bias = value
            self._busy_outside_scan = False

    @command(dtype_out=(str,))
    def get_all_ranges(self):
        """Get all available current ranges."""
        if not self._in_scan:
            self._busy_outside_scan = True
            ans = [r for r in self.device.all_ranges.values()]
            self._busy_outside_scan = False
            return ans

    @command(dtype_out=(str,))
    def get_range(self):
        """Get current range status of all channels."""
        if not self._in_scan:
            self._busy_outside_scan = True
            ans = self.device.get_range()
            self._busy_outside_scan = False
            return ans
        else:
            return self.device._nch*['']

    @command(dtype_in=str)
    def set_range(self, value):
        """Set current range of all channels."""
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.set_range(value, channel='all')
            self._busy_outside_scan = False

    @command(dtype_in=str)
    def set_range_ch1(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.set_range(value, channel=1)
            self._busy_outside_scan = False

    @command(dtype_in=str)
    def set_range_ch2(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.set_range(value, channel=2)
            self._busy_outside_scan = False

    @command(dtype_in=str)
    def set_range_ch3(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.set_range(value, channel=3)
            self._busy_outside_scan = False

    @command(dtype_in=str)
    def set_range_ch4(self, value):
        if not self._in_scan:
            self._busy_outside_scan = True
            self.device.set_range(value, channel=4)
            self._busy_outside_scan = False


def main():
    run([TetrammServer], green_mode=GreenMode.Gevent)


if __name__ == "__main__":
    main()
