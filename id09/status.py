# -*- coding: utf-8 -*-
"""Define X-ray mode/energy."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "22/01/2023"
__version__ = "0.0.1"


from tango import DevFailed
from bliss import config
from id09.unit import convert_float2str
# from id09.physics import photon_flux, photons_per_pulse
from id09.controllers.temperature_ctrl import TemperatureSensor

# from id09 import config
config.get_sessions_list()
config = config.static.get_config()


class BeamlineStatus:

    def __init__(self):

        try:  # MACHINE import
            self.machinfo = config.get("machinfo")
            self.emh = config.get("emh")
            self.emv = config.get("emv")
            self.srcur = self.machinfo.counters.current
            self.sbcur = self.machinfo.counters.sbcurr
            self.fe = config.get("fe")
            self.u17 = config.get("u17")
            self.u23 = config.get("u23")
            self.ebpm_hpos = config.get("ebpm_hpos")
            self.ebpm_vpos = config.get("ebpm_vpos")
            self.ebpm_hangle = config.get("ebpm_hangle")
            self.ebpm_vangle = config.get("ebpm_vangle")
        except Exception:
            self.machinfo = None
            self.u17, self.u23 = None, None
            self.emh, self.emv, self.srcur, self.sbcur = None, None, None, None
            self.ebpm_hpos, self.ebpm_vpos = None, None
            self.ebpm_hangle, self.ebpm_vangle = None, None

        try:  # MOTORS import
            self.phg = config.get("phg")
            self.pvg = config.get("pvg")
            self.mono1 = config.get("mono1")
            self.sh1 = config.get("sh1")
            self.sh2 = config.get("sh2")
            self.s1hg = config.get("s1hg")
            self.s1vg = config.get("s1vg")
            self.s2hg = config.get("s2hg")
            self.s2vg = config.get("s2vg")
            self.ss1hg = config.get("ss1hg")
            self.ss1vg = config.get("ss1vg")
            self.ss2hg = config.get("ss2hg")
            self.ss2vg = config.get("ss2vg")
            self.tabz = config.get("tabz")
        except Exception:
            self.mono1, self.tabz = None, None

        try:
            self.mono2 = config.get("mono2")
        except Exception:
            self.mono2 = None

        try:  # ELECTRONICS import
            self.tetramm = config.get("tetramm")
            self.tetramm_ch1 = config.get("ch1")
            self.tetramm_ch2 = config.get("ch2")
            self.tetramm_ch3 = config.get("ch3")
            self.tetramm_ch4 = config.get("ch4")
        except Exception:
            self.tetramm = None
        try:
            self.pd0 = config.get("pd0")
        except Exception:
            self.pd0 = None

        try:  # INOUT import
            self.inout_pd1 = config.get("inout_pd1")
            self.inout_pd3 = config.get("inout_pd3")
            self.inout_pd4 = config.get("inout_pd4")
        except Exception:
            self.inout_pd1, self.inout_pd3, self.inout_pd4 = None, None, None
        try:
            self.inout_hlc = config.get("inout_hlc")
            self.inout_pic = config.get("inout_pic")
            self.inout_mono1 = config.get("inout_mono1")
            self.inout_xshut = config.get("inout_xshut")
            self.hsc = config.get("chop")
        except Exception:
            self.inout_hlc, self.inout_pic = None, None
            self.inout_mono1, self.inout_xshut = None, None
            self.hsc = None

        try:  # CALC MOTOR import
            self.u17e = config.get("u17e")
            self.u23e = config.get("u23e")
            self.mono1e = config.get("mono1e")
            self.mono2e = config.get("mono2e")
        except Exception:
            self.u17e, self.u23e = None, None
            self.mono1e, self.mono2e = None, None

        try:  # WAGO
            self.wcid09b = config.get("wcid09b")
            self.eh2tc = TemperatureSensor(config=None, name='eh2',
                                           wago=self.wcid09b, wago_ch='eh2')
        except Exception:
            self.eh2tc = None

        try:
            self.n354 = config.get("n354")
            self.trigger_ctrl = config.get("trigger_ctrl")
        except Exception:
            self.n354 = None
            self.trigger_ctrl = None

        try:
            self.xshut = config.get("xshut")
            self.lshut = config.get("lshut")
        except Exception:
            self.xshut = None
            self.lshut = None

    def get_machine_mode(self, verbose=False):

        if self.machinfo is None:
            return
        try:
            ret = self.machinfo.all_information['SR_Mode']
        except DevFailed:
            ret = "no tango answer"
        except Exception as e:
            print(e)
        if verbose:
            print("storage ring mode =", ret)
        else:
            return ret

    def get_filling_mode(self, verbose=True):
        if self.machinfo is None:
            return
        try:
            ret = self.machinfo.all_information['SR_Filling_Mode']
        except DevFailed:
            ret = "no tango answer"
        except Exception as e:
            print(e)
        if ret == "7/8 multibunch":
            ret = "7/8+1"
        if verbose:
            print("filling mode = %s\n" % ret)
        else:
            return ret

    def get_emittance(self, verbose=True):
        """Get horizontal and vertical emittance (pm)."""
        if self.emh is None or self.emv is None:
            return
        em = (round(self.emh.value, 1), round(self.emv.value, 1))
        if verbose:
            print("emittance = %g pm x %g pm (HxV)\n" % em)
        else:
            return em

    def get_ebpm(self, verbose=True):
        """Get hor/ver electron beam position/angle (um/urad) at ID09 SS."""
        if self.ebpm_hpos is None:
            return
        ret = (round(self.ebpm_hpos.value, 2),
               round(self.ebpm_vpos.value, 2),
               round(self.ebpm_hangle.value, 2),
               round(self.ebpm_vangle.value, 2))
        if verbose:
            print("ebeam_pos   = %+g um x %+g um (HxV)" % ret[:2])
            print("ebeam_angle = %+g urad x %+g urad (HxV)\n" % ret[2:])
        else:
            return ret

    def get_srcur(self, verbose=True):
        """Get storage ring current (mA)."""
        if self.srcur is None:
            return
        ret = round(self.srcur.value, 2)
        if verbose:
            print("storage ring current = %g mA\n" % ret)
        else:
            return ret

    def get_sbcur(self, verbose=True):
        """Get single bunch current (mA)."""
        if self.sbcur is None:
            return
        ret = round(self.sbcur.value, 2)
        if verbose:
            print("single bunch current = %g mA\n" % ret)
        else:
            return ret

    def get_sblen(self, cur=None, verbose=True):
        """Convert single bunch current (mA) to length (ps)."""
        if cur is None:
            if self.sbcur is None:
                return
            else:
                cur = self.sbcur.value

        if cur is not None:
            ret = round(40.94*(cur+0.2994)**0.3991, 1)
            if verbose:
                print("single bunch length (duration) = %g ps\n" % ret)
            else:
                return ret

    def get_max_srcur(self, filling_mode=None, verbose=True):
        """Get max storage ring current at given filling mode (mA)."""
        if filling_mode is None:
            filling_mode = get_filling_mode(verbose=False)
        srcur_max = None
        if filling_mode in ['7/8+1', 'uniform', '24*8+1']:
            srcur_max = 200
        elif filling_mode == '16 bunch':
            srcur_max = 75
        elif filling_mode == '4 bunch':
            srcur_max = 32
        if verbose:
            if srcur_max is not None:
                print(f"max storage ring current = {srcur_max:g} mA\n")
            else:
                if filling_mode is not None:
                    print("max storage ring current for filling mode " +
                          f"'{filling_mode}' not available.\n")
                else:
                    print("ERROR: get_filling_mode() returned None.\n")
        else:
            return srcur_max

    def get_max_sbcur(self, filling_mode=None, verbose=True):
        """Get max single bunch current at given filling mode (mA)."""
        if filling_mode is None:
            filling_mode = get_filling_mode(verbose=False)
        sbcur_max = None
        if filling_mode in ['7/8+1', 'uniform', '24*8+1']:
            sbcur_max = 8
        elif filling_mode == '16 bunch':
            sbcur_max = 75/16
        elif filling_mode == '4 bunch':
            sbcur_max = 10
        if verbose:
            if sbcur_max is not None:
                print(f"max single bunch current = {sbcur_max:g} mA\n")
            else:
                if filling_mode is not None:
                    print("max single bunch current for filling mode " +
                          f"'{filling_mode}' not available.\n")
                else:
                    print("ERROR: get_filling_mode() returned None.\n")
        else:
            return sbcur_max

    def get_xray_mode(self):
        """Get X-ray mode ('monochromatic', 'multilayer', 'polychromatic')."""
        if self.mono2 is not None:
            if self.mono2._xtals.xtal_sel != "OUT":
                return 'multilayer'
        if self.inout_mono1.position != "OUT":
            return 'monochromatic'
        else:
            return 'polychromatic'

    def get_xray_energy(self):
        """Get X-ray photon energy (eV)."""
        mode = self.get_xray_mode()
        energy = None
        if mode == 'multilayer':
            energy = self.mono2e.position
        elif mode == 'monochromatic':
            energy = self.mono1e.position
        elif mode == 'polychromatic':
            if self.u17 is not None and self.u23 is not None:
                try:
                    if self.u17.position > self.u23.position + 0.1:
                        energy = self.u23e.position
                    else:
                        energy = self.u17e.position
                except Exception:
                    pass
        if energy is not None:
            return round(energy, 3)*1e3

    def get_xray_pulse_duration(self, verbose=False):
        """Get X-ray pulse duration (sec)."""
        duration = None
        if self.inout_xshut.position == "GATED_MODE":
            duration = 1.5e-3
            if verbose:
                print("xshut position: GATED_MODE")
        elif self.inout_xshut.position == "PULSED_MODE":
            duration = 0.6e-3
            if verbose:
                print("xshut position: PULSED_MODE")
        if self.inout_hlc.position == 'IN':
            duration = 100e-6
            if verbose:
                print("hlc position: IN")
        if self.inout_pic.position == 'IN':
            duration = 36e-6
            if verbose:
                print("pic position: IN")
        if self.hsc.position != 'OUT':
            duration = self.hsc.pulse_duration
            if verbose:
                print(f"hsc position: {self.hsc.position}")
                print("pulse duration:", convert_float2str(duration, 's'))
        return duration

    def get_mono(self):
        if self.mono2 is not None:
            if self.mono2._xtals.xtal_sel != "OUT":
                return self.mono2._xtals.xtal_sel
        if self.inout_mono1.position != "OUT":
            return self.mono1._xtals.xtal_sel

    def get_xray_frequency(self):
        if self.n354 is None:
            return
        chopt_freq = self.n354.core.chopt.read_frequency()
        xshut_freq = self.n354.core.xshut.read_frequency()
        if self.xshut.is_open or self.xshut.get_scan_mode() == 'gated':
            return chopt_freq
        elif self.xshut.get_scan_mode() == 'pulsed':
            return xshut_freq
        else:
            return 'unknown'

    def get_fe_state(self):
        if self.fe is None:
            return "unknown"
        else:
            return "open" if self.fe.is_open else "closed"

    def get_sh1_state(self):
        if self.sh1 is None:
            return "unknown"
        else:
            return "open" if self.sh1.is_open else "closed"

    def get_sh2_state(self):
        if self.sh2 is None:
            return "unknown"
        else:
            return "open" if self.sh2.is_open else "closed"

    def get_hlc_state(self):
        if self.inout_hlc is None:
            return "unknown"
        else:
            return "in" if self.inout_hlc.position == "IN" else "out"

    def get_pic_state(self):
        if self.inout_pic is None:
            return "unknown"
        else:
            return "in" if self.inout_pic.position == "IN" else "out"

    def get_hsc_state(self):
        if self.hsc is None:
            return "unknown"
        elif self.hsc.position is None:
            return "unknown"
        else:
            return self.hsc.position.lower()

    def get_xshut_state(self):
        if self.xshut is None:
            return "unknown"
        else:
            return self.xshut.get_state()

    def get_xshut_scan_mode(self):
        if self.xshut is None:
            return "unknown"
        else:
            return self.xshut.get_scan_mode()

    def get_lshut_state(self):
        if self.lshut is None:
            return "unknown"
        else:
            return self.lshut.get_state()

    def get_lshut_scan_mode(self):
        if self.lshut is None:
            return "unknown"
        else:
            return self.lshut.get_scan_mode()


blstatus = BeamlineStatus()

# shortcuts
get_filling_mode = blstatus.get_filling_mode
get_emittance = blstatus.get_emittance
get_srcur = blstatus.get_srcur
get_sbcur = blstatus.get_sbcur
get_sblen = blstatus.get_sblen
get_max_srcur = blstatus.get_max_srcur
get_max_sbcur = blstatus.get_max_sbcur
get_xray_energy = blstatus.get_xray_energy
get_xray_mode = blstatus.get_xray_mode
get_xray_pulse_duration = blstatus.get_xray_pulse_duration
get_xray_frequency = blstatus.get_xray_frequency
get_mono = blstatus.get_mono
