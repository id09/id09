# -*- coding: utf-8 -*-
"""General properties of materials."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "04/03/2020"
__version__ = "0.0.1"


import numpy as np
from collections import OrderedDict
import joblib
import xraydb.materials
from xraydb import material_mu
from .math import gauss_convolve

memory = joblib.Memory("/tmp/.id09_att_cache", verbose=False)

xraydb_materials = xraydb.get_materials()


if any(m not in xraydb_materials.keys() for m in ['zirconium', 'gadox']):
    # update xraydb users local material database
    xraydb.add_material('zirconium', 'Zr', 6.52, categories=['metal'])
    xraydb.add_material('gadox', 'Gd2O2S', 4.3, categories=['scintillator'])
    xraydb_materials = xraydb.get_materials()


def get_density(material):
    """Returns the density of a material.

    Parameters
    ----------
    material : str or list of str
        Chemical formula (e.g, 'Zr' or 'Si3N4').

    Returns
    ------
    density : float or list of float
        Density (kg/m3).

    Notes
    -----
    - For a limited number of compounds (see 'cmpnd_density'), it is possible
      to provide the compound name (polymide, pmma, ...) in place of the
      chemical formula in the 'material' input parameter.

    """

    material = find_material(material, verbose=False)

    if material is None:
        raise ValueError("Material '%s' not in database." % material)

    density = material.density*1e3

    return density


def f0(ion, q):
    """Elastic X-ray scattering factor (form factor) for an ion: f0(q)

    Parameters
    ----------
    ion : str or int
        Atom symbol, ion symbol or atomic number (e.g. 'Fe', 'Fe2+' or 26).
    q : array_like
        Scattering vector magnitude (1/Ang).
        q = 4*pi*sin(theta)/lambda with theta=scattering angle,
        lambda=X-ray wavelength.

    Returns
    -------
    f0: ndarray
        Elastic X-ray scatterring factor.

    """

    return xraydb.f0(ion, q/4/np.pi)


def f1(element, energy):
    """Dispersive component of the atomic scattering factors at q=0.

    f(q, E) = f0(q) + f'(E) + i*f''(E)

    At q=0 (forward scattering):
        f = Z + f'(E) + i*f''(E) = f1(E) + i*f2(E)

    Parameters
    ----------
    element : str
        Atom symbol or atomic number.
    energy : array_like
        X-ray photon energy (eV).

    Returns
    -------
    f1: float or ndarray
        Dispersive component of the scattering factor (electrons/atom).

    """

    f1 = xraydb.f1_chantler(element, energy)

    Z = xraydb.atomic_number(element)

    f1 += Z

    return f1


def f2(element, energy):
    """Absorptive component of the atomic scattering factors at q=0.

    f(q, E) = f0(q) + f'(E) + i*f''(E)

    At q=0 (forward scattering):
        f = Z + f'(E) + i*f''(E) = f1(E) + i*f2(E)

    Parameters
    ----------
    element : str
        Atom symbol or atomic number.
    energy : array_like
        X-ray photon energy (eV).

    xReturns
    -------
    f2: float or ndarray
        Absorptive component of the scattering factor (electrons/atom).

    """

    f2 = xraydb.f2_chantler(element, energy)

    return f2


def f1f2(element, energy):
    """Atomic (anomalous) scattering factors at q=0.

    f(q, E) = f0(q) + f'(E) + i*f''(E)

    At q=0 (forward scattering):
        f = Z + f'(E) + i*f''(E) = f1(E) + i*f2(E)

    Parameters
    ----------
    element : str
        Atom symbol or atomic number.
    energy : array_like
        X-ray photon energy (eV).

    Returns
    -------
    f1: float or ndarray
        Dispersive component of the scattering factor (electrons/atom).
    f2: float or ndarray
        Absorptive component of the scattering factor (electrons/atom).

    """

    f1 = xraydb.f1_chantler(element, energy)
    Z = xraydb.atomic_number(element)
    f1 += Z

    f2 = xraydb.f2_chantler(element, energy)

    # f1, f2 = periodictable.xray_sld(element, energy=energy*1e-3)

    return f1, f2


def index_of_refraction(material, energy, density=None):
    """Calc index of refraction of a material at given X-ray energy.

    Parameters
    ----------
    material : str
        Material name or chemical formula (e.g, 'Zr' or 'Si3N4').
    energy : float
        X-ray photon energy (eV).
    density : float or None, optional
        Material density (kg/m3). If None, density is taken from
        the cmpnd_density dictionary if the corresponding entry is
        available.

    Returns
    -------
    n : array-like of complex
        Index of refraction at given X-ray energy.

    """

    material = find_material(material, verbose=False)

    if material is None:
        raise ValueError("Material '%s' not in database." % material)

    if density is not None:
        density *= 1e-3
    else:
        density = material.density

    delta, beta, al = xraydb.xray_delta_beta(
            material.fornula, density, energy, photo_only=False)

    n = 1 - delta - 1j*beta

    return n

@memory.cache
def attenuation_coefficient(material, energy, density=None, bw=None):

    """Calc linear attenuation coefficient of a material at given X-ray energy.

    Parameters
    ----------
    material : str
        Material name or chemical formula (e.g, 'pmma', 'Zr' or 'Si3N4').
    energy : array-like
        X-ray photon energy (eV).
    density : float or None, optional
        Material density (kg/m3). If None, density is taken from
        the materials_density dictionary if the corresponding entry is
        available.
    bw : float or None, optional
        Convolution spectrum (Gaussian) relative bandwidth (%).
        If energies spaced by more than one-tenth of 'bw' are present,
        each of these energies is convoluted with a Gaussian having
        nregrid=11 points between -3*sigma and +3*sigma.


    Returns
    -------
    mu: array-like of float
        Linear attenuation coefficient at given X-ray energy (1/m).

    """

    material = find_material(material, verbose=False)

    if material is None:
        raise ValueError("Material '%s' not in database." % material)

    if density is not None:
        density *= 1e-3
    else:
        density = material.density

    name = material.name

    mu = material_mu(name=name, energy=energy, density=density)*1e2

    if bw is not None:
        regrid = 11
        energy = np.array(energy)
        sig = energy/100*bw/2.355
        if energy.size > 1:
            mu = gauss_convolve(energy, mu, sig)
            bws = np.diff(energy)/energy[1:]*100
            mask = bws > bw/nregrid
            mu_patched = []
            for E in energy[mask]:
                sigE = E/100*bw/2.355
                e = np.linspace(E-3*sigE, E+3*sigE, nregrid)
                y = material_mu(name=name, energy=e, density=density)*1e2
                y = gauss_convolve(e, y, sigE)
                mu_patched.append(y[len(e)//2])
            mu = np.where(mask, energy, mu_patched)
        else:
            e = np.linspace(energy-3*sig, energy+3*sig, nregrid)
            y = material_mu(name=name, energy=e, density=density)*1e2
            y = gauss_convolve(e, y, sig)
            mu = y[len(e)//2]

    return mu


def attenuation_length(material, energy, density=None, bw=None):
    """Calc attenuation_length of a material at given X-ray energy.

    Parameters
    ----------
    material : str
        Material name or chemical formula (e.g, 'pmma', 'Zr' or 'Si3N4').
    energy : array-like
        X-ray photon energy (eV).
    density : float or None, optional
        Material density (kg/m3). If None, density is taken from
        the materials dictionary if the corresponding entry is
        available.
    bw : float or None, optional
        Convolution spectrum (Gaussian) relative bandwidth.
        If energies spaced by more than one-tenth of 'bw' are present,
        each of these energies is convoluted with a Gaussian having
        nregrid=11 points between -3*sigma and +3*sigma.


    Returns
    -------
    al: ndarray of float
        Attenuation length at given X-ray energy (1/m).

    """

    mu = attenuation_coefficient(material=material, energy=energy,
                                 density=density, bw=bw)

    al = 1/mu

    return al


def transmission(material, energy, thickness, density=None, angle=None,
                 bw=None):
    """Calc X-ray transmission of a solid at a given energy.

    Parameters
    ----------
    material : str
        Material name or chemical formula (e.g, 'pmma', 'Zr' or 'Si3N4').
    energy : array-like
        X-ray photon energy (eV).
    thickness : array-like
        Solid target thickness (m).
    density : float or None, optional
        Material density (kg/m3). If None, density is taken from
        the materials dictionary if the corresponding entry is
        available.
    angle : float or None, optional
        Effective angle between the attenuator normal and the
        incoming beam (deg).
        Default (None): normal incidence.
    bw : float or None, optional
        Convolution spectrum (Gaussian) relative bandwidth.
        If energies spaced by more than one-tenth of 'bw' are present,
        each of these energies is convoluted with a Gaussian having
        nregrid=11 points between -3*sigma and +3*sigma.

    Returns
    -------
    T: ndarray of float
        sample transmission.

    """

    if angle is None:
        angle = 0

    d = thickness / np.cos(np.deg2rad(angle))

    mu = attenuation_coefficient(material=material, energy=energy,
                                 density=density, bw=bw)

    if len(np.shape(mu)) > 1 and len(np.shape(d)) > 1:
        alpha = np.outer(mu, d)
    else:
        alpha = mu*d

    T = np.exp(-alpha)

    return T


def find_material(material, verbose=False):
    """
    Look up material name from xraydb database.

    Parameters
    ----------
    material : str or list
        Chemical formula (e.g, 'Zr' or 'Si3N4') or material name
        (e.g. 'zirconium' or 'silicon nitride').

    Returns
    -------
    xraydb_material : xraydb.materials.Material instance
        Material properties.

    """

    xraydb_material = xraydb.find_material(material)

    if verbose:
        if xraydb_material is None:
            print("Material '%s' is not in xraydb database." % material)
        else:
            print("Material found in xraydb database: ")
            for prop in ['name', 'formula', 'density', 'categories']:
                print('%s: ' % prop, xraydb_material.__getattribute__(prop))

    return xraydb_material


_materials = OrderedDict()


_materials['thermal_conductivity'] = {
    # units: W/m/K
    # conditions: room temperature
    'Ag': 406,
    'Al': 220,
    'Au': 314,
    'Be': 200,
    # 'B4C': 17,
    # 'brass': 109,
    'diamond carbon': 1800,
    'CdTe': 6.2,
    'Cu': 400,  # Cu OHFC: 385 W/m/K
    'Fe': 80,
    'GaAs': 55,
    'Ge': 58,
    'Pb': 35,
    'sapphire': 34.6,
    'Si': 130,
    # 'SiC': 360,
    'Si3N4': 10,
    'silica': 1.4,
    'quartz': 12,
    'Ta': 57.5,
    'Ti': 17,
    'W': 173,
    # 'WC': 110,
    'yag': 14,
    'Zr': 23
    }

_materials['melting_temperature'] = {
    # units: C
    'Ag': 961,
    'Al': 660,
    'Au': 1064,
    'Be': 1285,
    # 'B4C': 2763,
    # 'brass': 900,
    'diamond carbon': 4000,  # at 1500 C diamond transforms into graphite
    'graphite carbon': 4000,
    'CdTe': 1041,
    'Cu': 1357.8,
    'Fe': 1538,
    'GaAs': 1238,
    'Ge': 937,
    'Si': 1412,
    # 'SiC': 2830,
    'Si3N4': 1900,
    'sapphire': 2030,
    'Ti': 1668,
    'Ta': 3017,
    'yag': 1970,
    'W': 3422,
    # 'WC': 2870,
    'Zr': 1855
    }

_materials['specific_heat'] = {
    # units: J/g/K
    # conditions: room temperature
    'Ag': 0.24,
    'Al': 0.9,
    'Au': 0.129,
    'air': 1.02,
    'Be': 1.87,
    # 'B4C': 0.84,
    # 'brass': 0.38,
    'diamond carbon': 0.5,
    'Cu': 0.385,
    'Fe': 0.44,
    'GaAs': 0.33,
    'Ge': 0.31,
    'He': 5.3,
    'Pb': 0.16,
    'sapphire': 0.65,
    'Si': 0.7,
    'Si3N4': 0.67,
    'silica': 0.84}

_materials['lattice_constant'] = {
    # units: Angstrom
    # 'B4C': [5.6, 12.12],
    'diamond carbon': 3.57,
    'CdTe': 6.5,
    'Fe': 2.87,
    'Ge': 5.43,
    'GaAs': 5.65,
    'Si': 5.65
    }

_materials['eV_band_gap'] = {
    # units: eV
    # 'B4C': 2.09,
    'diamond carbon': 5.5,
    'CdTe': 1.5,
    'GaAs': 1.44,
    # 'gadolinium oxysulfide': 4.4,
    'Ge': 0.66,
    'Si': 1.12
    }

_materials['eV_per_eh_pair'] = {
    # units: eV
    # conditions: room temperature
    'diamond carbon': 13,
    'CdTe': 4.4,
    'GaAs': 4.1,
    # 'gadolinium oxysulfide': 4.4,
    'Ge': 2.9,
    'Si': 3.6
    }
