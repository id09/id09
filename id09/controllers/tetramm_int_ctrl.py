# -*- coding: utf-8 -*-
"""CAENels TetrAMM picoammeter BLISS controller."""

__author__ = "Matteo Levantino, Celine Mariette, Perceval Guillou, \
              Laurent Claustre"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "26/06/2024"
__version__ = "0.0.1"


import numpy as np
import time
import gevent

from bliss import global_map
from bliss.common.utils import all_equal
from bliss.common.utils import autocomplete_property

from bliss.common.tango import DeviceProxy

from bliss.common.counter import IntegratingCounter
from bliss.common.protocols import CounterContainer

from bliss.controllers.counter import (
    IntegratingCounterController,
    IntegratingCounterAcquisitionSlave,
)

from id09.colors import bold


class TetrammCounter(IntegratingCounter):

    def __init__(self, config, name=None, controller=None, channel=None,
                 conversion_function=None, unit=None):

        if config is not None:

            self._config = config
            name = config.get("name")
            self.controller = config.get("controller", default=None)
            self.channel = config.get("channel", default=None)
            conversion_function = config.get("conversion_function",
                                             default=None)
            self.unit = config.get("unit", default=None)

        else:

            self.controller = controller
            self.channel = channel
            self.conversion_function = conversion_function
            self.unit = unit

        super().__init__(name=name, controller=self.controller,
                         conversion_function=conversion_function,
                         unit=self.unit)

        if self.channel is not None:
            self.channel = int(self.channel)


class TetrammICC(IntegratingCounterController):

    def __init__(self, config, name=None, tango_name=None):

        if config is not None:
            self._config = config
            name = config.get("name")
            tango = config.get("tango")
            if tango is not None:
                tango_name = tango['name']
            else:
                raise ValueError("'tango' cannot be None.")
        else:
            if tango_name is None:
                raise ValueError("'tango_name' and 'url' cannot be both None.")
        self.burst_npulses = None
        self.ds = DeviceProxy(tango_name)
        # 10 min timeout is needed for triggered acquisitions outside scans:
        # self.ds.set_timeout_millis(1000*60*10)
        # self.ds.set_timeout_millis(1000*10)  # 10 sec timeout for tests

        super().__init__(name=name, master_controller=None,
                         register_counters=True)

        self._debug = False
        self.xray_freq = 0

    @property
    def debug(self):
        return self._debug

    @debug.setter
    def debug(self, value):
        self._debug = value

    def get_acquisition_object(self, acq_params, ctrl_params,
                               parent_acq_params):
        """
        Return a IntegratingCounterAcquisitionSlave.

        args:
         - `acq_params`: parameters for the acquisition object (dict)
         - `ctrl_params`: parameters for the controller (dict)
         - `parent_acq_params`: acquisition parameters of the master (if any)
        """
        trigger_mode = acq_params.pop("trigger_mode", "SOFTWARE")
        return TetrammICAS(self, trigger_mode=trigger_mode, debug=self.debug,
                           ctrl_params=ctrl_params, **acq_params)

    def get_default_chain_parameters(self, scan_params, acq_params):
        """
        Return necessary acquisition parameters in the context of step by
        step scans.

        args:
         - `scan_params`: parameters of the scan (dict)
         - `acq_params`: parameters for the acquisition (dict)

        return: a dictionary of acquisition parameters

        In the context of a step-by-step scan, `acq_params` is usually empty
        and the returned dict must be deduced from `scan_params`.

        However, in the case of a customized DEFAULT_CHAIN, `acq_params` may
        be not  self._stop_flag = True
        empty and these parameters must override the default ones.
        """
        try:
            count_time = acq_params["count_time"]
        except KeyError:
            count_time = scan_params["count_time"]

        params = {"count_time": count_time}

        if scan_params["npoints"] > 0:
            params["npoints"] = scan_params["npoints"]
        else:
            params["npoints"] = 1

        return params

    def get_values(self, from_index, *counters):
        """Get counter values corresponding to 'counters' as a list.

        For each counter, data is a list of values (one per measurement).
        All counters must retrieve the same number of data!

        args:
          - from_index: an integer corresponding to the index of the
            measurement from which new data should be retrieved
          - counters: the list of counters for which measurements should be
            retrieved.

        example:
            tmp = [self.get_available_measurements(cnt, from_index) for cnt in
                   counters]
            dmin = min([len(cnt_data) for cnt_data in tmp])


            return [cnt_data[:dmin] for cnt_data in tmp]
        """

        data = self.ds.get_data(from_index)  # flattened array of readouts

        if len(data) == 0:
            cnt_values = [[] for cnt in counters]

        else:
            nch = self.ds.last_nch
            nreadouts = len(data) // nch
            data = data.reshape((nreadouts, nch))
            cnt_values = []
            for cnt in counters:
                counter_readouts = list(data[:, cnt.channel-1])
                cnt_values.append(counter_readouts)

        return cnt_values


class TetrammICAS(IntegratingCounterAcquisitionSlave):

    def __init__(
            self,
            *counters,
            ctrl_params=None,
            count_time=None,
            npoints=1,
            prepare_once=True,
            start_once=False,
            trigger_mode="SOFTWARE",
            debug=False,
    ):
        self.trigger_mode = trigger_mode
        self.debug = debug
        self._is_prepared = False
        super().__init__(
            *counters,
            count_time=count_time,
            npoints=npoints,
            prepare_once=prepare_once,
            start_once=start_once,
            ctrl_params=ctrl_params,
        )

        # self._AcquisitionObject__prepare_once = True

    def wait_ready(self):
        """Wait at beginning of scan and at beginning/end of the each point."""

        if self.debug:
            print("=== AcquisitionSlave: wait_ready() start")

        while self.device.ds.busy_outside_scan:
            time.sleep(0.01)

        if not self._is_prepared:
            if self.device.ds.in_scan:
                raise Exception("Another scan using the same counter is " +
                                "running (tetramm.ds.in_scan=True).")
            self.device.ds.in_scan = True
        else:
            while self.device.ds.busy_in_scan_readout:
                if self.debug:
                    print("=== AcquisitionSlave: wait_ready() busy in scan " +
                          "point (readout)")
                time.sleep(0.01)

        if self.debug:
            print("=== AcquisitionSlave: wait_ready() stop")

    def _prepare_device(self):
        """
        Prepare device (tetramm) once.
        The underscore is needed to distinguish this method from the standard
        BLISS prepare_device().
        What we want here is to create a function that is called only through
        start_device() and not directly by BLISS.
        """
        if self.trigger_mode == 'HARDWARE':
            if self.debug:
                print("=== AcquisitionSlave: _prepare_device():" +
                      " --> HARDWARE trigger")
            naq = 1
            if self.device.single_pulse:
                ntrg = 1
            else:
                ntrg = int(np.round(self.device.xray_freq*self.count_time, 0))
            nch = 4
        elif self.trigger_mode == 'SOFTWARE':
            if self.debug:
                print("=== AcquisitionSlave: _prepare_device():" +
                      " --> SOFTWARE trigger")
            naq = int(self.count_time*self.device.ds.last_data_rate)
            ntrg = 1
            nch = 4
        else:
            raise Exception(f"trigger_mode={self.trigger_mode} not recognized")

        self.device.ds.in_scan = True  # redundant: done on 1st wait_ready

        if naq >= 1:
            if self.trigger_mode == "HARDWARE":
                self.device.ds.trg_on()
            elif self.trigger_mode == "SOFTWARE":
                self.device.ds.trg_off()
            self.device.ds.prepare_acq_once([naq, ntrg, nch])
        else:
            raise Exception("'count_time' too small for TetrAMM 'data_rate'" +
                            f"={self.device.ds.last_data_rate:g}Hz.")

        self._is_prepared = True

    def prepare_device(self):
        """
        Nothing to do since, we prefer to late prepare in the first call
        of the start_device(). By this way acq_on() cannot be executed
        if a Ctrl-C happens during the prepare phase. Remember that
        stop_device() is only called if start_device() has been run before.
        """
        pass

    def start_device(self):
        """
        Arm device: done at every scan point before trigger().

        The master will not trigger the aquisition (e.g. n354 burst) until
        the execution of this function is completed.

        For the above reason, data buffering cannot be initiated here.
        """
        if self.debug:
            print("=== AcquisitionSlave: start_device()")
        if not self._is_prepared:
            self._prepare_device()
        self.device.ds.acq_on()

    def reading(self):
        try:
            if self.debug:
                print("=== AcquisitionSlave: reading() try")
            from_index = 0
            while (
                not self.npoints or self._nb_acq_points < self.npoints
            ) and not self._stop_flag:
                counters = list(self._counters.keys())

                # data should be a list:
                # - each element of the list contains all readouts of a given
                #   counter
                # - all counters should have the same number of readouts
                data = [
                    counters[i].conversion_function(x)
                    for i, x in enumerate(self.device.get_values(from_index,
                                                                 *counters))
                ]
                # same as:
                # data = self.device.get_values(from_index, *counters)
                # if not conversion_function is applied

                if not all_equal([len(d) for d in data]):
                    raise RuntimeError("Read data can't have different sizes")

                if len(data[0]) > 0:
                    if self.debug:
                        print("=== AcquisitionSlave: reading() while")
                        print("===                   " +
                              bold(f"from_index: {from_index}->" +
                                   f"{from_index+len(data[0])}"))
                    from_index += len(data[0])
                    self._nb_acq_points += len(data[0])
                    self._emit_new_data(data)
                    gevent.sleep(0.1)
        finally:
            if self.debug:
                print("=== AcquisitionSlave: reading() finally")
            self.stop_device()

    def trigger(self):
        """
        Trigger the readout at every scan point.

        This is called always after acq_on() AND after the master has
        triggered the acquisition (e.g. after the call from within the n354
        controller trigger() function).

        Once the above is done, the burst at some point WILL BE sent.

        So, it is fine to start the data buffering here.
        """
        if self.debug:
            print("=== AcquisitionSlave: " + bold("trigger()"))
        # to avoid blocking calls to DS we have removed the following command:
        # self.device.ds.trigger_readout()
        # and replaced it with:
        self.device.ds.command_inout_asynch('trigger_readout')
        # the trigger should not be a blocking call

    def stop_device(self):
        """
        Stop device at the end of the scan or at CTRL-C.

        NOTE: if CTRL-C is pressed before start_device() is executed, the
              stop_device() is not called (that is why we are not using
              apply_parameters() anymore)
        Bliss call stop() which set the _stop_flag to True, then reading() will
        call stop_device() in the finally statement.
        """
        if self.debug:
            print("=== AcquisitionSlave: stop()")
        if self.trigger_mode == 'HARDWARE':
            self.device.ds.acq_off()
            self.device.ds.trg_off()

        # self.device.ds.empty_buffer()  # in principle not needed

        self.device.ds.in_scan = False


class TetrammCC(CounterContainer):

    # NOT WORKING!

    def __init__(self, config):
        super().__init__()
        self._hw_controller = None
        self._icc = TetrammICC(config)
        self.ds = self._icc.ds
        # global_map.register(self, parents_list=["counters"])

    @property
    def counters(self):
        return self._icc.counters
