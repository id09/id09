import numpy as np
import warnings
from bliss.controllers.motor import CalcController


class MotorOffset(CalcController):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.offset = float(self.config.get("offset", default=0))

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        calc_pos = positions_dict["real_mot"] + self.offset
        return {"calc_mot": calc_pos}

    def calc_to_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_pos = positions_dict["calc_mot"] - self.offset
        return {"real_mot": real_pos}


class MotorScale(CalcController):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scale = self.config.get("scale", default=1)  # from YAML
        if self.scale is None or self.scale == 0:
            raise ValueError("'self.scale' must be not None and not zero.")
        self.scale = float(self.scale)

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        self.calc_mot_offset = self._tagged["calc_mot"][0].offset  # from REDIS
        self.real_mot_offset = self._tagged["real_mot"][0].offset  # from REDIS
        calc_pos = positions_dict["real_mot"]  # + self.real_mot_offset
        calc_pos *= self.scale
        # calc_pos -= self.calc_mot_offset
        return {"calc_mot": calc_pos}

    def calc_to_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        self.calc_mot_offset = self._tagged["calc_mot"][0].offset  # from REDIS
        self.real_mot_offset = self._tagged["real_mot"][0].offset  # from REDIS
        real_pos = positions_dict["calc_mot"]  # -self.calc_mot_offset
        real_pos /= self.scale
        # real_pos += self.real_mot_offset
        return {"real_mot": real_pos}


class MotorAxesRotation(CalcController):

    """
    Calculational Motors defined as linear combination of 2 real motors.


    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scale11 = float(self.config.get("scale11"))
        self.scale12 = float(self.config.get("scale12"))
        self.scale21 = float(self.config.get("scale21"))
        self.scale22 = float(self.config.get("scale22"))

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_pos1 = positions_dict["real_mot1"]
        real_pos2 = positions_dict["real_mot2"]
        calc_pos1 = self.scale11 * real_pos1 + self.scale12 * real_pos2
        calc_pos2 = self.scale21 * real_pos1 + self.scale22 * real_pos2
        return {"calc_mot1": calc_pos1, "calc_mot2": calc_pos2}

    def calc_to_real(self, positions_dict):
        """
        positions_dict : dict of np.array

        TO DO: matrix inversion
        """
        pass
        # real_pos1 =
        # real_pos2 =
        # return {'real_mot1': real_pos1. 'real_mot2': real_pos2}


class MotorBender(CalcController):

    """
    Calculational motor: bender real motor --> surface radius of curvature.

    The correspondance is based on a linear fit of a 1/Rc vs. bend_mot curve.
    Rc: measured radius of curvature (optics group)
    bend_mod: real motor dial position in steps

    The parameters derived from the calibration curve fit are:
    - cal_intercept (1/km)
    - cal_slope (1/km/steps)

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.slope = self.config.get("slope")
        self.intercept = self.config.get("intercept")
        if self.slope is None or self.slope == 0:
            raise ValueError("'self.scale' must be not None and not zero.")
        if self.intercept is None:
            raise ValueError("'self.intercept' cannot be None.")
        self.slope = float(self.slope)
        self.intercept = float(self.intercept)

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_pos = positions_dict["real_mot"]
        calc_pos = 1 / (self.slope * real_pos + self.intercept)
        return {"calc_mot": calc_pos}

    def calc_to_real(self, positions_dict):
        calc_pos = positions_dict["calc_mot"]
        real_mot = (1 / calc_pos - self.intercept) / self.slope
        return {"real_mot": real_mot}


class CollimatorPipe(CalcController):
    """
    ...

    Properties
    ----------
    d : float
        Distance between hinge points (mm).
    fla : float
        Front lever arm, i.e. distance between pipe front tip and
        front hinge point (mm).
    length : float
        Pipe total length (mm).

    """

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.d = self.config.get("d")
        self.fla = abs(self.config.get("fla"))
        self.length = self.config.get("l")

    def calc_from_real(self, positions_dict):
        pos = positions_dict["position"]
        tilt_mm = positions_dict["tilt_mm"]
        tilt_rad = np.arcsin(tilt_mm / self.d)
        back = pos + self.fla * np.sin(tilt_rad)
        front = pos - (self.length - self.fla) * np.sin(tilt_rad)
        return {"front": front, "back": back}

    def calc_to_real(self, positions_dict):
        front = positions_dict["front"]
        back = positions_dict["back"]
        tilt_mm = self.d * (back - front) / self.length
        pos = back - self.fla * tilt_mm / self.d
        return {"position": pos, "tilt_mm": tilt_mm}


class UndulatorEnergy(CalcController):

    """
    Calculational motor: ...

    NOTE: it looks like when u17 is disabled also u17e is 
          disabled, then we do not need to check here if 
          u17.position is NaN.

    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.p1 = self.config.get("p1")
        self.p2 = self.config.get("p2")
        self.p3 = self.config.get("p3")
        self.harmonic = self.config.get("harmonic")

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_pos = positions_dict["real_mot"]
        calc_pos = self.p1 / (1 + self.p2*np.exp(-self.p3*real_pos))
        calc_pos *= self.harmonic
        return {"calc_mot": calc_pos}

    def calc_to_real(self, positions_dict):
        calc_pos = positions_dict["calc_mot"] / self.harmonic
        log_arg = (self.p1/calc_pos - 1)/self.p2
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=RuntimeWarning)
            real_pos = -1/self.p3*np.log(log_arg)
        return {"real_mot": real_pos}


class UndulatorMonoEnergy(CalcController):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        und_pos = positions_dict["und"]
        mono_pos = positions_dict["mon"]
        return {"calc_mot": mono_pos}

    def calc_to_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_mot = {"und": positions_dict["calc_mot"],
                    "mon": positions_dict["calc_mot"]}
        return real_mot           


class BeamPointing(CalcController):

    def __init__(self, config):
        super().__init__(config)
        self.d = self.config.get("d")
        self.incl0 = self.config.get("incl0")
        self.tabz0 = self.config.get("tabz0")

    def get_beam_offset(self, incl):
        return 2*self.d*np.tan((incl-self.incl0)*1e-3)*1e3

    def calc_from_real(self, real_user_pos_dict):
        m1ry_pos = real_user_pos_dict['m1ry_pos']
        tabz_pos = real_user_pos_dict['tabz_pos']
        beam_inclination = -m1ry_pos
        beam_offset = self.get_beam_offset(beam_inclination)
        table_offset = tabz_pos-self.tabz0-beam_offset
        pseudo_dial_pos_dict = {'beam_inclination': beam_inclination,
                                'table_offset': table_offset}
        return pseudo_dial_pos_dict

    def calc_to_real(self, pseudo_dial_pos_dict):
        beam_inclination = pseudo_dial_pos_dict['beam_inclination']
        table_offset = pseudo_dial_pos_dict['table_offset']
        m1ry_pos = -beam_inclination
        beam_offset = self.get_beam_offset(beam_inclination)
        tabz_pos = self.tabz0 + table_offset + beam_offset
        real_user_pos_dict = {'m1ry_pos': m1ry_pos, 'tabz_pos': tabz_pos}
        return real_user_pos_dict


class MotorCombination(CalcController):
    """Quick fix for ls3226 to move simultaneously both the lens and mirror
       motors.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def calc_from_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        calc_pos = positions_dict["mot1"]
        offset = positions_dict["mot2"] - positions_dict["mot1"]
        return {"calc_mot": calc_pos, 'offset': offset}

    def calc_to_real(self, positions_dict):
        """
        positions_dict : dict of np.array
        """
        real_mot = {"mot1": positions_dict["calc_mot"],
                    "mot2": positions_dict["offset"] + positions_dict["calc_mot"]}
        return real_mot                             
