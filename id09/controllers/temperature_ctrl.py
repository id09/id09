# -*- coding: utf-8 -*-
"""Define ID09 temperature sensors."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "08/06/2022"
__version__ = "0.0.1"


import numpy as np

from bliss.controllers.counter import SamplingCounterController


class TemperatureSensor:

    def __init__(self, config, material=None, name=None, description=None,
                 wago=None, wago_ch=None, calibration_intercept=None,
                 calibration_slope=None):
 
        self._config = config

        if config is not None:
            # load from YAML
            self.name = config.get("name")
            self.description = config.get("description", default=None)
            self.material = config.get("material", default=None)
            wago = config.get("wago", default=None)
            wago_ch = config.get("wago_ch", default=None)
            intercept = config.get("calibration_intercept", default=None)
            slope = config.get("calibration_slope", default=None)
            self.calibration = [intercept, slope]
        else:
            self.name = name
            self.description = description
            self.material = material
            self.calibration = [calibration_intercept, calibration_slope]

        if wago is not None:

            if wago_ch is None:
                raise TypeError("'wago_ch' cannot be None if 'wago' is " +
                                "not None.")

            self._wago = wago
            self.wago = wago.name
            self._wago_ch = wago_ch
            
    @property 
    def T(self, int_time=None):
        self._T = None
        if self.wago is not None:
            value = self._wago.get(self._wago_ch)
            if self.calibration[0] is not None:
                self._T = (value - self.calibration[0])/self.calibration[1]
            else:
                self._T = value
        return self._T
