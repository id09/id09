#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.


from tango import DevState
from tango.server import run
from tango.server import Device
from tango.server import command, device_property
from tango import GreenMode

from bliss.config.static import get_config


def switch_state(tg_dev, state=None, status=None):
    """Helper to switch state and/or status and send event"""
    if state is not None:
        tg_dev.set_state(state)
        if state in (DevState.ALARM, DevState.UNKNOWN, DevState.FAULT):
            msg = "State changed to " + str(state)
            if status is not None:
                msg += ": " + status
    if status is not None:
        tg_dev.set_status(status)


class Keithley(Device):

    beacon_name = device_property(dtype=str, doc="keithley bliss object name")

    def init_device(self):
        Device.init_device(self)

        try:
            config = get_config()
            self.device = config.get(self.beacon_name)
            switch_state(self, DevState.ON, "Ready!")
        except Exception as e:
            msg = "Exception initializing device: {0}".format(e)
            self.error_stream(msg)
            switch_state(self, DevState.FAULT, msg)

        self._last_read = None

    def delete_device(self):
        if self.device:
            self.device.abort()

    @command(dtype_out=str)
    def get_info(self):
        return self.device.__info__()

    @command(dtype_out=float)
    def read_now(self):
        self._last_read = float(self.device.get_current())
        return self._last_read

    @command(dtype_out=float)
    def read_last(self):
        if self._last_read is not None:
            return self._last_read
        else:
            return self.read_now()

    @command(dtype_out=str)
    def get_range(self):
        return self.device.get_range()

    @command(dtype_in=float)
    def set_range(self, cur_range):
        self.device.set_range(cur_range)

    @command(dtype_out=str)
    def get_all_ranges(self):
        all_ranges = self.device.get_all_ranges()
        return ", ".join([str(r) for r in all_ranges])

    @command
    def set_range_auto(self):
        self.device.set_range_auto()

    @command
    def set_range_manual(self):
        self.device.set_range_manual()

    @command(dtype_out=str)
    def get_rate(self):
        return self.device.get_rate()

    @command(dtype_out=str)
    def set_rate_slow(self):
        ans = self.device.set_rate_slow()
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def set_rate_med(self):
        ans = self.device.set_rate_med()
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def set_rate_fast(self):
        ans = self.device.set_rate_fast()
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def get_analog_filter(self):
        return self.device.get_analog_filter()

    @command
    def set_analog_filter_off(self):
        self.device.set_analog_filter_off()

    @command
    def set_analog_filter_on(self):
        self.device.set_analog_filter_on()

    @command(dtype_out=str)
    def get_digital_filter(self):
        return self.device.get_digital_filter()

    @command
    def set_digital_filter_off(self):
        self.device.set_digital_filter_off()

    @command(dtype_out=str)
    def set_remote_on(self):
        ans = self.device.set_remote_on()
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def set_remote_off(self):
        ans = self.device.set_remote_off()
        if ans is None:
            ans = "OK"
        return ans

    @command
    def set_display_enable(self):
        self.device.set_display_enable()

    @command(dtype_out=str)
    def set_display_disable(self):
        ans = self.device.set_display_disable()
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def get_display_intensity(self):
        return self.device.get_display_intensity()

    @command(dtype_out=str)
    def get_display_digits(self):
        return str(self.device.get_display_digits())

    @command(dtype_in=int, dtype_out=str)
    def set_display_digits(self, digits):
        ans = self.device.set_display_digits(digits)
        if ans is None:
            ans = "OK"
        return ans

    @command(dtype_out=str)
    def get_zerocheck(self):
        return self.device.get_zerocheck()

    @command
    def set_zerocheck_on(self):
        self.device.set_zerocheck_on()

    @command
    def set_zerocheck_off(self):
        self.device.set_zerocheck_off()

    @command(dtype_out=str)
    def get_zerocorrect(self):
        return self.device.get_zerocheck()

    @command
    def set_zerocorrect_on(self):
        self.device.set_zerocorrect_on()

    @command
    def set_zerocorrect_off(self):
        self.device.set_zerocorrect_off()

    @command(dtype_out=str)
    def get_autozero(self):
        return self.device.get_autozero()

    @command
    def set_autozero_on(self):
        self.device.set_autozero_on()

    @command
    def set_autozero_off(self):
        self.device.set_autozero_off()

    @command(dtype_out=str)
    def get_error_queue(self):
        return self.device.get_error_queue()

    @command(dtype_out=str)
    def get_model(self):
        return self.device.get_model()


def main():
    run([Keithley], green_mode=GreenMode.Gevent)


if __name__ == "__main__":
    main()
