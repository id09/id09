from bliss.common.counter import CalcCounter
from bliss.controllers.counter import CalcCounterController
from bliss.controllers.expression_based_calc import ExprCalcParameters
from scipy.constants import e
from id09.status import get_xray_energy, get_max_sbcur
from id09.controllers.pindiode_calc_ctrl import photons_per_pulse


class FluxCalcCounterController(CalcCounterController):

    def __init__(self, name, config, constants, unit):
        super().__init__(name, config)
        self._constants = constants
        self._unit = unit

    def calc_function(self, input_dict):
        const_dict = self._constants.to_dict()
        sbcur_max = const_dict['sbcur_max']
        if sbcur_max == 'auto':
            sbcur_max = get_max_sbcur(verbose=False)
        thickness = const_dict['thickness']
        vals = [input_dict[self.tags[cnt.name]] for cnt in self.inputs]
        if vals[1] is None:
            cur = vals[0]
        else:
            cur = vals[0] / vals[1] * sbcur_max
        energy = get_xray_energy()
        flux = photons_per_pulse(cur, energy, thickness=thickness)
        if self._unit == 'nJ':
            flux = [f*energy*e*1e9 for f in flux]
        return {self.tags[self.outputs[0].name]: flux}


class FluxCalcCounter(CalcCounter):

    def __init__(self, name, config):
        super().__init__(name, unit=config.get("unit"))
        self.constants = ExprCalcParameters(name, config)
        self._config = config
        self.apply_config()

    def apply_config(self, reload=False):
        self.constants.apply_config(reload)
        if reload:
            self._config.reload()

        self._unit = self._config.get("unit")
        name = self._config["name"]
        calc_ctrl_config = {
            "inputs": self._config["inputs"],
            "outputs": [{"name": name, "tags": name}],
        }
        self._set_controller(
            FluxCalcCounterController(
                name + "_ctrl",
                calc_ctrl_config,
                self.constants,
                self._unit
            )
        )

    def __info__(self):
        txt = super().__info__()

        txt += " inputs:\n"
        for cfg in self._config.get("inputs", []):
            obj = cfg["counter"]
            if hasattr(obj, "name"):
                obj = obj.name
            txt += f"  - {cfg['tags']}: {obj}\n"

        txt += " constants:\n"
        for k, v in self._config.get("constants", {}).items():
            txt += f"  - {k}: {v}\n"

        return txt


class DewPointCounterController(CalcCounterController):

    def __init__(self, name, config, constants):
        super().__init__(name, config)
        self._constants = constants

    def calc_function(self, input_dict):
        const_dict = self._constants.to_dict()
        cur = input_dict['analog_cur']
        dp1 = const_dict['dewpoint_at_4mA']
        dp2 = const_dict['dewpoint_at_20mA']
        m = (dp2 - dp1) / (20 - 4)
        q = dp2 - m * 20
        dew_point = m * cur + q
        return {self.tags[self.outputs[0].name]: dew_point}


class DewPointCalcCounter(CalcCounter):

    def __init__(self, name, config):
        super().__init__(name, unit=config.get("unit"))
        self.constants = ExprCalcParameters(name, config)
        self._config = config
        self.apply_config()

    def apply_config(self, reload=False):
        self.constants.apply_config(reload)
        if reload:
            self._config.reload()

        self._unit = self._config.get("unit")
        name = self._config["name"]
        calc_ctrl_config = {
            "inputs": self._config["inputs"],
            "outputs": [{"name": name, "tags": name}],
        }
        self._set_controller(
            DewPointCounterController(
                name + "_ctrl",
                calc_ctrl_config,
                self.constants,
            )
        )

    def __info__(self):
        txt = super().__info__()

        txt += " inputs:\n"
        for cfg in self._config.get("inputs", []):
            obj = cfg["counter"]
            if hasattr(obj, "name"):
                obj = obj.name
            txt += f"  - {cfg['tags']}: {obj}\n"

        txt += " constants:\n"
        for k, v in self._config.get("constants", {}).items():
            txt += f"  - {k}: {v}\n"

        return txt
