# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Control of PIN diodes

yml configuration example:

- class: PindiodeCounterContainer
  plugin: generic
  package: id09.controllers.pindiodes_ctrl
  name: pd3
  picoammeter: tetramm (or keithley)
  url: "id09tetramm1.esrf.fr"  # (or "enet://gpibid9b.esrf.fr")
  port: 10001  # needed only for tetramm
  channel: 3
  pad: 3  # needed only for gpib
  tango_name: id09/tetramm1/1  # (or id09/keithley/pd3)
  counters:
    - counter_name: pd1
      mode: STATS
    - counter_name: pd2
      mode: STATS
    - counter_name: pd3
      mode: STATS
    - counter_name: pd4
      mode: STATS

"""

from bliss.common.protocols import CounterContainer
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.common.tango import DeviceProxy

from id09.core.tetramm import TetrAMM


class PindiodeCounterContainer(CounterContainer):

    """
    This is a BLISS CounterContainer: it is an intermediate
    object that allows you to define other attributes and methods besides
    those necessary for counting.
    """

    def __init__(self, config):

        # read parameters from YAML
        self._config = config
        self._name = config.get("name")
        # # read 'inout' and 'material' parameters
        # self._inout = config.get("inout", None)
        # self.material = config.get("material", None)
        # self.thickness = config.get("thickness", None)
        # self.density = config.get("density", None)
        # self.diameter = config.get("diameter", None)
        # self.css = config.get("css", None)
        # if self.thickness is not None:
        #     self.thickness = float(self.thickness)
        # if self.density is not None:
        #     self.density = float(self.density)
        # if self.diameter is not None:
        #     self.diameter = float(self.diameter)
        # if self.css is not None:
        #     self.css = float(self.css)
        # read picoammeter parameters
        self._picoammeter = config.get("picoammeter", None)
        self._channel = config.get("channel")
        tango_name = config.get("tango_name", None)

        if tango_name is not None:
            self.picoammeter = DeviceProxy(tango_name)
            self.picoammeter.set_timeout_millis(5000)
        else:
            # assume direct connection (DeviceServer is bypassed)
            url = config.get("url")
            if self._picoammeter == 'tetramm':
                port = int(config.get("port"))
                self.picoammeter = TetrAMM(url=url, port=port)

        # create BLISS Counter Controller
        self._cc = PindiodeCounterController(self.name, self.picoammeter)
        self._cc.max_sampling_frequency = config.get("max_sampling_freq", 10)
        # TBD: add previous line also for pd0

        # create BLISS Sampling Counter(s)
        for conf in config.get("counters"):
            name = conf["counter_name"].strip()
            mode = conf.get("mode", default="STATS")
            unit = conf.get("unit", default="A")
            # channel = conf.get("channel")
            self._cc.create_counter(SamplingCounter, name, mode=mode,
                                    unit=unit)

    def __info__(self):
        info_list = []
        info_list.append("Si PIN diodes counters container")
        model, firmware = self.picoammeter.get_info()
        info_list.append(model+", firmware "+firmware)
        return "\n".join(info_list)

    @property
    def name(self):
        return self._name

    @property
    def counters(self):
        """Standard counter namespace."""
        return self._cc.counters

    def apply_config(self):
        """Ad hoc apply_config() for updating the object in case YAML
           is changed when the session is already open."""
        self._config.reload()
        self.__init__(self._config)

    # methods from the BLISS independent part are exposed below:

    def get_range(self):
        return self.picoammeter.get_range()

    def get_all_ranges(self):
        return self.picoammeter.get_all_ranges()

    def set_range(self, value):
        self.picoammeter.set_range(value)


class PindiodeCounterController(SamplingCounterController):
    """
    BLISS Counter Controller standard definition.

    It requires as a minimum that the read() and read_all() methods
    are defined.

    """

    def __init__(self, name, core):
        super().__init__(name)
        self.core = core

    def read_all(self, *counters):
        values = []
        counters_name = [val.name for val in self.counters]

        # number of acquisition fixed at 100 for the moment
        # data transfer rate is fixed (hard coded) at 1 kHz
        cur_values = self.core.get_current_multi(100)  # flat list from DS
        cur_values = cur_values.reshape(4, 100)
        cur_values = cur_values.mean(axis=1)

        for cnt in counters:
            idx = counters_name.index(cnt.name)
            values.append(cur_values[idx])

        return values

    def read(self, counter):
        print("Looks like this is never called.")
        counters_name = [val.name for val in self.core._cc.counters]
        idx = counters_name.index(counter)
        out = self.core.get_current()[idx]
        return out
