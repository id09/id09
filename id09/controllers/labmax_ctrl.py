# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Control of Coherent LabMax laser power meter


yml configuration example:

- class: LabMax
  plugin: generic
  package: id09.controllers.labmax
  name: labmax
  serial:
    url: ser2net://lid093:28000/dev/ttyRP16

  counters:
    - counter_name: laspw
"""

from bliss.common.protocols import CounterContainer
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController

from id09.core.labmax import Labmax


class LabmaxCounterContainer(CounterContainer):

    """This is a BLISS CounterContainer: it is an intermediate
       object that allows you to define other attributes and methods besides
       those necessary for counting."""

    def __init__(self, config):

        # read parameters from YAML
        self._name = config.get("name")
        self._config = config
        host = config.get("host")
        port = config.get("port")
        sleep = config.get("sleep", default=0.1)
        verbose = config.get("verbose", default=True)

        # create BLISS independent part
        self.core = Labmax(host=host, port=port, sleep=sleep,
                           verbose=verbose)

        # create BLISS Counter Controller
        self._cc = LabmaxCounterController(self.name, self.core)

        # create BLISS Sampling Counter(s)
        for conf in config.get("counters"):
            name = conf["counter_name"].strip()
            mode = conf.get("mode", "SINGLE")
            self._cc.create_counter(SamplingCounter, name, mode=mode)

    def __info__(self):
        info_list = []
        info_list.append("Coherent Labmax TOP powermeter")
        return "\n".join(info_list)

    @property
    def name(self):
        return self._name

    @property
    def counters(self):
        """Standard counter namespace."""
        return self._cc.counters

    def apply_config(self):
        """Ad hoc apply_config() for updating the object in case YAML
           is changed when the session is already open."""
        self._config.reload()
        self.__init__(self._config)

    # methods from the BLISS independent part are exposed below:

    def get_wavelength(self):
        return self.core.get_wavelength()


class LabmaxCounterController(SamplingCounterController):

    """This is the BLISS Counter Controller standar definition.
       It requires as a minimum that the read() and read_all() methods
       are defined.
    """

    def __init__(self, name, core):
        super().__init__(name)
        self.core = core

    def read_all(self, *counters):
        values = []
        for cnt in counters:
            # TO DO: solve the '0.2' issue!
            values.append(self.core.get_power(0.2))
        return values

    def read(self, counter):
        # TO DO: solve the '0.2' issue!
        return self.core.get_power(0.2)
