# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Control of PIN diodes

yml configuration example:

- class: DiodeCounterContainer
  plugin: generic
  package: id09.controllers.diode_ctrl
  name: pd3
  url: "enet://gpibid9b.esrf.fr"
  pad: 3
  tango_name: id09/keithley/pd3
  counters:
    - counter_name: cur
      mode: SINGLE
  inout: $inout_pd3
  material: Si
  thickness: 500e-6
  cover_material: kapton
  cover_thickness: 70e-6
  diameter: 20e-3
  css: 56.5

"""

from bliss.common.protocols import CounterContainer
from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.common.tango import DeviceProxy

from id09.core.keithley import Keithley


class DiodeCounterContainer(CounterContainer):

    """
    This is a BLISS CounterContainer: it is an intermediate
    object that allows you to define other attributes and methods besides
    those necessary for counting.
    """

    def __init__(self, config):

        # read parameters from YAML
        self._name = config.get("name")
        self._config = config
        tango_name = config.get("tango_name", None)
        if tango_name is not None:
            self.keithley = DeviceProxy(tango_name)
            self.keithley.set_timeout_millis(5000)
            #uncomment if cpptango>=9.3.5
            #self.keithley.unfreeze_dynamic_interface()
        else:
            url = config.get("url")
            pad = config.get("pad")
            self.keithley = Keithley(url=url, pad=pad)

        self._inout = config.get("inout", None)
        self.material = config.get("material", None)
        self.thickness = config.get("thickness", None)
        self.density = config.get("density", None)
        self.diameter = config.get("diameter", None)
        self.css = config.get("css", None)
        if self.thickness is not None:
            self.thickness = float(self.thickness)
        if self.density is not None:
            self.density = float(self.density)
        if self.diameter is not None:
            self.diameter = float(self.diameter)
        if self.css is not None:
            self.css = float(self.css)

        # create BLISS Counter Controller
        self._cc = DiodeCounterController(self.name, self.keithley)

        # create BLISS Sampling Counter(s)
        for conf in config.get("counters"):
            name = conf["counter_name"].strip()
            mode = conf.get("mode", default="STATS")
            self._cc.create_counter(SamplingCounter, name, mode=mode)

    def __info__(self):
        info_list = []
        info_list.append("sensor: Si PIN diode")
        if isinstance(self.keithley, DeviceProxy):
            info_list.append("controller: " + self.keithley.get_info())
        else:
            info_list.append("controller: " + self.keithley.__info__())
        if self._inout is not None:
            info_list.append("position: " + self._inout.position)
        return "\n".join(info_list)

    @property
    def name(self):
        return self._name

    @property
    def counters(self):
        """Standard counter namespace."""
        return self._cc.counters

    def apply_config(self):
        """Ad hoc apply_config() for updating the object in case YAML
           is changed when the session is already open."""
        self._config.reload()
        self.__init__(self._config)

    # methods from the BLISS independent part are exposed below:

    def get_range(self):
        return self.keithley.get_range()

    def get_all_ranges(self):
        return self.keithley.get_all_ranges()

    def set_range(self, cur_range):
        self.keithley.set_range(cur_range)

    def set_range_auto(self):
        self.keithley.set_range_auto()

    def set_range_manual(self):
        self.keithley.set_range_manual()

    def IN(self):
        if hasattr(self._inout, 'IN'):
            self._inout.IN()
        # TBD: not OK for multiple IN position (e.g. pd0)

    def OUT(self):
        self._inout.OUT()

    def get_position(self):
        if self._inout is not None:
            return self._inout.position
        else:
            return None


class DiodeCounterController(SamplingCounterController):

    """This is the BLISS Counter Controller standard definition.
       It requires as a minimum that the read() and read_all() methods
       are defined.
    """

    def __init__(self, name, core):
        super().__init__(name)
        self.core = core

    def read_all(self, *counters):
        values = []
        for cnt in counters:
            if isinstance(self.core, DeviceProxy):
                values.append(self.core.read_now())
            else:
                values.append(self.core.get_current())
        return values

    def read(self, counter):
        if isinstance(self.core, DeviceProxy):
            return self.core.read_now()
        else:
            return self.core.get_current()
