# -*- coding: utf-8 -*-
"""Class for handling ID09 objects with multiple positions."""

import numpy as np
import functools
from tabulate import tabulate

from bliss.shell.standard._motion import move
from bliss import config
from bliss import setup_globals as bliss_globals

config.get_sessions_list()
config = config.static.get_config()


def get_from_bliss(name):
    if bliss_globals is not None and hasattr(bliss_globals, name):
        return getattr(bliss_globals, name)
    if config is not None:
        try:
            obj = config.get(name)
        except RuntimeError:
            obj = None
        return obj


def convert_str_list_with_none(str_list):
    """Convert list of strings including 'None' to list of floats."""
    float_list_with_none = []
    for value in str_list:
        if value.lower().strip() == "none":
            float_list_with_none.append(None)
        try:
            float_list_with_none.append(float(value))
        except ValueError:
            pass
    return float_list_with_none


class MultiplePositions:

    def __init__(self, config, name=None, axes=None, directions=None,
                 positions=None, css_distance=None, safety_shutters=None,
                 simultaneous=True):

        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the
            session.
            If 'config' is not None (default), all other parameters are
            taken from the YAML file.`
            If 'config' is None, 'atts' and 'labels' cannot be None.
        name : str or None
            Object name.
        axes : list of motors
        directions: list of directions
        positions : list or None
            List of dictionary defining the different possible predefined
            positions of the obj.
        css_distance : float or None.
            Distance of the object from the center of the straight
            session (CSS).
        safety_shutters : list or None
            List of safety shutter to close before moving (they will be
            reopened once done if initially open).
        simultaneous : bool
            If True (default), all motors are moved simultaneously.
            If False, one motor moves at a time.

        """

        self._config = config
        self._labels = []

        if config is not None:
            self._name = config.get("name")
            self._axes = config.get('axes')
            self._directions = config.get('directions', None)
            self._positions = config.get('positions', None)
            self._css_distance = config.get("css_distance", None)
            self._safety_shutters = config.get("safety_shutters", None)
            self._simultaneous = config.get("simultaneous", True)
        else:
            self._name = name
            self._axes = axes
            self._directions = directions
            self._positions = positions
            self._css_distance = css_distance
            self._safety_shutters = safety_shutters
            self._simultaneous = simultaneous

        self._axes = [get_from_bliss(a.strip()) for a in self._axes.split(",")]
        if self._directions is not None:
            self._directions = self._directions.split(",")
            self._directions = [d.strip() for d in self._directions]

        if self._positions is None:
            self._positions = []

        self._update_info()

    def _update_info(self):
        """Update the following attributes:
           - _labels : list
           - _naxes : int
           - _as_dials : dict
           - _destinations : dict
           - _tolerances : dict
           - _descriptions : dict
           Aa method named after each label is also created.
        """
        self._labels = [pos.get("label") for pos in self._positions]
        self._naxes = len(self._axes)
        self._as_dials = dict()
        self._destinations = dict()
        self._tolerances = dict()
        self._descriptions = dict()
        for position in self._positions:
            label = position.get("label")
            self._descriptions[label] = position.get("description", "")
            if "as_dial" not in position:
                as_dials = [False]*self._naxes
            elif isinstance(position.get("as_dial"), bool):
                as_dials = [position.get("as_dial")]*self._naxes
            else:
                as_dials = list(map(bool, position.get("as_dial").split(",")))
            self._as_dials[label] = as_dials
            if "tolerance" not in position:
                tolerance = self._config.get("tolerance")
            else:
                tolerance = position.get("tolerance")
            if isinstance(tolerance, (int, float)):
                tolerances = [tolerance]*self._naxes
            else:
                tolerances = position.get("tolerance").split(",")
                tolerances = convert_str_list_with_none(tolerances)
                if len(tolerances) == 1:
                    tolerances = [tolerances]*self._naxes
            self._tolerances[label] = tolerances
            destinations = position.get("destination").split(",")
            destinations = convert_str_list_with_none(destinations)
            self._destinations[label] = destinations
        for label in self._labels:
            self._add_pos_move_method(label)

    def _add_pos_move_method(self, pos):
        """Add a moving method named after a position label."""
        def label_move_func(label):
            self.move(label)
        setattr(self, pos, functools.partial(label_move_func, pos))

    def _add_position(self, label, destination, description="",
                      tolerance=None, **kw):
        """Add a predifined position to the MultiplePosition obj."""
        newpos = dict(label=label, destination=destination,
                      description=description)
        newpos.update(kw)
        self._positions.append(newpos)
        self._update_info()

    def __str__(self):
        return "MultiplePositionsAxis obj"

    def __info__(self, position=None):

        # HEADER
        table = [("", "LABEL", "DESCRIPTION", "MOTOR POSITION(S)")]

        # PRE-DEFINED POSITIONS
        for label in self._labels:
            motstr = ""
            descr = self._descriptions[label]
            mystr = "* " if label == self.position else ""
            for iaxis, axis in enumerate(self._axes):
                if self._destinations[label][iaxis] is None:
                    continue
                motstr += "%s: %3.3f  (± %2.3f)" % (
                    axis.name,
                    self._destinations[label][iaxis],
                    self._tolerances[label][iaxis],
                )
                as_dial = self._as_dials[label][iaxis]
                if as_dial:
                    motstr += " (dial)"
                motstr += "\n"

            table.append((mystr, label, descr, motstr))

        # EMPTY LINE
        table.append((" ", "", "", ""))

        # CURRENT POSITION
        table.append(("", "", "", "CURRENT POSITION(S)"))
        for axis in self._axes:
            n = axis.name
            p = axis.position
            d = axis.dial
            line = ("", "", "", f"{n} {p:3.4f} (dial: {d:3.4f})")
            table.append(line)

        pos_str = tabulate(tuple(table), numalign="right",
                           tablefmt="plain")

        return f"{pos_str}"

    @property
    def position(self):
        """MultiplePositionsAxis position (label)."""
        pos_label = None
        for label in self._labels:
            within_tolerance = []
            for iaxis, axis in enumerate(self._axes):
                dest = self._destinations[label][iaxis]
                tol = self._tolerances[label][iaxis]
                as_dial = self._as_dials[label][iaxis]
                pos = axis.dial if as_dial else axis.position
                if dest is None:
                    continue
                else:
                    within_tolerance.append(np.isclose(pos, dest, atol=tol))
            if all(within_tolerance):
                pos_label = label
        return pos_label

    def move(self, label, wait=True):
        """Move to one of the predefined positions."""

        if self.position == label:
            print("Already in position: %s\n" % label)
            return

        if self._safety_shutters is not None:
            init_status = [shut.is_open for shut in self._safety_shutters]
            for shut in self._safety_shutters:
                if shut.is_open:
                    shut.close()

        user_move_list = []

        for iaxis, axis in enumerate(self._axes):
        
            destination = self._destinations[label][iaxis]
            
            if destination is None:
                continue
            
            if self._as_dials[label][iaxis]:
                destination = axis.dial2user(destination)
            
            if self._simultaneous:
                user_move_list.append(axis)
                user_move_list.append(destination)
            else:
                axis.move(destination, wait=True)

        if self._simultaneous:
            move(*user_move_list, relative=False, wait=wait)

        if self._safety_shutters is not None:
            for shut, status in zip(self._safety_shutters, init_status):
                if status:
                    shut.open()
