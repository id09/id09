# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# extra methods (not related to Axes handling):
#  - get_burst_npulses

from bliss.common.types import IterableNamespace
from bliss.common.utils import autocomplete_property
from bliss import global_map
from bliss.controllers.bliss_controller import BlissController
from bliss.common.standard import SoftAxis

from id09.core.city import City


class CityController(BlissController):

    def __init__(self, config):

        BlissController.__init__(self, config)

        self._name = config.get("name")
        self.url = config.get("url")
        self.port = config.get("port")

        self._soft_axes = {}

        self.lxt_laser_off_position = config.get(
                "lxt_laser_off_position", 9999)

        self.core = City(
                url=self.url, port=self.port,
                lxt_laser_off_position=self.lxt_laser_off_position)

        global_map.register(self, parents_list=["controllers"])

        self._create_soft_axes()
        print(self.__info__())

    def __info__(self):
        info = "CITY Bliss controller, "
        info += "IP=%s" % self.core.address[0]
        return info

    def _load_config(self):
        pass

    @autocomplete_property
    def axes(self):
        return IterableNamespace(**self._soft_axes)

    def as_bliss_soft_axis(self, name, export_to_session=False):

        def read_user():
            return getattr(self.core, name).read_delay()

        def move_user(delay):
            getattr(self.core, name).move_delay(delay)

        def read_dial():
            return getattr(self.core, name).read_delay(as_dial=True)

        def move_dial(delay):
            getattr(self.core, name).move_delay(delay, as_dial=True)

        delay = getattr(self.core, name)
        if delay.has_fine_delays:
            tolerance = 5e-12
        else:
            tolerance = 1e-9

        bliss_axis = SoftAxis(
            name,
            self,
            position=read_user,
            move=move_user,
            tolerance=tolerance,
            unit="s",
            export_to_session=export_to_session,
        )

        original_set_position = bliss_axis._Axis__do_set_position

        def my_set(delay=None, offset=None):
            if offset is not None:
                delay = read_dial() + offset
            getattr(self.core, name).set_delay(delay)
            original_set_position(new_pos=delay)

        bliss_axis._Axis__do_set_position = my_set

        # Bliss does not keep offset in database (?). Forcing from n354.py
        bliss_axis.offset = -getattr(self.core, name).dial_offset

        return bliss_axis

    def _create_soft_axes(self):

        def you_should_not_do_it(pos=None, offset=None):
            raise ValueError("please reset individual motors")

        for name in self.core.delay_channels:
            self._soft_axes[name] = self.as_bliss_soft_axis(
                name, export_to_session=False
            )
            self._soft_axes[name]._Axis__do_set_position = you_should_not_do_it

        self._soft_axes["lxt"] = SoftAxis(
            "lxt",
            self.core,
            position="lxt_read",
            move="lxt_move",
            unit="s",
            tolerance=5e-12,
            export_to_session=False,
        )

        self._soft_axes["lxt"]._Axis__do_set_position = you_should_not_do_it
