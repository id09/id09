# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# extra methods (not related to Axes handling):
#  - get_burst_npulses

import numpy as np

from bliss.common.types import IterableNamespace
from bliss.common.utils import autocomplete_property
from bliss import global_map
from bliss.controllers.bliss_controller import BlissController
from bliss.common.standard import SoftAxis

from id09.core.n354 import N354


class N354Controller(BlissController):

    """BLISS controller for N354 board.

    YML configuration
    -----------------
    - controller:
      plugin: generic
      package: id09.controllers.n354_ctrl
      class: N354Controller
      name: n354
      ip: id09n354a
      port: 5001
      lxt_laser_off_position: 9999.9999

    """

    def __init__(self, config):

        BlissController.__init__(self, config)

        # self._config = config
        # delattr(self, "config")  # ML 12/01/2023: to remove n354.config...
        self._name = config.get("name")
        self.ip = config.get("ip")
        self.port = config.get("port")

        self._soft_axes = {}

        self.lxt_laser_off_position = config.get(
                "lxt_laser_off_position", 9999.9999)

        self.core = N354(ip=self.ip, port=self.port,
                         lxt_laser_off_position=self.lxt_laser_off_position)

        global_map.register(self, parents_list=["controllers"])

        self._create_soft_axes()
        print(self.__info__())

    def __info__(self):
        info = "N354 Bliss controller, "
        info += "IP=%s" % self.core.address[0]
        return info

    def _load_config(self):
        pass

    @autocomplete_property
    def axes(self):
        return IterableNamespace(**self._soft_axes)

    def as_bliss_soft_axis(self, name, export_to_session=False):

        def read_user():
            return getattr(self.core, name).read_delay()

        def move_user(delay):
            getattr(self.core, name).move_delay(delay)

        def read_dial():
            return getattr(self.core, name).read_delay(as_dial=True)

        def move_dial(delay):
            getattr(self.core, name).move_delay(delay, as_dial=True)

        delay = getattr(self.core, name)
        if name == "oscillator" or delay.has_fine_delays:
            tolerance = 5e-12
            display_digits = 12
        else:
            tolerance = 1e-9
            display_digits = 9

        bliss_axis = SoftAxis(
            name,
            self,
            position=read_user,
            move=move_user,
            tolerance=tolerance,
            unit="s",
            export_to_session=export_to_session,
            display_digits=display_digits
        )

        original_set_position = bliss_axis._Axis__do_set_position

        def my_set(delay=None, offset=None):
            if offset is not None:
                delay = read_dial() + offset
            getattr(self.core, name).set_delay(delay)
            original_set_position(new_pos=delay)

        bliss_axis._Axis__do_set_position = my_set

        # Bliss does not keep offset in database (?). Forcing from n354.py
        bliss_axis.offset = -getattr(self.core, name).dial_offset

        return bliss_axis

    def _create_soft_axes(self):

        def you_should_not_do_it(pos=None, offset=None):
            raise ValueError("please reset individual motors")

        # create first the delay axes
        for name in self.core.delay_channels_and_osc:
            self._soft_axes[name] = self.as_bliss_soft_axis(
                name, export_to_session=False
            )
            self._soft_axes[name]._Axis__do_set_position = you_should_not_do_it

        # create now the lxt axes
        self._soft_axes["lxt_ps"] = SoftAxis(
            "lxt_ps",
            self.core,
            position="lxt_read_ps",
            move="lxt_move_ps",
            unit="s",
            tolerance=5e-12,
            export_to_session=False,
            display_digits=12
        )

        self._soft_axes["lxt_ns"] = SoftAxis(
            "lxt_ns",
            self.core,
            position="lxt_read_ns",
            move="lxt_move_ns",
            unit="s",
            tolerance=5e-12,
            export_to_session=False,
            display_digits=12
        )
        self._soft_axes["lxt_ps"]._Axis__do_set_position = you_should_not_do_it
        self._soft_axes["lxt_ns"]._Axis__do_set_position = you_should_not_do_it

    def get_burst_npulses(self, count_time):
        """
        Get N354 number of pulses in the burst corresponding to count_time.

        This is needed for preparing "standard" scans in BLISS.

        Returns
        -------
        npulses : int
            Number of pulses in the N354 burst

        """
        xshut_freq = self.core.xshut.read_frequency()
        npulses = np.round(xshut_freq*count_time, 0)
        npulses = int(npulses)
        return npulses

    def burst_off(self):
        self.core.burst_off()

    def burst_set_count(self, npulses):
        self.core.burst_set_count(npulses)
