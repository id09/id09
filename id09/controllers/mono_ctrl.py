

from bliss.controllers.monochromator import Monochromator
from bliss.shell.standard import umv

import numpy as np

class ID09Mono(Monochromator):

    """
    Load Configuration
    """
    def _load_config(self):
        super()._load_config()

        self._ver_target = {}
        self._hor_target = {}
        self._iotz_target = {}
        self._xtal_bragg_offset = {}

        self._ver_motor = self.config.get("ver_motor")
        self._hor_motor = self.config.get("hor_motor")
        self._iotz_motor = self.config.get("iotz_motor")
        self._ver_tolerance = self.config.get("ver_tolerance")
        self._hor_tolerance = self.config.get("hor_tolerance")
        self._ver_target = self._xtals.get_xtals_config("ver_target")
        self._hor_target = self._xtals.get_xtals_config("hor_target")
        self._iotz_target = self._xtals.get_xtals_config("iotz_target")
        #_bragg_offset is reserved for use of setE(), so here use _xtal_bragg_offset
        # L.C 24/05/24
        self._xtal_bragg_offset = self._xtals.get_xtals_config("bragg_offset")

    def _xtal_is_in(self, xtal):
        ver_pos = self._ver_motor.position
        hor_pos = self._hor_motor.position

        in_pos = True
        if xtal in self._hor_target.keys():
            if not np.isclose(hor_pos,  self._hor_target[xtal],atol=self._hor_tolerance):
                in_pos = False
        if xtal in self._ver_target.keys():
            if not np.isclose(ver_pos,  self._ver_target[xtal],atol=self._ver_tolerance):
                in_pos = False

        return in_pos

    def _xtal_change(self, xtal):
        mv_list = []
        if xtal in self._hor_target.keys():
            mv_list.append(self._hor_motor)
            mv_list.append(self._hor_target[xtal])
        if xtal in self._ver_target.keys():
            mv_list.append(self._ver_motor)
            mv_list.append(self._ver_target[xtal])
        if xtal in self._iotz_target.keys():
            mv_list.append(self._iotz_motor)
            mv_list.append(self._iotz_target[xtal])
            
        if xtal in self._xtal_bragg_offset.keys():
            self._motors["bragg"].offset = self._xtal_bragg_offset[xtal]
        
        if len(mv_list) > 0:
            umv(*mv_list)
