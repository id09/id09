# -*- coding: utf-8 -*-
"""Description and properties of ID09 choppers."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "01/11/2022"
__version__ = "0.0.1"


import numpy as np

from bliss.shell.standard._motion import move
from id09.controllers.multiplepositions import MultiplePositions


class Chopper(MultiplePositions):

    def __init__(self, config, name=None, positions=None,
                 css_distance=None, safety_shutters=None,
                 ntunnels=1, tunnel_height=None, tunnel_length=None,
                 tunnel_width=None, rotation_frequency=None, shape=None,
                 radius=None, material=None):

        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the
            session.
            If 'config' is not None (default), all other parameters are
            taken from the YAML file.`
            If 'config' is None, 'atts' and 'labels' cannot be None.
        name : str or None
            Object name.
        positions : list or None
            List of dictionary defining the different possible predefined
            positions of the obj.
        css_distance : float or None.
            Distance of the object from the center of the straight
            session (CSS).
        safety_shutters : list or None.
            List of safety shutter to close before moving (they will be
            reopened once done if initially open).

        """

        super().__init__(config, name=name, positions=positions,
                         css_distance=css_distance,
                         safety_shutters=safety_shutters)

        if self._config is not None:
            self._ntunnels = self._config.get("ntunnels", 1)
            self._tunnel_height = self._config.get("tunnel_height", None)
            self._tunnel_width = self._config.get("tunnel_width", None)
            self._tunnel_length = self._config.get("tunnel_length", None)
            self._rotation_frequency = self._config.get("rotation_frequency",
                                                        None)
            self._shape = self._config.get("shape", None)
            self._radius = self._config.get("radius", None)
            self._material = self._config.get("material", None)
        else:
            self._ntunnels = ntunnels
            self._tunnel_height = tunnel_height
            self._tunnel_width = tunnel_width
            self._rotation_frequency = rotation_frequency
            self._shape = shape
            self._radius = radius
            self._material = material

    def pulse_duration(self, filling_mode='7/8+1'):
        pass


class HighSpeedChopper(Chopper):

    def __init__(self, config, name=None, positions=None,
                 css_distance=None, safety_shutters=None,
                 ntunnels=1, tunnel_height=None, tunnel_length=None,
                 rotation_frequency=None, step_height=None,
                 step_width=None, slope_chopz_vs_duration=-0.25,
                 intercept_chopz_vs_duration=5.035):

        """

        Parameters
        ----------
        config : bliss.config.static.Config or None
            Proxy to the bliss.config.static.Config associated to the session.
            If 'config' is not None (default), all other parameters are taken
            from the YAML file.`
            If 'config' is None, 'atts' and 'labels' cannot be None.

        """

        super().__init__(config, name=name, positions=positions,
                         css_distance=css_distance,
                         safety_shutters=safety_shutters,
                         ntunnels=ntunnels, tunnel_height=tunnel_height,
                         tunnel_length=tunnel_length,
                         rotation_frequency=rotation_frequency)

        if self._config is not None:
            self._step_height = self._config.get("step_height", None)
            self._step_width = self._config.get("step_width", None)
        else:
            self._step_height = step_height
            self._step_width = step_width

        self.step_fit = (slope_chopz_vs_duration, intercept_chopz_vs_duration)

        delattr(self, "STEP")
        setattr(self, "STEP", self._step)
        delattr(self, "OUT")
        setattr(self, "OUT", self._out)

    def _get_tunnel_opening_window(self):
        pass

    def _get_step_opening_window(self, zpos):
        # opening_window = (zpos - 5.035)/(-0.25)
        opening_window = (zpos - self.step_fit[1]) / self.step_fit[0]
        opening_window = np.round(opening_window, 3)
        opening_window *= 1e-6
        return opening_window

    def _step(self, opening_window=None):

        index_h = self._directions.index("h")
        index_v = self._directions.index("v")

        def move_chopper(ypos, zpos):
            cur_pulse_t = self.pulse_duration
            new_pulse_t = self._get_step_opening_window(zpos)
            msg = f"Moving to chopper step {new_pulse_t*1e6:.0f}us"
            msg += f" (chopy={ypos:.3f}mm, chopz={zpos:.3f}mm).\n"
            msg += "WARNING: intensity may increase by a factor 1e3 to 1e4."
            if cur_pulse_t is None or new_pulse_t is None:
                print(msg)
                proceed = input("Proceed (y/N)? ")
            elif new_pulse_t > cur_pulse_t:
                print(msg)
                proceed = input("Proceed (y/N)? ")
            else:
                proceed = 'y'
            if proceed.lower() in ['y', 'yes']:
                axis_h = self._axes[index_h]
                axis_v = self._axes[index_v]
                move(axis_h, ypos, axis_v, zpos, wait=True)

        if opening_window is None:
            ypos = self._destinations["STEP"][index_h]
            zpos = self._axes[index_v].position
            move_chopper(ypos, zpos)
            return

        if isinstance(opening_window, str):
            opening_window = float(opening_window)

        if opening_window > 30e-6:
            print("ERROR: 'opening_window' is too high.")
            return

        if opening_window <= 0:
            print("ERROR: 'opening_window' must be positive.")
            return

        index_h = self._directions.index("h")
        ypos = self._destinations["STEP"][index_h]
        # zpos = 5.035-0.25*(pulse_duration/1e-6)
        zpos = self.step_fit[1] + self.step_fit[0]*(opening_window/1e-6)

        if move:
            move_chopper(ypos, zpos)
        else:
            return ypos, zpos

    def _out(self):

        index_h = self._directions.index("h")
        index_v = self._directions.index("v")
        ypos = self._destinations["OUT"][index_h]
        zpos = self._destinations["OUT"][index_v]

        msg = "Moving the chopper out of the beam!!!"
        msg += f" (chopy={ypos:.3f}mm, chopz={zpos:.3f}mm)\n"
        msg += "WARNING: intensity may increase by a factor 1e3 to 1e4."
        print(msg)
        proceed = input("Proceed (y/N)? ")
        if proceed.lower() in ['y', 'yes']:
            axis_h = self._axes[index_h]
            axis_v = self._axes[index_v]
            move(axis_h, ypos, axis_v, zpos, wait=True)

    @property
    def pulse_duration(self):
        if self.position == "TUNNEL":
            return 100e-12
        elif self.position == "STEP":
            index_v = self._directions.index("v")
            zpos = self._axes[index_v].position
            return self._get_step_opening_window(zpos)
