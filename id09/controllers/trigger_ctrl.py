# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import sleep
import numpy as np

from bliss import global_map
from bliss.common.logtools import log_exception
from bliss.common.session import DEFAULT_CHAIN

from id09.scans.presets import FastShutterPreset
from id09.colors import BOLD, ENDC


class TriggerController:
    """
    Takes care of:
    - switching the OPIOM2 multiplexer status
    - defining the default BLISS acquisition chain for scans (DEFAULT_CHAIN)
    - defining the acquisition chain master
    """

    _prepared_axes = {"gony": False, "hphi": False, "gonz": False}

    def __init__(self, name, config):
        self.name = name
        self.config = config

        self._mux = config.get("multiplexer")
        self._opiom = list(self._mux._boards.keys())[0]
        self._op = self._mux._boards[self._opiom]

        chain_n354 = config.get("chain_n354")
        chain_n354_single = config.get("chain_n354_single")
        chain_p201 = config.get("chain_p201")
        self._chain_config = {'n354': chain_n354['chain_config'],
                              'n354_single': chain_n354_single['chain_config'],
                              'p201': chain_p201['chain_config']}

        self._scan_master = None
        self._single_pulse = None
        self._use_lshut = None

        global_map.register(self, children_list=[self._mux])

    def __info__(self):
        info = "Scan master: %s\n" % self._scan_master
        if self._scan_master == 'soft':
            info += "Use lshut: %s\n" % self._use_lshut
        info += "Single pulse: %s\n" % self._single_pulse
        info += "\nMultiplexer: %s\n\n" % self._mux.name
        info += self._mux.__info__()
        return info

    def set_n354_as_master_for_xshut_and_camera(self):
        # self._mux.switch("trig_shutr_source", "n354")
        # self._mux.switch("trig_camera_source", "n354")
        self._mux.switch("shutr_trig_source", "n354")
        self._mux.switch("camera_trig_source", "n354")

    def set_n354_as_master_for_xshut(self):
        # self._mux.switch("trig_shutr_source", "n354")
        self._mux.switch("shutr_trig_source", "n354")

    def set_I1_to_get_gate(self):
        """
        Tell OPIOM2 that the N354-xshut signal on I1 will be a gate.
        The OPIOM2 prg will convert this signal in 2 short (50 us) pulses,
        one at the rising-edge and an other one at the falling-edge
        """
        # self._mux.switch("i1_shape", "gate")
        self._mux.switch("shutr_trig_shape", "gate")

    def set_I1_to_get_burst_of_pulse(self):
        """
        Tell OPIOM2 that the N354-xshut signal on I1 will be a burst of pulses.
        """
        # self._mux.switch("i1_shape", "pulse_x2")
        self._mux.switch("shutr_trig_shape", "pulse_x2")

    def set_p201_as_master_for_camera(self):
        # self._mux.switch("trig_camera_source", "p201")
        self._mux.switch("camera_trig_source", "p201")

    def gate_off_camera(self):
        # self._mux.switch("trig_camera_source", "cmd")
        # self._mux.switch("trig_camera_cmd", "off")
        self._mux.switch("camera_trig_source", "p201")
        self._mux.switch("camera_trig_active", "off")
        # self._op.comm("IM 0x20 0x20")

    def gate_on_camera(self):
        # self._mux.switch("trig_camera_source", "cmd")
        # self._mux.switch("trig_camera_cmd", "on")
        self._mux.switch("camera_trig_source", "cmd")
        self._mux.switch("camera_trig_active", "on")
        # self._op.comm("IM 0x0 0x20")

    def set_n354_as_trig_for_camera(self):
        # self._mux.switch("trig_camera_source", "n354")
        self._mux.switch("camera_trig_source", "n354")

    def trig_n354(self, duration=0.01):  # standard duration = 0.01 changed on Nov 3, 2024
        # self._mux.switch("trig_n354_cmd", "off")
        # self._mux.switch("trig_n354_cmd", "on")
        self._mux.switch("n354_trig_active", "off")
        self._mux.switch("n354_trig_active", "on")
        sleep(duration)
        # self._mux.switch("trig_n354_cmd", "off")
        self._mux.switch("n354_trig_active", "off")
        # self._op.comm("IM 0x0 0x80")
        # self._op.comm("IM 0x80 0x80")
        # sleep(duration)
        # self._op.comm("IM 0x0 0x80")

    def prepare_sample_axis_tracking(self, axis, start, stop, intervals,
                                     backnforth=True):
        """Upload the tracking position list to the icepap axis."""

        if axis.name not in self._prepared_axes.keys():
            msg = "Sorry, supported axes for sample tracking: "
            msg += "{list(self._prepared_axes.keys())}"
            log_exception(self, msg)
            raise Exception(msg)

        axis.activate_tracking(False)

        if backnforth:
            l1 = np.linspace(start, stop, intervals, endpoint=False)
            l2 = np.linspace(stop, start, intervals, endpoint=False)
            axis.set_tracking_positions(np.concatenate([l1, l2]), cyclic=True)
        else:
            l1 = np.linspace(start, stop, intervals+1)
            axis.set_tracking_positions(l1, cyclic=True)

        self._prepared_axes[axis.name] = True

    def start_sample_axis_tracking(self, axis, waittime=0, velocity=None,
                                   acceleration=None):
        """
        Start triggering 'axis' motion with N354-xshut burst of pulses.

        The OPIOM2-I1 receives the input burst and converts it into a burst
        with 1 ms long pulses that can be delayed by 'waittime'.
        """
        self._op.comm("CNT 2 CLKB PULSE 1000 %d 0" % (waittime*1000))

        if velocity is not None:
            axis.velocity = velocity
        if acceleration is not None:
            axis.acceleration = acceleration

        if not self._prepared_axes[axis.name]:
            msg = "Axis {axis.name} is not prepared for tracking, "
            msg += "call first prepare_sample_axis_tracking()"
            raise Exception(msg)

        # Activate the motor tracking mode with external trigger (INPOS)
        axis.activate_tracking(True, "INPOS")

        self._op.comm("CNT 2 START")

    def stop_sample_axis_tracking(self, axis):
        """Stop the clock on output 2,3 and 4"""
        self._op.comm("CNT 2 STOP")

        while axis.is_moving:
            continue

        axis.activate_tracking(False)
        axis.velocity = axis.config.get("velocity")
        axis.acceleration = axis.config.get("acceleration")

    def set_scan_master(self, chain_name="soft", single_pulse=False,
                        use_lshut=False, verbose=True):

        msg = "The trigger master for scans is now: "
        self._single_pulse = single_pulse
        self._use_lshut = use_lshut

        if chain_name == "n354":
            if not single_pulse:
                DEFAULT_CHAIN.set_settings(self._chain_config['n354'])
                self._single_pulse = False
            else:
                DEFAULT_CHAIN.set_settings(self._chain_config['n354_single'])
            msg += BOLD + chain_name + ENDC + " (FPGA Board)"
            DEFAULT_CHAIN.remove_preset(name="count_preset")
            self._scan_master = "n354"
        elif chain_name == "p201":
            DEFAULT_CHAIN.set_settings(self._chain_config['p201'])
            msg += BOLD + chain_name + ENDC + " (Counting Card)"
            self._scan_master = "p201"
        elif chain_name == "soft":
            DEFAULT_CHAIN.set_settings([])  # go back to default settings
            msg += BOLD + chain_name + ENDC + " (Software Timer)"
            count_preset = FastShutterPreset(chain_name, use_lshut=use_lshut)
            DEFAULT_CHAIN.add_preset(count_preset, "count_preset")
            self._scan_master = "soft"
        else:
            msg = "This scan master,'%s' name is not " % chain_name
            msg += "supported. Use 'soft', 'p201' or 'n354' instead."

        if verbose:
            print(msg)

    def soft_mode(self, single_pulse=False, use_lshut=False, verbose=False):
        self.set_scan_master(chain_name="soft", single_pulse=single_pulse,
                             use_lshut=use_lshut, verbose=verbose)

    def n354_mode(self, single_pulse=False, use_lshut=False, verbose=False):
        self.set_scan_master(chain_name="n354", single_pulse=single_pulse,
                             use_lshut=use_lshut, verbose=verbose)
