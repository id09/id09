# The code below is a copy of what is available in me1518.py


class IrradiationOpiom:
    """
    The OPIOM3 is programmed so that, if a TTL pulse (>1.2V, >10 us) is
    sent to OPIOM3-I4, the OM register changes from '0x00' (0) to '0x08' (1).

    A reset of the IM register has to be done.
    """
    
    def __init__(self, config, opiom=None, sleep=0.1):
        
        if config is not None:
            self.opiom = config.get("opiom")
            self.sleep = config.get("sleep", sleep)
        else:
            self.opiom = opiom
            self.sleep = sleep

    def reset(self):
        # reset on bit 5 (pos. 4)
        self.opiom.comm("#IM 0x10 0x10")
        self.opiom.comm("#IM 0x0 0x10")

    def event(self):
        """0 = no event; 1 = pulse detected at OPIOM3-I4"""
        # event latched on OM4 (pos3) from input 4
        val = self.opiom.comm("?OM")
        val = (int(val, 16) >> 3) & 0x1
        return val
