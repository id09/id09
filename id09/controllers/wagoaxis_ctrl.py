# -*- coding: utf-8 -*-
"""Define Axis associated to wago pneumatic IN/OUT."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "24/05/2022"
__version__ = "0.0.1"


import numpy as np

from bliss.controllers.wago.wago import Wago


class WagoAxis:
    """

    Attributes
    ----------
    _wago_inverted : bool
       If True (default), the IN position of 'wago_motion' is inverted
       with respect to that of 'wago_status'.

    """

    def __init__(self, config, wago=None, wago_ch=None, wago_motion="att_ctl",
                 wago_status="att_sta", wago_inverted=True, tolerance=0.1):

        self._config = config

        if config is not None:

            self.wago = config.get("wago")
            self._wago_ctrl = config.get(self.wago)
            # ...

        else:

            if wago is None or wago_ch is None:
                raise ValueError("'wago' and 'wago_ch' cannot be None " +
                                 "if 'config' is None.")

            if not isinstance(wago, Wago):
                raise TypeError("'wago' must be a bliss Wago instance.")

            self.wago = wago.name
            self._wago_ctrl = wago
            self._wago_motion = wago_motion
            self._wago_status = wago_status
            self._wago_ch = wago_ch
            self._wago_inverted = wago_inverted
            self._tolerance = tolerance
            self.positions = (0, 1)

    def __str__(self):
        return "WagoAxis obj"

    def __info__(self):
        return "WagoAxis obj\n" + "position = %g" % self.position

    def _get_wago_status(self):
        """get positions of all wago axes (list)"""
        wago_status = self._wago_ctrl.get(self._wago_status)
        return wago_status

    @property
    def status(self):
        status = "?"
        if np.isclose(self.position, 1, atol=self._tolerance):
            status = "IN"
        elif np.isclose(self.position, 0, atol=self._tolerance):
            status = "OUT"
        return status

    @property
    def tolerance(self):
        return self._tolerance

    @property
    def position(self):
        # get positions of all wago axes (list)
        wago_status = self._get_wago_status()
        self._position = wago_status[self._wago_ch]
        return self._position

    @property
    def unit(self):
        return None

    def move(self, target_pos, wait=True):
        """Move axis."""

        positions = self.positions
        tolerance = self.tolerance

        if not any(np.isclose(target_pos, positions, atol=tolerance)):
            raise ValueError("'target_pos' must be one of ", positions)

        if not np.isclose(target_pos, self.position, atol=tolerance):

            target_config = self._wago_ctrl.get(self._wago_motion)

            # line below is needed to avoid numpy.bool as an input for pytango
            target_config = [bool(val) for val in target_config]

            ch_value = target_config[self._wago_ch]

            target_config[self._wago_ch] = int(not bool(ch_value))
            self._wago_ctrl.set(self._wago_motion, target_config)

    def wait_move(self):
        pass
