"""
Module where the time-resolved AcquisitionMaster is defined.

Currently, it is based on the use of the n354 fpga synchronization board.
It also makes use of the xshut and lshut objects.

classes
-------
- N354CounterController(SamplingCounterController)
- N354Counter(SamplingCounter)
- N354AcquisitionMaster(AcquisitionMaster)
- TimeResolvedAcquisition(BlissController)

"""

import gevent
from gevent import event, lock
import time

from bliss import global_map
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.counter import SamplingCounterController
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.common.utils import autocomplete_property

from bliss.scanning.chain import AcquisitionMaster
from id09.containers.shutters import LaserShutter


class N354CounterController(SamplingCounterController):

    def __init__(self, tr_acq):
        super().__init__(name=tr_acq.name, register_counters=False)
        self.tr_acq = tr_acq
        self.n354 = self.tr_acq.n354
        if self.tr_acq.city is not None:
            self.city = self.tr_acq.city
        else:
            self.city = None
        self.tags = ["xshut_freq"]

    def read_all(self, *counters):
        values = []
        for cnt in counters:
            if cnt.tag == "xshut_freq":
                values.append(self.n354.core.xshut.read_frequency())
        return values

    def get_acquisition_object(self, acq_params, ctrl_params,
                               parent_acq_params):

        return N354AcquisitionMaster(
            self, name=self.name, ctrl_params=ctrl_params, **acq_params)

    def get_default_chain_parameters(self, scan_params, acq_params):
        npoints = scan_params.get("npoints", 1)
        count_time = scan_params["count_time"]
        prepare_once = True
        start_once = True

        # Return required parameters
        params = {}
        params["npoints"] = npoints
        params["count_time"] = count_time
        params["prepare_once"] = prepare_once
        params["start_once"] = start_once

        return params


class N354Counter(SamplingCounter):

    def __init__(self, name, config, controller):

        self.tag = config["tag"]
        unit = config.get("unit", None)
        if self.tag not in controller.tags:
            raise RuntimeError("N354 {controller.device.name}: " +
                               "counter {name} tag {self.tag} is not "
                               " supported, only {controller.tags}")

        SamplingCounter.__init__(
            self, name, controller, mode=SamplingMode.SINGLE, unit=unit)


class N354AcquisitionMaster(AcquisitionMaster):
    """
    Acquisition device for N354.
    """

    def __init__(self, *devices, xshut=None, trigger_ctrl=None, name=None,
                 count_time=None, npoints=1, start_once=True,
                 prepare_once=True, ctrl_params=None):

        # ensure that ctrl-params have been completed
        # ctrl_params = self.init_ctrl_params(device, ctrl_params)

        # !!! warning: validate params requires a completed ctrl_params dict
        # self.acq_params = OrderedDict(self.validate_params(acq_params,
        #                               ctrl_params))

        trigger_type = AcquisitionMaster.SOFTWARE

        super().__init__(*devices, name=name, prepare_once=prepare_once,
                         start_once=start_once, npoints=npoints,
                         trigger_type=trigger_type, ctrl_params=ctrl_params)

        self.n354 = self.device.tr_acq.n354
        self.city = self.device.tr_acq.city
        self.xshut = self.device.tr_acq.xshut
        self.lshut = self.device.tr_acq.lshut
        self.trigger_ctrl = self.device.tr_acq.trigger_ctrl
        self.rayonix = self.trigger_ctrl._chain_config['n354'][0]["device"]
        # unless we decide to set ntrg=0: tetramm trigger continuous mode
        # we need to import the tetramm here
        self.tetramm = self.trigger_ctrl._chain_config['n354'][2]["device"]
        self.tetramm.xray_freq = self.n354.core.xshut.read_frequency()
        self.tetramm.single_pulse = self.trigger_ctrl._single_pulse
        self._count_time = count_time

        self.__lock = lock.Semaphore()
        self._ready_event = event.Event()

        self._nb_point = 0
        self._prepared = False

    def __iter__(self):
        return

    def rayonix_trigger_mode(self):
        return self.rayonix.acquisition.trigger_mode

    def prepare_xshut(self):
        """Prepare the X-ray ms-shutter for synchronized scans.

        Three things are done:
        - prepare the n354
        - prepare the OPION2
        - prepare the xshut IcePAP
        according to xshut._scan_mode value

        """
        if self.xshut._scan_mode == "pulsed":
            # TO DO : check following 2 lines are correct (raise Exception?)
            # if n354.core.xshut.read_frequency() > 40:
            #     raise Exception("xshut frequency too high for pulsed mode")
            self.n354.core.xshut_prepare_for_pulsed()
            self.trigger_ctrl.set_I1_to_get_burst_of_pulse()
            self.xshut.sync_scan_prepare()
        elif self.xshut._scan_mode == "gated":
            self.n354.core.xshut_prepare_for_gated()
            self.trigger_ctrl.set_I1_to_get_gate()
            self.xshut.sync_scan_prepare()
        elif self.xshut._scan_mode == "closed":
            self.xshut.close()
        elif self.xshut._scan_mode == "open":
            self.xshut.open()

    def prepare_lshut(self, in_prepare=True):
        """Prepare the laser shutter for synchronized scans.

        Set the laser shutter according to lshut._scan_mode value.

        Paramaters
        ----------
        in_prepare : bool
           If True (default), it means that prepare_lshut() was called withing
           the prepare() method of the scan (i.e. only at the beginning
           of the scan).
           If False, it means that it was called from the trigger() method
           of the scan.

        Notes:
         - called during the scan prepare() and after each scan step
           where lxt='off'

        """

        if in_prepare:
            if not isinstance(self.lshut, LaserShutter):
                print("No valid laser shutter object.")
            # self.lshut._is_timeout = self.lshut.core.check_timeout()

        # if self.lshut._is_timeout:
        #     print("Connection timeout error in lshut detected.")
        #     print("Hint: check if lshut controller is in manual mode.")
        #     print("Scan will be continued but with no control on lshut.\n")
        #     return

        if self.lshut.get_scan_mode() == "pulsed":
            self.lshut.close()
            if self.n354.core.lshut.read_frequency() > 40:
                if in_prepare:
                    self.lshut.set_scan_mode("closed")
                    print("lshut frequency is too high for pulsed mode")
                    print("lshut mode set to closed")
            else:
                self.n354.core.lshut_prepare_for_pulsed()
                self.lshut.ext_trig_on()
        elif self.lshut.get_scan_mode() == "gated":
            self.lshut.close()
            self.n354.core.lshut_prepare_for_gated()
            self.lshut.ext_trig_on()
        elif self.lshut.get_scan_mode() == "closed":
            self.lshut.close()
            self.lshut.ext_trig_off()
        elif self.lshut.get_scan_mode() == "open":
            self.lshut.open()
            self.lshut.ext_trig_off()

    def apply_parameters(self):
        # set burst nb. pulses according to the choosen freq and count_time
        nb_pulses = self.n354.get_burst_npulses(self._count_time)
        slave_names = [slave.name for slave in self.slaves]
        if 'tetramm' in slave_names:
            self.tetramm.burst_npulses = nb_pulses
        super().apply_parameters()

    def prepare(self):
        """Prepare devices for scanning.
        Called only once if 'prepare_once' is True.
        """
        slave_names = [slave.name for slave in self.slaves]

        self.n354.core.burst_disable_ext_trig()

        # set burst nb. pulses according to the choosen freq and count_time
        nb_pulses = self.n354.get_burst_npulses(self._count_time)

        self.wait_slaves_prepare()

        self.trigger_ctrl.set_n354_as_master_for_xshut()

        # check if the camera is enabled (in the chain)
        # and we will trigger it if this is true
        self.camera_enabled = False
        if 'rayonix' in slave_names:
            self.camera_enabled = True
        # for slave in self.slaves:
        #     if slave.name == 'rayonix':
        #         self.camera_enabled = True

        if not self.trigger_ctrl._single_pulse:
            if self.camera_enabled:
                if self.rayonix_trigger_mode() == "EXTERNAL_TRIGGER_MULTI":
                    print("WARNING: rayonix detector in ExtTrigMulti mode.")
                    print("         cannot perform a BLISS scan unless a " +
                          "         single pulse per image is used.")
                self.trigger_ctrl.gate_off_camera()
            if nb_pulses == 0:
                self.xshut.close()
                raise Exception("'nb_pulses'=0 HINT: check count_time vs. " +
                                "X-ray frequency.")
        else:
            nb_pulses = 1
            if self.camera_enabled:
                self.trigger_ctrl.set_n354_as_trig_for_camera()
                # self.rayonix.acquisition.nb_frames = 1
                # Previous line commented by KP on Feb 2024
                # otherwise we get only one image perr scan, instead of 1 image
                # per scan point

        # if self.rayonix_trigger_mode() == "EXTERNAL_GATE":
        #     self.trigger_ctrl.gate_off_camera()  # to set the O6 channel to
        #                                          # cmd mode
        # elif self.rayonix_trigger_mode() == "EXTERNAL_TRIGGER_MULTI":
        #     self.trigger_ctrl.set_n354_as_trig_for_camera()
        #     self.rayonix.nb_frames = 1
        #     nb_pulses = 1

        self.n354.core.burst_enable_ext_trig()
        self.n354.core.burst_set_count(nb_pulses)

        self.prepare_xshut()
        self.prepare_lshut()
        self._previous_step_was_off = False
        self._prepared = True

    def start(self):
        """Start data collection or make sure devices are ready for collecting
           data upon receiving triggers (hardware or software).
           Called only once if 'start_once' is True.
        """
        # The code below is a bit disgusting and should be better written
        # it looks like in a typical case (with n354 triggering), the
        # self.trigger() is never called from here.
        if (
            self.trigger_type == AcquisitionMaster.SOFTWARE and self.parent
        ):  # otherwise top master trigger would never be called
            return
        self.trigger()

    def trigger(self):
        """Trigger device data production and eventually trigger slaves."""

        if self._previous_step_was_off:
            # in this case we need to re-prepare lshut
            self.prepare_lshut(in_prepare=False)
            self._previous_step_was_off = False

        # check if laser has to be switched off
        if self.city is not None:
            if self.city.core.is_off:
                self.lshut.close()
                self._previous_step_was_off = True

        if self.n354.core.is_off:
            self.lshut.close()
            self._previous_step_was_off = True

        # Use the TriggersControl controller to send ext pulse to the card
        # Gate manually the camera as well
        # neeeded only in cases where other devices need to be triggered via
        # software
        self.trigger_slaves()

        if self.camera_enabled:
            if self.rayonix_trigger_mode() == "EXTERNAL_GATE":
                self.trigger_ctrl.gate_on_camera()

        # print("checking what slaves are available")
        # for slave in self.slaves:
        #     print(slave.name)
        #     if slave.name == 'ticc':
        #         if self.tetramm.core.trig == "ON":
        #             print("calling ticc trigger()")
        #             slave.trigger()
        self.trigger_ctrl.trig_n354()

        if self._counters:
            data = (
                c.conversion_function(x)
                for c, x in zip(self._counters,
                                self.device.read_all(*self._counters))
            )
            for channel_data, channel in zip(data, self.channels):
                channel.emit(channel_data)

    def trigger_ready(self):
        """Return if ready for next trigger."""
        return True

    def stop(self):
        """Called at the end of the scan (not at the end of each scan point) to
           stop data production.
        """
        # stop the camera acquisition by switching off the gate signal
        if self.camera_enabled:
            self.trigger_ctrl.gate_off_camera()  # set OPIOM2-06 to low

        # make sure no pulses are sent before communicating with lshut
        if self.n354.core.burst_is_running():
            self.n354.core.burst_set_count(1)
            time.sleep(0.1)
            self.trigger_ctrl.trig_n354()
            time.sleep(0.1)
        self.n354.core.burst_disable_ext_trig()

        self.xshut.close()
        # self.lshut.close()  # Oct 2023: for Peter's experiment this is not OK

        # once we are sure that both shutters are not in external trigger mode
        # it should be safe to turn the burst off
        time.sleep(0.1)
        # n354.core.burst_off()  # to be implemented

        self._prepared = False

    def wait_ready(self):
        """Block/wait until ready for next scan iteration. Called at the
           end of each scan point after trigger() has been executed.
        """
        # time it takes the n354 to set the burst as running after receiving
        # the trigger this time depends on the period of n354 burst master
        # (typically xshut)
        # WE ASSUME: setting it equal to the period of the n354 burst master
        #            is SAFE

        xshut_freq = self.n354.core.xshut.read_frequency()
        time.sleep(1/xshut_freq)

        while self.n354.core.burst_is_running():
            gevent.sleep(0.01)
        # stop the camera only once the acquisition is running
        if self._prepared and self.camera_enabled:
            if self.rayonix_trigger_mode() == "EXTERNAL_GATE":
                self.trigger_ctrl.gate_off_camera()

    def add_counters(self):
        pass

    def get_acquisition_metadata(self, timing=None):
        tmp_dict = super().get_acquisition_metadata(timing=timing)
        if timing == self.META_TIMING.END:
            if tmp_dict is None:
                tmp_dict = dict()
            # tmp_dict["acq_parameters"] = self.acq_params
            tmp_dict["ctrl_parameters"] = self.ctrl_params
        return tmp_dict


class TimeResolvedAcquisition(BlissController):

    def __init__(self, config):

        BlissController.__init__(self, config)

        self._config = config
        self.n354 = config.get("n354")
        self.city = config.get("city", None)

        # ---
        self.xray_freq = self.n354.core.xshut.read_frequency()
        # ---

        for device in config.get("extra_devices"):
            self.trigger_ctrl = device["trigger_ctrl"]
            self.xshut = device["xshut"]
            self.lshut = device["lshut"]
        self.counter_ctrl = N354CounterController(self)

        # add self to BLISS session map
        global_map.register(
            self,
            parents_list=["controllers", "counters", "extra_devices"],
            children_list=[self.trigger_ctrl]
        )

        # create counter
        for cnt_conf in self._config.get("counters", list()):
            name = cnt_conf["counter_name"].strip()
            N354Counter(name, cnt_conf, self.counter_ctrl)

    def __info__(self):
        info = "TimeResolvedAcquisition obj."
        return info

    def _load_config(self):
        pass

    def _get_default_chain_counter_controller(self):
        return self.counter_ctrl

    @autocomplete_property
    def counters(self):
        return self.counter_ctrl.counters
