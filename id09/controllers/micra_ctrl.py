# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2022 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Control of Coherent Micra laser

yml configuration example:

- class: MicraCounterContainer
  plugin: generic
  package: id09.controllers.micra_ctrl
  name: micra_ctrl
  counters:
    - name: verdi_power
      unit: W
    - name: power
      unit: mW

"""

from bliss.common.counter import SamplingCounter
from bliss.controllers.counter import SamplingCounterController
from bliss.common.protocols import CounterContainer
from bliss.common.utils import autocomplete_property
from id09.core.micra import Micra


class MicraCounterController(SamplingCounterController):

    def __init__(self, core, name, master_controller=None,
                 register_counters=True):

        super().__init__(name=name, master_controller=master_controller,
                         register_counters=register_counters)

        self.core = core

    def read(self, counter):
        return self.core.read_data(counter.name)

    # def read_all(self, *counters):
    #     values = []
    #     for cnt in counters:
    #         values.append(self.core.__getattribute__(cnt))
    #     return values


class MicraCounterContainer(CounterContainer):

    def __init__(self, config):

        self._name = config.get("name")
        self._config = config
        url = config.get("url", "id09brainbox02:9002")
        timeout = config.get("timeout", 0.5)

        self.core = Micra(config=None, url=url, timeout=timeout)

        self._cc = MicraCounterController(self.core, self.name)

        # create BLISS Sampling Counter(s)
        for conf in config.get("counters"):
            name = conf["name"].strip()
            mode = conf.get("mode", "SINGLE")
            unit = conf.get("unit")
            self._cc.create_counter(SamplingCounter, name=name, mode=mode,
                                    unit=unit)

    def __info__(self):
        info_list = []
        info_list.append("Micra Counter Container")
        return "\n".join(info_list)

    @property
    def name(self):
        return self._name

    @autocomplete_property
    def counters(self):
        """Standard counter namespace."""
        return self._cc.counters

    def apply_config(self):
        """Ad hoc apply_config() for updating the object in case YAML
           is changed when the session is already open."""
        self._config.reload()
        self.__init__(self._config)

    # methods from the BLISS independent part are exposed below:

    @property
    def diode_hours(self):
        return self.core.verdi.diode_hours

    @property
    def head_hours(self):
        return self.core.verdi.head_hours

    @property
    def is_modelocked(self):
        return self.core.is_modelocked

    @property
    def pzt_mode(self):
        return self.core.pzt_mode

    @property
    def powertrack(self):
        return self.core.powertrack

    @property
    def peakhold(self):
        return self.core.peakhold

    @property
    def automodelock(self):
        return self.core.automodelock
