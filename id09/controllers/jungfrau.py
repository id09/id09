import time
import pathlib
import PyTango


class Jungfrau:

    def __init__(self, tangoaddr="id09/jungfrau/jf1m", base_folder=None):
        self.ds = PyTango.DeviceProxy(tangoaddr)
        self.base_folder = base_folder

    @property
    def scmode(self):
        return self.ds.EnableSCMode

    @scmode.setter
    def scmode(self, value):
        if not isinstance(value, bool):
            raise TypeError("'value' must be bool.")
        self.ds.EnableSCMode = value

    @property
    def save(self):
        return self.ds.EnableFWrite

    @save.setter
    def save(self, value):
        if not isinstance(value, bool):
            raise TypeError("'value' must be bool.")
        self.ds.EnableFWrite = value

    @property
    def nframes(self):
        """Expected number of frames."""
        return self.ds.ExpectedFrames

    @nframes.setter
    def nframes(self, value):
        self.ds.ExpectedFrames = int(value)

    @property
    def exp_period(self):
        """Exposure period in seconds."""
        return self.ds.ExposurePeriod

    @exp_period.setter
    def exp_period(self, value):
        self.ds.ExposurePeriod = value

    @property
    def exp_time(self):
        """Exposure time in seconds."""
        return self.ds.ExposureTime

    @exp_time.setter
    def exp_time(self, value):
        self.ds.ExposureTime = value

    @property
    def file_save_path(self):
        return self.ds.FileSavePath

    @file_save_path.setter
    def file_save_path(self, fpath):
        self.ds.FileSavePath = str(fpath)
        self.ds.FileIndex = 0

    @property
    def file_index(self):
        return self.ds.FileIndex

    @file_index.setter
    def file_index(self, value):
        self.ds.FileIndex = int(value)

    @property
    def ext_trig(self):
        if self.ds.TimingMode == 'AUTO':
            return False
        elif self.ds.TimingMode == 'TRIGGER EXPOSURE':
            return True
        else:
            raise Exception(f"'TimingMode' = {self.ds.TimingMode} unknown.")

    @ext_trig.setter
    def ext_trig(self, value):
        if not isinstance(value, bool):
            raise TypeError("'value' must be bool.")
        if value:
            self.ds.TimingMode = 'TRIGGER EXPOSURE'
        else:
            self.ds.TimingMode = 'AUTO'

    @property
    def ntriggers(self):
        """Expected number of triggerss."""
        return self.ds.ExpectedNumOfTriggers

    @ntriggers.setter
    def ntriggers(self, value):
        self.ds.ExpectedNumOfTriggers = int(value)

    @property
    def delay_after_trigger(self):
        """Delay between JF trig in and first exposure in seconds."""
        return self.ds.DelayAfterTrigger*1e-9

    @delay_after_trigger.setter
    def delay_after_trigger(self, value):
        self.ds.DelayAfterTrigger = int(value*1e9)

    @property
    def delay_after_sc(self):
        """Delay after each SC exposure in seconds."""
        return self.ds.DelayAfterSC*1e-9

    @delay_after_sc.setter
    def delay_after_sc(self, value):
        self.ds.DelayAfterSC = int(value*1e9)

    @property
    def streamvis_freq(self):
        """Data streaming frequency."""
        return self.ds.DataStreamingFrequency

    @streamvis_freq.setter
    def streamvis_freq(self, expected_frame_rate):
        if (expected_frame_rate > 0):
            self.ds.DataStreamingFrequency = int(expected_frame_rate/5)
        else:
            raise Exception("'expected_frame_rate' must be > 0")

    @property
    def temp0(self):
        return self.ds.Temp0

    @property
    def temp1(self):
        return self.ds.Temp1

    @property
    def status(self):
        return self.ds.State().name

    def start(self):
        if self.save:
            path = pathlib.Path(self.file_save_path)
            path.parent.mkdir(exist_ok=True)
        self.ds.StartAcquisition()

    def stop(self):
        if self.status == 'RUNNING':
            self.ds.StopAcquisition()

    def wait(self):
        while (self.status == 'RUNNING'):
            time.sleep(0.05)

    def __info__(self):
        out = []
        out.append("Jungfrau 1M Bliss controller")
        out.append(f"Status: {self.status}")
        out.append(f"ExpectedFrames: {self.nframes}")
        out.append(f"ExposureTime: {self.exp_time*1e6:.3f} us")
        out.append(f"FileSavePath: {self.file_save_path}")
        out.append(f"FileIndex: {self.file_index}")
        out.append(f"TimingMode: {self.ds.TimingMode}")
        out.append(f"ExpectedTriggers: {self.ntriggers}")
        out.append(f"DelayAfterTrigger: {self.delay_after_trigger*1e6:.3f} us")
        out.append(f"DelayAfterSC: {self.delay_after_sc*1e6:.3f} us")
        out.append(f"SCMode: {self.scmode}")
        out.append(f"Save: {self.save}")
        out.append(f"Temp0: {self.temp0} degC")
        out.append(f"Temp1: {self.temp1} degC")
        out.append(f"Base folder: {self.base_folder}")
        return "\n".join(out)

    def prepare_ext_trig(self, nimages, trig_freq):

        if self.scmode:
            self.streamvis_freq = trig_freq*10
        else:
            self.streamvis_freq = trig_freq

        self.ext_trig = True
        self.ntriggers = nimages

    def prepare_int_trig(self, nimages, frame_rate):

        self.streamvis_freq = frame_rate
        period = 1/frame_rate

        if self.scmode:
            period *= 16  # ...

        self.ext_trig = False
        self.nframes = nimages
        self.exp_period = period

    def prepare_burst_mode(self, exp_time, delay_after_sc,
                           trigger_xray_delay=1e-3, xray_pulse_duration=80e-6,
                           extra_exp_time=2e-6, extra_trigger=29e-6,
                           verbose=False):
        """
        Prepare burst mode and make sure that burst is centered around
        the X-ray pulse (xscope rising edge).

        Parameters
        ----------
        delay_after_sc : float
            Delay between the end of exposure of a SC and the start of the
            following one. Since an extra 2.25 us delay is added internally
            (in the firmware), here we have to add it back
        xray_pulse_duration : float
            Approximate duration of X-ray pulse (1ms in dynamic-DAC exp,
            80us in beam sweeping exp, etc.)

        """

        self.scmode = True

        full_time_window = (exp_time+extra_exp_time)*16 + delay_after_sc*15

        if full_time_window > xray_pulse_duration:
            print("WARNING: the burst is longer than the xray pulse duration.")

        delay_after_trigger = trigger_xray_delay - full_time_window/2

        # 1.0 ms is an arbitrary delay that is big enough to cope with
        # the half of the biggest possible full time window (which we know,
        # in the worst case - that of experiments hc4895 and hc5293 - to be
        # of the order of 1.5 ms)

        if verbose:
            print("Delay between jungfrau trigger in and xray pulse center:" +
                  f" {trigger_xray_delay*1e6:g} us")
            print("Delay between jungfrau trigger in and first exposure:" +
                  f" {delay_after_trigger*1e6:g} us")
            print(f"Burst full time window: {full_time_window*1e6:g} us")

        if delay_after_trigger < 0:
            raise Exception("ERROR: 'delay_after_trigger' cannote be negative")

        self.exp_time = exp_time
        self.delay_after_sc = delay_after_sc
        self.delay_after_trigger = delay_after_trigger - extra_trigger

    def _take_darks(self, exp_time, path=None):
        """
        Take dark images.

        If the dector has n modules: 2n+2 files will be created for each gain.
        Example (jf1m):
          - jf_darks_d0_f0_0.h5
          - jf_darks_d1_f0_0.h5
          - jf_darks_virtual_0.h5
          - jf_darks_master_0.h5
          - jf_darks_d0_f0_1.h5
          - jf_darks_d1_f0_1.h5
          - jf_darks_virtual_1.h5
          - jf_darks_master_1.h5
          - jf_darks_d0_f0_2.h5
          - jf_darks_d1_f0_2.h5
          - jf_darks_virtual_2.h5
          - jf_darks_master_2.h5

        6 files will be created with following suffices:
        - _f0_0.h5
        - _

        Inputs
        ------
        exp_time : float
           Detector exposure time (sec).
        path : str
           Path for data saving.

        """

        previous_path = self.file_save_path

        self.exp_time = exp_time

        self.save = True
        if path is not None:
            path = pathlib.Path(path)
            path.parent.mkdir(exist_ok=True)
            self.file_save_path = path

        # self.ds.FileIndex = 0
        self.file_index = 0

        self.ds.RecordPedestals()

        self.wait()

        self.file_save_path = previous_path

    def take_darks(self, exp_time, path):
        """
        delay_after_sc : float
           ... (sec).
        """
        self.scmode = False

        # self.ds.TimingMode = 'AUTO'
        self.ext_trig = False
        self.exp_period = 10e-3  # assuming 1000 images will be taken

        self._take_darks(exp_time, path)

    def take_sc_darks(self, exp_time, delay_after_sc, path,
                      trigger_xray_delay=1e-3,
                      xray_pulse_duration=80e-6,
                      extra_exp_time=2e-6,
                      extra_trigger=29e-6):
        """
        delay_after_sc : float
           ... (sec).
        """
        self.scmode = True
        self.prepare_burst_mode(exp_time, delay_after_sc,
                                trigger_xray_delay=trigger_xray_delay,
                                xray_pulse_duration=xray_pulse_duration,
                                extra_exp_time=extra_exp_time,
                                extra_trigger=extra_trigger)

        # self.ds.TimingMode = 'AUTO'
        self.ext_trig = False
        self.exp_period = 5e-3  # assuming 200 images will be taken per SC

        self._take_darks(exp_time, path)
