# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2023 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from blissdata import settings

from bliss.common.scans import ct
from bliss.common.cleanup import cleanup
from bliss.controllers.counter import CalcCounterController
from bliss.scanning.acquisition.calc import CalcCounterAcquisitionSlave
from bliss.common.logtools import user_print


class DarkCounterController(CalcCounterController):

    def __init__(self, config=None, name=None, input_counters=None,
                 output_cnt_names=None):
        """
        Parameters
        ----------
        input_counters : list of BLISS counters
        output_cnt_names : list of strings
        """

        if config is None:
            if input_counters is None or output_cnt_names is None:
                raise ValueError("'input_counters' and 'output_cnt_name' " +
                                 "cannot be None if 'config' is None.")
            inputs = [{'counter': cnt} for cnt in input_counters]
            outputs = [{'name': cnt} for cnt in output_cnt_names]
            config = {'inputs': inputs, 'outputs': outputs}

        CalcCounterController.__init__(self, config=config, name=name)

        self._integration_time = None
        self._integration_time_index = {}

        dark_setting_name = f"{self.name}_dark"
        dark_setting_default = {}

        for cnt in self.inputs:
            breakpoint()
            if self.tags[cnt.name] is None:
                self.tags[cnt.name] = f"{cnt.name}_dark"
            tag = self.tags[cnt.name]
            dark_setting_default[tag] = 0.0

        self.dark_setting = settings.HashSetting(
            dark_setting_name, default_values=dark_setting_default
        )

    def __info__(self):
        mystr = ""
        for cnt in self.outputs:
            tag = self.tags[cnt.name]
            dark = self.dark_setting[tag]
            mystr += f"{cnt.name} - {dark}\n"
        return mystr

    def get_acquisition_object(self, acq_devices, acq_params, ctrl_params,
                               parent_acq_params):

        return DarkAcquisitionSlave(self, acq_devices, acq_params,
                                    ctrl_params=ctrl_params)

    def get_default_chain_parameters(self, scan_params, acq_params):
        acq_params = super().get_default_chain_parameters(scan_params,
                                                          acq_params)
        if acq_params.get("count_time") is None:
            acq_params["count_time"] = scan_params["count_time"]
        return acq_params

    def get_input_counter_from_tag(self, tag):
        for cnt in self.inputs:
            if self.tags[cnt.name] == tag:
                return cnt
        return None

    def take_dark(self, time=1.0, set_value=None):
        if set_value is not None:
            for cnt in self.inputs:
                tag = self.tags[cnt.name]
                self.dark_setting[tag] = set_value
        else:
            if self.background_object is None:
                self.take_dark_data(time)
            else:
                # Store initial state
                self.dark_object_initial_state = self.dark_object.state

                # Close beam
                self.dark_object.close()

                # Take background
                with cleanup(self._close):
                    self.take_dark_data(time)

                if self.dark_object.state == "CLOSED":
                    user_print("WARNING: state was not 'CLOSED' while " +
                               "collecting dark.")

    def _close(self):
        """Re-open if initial state was OPEN"""
        if self.dark_object_initial_state == "OPEN":
            self.dark_object.open()

    def take_dark_data(self, int_time):
        scan_ct = ct(int_time, self.inputs, run=False)
        scan_ct.run()
        data_dark = scan_ct.get_data()
        for cnt in self.inputs:
            tag = self.tags[cnt.name]
            dark = data_dark[cnt.name][0]
            self.dark_setting[tag] = data_dark[cnt.name][0]
            self.dark_setting["int_time"] = int_time
            user_print(f"{cnt.name} - {dark}")

    def calc_function(self, input_dict):
        value = {}
        for tag in input_dict.keys():
            # cnt = self.get_input_counter_from_tag(tag)
            dark = self.dark_setting[tag]
            dark /= self.dark_setting["int_time"]
            value[tag] = input_dict[tag] - dark
        return value


class DarkAcquisitionSlave(CalcCounterAcquisitionSlave):

    def __init__(self, controller, src_acq_devices_list, acq_params,
                 ctrl_params=None):

        super().__init__(controller, src_acq_devices_list, acq_params,
                         ctrl_params=ctrl_params)

        self._int_time = acq_params["count_time"]

    def prepare(self):
        super().prepare()
        if self._int_time is not None:
            self.device._int_time = self._int_time
            for o_cnt in self.device._output_counters:
                self.device._int_time_index[self.device.tags[o_cnt.name]] = 0
