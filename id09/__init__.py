# -*- coding: utf-8 -*-

"""Top-level package for ID09 project."""

__author__ = """BCU Team"""
__email__ = 'bliss@esrf.fr'
__version__ = '0.1.0'


from bliss import config

config.get_sessions_list()
config = config.static.get_config()
