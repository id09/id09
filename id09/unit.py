import numpy as np
import re


si = {-15: {'multiplier': 1e15, 'prefix': 'f'},
      -14: {'multiplier': 1e15, 'prefix': 'f'},
      -13: {'multiplier': 1e15, 'prefix': 'f'},
      -12: {'multiplier': 1e12, 'prefix': 'p'},
      -11: {'multiplier': 1e12, 'prefix': 'p'},
      -10: {'multiplier': 1e12, 'prefix': 'p'},
      -9: {'multiplier': 1e9, 'prefix': 'n'},
      -8: {'multiplier': 1e9, 'prefix': 'n'},
      -7: {'multiplier': 1e9, 'prefix': 'n'},
      -6: {'multiplier': 1e6, 'prefix': 'u'},
      -5: {'multiplier': 1e6, 'prefix': 'u'},
      -4: {'multiplier': 1e6, 'prefix': 'u'},
      -3: {'multiplier': 1e3, 'prefix': 'm'},
      -2: {'multiplier': 1e3, 'prefix': 'm'},
      -1: {'multiplier': 1e3, 'prefix': 'm'},
      0: {'multiplier': 1, 'prefix': ''},
      1: {'multiplier': 1, 'prefix': ''},
      2: {'multiplier': 1, 'prefix': ''},
      3: {'multiplier': 1e-3, 'prefix': 'k'},
      4: {'multiplier': 1e-3, 'prefix': 'k'},
      5: {'multiplier': 1e-3, 'prefix': 'k'},
      6: {'multiplier': 1e-6, 'prefix': 'M'},
      7: {'multiplier': 1e-6, 'prefix': 'M'},
      8: {'multiplier': 1e-6, 'prefix': 'M'},
      9: {'multiplier': 1e-9, 'prefix': 'G'},
      10: {'multiplier': 1e-9, 'prefix': 'G'},
      11: {'multiplier': 1e-9, 'prefix': 'G'},
      12: {'multiplier': 1e-12, 'prefix': 'T'},
      13: {'multiplier': 1e-12, 'prefix': 'T'},
      14: {'ultiplier': 1e-12, 'prefix': 'T'},
      }


inv_si = {'f': 1e-15, 'p': 1e-12, 'n': 1e-9, 'u': 1e-6, 'm': 1e-3, '': 1}


def convert_str2float(string_value, decimals=3):
    """Convert physical quantity string (including unit) to float."""

    if not isinstance(string_value, str):
        raise TypeError("'string_value' must be int or float.")

    _regex = re.compile("(-?\\d+\\.?\\d*)\\s*((?:f|m|n|p|u)?)")

    match = _regex.search(string_value.replace(" ", ""))
    if match:
        n, t = float(match.group(1)), match.group(2)
        if not t:
            value = 1
        else:
            value = inv_si.get(t)
        return n*value
    else:
        return None


def tstr2float(delay):

    _t_regex = re.compile("(-?\d+\.?\d*)\s*((?:s|fs|ms|ns|ps|us)?)")

    if isinstance(delay, str):
        delay = [delay]
        return_array = False
    else:
        return_array = True

    if not isinstance(delay, (list, tuple, np.ndarray)):
        raise TypeError("'delay' must be float or array-like.")

    t2val = dict(fs=1e-15, ps=1e-12, ns=1e-9, us=1e-6, ms=1e-3, s=1)

    t = []

    for k, d in enumerate(delay):

        if isinstance(d, bytes):
            d = d.decode('ascii')

        match = _t_regex.search(d)

        if match:
            n, t0 = float(match.group(1)), match.group(2)
            val = t2val.get(t0, 1)
            t.append(n*val)
        else:
            # unrecognized string will be kept
            t.append(d)

    if not return_array:
        t = t[0]

    return t


def convert_float2str(number, unit=None, digits=None, decimals=None,
                      strip_spaces=False):
    """Convert float to physical quantity in SI format."""

    if not isinstance(number, (int, float)):
        raise TypeError("'number' must be int or float.")

    if digits is not None:
        if digits <= 0:
            raise ValueError("'digits' must be > 0.")

    if abs(number) < 1e-15:
        return "0"

    exponent = np.floor(np.log10(np.abs(number)))
    mantissa = number*si[exponent]['multiplier']

    if digits is not None:
        # check if mantissa is >10, >100, ... avoid having '1000us'
        mant_exp = np.floor(np.log10(np.abs(mantissa)))
        mantissa /= 10**mant_exp
        mantissa = np.round(mantissa, digits-1)
        mantissa *= 10**mant_exp

        if mantissa == 1000:
            exponent += 1
            mantissa = 1

    if decimals is not None:
        mantissa = np.round(mantissa, decimals)

    if (mantissa - int(mantissa)) == 0:
        mantissa = int(mantissa)

    string = str(mantissa) + " " + si[exponent]["prefix"]

    if unit is not None:
        string += unit

    if strip_spaces:
        string = string.replace(" ", "")

    return string
