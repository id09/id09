import os
import signal
import time
from tango import DevFailed
from id09.colors import bold, colorize
from id09.status import blstatus


def display_essentials(all_the_time=True, period=1):

    def handler(signum, frame):
        ans = input("\nCtrl-c was pressed. Do you really want to exit (y/n)? ")
        if ans.lower() in ['y', 'yes']:
            cleanup()
            print()
            os._exit(1)

    def cleanup():
        # put here all the cleanups for different hardware
        pass

    def display_once():
        out = []
        out += get_pindiodes()
        out += get_beamline()
        out += get_machine()
        # out += get_trigger()
        os.system('clear')
        print("\n".join(out))

    signal.signal(signal.SIGINT, handler)

    while all_the_time:
        t0 = time.time()
        display_once()
        t1 = time.time()
        dt = period-(t1-t0)
        if dt > 0:
            time.sleep(dt)
    else:
        display_once()


def get_pindiodes():
    out = [bold("PIN diodes")]
    out += get_oh_pindiodes()
    out += get_eh2_pindiodes()
    return out


def get_oh_pindiodes():
    if blstatus.pd0 is None:
        return ['']
    name = "pd0"
    try:
        cur = blstatus.pd0.keithley.read_now()
        rng = blstatus.pd0.keithley.get_range()
    except (DevFailed, AttributeError):
        return ['']
    except Exception:
        print("\nERROR: get_oh_pindiodes() crashed.")
        print("HINT: check keithley device server.\n")
        os._exit(1)
    pos = blstatus.pd0.get_position()
    line = f"  {name:4s}"
    line += f" {cur:+5.4e} A, {rng}"
    line += f", {pos}"
    return [line]


def get_eh2_pindiodes_position():
    inout = [blstatus.inout_pd1, None, blstatus.inout_pd3, blstatus.inout_pd4]
    pos = 4*[None]
    for k, pd in enumerate(inout):
        if pd is not None:
            try:
                pd_pos = pd.position
            except (DevFailed, AttributeError):
                return ['']
            except Exception:
                print("\nERROR: get_eh2_pindiodes_position() crashed.")
                print("HINT: check wcid09e device server.\n")
                os._exit(1)
        pos[k] = pd_pos if pd_pos != 'unknown' else '?'
    return pos


def get_eh2_pindiodes():
    if blstatus.tetramm is None:
        return ['']
    try:
        # get_current input is a tuple: naq, ntrg, nch
        cur = blstatus.tetramm.ds.get_current((1, 1, 4))
        rng = blstatus.tetramm.ds.get_range()
    except (DevFailed, AttributeError):
        return 4*['']
    except Exception as e:
        print(e)
        print("\nERROR: get_eh2_pindiodes() crashed.")
        print("HINT: check tetramm device server.\n")
        os._exit(1)
    names = ["pd1", "pd2", "pd3", "pd4"]
    pos = get_eh2_pindiodes_position()
    if cur.size == 0:
        cur = 4*[0]

    out = []
    for k in range(len(names)):
        line = f"  {names[k]:4s}"
        line += f" {cur[k]:+10.4e} A"
        if rng[k] != "":
            line += f", {rng[k]}"
        try:
            if pos[k] is not None:
                line += f", {pos[k]}"
        except Exception as e:
            print(f"length of pos is: {len(pos)}")
            print("Following exception occurred:", e)
        out.append(line)
    return out


def get_beamline():
    out = [bold("Beamline")]
    energy = blstatus.get_xray_energy()
    mono = blstatus.get_mono()
    out = bold("Beamline")
    if mono is not None:
        out += f" ({mono} "
    else:
        out += " (polychromatic"
    if energy is not None:
        out += f" @ {energy*1e-3:6.3f} keV)"
    else:
        out += ", photon energy unknown)"
    out = [out]
    try:
        fe_state = colorize(blstatus.get_fe_state().upper())
        sh1_state = colorize(blstatus.get_sh1_state().upper())
        sh2_state = colorize(blstatus.get_sh2_state().upper())
        sh_state = f"{fe_state:5s}, {sh1_state:5s}, {sh2_state:5s}"
    except (DevFailed, AttributeError):
        sh_state = ""
    except Exception as e:
        print("\nERROR: get_beamline() crashed.")
        print("HINT: check safety shutters device server.\n")
        print(f"EXCEPTION: {e}")
        os._exit(1)
    out += [f"  fe, sh1, sh2 = {sh_state}"]
    if blstatus.u17 is None or blstatus.u23 is None:
        out += ["  u17, u23     = "]
    elif blstatus.u17.disabled or blstatus.u23.disabled:
        out += ["  u17, u23     = "]
    else:
        try:
            u17 = blstatus.u17.position
            u23 = blstatus.u23.position
            out += [f"  u17, u23     = {u17:5.2f}, {u23:5.2f} mm"]
        except (DevFailed, AttributeError):
            out += ["  u17, u23     = ?????, ?????"]
        except Exception as e:
            print("\nERROR: get_beamline() crashed.")
            print("HINT: check undulators device server.\n")
            print(f"EXCEPTION: {e}")
            os._exit(1)
    phg = blstatus.phg.position
    pvg = blstatus.pvg.position
    out += [f"  phg, pvg     = {phg:+5.2f}, {pvg:+5.2f} mm"]
    hlc_state = colorize(blstatus.get_hlc_state().upper())
    pic_state = colorize(blstatus.get_pic_state().upper())
    out += [f"  hlc, pic     = {hlc_state}, {pic_state}"]
    s1, s2 = blstatus.s1hg.position, blstatus.s1vg.position
    out += [f"  s1hg, s1vg   = {s1:+5.2f}, {s2:+5.2f} mm"]
    s1, s2 = blstatus.s2hg.position, blstatus.s2vg.position
    out += [f"  s2hg, s2vg   = {s1:+5.2f}, {s2:+5.2f} mm"]
    hsc_state = colorize(blstatus.get_hsc_state().upper())
    out += [f"  hs-chopper   = {hsc_state}"]
    s1, s2 = blstatus.ss1hg.position, blstatus.ss1vg.position
    out += [f"  ss1hg, ss1vg = {s1:+5.2f}, {s2:+5.2f} mm"]
    s1, s2 = blstatus.ss2hg.position, blstatus.ss2vg.position
    out += [f"  ss2hg, ss2vg = {s1:+5.2f}, {s2:+5.2f} mm"]
    xshut_state = colorize(blstatus.xshut.get_state().upper())
    xshut_scan_mode = blstatus.xshut._scan_mode  # not synchronized
    out += [f"  xshut = {xshut_state}, {xshut_scan_mode} (experimental)"]
    # try:
    #     lshut_state = colorize(blstatus.lshut.get_state().upper())
    #     lshut_mode = blstatus.lshut._scan_mode
    #     out += [f"  lshut = {lshut_state}, {lshut_mode} (experimental)"]
    # except Exception:
    #     out += [f"  lshut ="]

    if blstatus.eh2tc is not None:
        out += [f"  xshut motor temperature = {blstatus.eh2tc.T} deg"]
    return out


def get_machine():
    out = [bold("Machine")]
    machine_mode = blstatus.get_machine_mode(verbose=False)
    filling_mode = blstatus.get_filling_mode(verbose=False)
    srcur = blstatus.get_srcur(verbose=False)
    sbcur = blstatus.get_sbcur(verbose=False)
    # sblen = blstatus.get_sblen(verbose=False)
    # emh, emv = blstatus.get_emittance(verbose=False)
    # ehpos, evpos, ehang, evang = blstatus.get_ebpm(verbose=False)
    out += [f"  machine mode = {machine_mode}"]
    out += [f"  filling mode = {filling_mode}"]
    out += [f"  srcur, sbcur = {srcur}, {sbcur} mA"]
    # out += [f"         sblen = {sblen} ps"]
    # out += [f"  emh, emv     = {emh}, {emv} um"]
    # out += [f"  ehpos, evpos = {ehpos}, {evpos} um"]
    # out += [f"  ehang, evang = {ehang}, {evang} urad"]
    return out


def get_trigger():
    out = [bold('Trigger')]
    trigger_scan_master = blstatus.trigger_ctrl._scan_master  # no synch
    out += [f" trigger scan master = {trigger_scan_master}"]
    return out


if __name__ == "__main__":
    display_essentials(all_the_time=True)
