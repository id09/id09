
HEADER = "\033[95m"
OKBLUE = "\033[94m"
OKGREEN = "\033[92m"
WARNING = "\033[93m"
FAILRED = "\033[91m"
ENDC = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"
DIM = "\033[2m"


def green(string):
    return OKGREEN + str(string) + ENDC


def blue(string):
    return OKGREEN + str(string) + ENDC


def red(string):
    return FAILRED + str(string) + ENDC


def bold(string):
    return BOLD + str(string) + ENDC


def bold_underline(string):
    return BOLD + UNDERLINE + str(string) + ENDC


def print_green(string):
    print(green(string))


def print_red(string):
    print(red(string))


def print_bold(string):
    print(BOLD + str(string) + ENDC)


def print_underline(string):
    print(UNDERLINE + str(string) + ENDC)


def print_bold_underline(string):
    print(BOLD + UNDERLINE + string + ENDC)


def print_dim(string):
    print(DIM + string + ENDC)


def colorize(string):
    # use str representation of obj
    string = str(string)
    if string.lower() in ["on", "ok", "true", "open", "in", 'tunnel']:
        string = green(string)
    elif string.lower() in ["off", "fail", "false", "close", "closed", "out",
                            "step", "unknown"]:
        string = red(string)
    return string
