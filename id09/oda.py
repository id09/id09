import numpy as np

from bliss import config

config.get_sessions_list()
config = config.static.get_config()

rayonix = config.get("rayonix")


def get_poni(center, pixel_size=None, detector='rayonix'):
    """Get poni1 and poni2 from center.

    Paramters
    ---------
    center : array-like
        Center (hor, ver) (pixel).
    pixel_size : array-like
        Pixel size (hor, ver) (m).
    detector : str
        Conventional detector name.

    """
    center = np.array(center)
    if pixel_size is None:
        if detector == 'rayonix':
            pixel_size = rayonix.proxy.camera_pixelsize*1e-3
        else:
            raise ValueError("Only 'rayonix' is implemented.")
    pixel_size = np.array(pixel_size)
    poni1, poni2 = center * pixel_size
    return poni1, poni2
