import time
import pathlib
import PyTango


class Jungfrau:

    def __init__(self, tangoaddr="id09/jungfrau/jf1m", base_folder=None):

        self.dev = PyTango.DeviceProxy(tangoaddr)
        self.base_folder = base_folder

    def __info__(self):
        out = []
        out.append("JUNGFRAU 1M tango DS:")
        out.append("Exposure period = %.2e s" % self.exp_period)
        out.append("Exposure time = %.2e s" % self.exp_time)
        out.append("Trigger mode = %s" % self.dev.TimingMode.name)
        out.append("Delay after trigger = %.2e s" % self.delay_after_trigger)
        out.append("Delay after SC = %.2e s" % self.delay_after_sc)
        out.append("SC mode enabled = %s" % self.scmode)
        out.append("Streamvis freq = %s Hz" % self.get_streamvis_freq())
        out.append("File save mode enabled = %s" % self.save)
        out.append("Module 1 temperature: %.2f degC" % self.dev.Temp0)
        out.append("Module 2 temperature: %.2f degC\n" % self.dev.Temp1)
        return "\n".join(out)

    @property
    def scmode(self):
        return self.dev.EnableSCMode

    @scmode.setter
    def scmode(self, value):
        if not isinstance(value, bool):
            raise TypeError("'value' must be bool.")
        self.dev.EnableSCMode = value

    @property
    def save(self):
        return self.dev.EnableFWrite

    @save.setter
    def save(self, value):
        if not isinstance(value, bool):
            raise TypeError("'value' must be bool.")
        self.dev.EnableFWrite = value

    @property
    def file_save_path(self):
        return self.dev.FileSavePath

    @file_save_path.setter
    def file_save_path(self, fpath):
        self.dev.FileSavePath = str(fpath)
        self.dev.FileIndex = 0

    @property
    def exp_time(self):
        """Exposure time in seconds."""
        return self.dev.ExposureTime

    @exp_time.setter
    def exp_time(self, value):
        self.dev.ExposureTime = value

    @property
    def delay_after_sc(self):
        """Delay after SC time in seconds."""
        return self.dev.DelayAfterSC*1e-9

    @delay_after_sc.setter
    def delay_after_sc(self, value):
        self.dev.DelayAfterSC = int(value*1e9)

    @property
    def exp_period(self):
        """Exposure period in seconds."""
        return self.dev.ExposurePeriod

    @exp_period.setter
    def exp_period(self, value):
        self.dev.ExposurePeriod = value

    @property
    def delay_after_trigger(self):
        """Delay after trigger in seconds."""
        return self.dev.DelayAfterTrigger*1e-9

    @delay_after_trigger.setter
    def delay_after_trigger(self, value):
        self.dev.DelayAfterTrigger = int(value*1e9)

    def status(self):
        return self.dev.State().name

    def get_T(self):
        print("Module 1 temperature: %.2f degC" % self.dev.Temp0)
        print("Module 2 temperature: %.2f degC\n" % self.dev.Temp1)

    def start(self):
        if self.save:
            path = pathlib.Path(self.file_save_path)
            path.parent.mkdir(exist_ok=True)
        self.dev.StartAcquisition()

    def stop(self):
        if self.status() == 'RUNNING':
            self.dev.StopAcquisition()

    def wait(self):
        while (self.status() == 'RUNNING'):
            time.sleep(0.05)

    def get_streamvis_freq(self):
        return self.dev.DataStreamingFrequency

    def set_streamvis_freq(self, expected_frame_rate):
        if (expected_frame_rate != 0):
            self.dev.DataStreamingFrequency = int(expected_frame_rate/5)

    def prepare_ext_trig(self, nimages, trig_freq):

        if self.scmode:
            self.set_streamvis_freq(trig_freq*10)
        else:
            self.set_streamvis_freq(trig_freq)

        self.dev.TimingMode = 'TRIGGER EXPOSURE'
        self.dev.ExpectedNumOfTriggers = nimages

    def prepare_int_trig(self, nimages, frame_rate):

        self.set_streamvis_freq(frame_rate)
        period = 1/frame_rate

        if self.scmode:
            period *= 16  # ...

        self.dev.TimingMode = 'AUTO'
        self.dev.ExpectedFrames = nimages
        self.dev.ExposurePeriod = period

    def prepare_burst_mode(self, exp_time, sc_delay_after,
                           sc_delay_after_offset=2.25e-6, verbose=False):
        """

        Parameters
        ----------
        sc_delay_after : float
            Delay between the end of exposure of a SC and the start of the
            following one. Since an extra 2.25 us delay is added internally
            (in the firmware), here we have to add it back

        """

        self.scmode = True

        # exp_time_ns = int(exp_time*1e9)
        # sc_delay_after_ns = int(sc_delay_after*1e9)

        full_time_window = exp_time*16
        full_time_window += (sc_delay_after + sc_delay_after_offset)*15

        extra_delay = exp_time + sc_delay_after + sc_delay_after_offset

        trigger_delay = 1e-3 - full_time_window/2 - extra_delay

        # 1.0 ms is an arbitrary delay that is big enough to cope with
        # the half of the biggest possible full time window (which we know,
        # in the worst case - that of experiments hc4895 and hc5293 - to be
        # of the order of 1.5 ms

        self.exp_time = exp_time
        self.delay_after_sc = sc_delay_after
        self.delay_after_trigger = trigger_delay  # delay between trig in and 1st SC exposure

        if verbose:
            print("full_time_window: %g s" % full_time_window)
            print("extra_delay: %g s" % extra_delay)
            print("trigger_delay: %g s\n" % trigger_delay)

    def _take_darks(self, exp_time, path=None, verbose=False):
        """
        Take dark images.

        If the dector has n modules: 2n+2 files will be created for each gain.
        Example (jf1m):
          - jf_darks_d0_f0_0.h5
          - jf_darks_d1_f0_0.h5
          - jf_darks_virtual_0.h5
          - jf_darks_master_0.h5
          - jf_darks_d0_f0_1.h5
          - jf_darks_d1_f0_1.h5
          - jf_darks_virtual_1.h5
          - jf_darks_master_1.h5
          - jf_darks_d0_f0_2.h5
          - jf_darks_d1_f0_2.h5
          - jf_darks_virtual_2.h5
          - jf_darks_master_2.h5

        6 files will be created with following suffices:
        - _f0_0.h5
        - _

        Inputs
        ------
        exp_time : float
           Detector exposure time (sec).
        path : str
           Path for data saving.

        """

        previous_path = self.file_save_path

        self.exp_time = exp_time

        self.save = True
        if path is not None:
            path = pathlib.Path(path)
            path.parent.mkdir(exist_ok=True)
            self.file_save_path = path

        self.dev.FileIndex = 0

        if verbose:
            print("Collecting darks with filename:")
            if path is not None:
                print(path)
            else:
                print(self.file_save_path)

        self.dev.RecordPedestals()

        self.wait()

        self.file_save_path = previous_path

    def take_darks(self, exp_time, path, verbose=False):
        """
        delay_after_sc : float
           ... (sec).
        """
        self.scmode = False

        self.dev.TimingMode = 'AUTO'
        self.exp_period = 10e-3  # assuming 1000 images will be taken

        self._take_darks(exp_time, path, verbose=verbose)

    def take_sc_darks(self, exp_time, delay_after_sc, path, verbose=False):
        """
        delay_after_sc : float
           ... (sec).
        """
        self.scmode = True
        self.prepare_burst_mode(exp_time, delay_after_sc)

        self.dev.TimingMode = 'AUTO'
        self.exp_period = 5e-3  # assuming 200 images will be taken per SC

        self._take_darks(exp_time, path, verbose=verbose)
