# -*- coding: utf-8 -*-
"""
Module to communicate with the MCS LA2000 controller.

The LA2000 is a 4 quadrant velocity servo amplifier intended to control
3-phase permanent magnet synchronous motors.

The LA2000 obtains rotor position information through:
    - a quadradure encoder (single-ended or differential)
    - Hall effect sensors (optional)

Possible feedback (commutation) types are:
    - Encoder + Hall effect sensors (TYPE:06)
    - Encoder only (TYPE:07)

Control Methods:
    - Frequency-Lock-Loop using the internal synthesizer
    - Frequency-Lock-Loop using an external frequency reference

Command definition:
    - ASCII strings terminated by a carriage-return character ("\r")
    - Multiple commands are entered as one string ("CMD1:CMD2\r")
    - Max command length: 511 characters
    - If the command string is accepted, the system will respond with
      a line feed character ("\n"). Otherwise, an error code followed
      by a "?\n" is returned.

"""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "26/05/2021"
__version__ = "0.0.1"


import time
import ipaddress

from bliss.comm.util import get_comm


class ControllerError(Exception):
    pass


class ConfigModeError(Exception):
    pass


class LA2000:
    """Class to communicate with the MCS LA2000 controller."""

    def __init__(self, config=None, url='id09brainbox01:9001', timeout=0.15,
                 debug=False, verbose=True):

        """Initialize communication with LA2000."""

        if config is not None:
            url = config.get("url", url)
            timeout = config.get("timeout", timeout)
            debug = config.get("debug", debug)
            verbose = config.get("verbose", verbose)

        conf = {'tcp-proxy': {"external": True, "tcp": {"url": url}}}
        opts = {"timeout": timeout}
        self.comm = get_comm(conf, **opts)

        self.timeout = timeout
        self.debug = debug
        self.verbose = verbose
        self._is_config_mode = False

    @property
    def _error_codes(self):
        return {"0?": "Unrecognized command",
                "2?": "Invalid request (parameter out of range or " +
                      "option not valid in current configuration)",
                "3?": "Unable to change setup (disable drive first)",
                "4?": "EEPROM read failure",
                "5?": "Decimal number length exceeds limit",
                "6?": "Command string is too long",
                "7?": "Reserved",
                "8?": "Max commutation freq exceeded (TOPSPEED is " +
                      "too high for the encoder)"}

    def _error_codes_show(self):
        for k, v in self._error_codes.items():
            print(k, ": ", v)
        print()

    def _error_code(self, error):
        """Return the error associated to a code."""
        if error not in self._error_codes.keys():
            raise ValueError(f"'{error}' error code not recognized")
        return self._error_codes[error]

    @property
    def _write_readlines_cmd(self):
        return ["DISFLTQ", "FLTQ", "IPLIST", "MENU", "PLIST", "STATQ"]

    def _is_write_readlines_cmd(self, cmd):
        cmd = cmd.replace("?", "")
        cmd = cmd.replace("\r", "")
        if cmd.upper() in self._write_readlines_cmd:
            return True
        else:
            return False

    def write_readline(self, cmd, eol=b'\n', timeout=None):

        if self._is_write_readlines_cmd(cmd):
            raise Exception(f"'{cmd}' not compatible with 'write_readline'")

        if self._is_config_mode and eol != b'\n':
            raise Exception(f"'{cmd}' cannot be used in configuration mode.")

        if not cmd.endswith("\r"):
            cmd += "\r"

        if timeout is None:
            timeout = self.timeout

        ans = self.comm.write_readline(cmd.encode(), eol=eol, timeout=timeout)

        if 'DUMP4' in cmd.upper():
            ans = ans[:-1]  # needed to remove \x9a

        if "?" in ans.decode():
            idx = ans.decode().find("?")
            err_code = ans.decode()[idx-1:idx+1]
            err = self._error_code(err_code)
            out = f"'{err_code}' = {err}"
            if self._is_config_mode:
                out += " (In Configuration Mode)"
                raise ConfigModeError(out)
            else:
                raise ControllerError(out)
        elif self._is_config_mode:
            self.comm.readline(eol=b'In configuration state\n',
                               timeout=timeout)

        ans = ans.decode()

        return ans

    def write_readlines(self, cmd, nb_lines=7, eol=b'\n', timeout=0.5):

        if not self._is_write_readlines_cmd(cmd):
            print(f"ERROR: cannot use 'write_readlines' with {cmd.upper()}")
            return

        if not cmd.endswith('\r'):
            cmd += '\r'

        if timeout is None:
            timeout = self.timeout

        lines = self.comm.write_readlines(cmd.encode(), nb_lines=nb_lines,
                                          eol=b'\n', timeout=timeout)

        lines = [line.decode() for line in lines]

        if self._is_config_mode:
            self.comm.readline(eol=b'In configuration state\n',
                               timeout=timeout)

        return lines

    # ------ ######## Class internal commands ######## --------

    def _check_int_in_range(self, value, vmin=0, vmax=99999):
        if not isinstance(value, int):
            raise TypeError("'value' must be int")
        if value not in range(vmin, vmax+1):
            raise ValueError(f"'value' must be in range ({vmin}, {vmax})")

    def _exit_config_mode(self):
        """Exit configuration mode."""
        try:
            self.comm.write_read("EXIT\r".encode(), size=1)
        except ControllerError:
            pass
        finally:
            self._is_config_mode = False
            if self.verbose:
                print("Controller is in 'Normal Mode'.")

    def _enter_config_mode(self):
        """Enter configuration mode."""
        try:
            self.comm.write_readline("CONFIGURE\r".encode(),
                                     eol=b'In configuration state\n')
        except ControllerError:
            pass
        finally:
            self._is_config_mode = True
            if self.verbose:
                print("Controller is in 'Configuration Mode'.")

    def _change_config(self, cmd, save=False):
        """Enter configuration mode and change single parameter."""

        self._enter_config_mode()

        try:
            self.write_readline(cmd)
        except ControllerError as err:
            self._exit_config_mode()
            err.add_note("Occurred in 'Configuration Mode'.")
            err.add_note(f"Command was: {cmd}")
            raise

        if not save:
            store = input("Store new configuration permanently (yes/NO)? ")
            if store.lower() in ['yes', 'y']:
                save = True
        if save:
            self.write_readline("WRITE")
            if self.verbose:
                print("New configuration stored permanently.")

        self._exit_config_mode()

    def save_config(self):
        """Store current configuration permanently."""
        if not self._is_config_mode:
            self._enter_config_mode()
        self.write_readline("WRITE")
        if self.verbose:
            print("New configuration stored permanently.")
        self._exit_config_mode()

    # -------- ######## CONFIGURATION PARAMETERS ######## --------

    @property
    def commutation_angle(self):
        """Motor commutation angle from 0 to 359 in electrical degrees"""
        ans = self.write_readline("ANGLE")
        return int(ans.strip())

    @commutation_angle.setter
    def commutation_angle(self, value):
        """Motor commutation angle from 0 to 359 in electrical degrees"""
        self._check_int_in_range(value, vmin=0, vmax=359)
        self._change_config(f"ANGLE:{value:03d}")

    @property
    def commutation_angle_offset(self):
        """
        Offset (0-99 degrees) added (if CW) or subtracted (if CCW) to the
        base motor commutation angle.

        Requires disabling of Stop and Lock operation (lockmode=0).
        """
        ans = self.write_readline("OFFSET")
        return int(ans)

    @commutation_angle_offset.setter
    def commutation_angle_offset(self, value):
        """Commutation angle offset (0-99 degrees)."""
        self._check_int_in_range(value, vmin=0, vmax=99)
        self._change_config(f"OFFSET:{value:02d}")

    @property
    def commutation_type(self):
        """Commutation type (6: encoder+hall, 7: encoder only)."""
        ans = self.get_dump(2)["Commutations Type"]
        return int(ans)

    @commutation_type.setter
    def commutation_type(self, value):
        """Commutation type (6: encoder+hall, 7: encoder only)."""
        self._check_int_in_range(value, vmin=6, vmax=7)
        self._change_config(f"TYPE:{value:02d}")

    @property
    def _is_analog_control(self):
        """Verify if driver is in Analog Control mode."""
        ans = self.get_dump(4)["Analog 1-In/0-Out"]
        return bool(int(ans))

    @property
    def _control_mode(self):
        """Drive control mode (Analog Torque, Analog Velocity, FLL)."""
        return "Analog" if self._is_analog_control else "Frequency Lock Loop"

    @_control_mode.setter
    def _control_mode(self, value):
        """Drive control mode (Analog Torque, Analog Velocity, FLL)."""
        if value.upper() not in ['ANALOG', 'FLL', 'FREQUENCY LOCK LOOP']:
            raise ValueError("'value' must be ANALOG or FLL.")
        if value.upper() == 'ANALOG':
            self._change_config("ANALOGIN")
        else:
            self._change_config("ANALOGOUT")

    @property
    def _is_auxiliary_pll(self):
        """Verify if the auxiliary PLL option is active."""
        ans = self.get_dump(4)["Auxiliary PLL 1-In/0-Out"]
        return bool(int(ans))

    def _auxiliary_pll_enable(self):
        """
        Enable auxiliary PLL option (Phase Lock Loop with respect to am
        external high frequency PECL reference), if equipped.

        The PECL signal is first divided to a once per revolution pulse
        and then multiplied up using the auxiliary PLL to the encoder line
        rate for FLL velocity control.
        """
        self._change_config("AUXPLL:IN")

    def _auxiliary_pll_disable(self):
        """
        Disable auxiliary PLL option (Phase Lock Loop with respect to an
        external high frequency PECL reference).
        """
        self._change_config("AUXPLL:OUT")

    @property
    def _clocksperrev(self):
        """
        Number of master clock (external high frequency PECL refeerence) pulses
        per motor revolution.

        This value is used to divide the master clock and generate a
        synchronized one pulse per revolution signal.

        Used only if the auxiliary PLL option is equipped and active.
        """
        ans = self.get_dump(4)["Clocks per rev"]
        return int(ans)

    @_clocksperrev.setter
    def _clocksperrev(self, value):
        """Master clock (Ext. High Freq PLL) pulses per motor revolution."""
        self._check_int_in_range(value, vmin=0, vmax=999999)
        self._change_config(f"CLOCKSPERREV:{value:06d}")

    @property
    def _is_master_clock_divider(self):
        """Master clock (Ext. High Freq PLL) frequency divider status."""
        ans = self.get_dump(4)["Clocks per rev mode 1-On/0-Off"]
        return bool(int(ans))

    def _master_clock_divider_enable(self):
        """Enable master clock (Ext. High Freq PLL) frequency divider."""
        self._change_config("CLOCKSPERREVMODE:ON")

    def _master_clock_divider_disable(self):
        """Disable master clock (External High Freq PLL) frequency divider."""
        self._change_config("CLOCKSPERREVMODE:OFF")

    @property
    def _is_handheld_terminal(self):
        """Handheld terminal status, if equipped."""
        ans = self.get_dump(5)["1-Enable/0-Disable handheld"]
        return bool(int(ans))

    def _handheld_terminal_enable(self):
        """Enable handheld terminal, if equipped."""
        self._change_config("ENABLEHH")

    def _handheld_terminal(self):
        """Disable handheld terminal."""
        self._change_config("DISABLEHH")

    def fault_auxiliary_disable(self):
        self._change_config(f"DISFAULTS:080{self._disfaults_wxyz[-1]}")

    def fault_auxiliary_enable(self):
        self._change_config(f"DISFAULTS:000{self._disfaults_wxyz[-1]}")

    def fault_air_bearing_disable(self):
        wxyz = int(self._disfaults_wxyz, base=16) + 1
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_air_bearing_enable(self):
        wxyz = int(self._disfaults_wxyz, base=16) - 1
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_clamp_air_disable(self):
        wxyz = int(self._disfaults_wxyz, base=16) + 2
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_clamp_air_enable(self):
        wxyz = int(self._disfaults_wxyz, base=16) - 2
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_main_air_disable(self):
        wxyz = int(self._disfaults_wxyz, base=16) + 4
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_main_air_enable(self):
        wxyz = int(self._disfaults_wxyz, base=16) - 4
        self._change_config(f"DISFAULTS:{wxyz:04x}")

    def fault_over_speed_disable(self):
        wxyz = int(self._disfaults_wxyz, base=16) + 8
        self._change_config(f"disfaults:{wxyz:04x}")

    def fault_over_speed_enable(self):
        wxyz = int(self._disfaults_wxyz, base=16) - 8
        self._change_config(f"disfaults:{wxyz:04x}")

    @property
    def _disfaults_wxyz(self):
        """Hexadecimal weights used to disable/enable faults."""
        ans = self.write_readline("DISFAULTS?")
        ans_hex = format(int(ans), '04x')
        return ans_hex

    @_disfaults_wxyz.setter
    def _disfaults_wxyz(self, wxyz):
        """Hexadecimal weights used to disable/enable faults."""
        if isinstance(wxyz, int):
            wxyz = str(wxyz)
        if not isinstance(wxyz, str):
            raise TypeError("'wxyz' must be int or str.")
        if len(wxyz) > 4:
            raise ValueError("'wxyz' must be a 4 digit integer.")
        wxyz = (4-len(wxyz))*'0' + wxyz
        self._change_config("DISFAULTS:%s" % wxyz)

    def _eth_check_addr(self, address):
        try:
            addr = ipaddress.ip_address(address)
            addr = '.'.join(f'{int(n):03d}' for n in str(addr).split('.'))
            return addr
        except ValueError:
            if self.verbose:
                print("Invalid address/netmask: %s" % address)

    @property
    def _eth_macddr(self):
        """Driver IP address (dotted decimal format)."""
        return self.get_ethernet_config(retdict=True)['MAC Address']

    @property
    def _eth_ipaddr(self):
        """Driver IP address (dotted decimal format)."""
        return self.get_ethernet_config(retdict=True)['IP Address']

    @_eth_ipaddr.setter
    def _eth_ipaddr(self, address):
        """Driver IP address (dotted decimal format)."""
        address = self._eth_check_addr(address)
        if address is not None:
            self._change_config(f"IPADDR:{address}")

    @property
    def _eth_gateway(self):
        """Driver IP address (dotted decimal format)."""
        return self.get_ethernet_config(retdict=True)['Gateway']

    @_eth_gateway.setter
    def _eth_gateway(self, address):
        """Gateway address (dotted decimal format)."""
        address = self._eth_check_addr(address)
        if address is not None:
            self._change_config(f"GWADDR:{address}")

    @property
    def _eth_dnsaddr(self):
        """Driver DNS address (dotted decimal format)."""
        return self.get_eth_config(retdict=True)['Domain Name Server']

    @_eth_dnsaddr.setter
    def _eth_dnsaddr(self, address):
        """Driver DNS address (dotted decimal format)."""
        address = self._eth_check_addr(address)
        if address is not None:
            self._change_config(f"DNSADDR:{address}")

    @property
    def _eth_submask(self):
        """Driver subnet mask address (dotted decimal format)."""
        return self.get_eth_config(retdict=True)['Subnet Mask']

    @_eth_submask.setter
    def _eth_submask(self, address):
        """Driver subnet mask address (dotted decimal format)."""
        address = self._eth_check_addr(address)
        if address is not None:
            self._change_config(f"SUBMASK:{address}")

    @property
    def encoder_count(self):
        """Encoder line count."""
        ans = self.get_dump(2)["Encoder Line Count"]
        return int(ans)

    @encoder_count.setter
    def encoder_count(self, value):
        """Encoder line count."""
        self._check_int_in_range(value, vmin=1, vmax=99999)
        self._change_config(f"ENCODERCOUNT:{value:06d}")

    @property
    def encoder_type(self):
        """Encoder type."""
        ans = self.get_dump(5)["1-Single-ended/0-Differential Encoder Type"]
        return "differential" if ans == "0" else "single-ended"

    def _encoder_type_single_ended(self):
        """Set the encoder type to single-ended."""
        self._change_config("ENCODERTYPE:S")

    def _encoder_type_differential(self):
        """Set the encoder type to differential."""
        self._change_config("ENCODERTYPE:D")

    @property
    def _enable_signal(self):
        """
        Driver enable signal (internal/external).

        If 'external' the drive will be enabled using the external enable
        signal on SC2, pin 6. In particular, a TTL low will enable the
        amplifier.
        If FLL speed control is used, the amplifier will also use the
        external direction input on SC2, pin 13 to determine what direction
        to run (TTL low = CW, TTL high/floating = CCW).

        If 'internal' the drive is enable through the ENABLE command and
        direction is controller through the CW and CCW commands.
        """
        ans = self.get_dump(4)["1-Enable/0-Disable external input mode"]
        return "internal" if ans == "0" else "external"

    @_enable_signal.setter
    def _enable_signal(self, value):
        """Driver enable signal (internal/external)."""
        if value.upper() not in ["EXTERNAL", "INTERNAL"]:
            raise ValueError("'value' must be 'external' or 'internal'.")
        if value.upper() == "INTERNAL":
            self._change_config("INTEN")
        else:
            self._change_config("EXTEN")

    @property
    def ext_trig_divider(self):
        """
        External triggering signal frequency divider (default=1).

        Used to add a divider to the FLL velocity control loop feedback when
        in external FLL mode. This is used if the maximum frequency available
        to the LA2000 is limited. Setting FEEDDIV to anything other than 1
        allows a higher speed for a given input frequency, but reduces
        performance, particularly low speed jitter.
        Choices are 1, 2, 4, and 8, with a default of 1.
        """
        ans = self.get_dump(4)["FLL Feedback Divisor"]
        return int(ans)

    @ext_trig_divider.setter
    def ext_trig_divider(self, divider):
        """External triggering signal frequency divider (default=1)."""
        if not isinstance(divider, int):
            raise TypeError("'divider' must be int.")
        if divider not in [1, 2, 4, 8]:
            raise ValueError("'divider' must be 1, 2, 4 or 8.")
        self._change_config(f"FEEDDIV:{divider}")

    def ext_trig_Hz_to_RPM(self, freq_Hz):
        speed_RPM = freq_Hz/self.encoder_count*self.ext_trig_divider*60
        return speed_RPM

    def ext_trig_RPM_to_Hz(self, speed_RPM):
        freq_Hz = (speed_RPM*self.encoder_count)/(self.ext_trig_divider*60)
        return freq_Hz

    @property
    def ext_trig(self):
        """
        External trigger mode (on/off).

        If on, FLL is performed relative to the external frequency reference.

        External reference must be connected to the optional BNC jack or the
        BDC compatibility connector.

        The relationship between input frequency and motor speed is given by:
        f_Hz = (speed_RPM * encoder_resolution) / (FEEDDIV * 60Hz)
        speed_RPM = f_Hz/encoder_resolution * (FEEDDIV * 60Hz)
        """
        ans = self.get_dump(3)["1-External/0-Internal FLL Frequency"]
        return "ON" if ans == "1" else "OFF"

    @ext_trig.setter
    def ext_trig(self, value):
        """External trigger mode (on/off)."""
        if value.upper() not in ["ON", "OFF"]:
            raise ValueError("'value' must be 'ON' or 'OFF'.")
        if value.upper() == "OFF":
            self._change_config("FLLINTERNAL")
        else:
            self._change_config("FLLEXTERNAL")

    def ext_trig_off(self):
        """Disable external trigger: FLL on internal frequency reference."""
        self.ext_trig = "OFF"

    def ext_trig_on(self):
        """Enable external trigger: FLL on external frequency reference."""
        self.ext_trig = "ON"

    @property
    def ext_trig_gain(self):
        """
        Proportional gain of the digital portion of the FLL loop.

        Valid range is -3 to 3, expressed in powers of two to result in
        gain settings from 1/8 to 8. The default and recommended setting is 0
        for unity digital gain.
        """
        ans = self.get_dump(5)["FLL gain"]
        return int(ans)

    @ext_trig_gain.setter
    def ext_trig_gain(self, gain):
        """Proportional gain of the digital portion of the FLL loop."""
        self._check_int_in_range(gain, vmin=-3, vmax=3)
        self._change_config(f"FLLGAIN:{gain}")

    @property
    def _lockmode(self):
        """Stop and lock operation mode (0=disabled, 1=enabled)."""
        ans = self.write_readline("LOCKMODE?")  # not available in DUMP
        return int(ans)

    @_lockmode.setter
    def _lockmode(self, value):
        """Stop and lock operation mode (0=disabled, 1=enabled)."""
        self._check_int_in_range(value, vmin=0, vmax=1)
        self._change_config(f"LOCKMODE:{value}")

    @property
    def _is_lockmode(self):
        """Check if stop and lock mode is enabled."""
        return True if self.lockmode == 1 else False

    @property
    def _minspdmode(self):
        """Disable drive when speed < 'stop speed' (0=disabled, 1=enabled)."""
        ans = self.get_dump(4)["Min Speed 1-In/0-Out"]
        return int(ans)

    @_minspdmode.setter
    def _minspdmode(self, value):
        """Disable drive when speed < 'stop speed' (0=disabled, 1=enabled)."""
        self._check_int_in_range(value, vmin=0, vmax=1)
        if value == 0:
            self._change_config("MINSPDIN")
        else:
            self._change_config("MINSPDOUT")

    @property
    def _is_minspdmode(self):
        """Check if minimum speed detection mode (MNSPD) is enabled."""
        return True if self.minspdmode == 1 else False

    @property
    def _motor_poles(self):
        """Motor number of magnetic poles."""
        ans = self.get_dump(2)["Pole Count"]
        return int(ans)

    @_motor_poles.setter
    def _motor_poles(self, value):
        """Motor number of magnetic poles."""
        self._check_int_in_range(value, vmin=1, vmax=99)
        self._change_config(f"POLES:{value:02d}")

    @property
    def speed_max(self):
        """Speed at which an over speed fault occurs (RPM)."""
        ans = self.write_readline("OVERSPEED?")
        return int(ans)

    @speed_max.setter
    def speed_max(self, value):
        """Speed at which an over speed fault occurs (RPM)."""
        maxspeed = int(self.speed_setpoint_max)
        if value > 1.1*maxspeed:
            raise ValueError("'speed_max' cannot be more than 10% greater " +
                             f"than 'speed_setpoint_max'={maxspeed:04d}.")
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self._change_config(f"OVERSPEED:{value:05d}")

    @property
    def _is_positioner(self):
        """Check if the optional positioner is installed."""
        ans = self.get_dump(5)["Positioner installed"]
        return bool(int(ans))

    @property
    def _positioner(self):
        """Positioner configuration."""
        ans = self.write_readline("POSGAINS", eol=b'\n\n', timeout=0.5)
        print(ans+'\n')

    @property
    def _positioner_captured(self):
        """Hardware captured position."""
        ans = self.write_readline("POSCAP?")
        return int(ans)

    def _positioner_enable(self):
        """Enable positioning mode (requires positioner option)."""
        self.write_readline("POSMODE:1")

    def _positioner_disable(self):
        """Disable positioning mode."""
        self.write_readline("POSMODE:0")

    def _positioner_goto(self, position):
        """Go to a specified position (counts)."""
        if not isinstance(position, int):
            raise TypeError("'position' must be int.")
        self.write_readline("POS:%05d" % position)

    def _positioner_goto_ref(self):
        """Go to the hardware captured ref position."""
        self.write_readline("POS:CAP")

    @property
    def _positioner_proportional_gain(self):
        ans = self.get_dump(5)["Proportional gain"]
        return int(ans)

    @_positioner_proportional_gain.setter
    def _positioner_proportional_gain(self, value):
        self._check_in_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSPGAIN:{value:04d}")

    @property
    def _positioner_derivative_gain(self):
        ans = self.get_dump(5)["Derivative gain"]
        return int(ans)

    @_positioner_derivative_gain.setter
    def _positioner_derivative_gain(self, value):
        self._check_in_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSDGAIN:{value:04d}")

    @property
    def _positioner_derivative_sample_time(self):
        """Positioner PID derivative sample time (for positioner option)."""
        ans = self.get_dump(5)["Derivative sample time"]
        return int(ans)

    @_positioner_derivative_sample_time.setter
    def _positioner_derivative_sample_time(self, value):
        """Positioner PID derivative sample time (for positioner option)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSDSAMPLE:{value:04d}")

    @property
    def _positioner_integral_gain(self):
        """Positioner integral gain (for positioner option)."""
        ans = self.get_dump(5)["Integral gain"]
        return int(ans)

    @_positioner_integral_gain.setter
    def _positioner_integral_gain(self, value):
        """Positioner PID integral limit (for positioner option)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSIGAIN:{value:04d}")

    @property
    def _positioner_integral_limit(self):
        """Positioner PID integral limit (for positioner option)."""
        ans = self.get_dump(5)["Integral limit"]
        return int(ans)

    @_positioner_integral_limit.setter
    def _positioner_integral_limit(self, value):
        """Positioner PID integral limit (for positioner option)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSILGAIN:{value:04d}")

    @property
    def _positioner_acceleration(self):
        """Positioner acceleration (RPM/sec)."""
        ans = self.get_dump(5)["Acceleration"]
        return int(ans)

    @_positioner_acceleration.setter
    def _positioner_acceleration(self, value):
        """Positioner acceleration (RPM/sec)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSACCEL:{value:04d}")

    @property
    def _positioner_deceleration(self):
        """Positioner deceleration (RPM/sec)."""
        ans = self.get_dump(5)["Deceleration"]
        return int(ans)

    @_positioner_deceleration.setter
    def _positioner_deceleration(self, value):
        """Positioner deceleration (RPM/sec)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSDECEL:{value:04d}")

    @property
    def _positioner_max_velocity(self):
        """Positioner max velocity (RPM/sec)."""
        ans = self.get_dump(5)["Maximum Velocity"]
        return int(ans)

    @_positioner_max_velocity.setter
    def _positioner_max_velocity(self, value):
        """Positioner max velocity (RPM/sec)."""
        self._check_int_in_range(value, vmin=1, vmax=9999)
        self._change_config(f"POSMAXVEL:{value:04d}")

    @property
    def _positioner_pid(self):
        """Positioner PID gain parameters (for positioner option)."""
        dump5 = self.get_dump(5)
        proportional_gain = int(dump5["Proportional gain"])
        integral_gain = int(dump5["Integral gain"])
        derivative_gain = int(dump5["Derivative gain"])
        return (proportional_gain, integral_gain, derivative_gain)

    @_positioner_pid.setter
    def _positioner_pid(self, pid):
        """Positioner PID gain parameters (for positioner option)."""
        proportional_gain, integral_gain, derivative_gain = pid
        if proportional_gain is not None:
            self._check_int_in_range(proportional_gain, vmin=1, vmax=9999)
            self._change_config(f"POSPGAIN:{proportional_gain:04d}")
        if integral_gain is not None:
            self._check_int_in_range(integral_gain, vmin=1, vmax=9999)
            self._change_config(f"POSIGAIN:{integral_gain:04d}")
        if derivative_gain is not None:
            self._check_int_in_range(derivative_gain, vmin=1, vmax=9999)
            self._change_config(f"POSDGAIN:{derivative_gain:04d}")

    @property
    def _positioner_jerk(self):
        """Jerk (acceleration rate) for S-curve positioning (RPM/sec2)."""
        ans = self.write_readline("JERK?")  # not available in DUMP
        return int(ans)

    @_positioner_jerk.setter
    def _positioner_jerk(self, value):
        """Jerk (acceleration rate) for S-curve positioning (RPM/sec2)."""
        self._check_int_in_range(value, vmin=0, vmax=9999)
        self._change_config(f"JERK:{value:04d}")

    @property
    def speed_start(self):
        """Speed at which the motor is enabled (if MINSPD is active)."""
        ans = self.get_dump(4)["Start Speed"]
        return int(ans)

    @speed_start.setter
    def speed_start(self, value):
        """Speed at which the motor is enabled (if MINSPD is active)."""
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self._change_config(f"STARTS:{value:04d}")

    @property
    def speed_stop(self):
        """Speed at which the motor is disabled (if MINSPD is active)."""
        ans = self.get_dump(3)["Stop Speed"]
        return int(ans)

    @speed_stop.setter
    def speed_stop(self, value):
        """Speed at which the motor is disabled (if MINSPD is active)."""
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self._change_config(f"STOP:{value:04d}")

    @property
    def speed_setpoint_max(self):
        """Max speed that can be set as a target (RPM)."""
        ret = self.write_readline("TOPSPEED?")
        return int(ret.strip())

    @speed_setpoint_max.setter
    def speed_setpoint_max(self, value):
        """Max speed that can be set as a target (RPM)."""
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self._change_config("TOPSPEED:%05d" % value)

    @property
    def ramp_winlimit(self):
        """
        Speed ramping window (tenths of a percent of external ref frequency).

        If WINLIMIT=30:
        - a change in the ref frequency of 3% or more will cause the drive to
          perform a controller ramp to the new speed.
        - a change in the ref frequency of less than 3% will cause a step
          response in speed.

        WINLIMIT has no effet in FLLINTERNAL or RAMPOFF.
        """
        ans = self.get_dump(4)["Window Limit"]
        return int(ans)

    @ramp_winlimit.setter
    def ramp_winlimit(self, winlimit):
        """Speed ramping window (per-mille of external ref frequency)."""
        self._check_int_in_range(winlimit, vmin=1, vmax=99)
        self._change_config(f"WINLIMIT:{winlimit:02d}")

    # -------- ######## NORMAL MODE PARAMETERS ######## --------

    @property
    def is_over_temperature(self):
        """Queries the amplifier over temperature status."""
        ret = self.write_readline("AOT?", eol=b'\n\n')
        return False if ret == "OK" else True

    @property
    def ramp_acceleration(self):
        """Acceleration set point during a ramp (RPM/sec)."""
        ans = self.write_readline("ACCEL?", eol=b'\n\n')
        return int(ans)

    @ramp_acceleration.setter
    def ramp_acceleration(self, value):
        """Acceleration set point during a ramp (RPM/sec)."""
        self._check_int_in_range(value, vmin=1, vmax=65535)
        self.write_readline(f"ACCEL:{value:05d}")

    @property
    def ramp_deceleration(self):
        """Deeleration set point during a ramp (RPM/sec)."""
        ans = self.write_readline("DECEL?")
        return int(ans)

    @ramp_deceleration.setter
    def ramp_deceleration(self, value):
        """deceleration set point during a ramp (RPM/sec)."""
        self._check_int_in_range(value, vmin=1, vmax=65535)
        self.write_readline(f"DECEL:{value:05d}")

    @property
    def is_at_speed(self):
        """Queries if motor is at the target speed."""
        ret = self.write_readline("ATSPD?", eol=b'\n\n')
        return True if ret == "YES" else False

    def _ccw(self):
        """Set counterclockwise rotation."""
        self.write_readline("CCW")

    def _clamp(self):
        """Clamp the motor."""
        self.write_readline("CLAMP")

    def encoder_clearzeropos(self):
        """Exit encoder set-up state and returns to normal operation mode."""
        self.write_readline("CLEARZEROPOS")

    def get_config(self):
        """Get minimal configuration info."""
        ans = self.write_readline("CONFIG?", eol=b'\n\n', timeout=0.5)
        print(ans+'\n')

    @property
    def peak_current(self):
        """Get approximate peak motor current (A)."""
        ret = self.write_readline("CURRENTQ", eol=b'\r\n\n')
        return float(ret)

    def _cw(self):
        """Set the clockwise rotation."""
        self.write_readline("CW")

    @property
    def direction(self):
        """Motor direction (CW or CCW)."""
        return self.write_readline("DIR?", eol=b'\n\n')

    @direction.setter
    def direction(self, value):
        if value.upper() not in ["CW", "CCW"]:
            raise ValueError("'direction' must be 'CW' or 'CCW'.")
        if value.upper() == "CCW":
            self.ccw()
        else:
            self.cw()

    def disable(self):
        """Coast motor to zero speed (no current applied to motor)."""
        self.write_readline("DISABLE")

    def enable(self):
        """Enables controller and accelerate to speed setpoint."""
        self.write_readline("ENABLE")

    @property
    def is_enabled(self):
        """Queries if drive is enabled."""
        ret = self.write_readline("ENABLE?", eol=b'\n\n')
        return True if ret == "ENABLED" else False

    def _get_disfaults(self):
        """Get disabled faults."""
        ans = self.write_readline("DISFAULTS?")
        return int(ans)

    def get_disfltq(self, retdict=False):
        """Get which faults are enabled and which are disabled."""
        lines = self.write_readlines("DISFLTQ")
        if not retdict:
            for line in lines:
                print(line)
            print()
        else:
            enabled, disabled = [], []
            for line in lines:
                if 'ENABLED' in line.strip():
                    line = line.replace("ENABLED", "").strip()
                    enabled.append(line)
                else:
                    line = line.replace("DISABLED", "").strip()
                    disabled.append(line)
            out = {'ENABLED': enabled, 'DISABLED': disabled}
            return out

    def _get_faults_disabled(self):
        """Get what faults are disabled."""
        return self.get_disfltq(retdict=True)['DISABLED']

    def _get_faults_enabled(self):
        """Get what faults are enabled."""
        return self.get_disfltq(retdict=True)['ENABLED']

    def get_dump(self, dump_index=None):
        if dump_index is None:
            klist = [1, 2, 3, 4, 5]
        elif isinstance(dump_index, int):
            klist = [dump_index]
        elif isinstance(dump_index, list):
            klist = dump_index
        else:
            raise TypeError("'dump_index' must be None, int or list.")
        if any(k not in [1, 2, 3, 4, 5] for k in klist):
            raise ValueError("'dump_index' must contain only 1, 2, 3, 4 or 5.")
        out = {}
        for k in klist:
            if k <= 4:
                eol = b'\n\n'
            else:
                eol = b'\n\n\n'
            dump = self.write_readline(f"DUMP{k}", eol=eol, timeout=0.5)
            dump = dump.split(";")[:-1]
            for line in dump:
                if "On power up:" in line:
                    out["On power up"] = line.split(":")[-1].strip()
                elif "Positioner:" in line:
                    line = line.replace("Positioner:", "")
                    par, val = line.split("=")
                    out[par.strip()] = val.strip()
                else:
                    par, val = line.split("=")
                    out[par.strip()] = val.strip()
        return out

    def _fan_on(self):
        """Turn on the cooling fan."""
        self.write_readline("FAN:ON")

    def _fan_off(self):
        """
        Turn off the cooling fan.

        For protection, the fan will turn on when the heatsink temperature
        reaches approx 50 degC.
        """
        self.write_readline("FAN:OFF")

    @property
    def is_fan_on(self):
        """Check if cooling fan is on."""
        ans = self.write_readline("FAN?")
        return True if not bool(int(ans)) else False

    @property
    def is_fault(self):
        """Queries if drive is in fault."""
        ret = self.write_readline("FAULT?", eol=b'\n\n')
        return True if ret == "FAULT" else False

    def get_faults(self, retdict=False):
        """Get faults status."""
        lines = self.write_readlines("FLTQ")
        if not retdict:
            for line in lines:
                print(line)
            print()
        else:
            lines = [line.strip() for line in lines]
            untripped, tripped = [], []
            for line in lines:
                if 'OK' in line.strip():
                    line = line.replace("OK", "").strip()
                    if 'fault' in line:
                        entry = line.split("fault")[0].strip()
                    elif 'Fault' in line:
                        entry = line.split("Fault")[0].strip()
                    else:
                        entry = line.split("0200")[0].strip()
                    untripped.append(entry)
                else:
                    line = line.replace("FAULT", "").strip()
                    if 'fault' in line:
                        entry = line.split("fault")[0].strip()
                    elif 'Fault' in line:
                        entry = line.split("Fault")[0].strip()
                    else:
                        entry = line.split("0200")[0].strip()
                    tripped.append(entry)
            out = {'UNTRIPPED': untripped, 'TRIPPED': tripped}
            return out

    def _get_faults_tripped(self):
        """Get tripped (active) faults."""
        return self.get_faults(retdict=True)['TRIPPED']

    def _get_faults_untripped(self):
        """Get untripped faults."""
        return self.get_faults(retdict=True)['UNTRIPPED']

    def get_faults_queue(self):
        """Get faults status."""
        ans = self.write_readline("FLTQREADQ")
        print(ans.strip())

    @property
    def heatsink_temperature(self):
        """Amplifier (controller) heat sink temperature (degC)."""
        ret = self.write_readline("HEATSINKT", eol=b'\r\n\n')
        return float(ret)

    def get_id(self, retdict=False):
        """
        Get controller identification.

        "Serial": drive serial number
        "Version": installed software version number
        "Configuration": amplifier configuration code
        """
        ans = self.write_readline("ID?", eol=b'\n\n')
        ans = ans.split(";")
        ans = [a.strip() for a in ans[:-1]]
        ans[0] = "Controller: " + ans[0]
        ans[-3] = "-".join(ans[-3:])
        ans = ans[:-2]
        if not retdict:
            print("\n".join(ans))
            print()
        else:
            out = {}
            for a in ans:
                par, val = a.split(":")
                out[par.strip()] = val.strip()
            return out

    def get_ethernet_config(self, retdict=False):
        """Get the network IP address and other network information."""
        lines = self.write_readlines("IPLIST", nb_lines=8)
        lines = [line.strip() for line in lines[1:]]
        if not retdict:
            for line in lines:
                print(line)
        else:
            out = {}
            for line in lines[1:-1]:
                par = line[:19].strip()
                val = line[21:].strip()
                out[par] = val
            return out

    @property
    def is_logic_power_fault(self):
        """Queries the logic power fault status."""
        ret = self.query("LPR")
        return False if ret == "OK" else True

    def menu(self):
        """Get command list."""
        lines = self.write_readlines("MENU", nb_lines=29, timeout=2)
        for line in lines:
            print(line.strip())
        print()

    @property
    def _motor_period_error(self):
        """
        Number of counts of error for one motor revolution.

        Used only in clocks per rev mode with the auxiliary PLL.
        """
        ans = self.write_readline("MOTORPERIODERROR?")
        return int(ans)

    @property
    def encoder_position(self):
        """Actual motor position relative to the index."""
        ans = self.write_readline("POSQ?")
        return int(ans)

    def ramp_on(self):
        """Activates the controlled acceleration ramping.

        Use set_ramp_acceleration() and set_ramp_deceleration() to control
        the ramp acceleration/deceleration value.

        Ramp is on in the default power-up state.

        Ramping can be used with either internal or external reference FLL
        velocity control.
        """
        self.write_readline("rampin")

    def ramp_off(self):
        """Deactivates the controlled acceleration ramping.

        If ramp is off, the motor will  accelerate at the maximum possible
        rate, dependent on inertia, load, etc. in an open loop manner.

        This mode should also be used if the drive is in external FLL mode
        and the command frequency is ramped in a controlled manner by customer
        supplied electronics, to provide a desired speed profile, such as a
        constant linear velocity spiral.
        """
        self.write_readline("rampout")

    @property
    def is_ready(self):
        """Queries if the drive can be enabled."""
        ret = self.write_readline("READY?", eol=b'\n\n')
        return True if ret == "YES" else False

    def _relay_on(self):
        """Close motor output relays to allow the motor to run."""
        self.write_readline("RELAYRUN")

    def _relay_off(self):
        """Disable the drive and open motor output relays."""
        self.write_readline("RELAYSTOP")

    def run(self):
        """
        Enables the controller and commands the motor to accelerate to the
        requested speed.
        """
        self.write_readline("RUN")

    def encoder_setzeropos(self):
        """Enter encoder set-up mode."""
        self.write_readline("SETZEROPOS")

    @property
    def speed_setpoint(self):
        """
        Motor speed setpoint (RPM).

        Used only in FLL speed controlled systems using the internal
        synthesizer frequency reference.

        This setpoint (target) speed is displayed as "Speed" in
        get_statq() or get_plist().
        """
        ans = self.write_readline("SPEED")
        return float(ans)

    @speed_setpoint.setter
    def speed_setpoint(self, value):
        """Motor speed setpoint (RPM)."""
        self._check_int_in_range(value, vmin=0, vmax=65535)
        self.write_readline("SPEED:%05d" % value)

    @property
    def speed(self):
        """Motor current speed (RPM)."""
        ans = self.write_readline("SPD", eol=b'\n\n')
        return float(ans)

    def get_statq(self, retdict=False):
        """Get controller status through STATQ."""
        lines = self.write_readlines("STATQ", nb_lines=11)
        if not retdict:
            for line in lines:
                print(line.strip())
            print()
        else:
            lines = [line.strip() for line in lines]
            out = {}
            for line in lines:
                par = line[:25].strip()[:-5]
                val = line[25:].strip()
                out[par] = val
            return out

    def stop(self):
        """
        Decelerate to zero speed and disable the controller when the motor
        speed is less than the stop speed.
        """
        self.write_readline("STOP")

    def _stoplock(self):
        """
        Bring the motor to a controlled stop and then locks it in position.
        Requires using unlock() to resume normal operation.
        Used only if LOCKMODE option (Configuration Mode) is set to ‘1’.
        """
        if self.is_lockmode:
            self.write_readline("STOPLOCK")
        elif self.verbose:
            print("ERROR: command available only if 'lockmode'=1.")

    @property
    def torque_max(self):
        """Max torque (percent of full scale)."""
        ret = self.write_readline("TORQUE", eol=b'\n\n')
        return int(ret)

    @torque_max.setter
    def torque_max(self, value):
        """Max torque (percent of full scale)."""
        self._check_int_in_range(value, vmin=1, vmax=100)
        self.write_readline("TORQUE:%03d" % value)

    def _unclamp(self):
        """Unclamp the motor."""
        self.write_readline("unclamp")

    def _unlock(self):
        """
        Releases the lock on the motor.

        The motor will now be in a disabled state (Internal Enable systems)
        or resume external control (Torque or External Enable systems).

        unlock() must be used to clear a stoplock() before
        normal operation may resume!
        """
        if self.is_lockmode:
            self.write_readline("UNLOCK")
        elif self.verbose:
            print("ERROR: command available only if 'lockmode'=1.")

    @property
    def is_zero_speed(self):
        """Queries the zero speed status."""
        ret = self.write_readline("ZERO?", eol=b'\n\n')
        return True if ret == "YES" else False

    # -------- ######## UNDOCUMENTED COMMANDS ######## --------

    def get_plist(self):
        """Get list of configuration parameters."""
        lines = self.write_readlines("PLIST", nb_lines=15, timeout=2)
        for line in lines:
            print(line.strip())
        print()

    @property
    def speed_average(self):
        ans = self.write_readline("SPDAVG?")
        return float(ans)

    # ------ ######## SHORTCUTS OR STATUS DISPLAY ######## --------

    def start(self):
        self.stop()
        self.run()

    def get_status(self, retlist=False):
        """Get status."""
        status = []
        fault = self.is_fault
        if fault:
            print("System is in fault:")
            self.get_faults()
            return
        status.append(f"Fault: {self.is_fault}")
        status.append(f"Direction: {self.direction}")
        status.append(f"At speed: {self.is_at_speed}")
        status.append(f"Zero speed: {self.is_zero_speed}")
        status.append(f"Enabled: {self.is_enabled}")
        status.append(f"Ready: {self.is_ready}")
        status.append(f"Position: {self.encoder_position} steps")
        spd = self.speed
        status.append(f"Speed: {spd:g} RPM ({(spd/60):g} Hz)")
        setp = self.speed_setpoint
        status.append(f"Set point: {setp:g} RPM ({(setp/60):g} Hz)")
        status.append(f"Peak Current: {self.peak_current} A")
        heatsinkt = self.heatsink_temperature
        status.append(f"Heatsink Temperature: {heatsinkt} \u00b0C")
        if not retlist:
            print('\n'.join(status)+'\n')
        else:
            return status

    def live_status(self, refresh_time=1):
        while True:
            try:
                status = self.get_status(retlist=True)
                if status is None:
                    break
                for line in status:
                    print(line)
                time.sleep(refresh_time)
                line_from_bottom = 0
                for line in status:
                    print("\033[1A", end="")  # line up
                    line_from_bottom += 1
                for line in status:
                    print("\x1b[2K")  # line clear
                    line_from_bottom += 1
                for line in status:
                    print("\033[1A", end="")  # line up
                    line_from_bottom -= 1
                time.sleep(0.001)
            except KeyboardInterrupt:
                for k in range(line_from_bottom):
                    print("\033[A", end="")  # line up
                for line in status:
                    print(line)
                print(end="\r")
                break

    def __str__(self):
        out = ["Manufacturer: MCS"]
        ctrl_id = self.get_id(retdict=True)
        for par, val in ctrl_id.items():
            out.append(par+": "+val)
        out.append(f"URL: '{self.comm.name}'")
        return "\n".join(out)

    def __repr__(self):
        return "MCS LA-2000 controller obj"
