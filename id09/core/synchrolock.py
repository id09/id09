__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "11/12/2024"
__version__ = "0.0.1"

# import numpy as np
# import time

try:
    import bliss.comm.util
    from bliss import global_map
    get_comm = bliss.comm.util.get_comm
    is_bliss = True
except ImportError:
    is_bliss = False


class Synchrolock:
    """
    Class to control the Coherent Synchrolock controller.

    The Synchro-Lock Advance Performance (SLAP) allows synchronization and
    of the Micra output with an external RF/4 source.

    The laser cavity length is adjusted through 3 actuators:
    - stepper motor --> output coupler mirror M8 (coarse)
    - low-frequency PZT (galvo) --> "starter" mirror M4 (mid-range)
    - high-frequency PZT (bias) --> "tweeter" mirror M7 (fine)

    """

    def __init__(self, config, url="id09brainbox02:9001", timeout=0.5,
                 gain=50, fundamental_phase_shift=0, harmonic_phase_shift=0,
                 fundamental_enable=False, harmonic_enable=False,
                 external_trigger=False, motor_step_size='small',
                 verbose=True):

        if config is not None:
            url = config.get("url", url)
            timeout = config.get("timeout", timeout)
            gain = config.get("gain", gain)
            fundamental_phase_shift = config.get("fundamental_phase_shift",
                                                 fundamental_phase_shift)
            harmonic_phase_shift = config.get("harmonic_phase_shift",
                                              harmonic_phase_shift)
            fundamental_enable = config.get("fundamental_enable",
                                            fundamental_enable)
            harmonic_enable = config.get("harmonic_enable", harmonic_enable)
            external_trigger = config.get("external_trigger", external_trigger)
            motor_step_size = config.get("motor_step_size", motor_step_size)

        conf = {"tcp-proxy": {"external": True, "tcp": {"url": url}}}
        opts = {"timeout": timeout}
        self.comm = get_comm(conf, **opts)

        self._url = url
        self.timeout = timeout

        # following paramters are needed for the MC and MD command
        self._par0 = False
        self._par1 = True
        self._fundamental_enable = fundamental_enable
        self._gain = gain
        if gain <= 120:
            self._gain_range = 'low'
        else:
            self._gain_range = 'high'
        self._harmonic_enable = harmonic_enable
        self._par5 = False
        self._external_trigger = external_trigger
        self._motor_step_size = motor_step_size

        self._fundamental_phase_shift = fundamental_phase_shift
        self._harmonic_phase_shift = harmonic_phase_shift

        global_map.register(self, children_list=[self.comm])

        self.init()
        self._update_mc()
        self._update_md()
        # self.set_phase_shift(fundamental_phase_shift, harmonic_phase_shift)

    def write(self, cmd):

        if not cmd.endswith("\r"):
            cmd += "\r"

        self.comm.write(cmd.encode())

    def write_readline(self, cmd, timeout=None):

        if not cmd.endswith("\r"):
            cmd += "\r"

        if timeout is None:
            timeout = self.timeout

        ans = self.comm.write_readline(cmd.encode(), eol=b'\r',
                                       timeout=timeout)

        ans = ans.decode()

        return ans

    def send(self, cmd):
        self.write_readline(cmd=cmd)

    def query(self, cmd):
        ans = self.write_readline(cmd=cmd)
        try:
            float(ans)
        except Exception as e:
            print(e)
        return float(ans)

    def init(self):
        """Initialize Synchrolock controller."""
        self.write("")
        self.write("")
        self.write("CPASTEP")
        self.write("MS100")

    def _update_mc(self):
        self._mc = 0
        self._mc += 2**0 * self._par0
        self._mc += 2**1 * self._par1
        self._mc += 2**2 * self._fundamental_enable
        self._mc += 2**3 * (self._gain_range == 'high')
        self._mc += 2**4 * self._harmonic_enable
        self._mc += 2**5 * self._par5
        self._mc += 2**6 * self._external_trigger
        self.write("MC%d" % self._mc)

    def _update_md(self):

        if self._gain <= 120:
            if self._gain_range != 'low':
                self._gain_range = 'low'
                self._update_mc()
            md_gain = (7 - self._gain % 8) * 16 + self._gain // 8
        elif self._gain > 120:
            if self._gain_range != 'high':
                self._gain_range = 'high'
                self._update_mc()
            md_gain = (7 - (self._gain - 53) % 8) * 16 + (self._gain - 53) // 8

        md_gain += 2**7 * (self._motor_step_size == 'small')

        self._md = md_gain

        self.write("MD%d" % self._md)

    @property
    def gain(self):
        return self._gain

    @gain.setter
    def gain(self, value):

        if not isinstance(value, int):
            raise TypeError("'value' must be int.")

        if value < 0 or value > 173:
            raise ValueError("'value' must be in the range [0, 173].")

        self._gain = value

        self._update_md()

    @property
    def frequency_error(self):
        """Frequency difference between reference and signal (kHz)."""
        return self.query("REA") / 100

    def _conv(self, x, scale=1):
        return (2*x/(2**12 - 1) - 1) * scale

    @property
    def galvo_voltage(self):
        """Low-frequency PZT voltage [-2, 2] Volt."""
        return self._conv(self.query("RD1"), scale=5)

    @property
    def pzt_voltage(self):
        """High-frequency PZT voltage [10, 130] Volt."""
        return self._conv(self.query("RD3"), scale=142)

    @property
    def photodiode_current(self):
        """Slave photodiode current [0, 5]."""
        return self._conv(self.query("RD5"), scale=5)

    @property
    def harmonic_phase_error(self):
        """Phase difference between reference and signal (-5, 5)."""
        return self._conv(self.query("RD6"), scale=5)

    @property
    def fundamental_phase_error(self):
        """Phase difference of ref and signal 9-th harmonics (-5, 5)."""
        return self._conv(self.query("RD7"), scale=5)

    @property
    def pzt_current(self):
        """High-frequency PZT current."""
        return self._conv(self.query("RD9"), scale=1)

    @property
    def phase_error(self):
        return self.fundamental_phase_error, self.harmonic_phase_error

    def fundamental_enable(self):
        self._fundamental_enable = True
        self._update_mc()

    def fundamental_disable(self):
        self._fundamental_enable = False
        self._update_mc()

    def harmonic_enable(self):
        self._harmonic_enable = True
        self._update_mc()

    def harmonic_disable(self):
        self._harmonic_enable = True
        self._update_mc()

    @property
    def motor_step_size(self):
        return self._motor_step_size

    @motor_step_size.setter
    def motor_step_size(self, value):
        if value.lower() not in ['small', 'large']:
            raise ValueError("'value' must be 'small' or 'large'.")
        self._motor_step_size = value
        self._update_md()

    def _get_pa(self, bit=None):
        out = int(self.write_readline("PA"))
        out = f'{out:08b}'
        if bit is not None:
            out = out[-bit]
        return out

    @property
    def stepper_motor_direction(self):
        pa = int(self._get_pa(bit=3))
        if pa == 0:
            self._stepper_motor_direction = 'left'
        else:
            self._stepper_motor_direction = 'right'
        return self._stepper_motor_direction

    # @property
    # def is_stepper_motor_busy(self):
    #     pa = int(self._get_pa(bit=4))
    #     return bool(pa)

    def stepper_motor_move(self, steps, direction='left', step_size='small'):
        direction = direction.lower()
        if direction not in ['left', 'right']:
            raise ValueError("'direction' must be 'left' or 'right'.")
        if direction == 'left':
            for k in range(steps):
                self.send("LAR1000")
                self.send("g")
        else:
            for k in range(steps):
                self.send("LAL1000")
                self.send("g")

    @property
    def fundamental_phase_shift(self):
        return self._fundamental_phase_shift

    @fundamental_phase_shift.setter
    def fundamental_phase_shift(self, value):
        if value is not None:
            self.send("VSA%d" % value)
            self._fundamental_phase_shift = value

    @property
    def harmonic_phase_shift(self):
        return self._harmonic_phase_shift

    @harmonic_phase_shift.setter
    def harmonic_phase_shift(self, value):
        if value is not None:
            self.send("VSB%d" % value)
            self._harmonic_phase_shift = value

    @property
    def phase_shift(self):
        return self.fundamental_phase_shift, self.harmonic_phase_shift

    @phase_shift.setter
    def phase_shift(self, fundamental_shift, harmonic_shift):
        self.fundamental_phase_shift = fundamental_shift
        self.harmonic_phase_shift = harmonic_shift

    def _status(self):
        out = []
        out.append(f"P/D Det. Slave = {self.photodiode_current:.2f}")
        out.append(f"Frequency (error) = {self.frequency_error:.2f} kHz")
        out.append(f"Galvo Voltage = {self.galvo_voltage:.2f} V")
        out.append(f"PZT Voltage = {self.pzt_voltage:.2f} V")
        out.append(f"PZT Current = {self.pzt_current:.2f} A")
        out.append(f"Fund. Phase Error = {self.fundamental_phase_error:.3f}")
        out.append(f"Harm. Phase Error = {self.harmonic_phase_error:.3f}")
        out.append(f"Fund. Phase Shift = {self.fundamental_phase_shift:d}")
        out.append(f"Harm. Phase Shift = {self.harmonic_phase_shift:d}")
        return out

    def __info__(self):
        out = self._status()
        out.insert(0, "Coherent Synchrolock Bliss controller:")
        return '\n'.join(out)
