# -*- coding: utf-8 -*-
"""Classes to control the Sutter Lamda SC laser shutter."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "09/01/2025"
__version__ = "0.0.1"

from bliss.comm.util import get_comm
from bliss import global_map


class LambdaSC:
    """Class to remotely control the Sutter Lambda SC laser shutter."""

    def __init__(self, config, url="id09brainbox02:9001", timeout=0.5,
                 ttl_in_mode='high_triggers_open',
                 ttl_out_mode='high_on_shutter_open'):

        if config is not None:
            url = config.get("url", url)
            timeout = config.get("timeout", timeout)
            ttl_in_mode = config.get("ttl_in_mode", ttl_in_mode)
            ttl_out_mode = config.get("ttl_out_mode", ttl_out_mode)

        conf = {"tcp-proxy": {"external": True, "tcp": {"url": url}}}
        opts = {"timeout": timeout}
        self.comm = get_comm(conf, **opts)

        self.timeout = timeout
        self.ttlindefault = ttl_in_mode
        self.ttloutdefault = ttl_out_mode

        global_map.register(self, children_list=[self.comm])

        # a reset() is needed every time the controller is switched off
        # it can also help to regain communication in some cases
        # in other cases, a hardware reboot is needed

        self.reset()

        # self.stop_free_run()
        # self.ext_trig_off()
        # self.fast_motion_on()
        # self.ttl_out_on()

    def reset(self):
        """Puts controller on-line."""
        self.comm.write(bytes.fromhex("EE"))
        ans = self.comm.read()
        if ans.hex() != 'ee':
            print("WARNGING: answer is not echo!")
        try:
            ans = self.comm.readline(eol=b'\r')
        except Exception:
            print("ERROR: operation not completed.")

    def write_readline(self, cmd, timeout=None):

        if timeout is None:
            timeout = self.timeout

        ans = self.comm.write_readline(bytes.fromhex(cmd), eol=b'\r',
                                       timeout=timeout)

        ans = ans.hex()

        return ans

    def open(self):
        """Set the state of the shutter to open."""
        self.write_readline("AA")

    def close(self):
        """Set the state of the shutter to closed."""
        self.write_readline("AC")

    def free_run_stop(self):
        """Stop the free run if currently running."""
        self.write_readline("BF")
        try:
            self.comm.read()
        except Exception:
            pass

    def _get_status(self):
        """Get unit status."""
        ans = self.comm.write_readline(bytes.fromhex("CC"), eol=b'\r',
                                       timeout=0.5)
        ans = ans[1:]
        values = []
        for k in range(len(ans)):
            values.append(ans[k:k+1].hex())
        status = {}
        status['state'] = values[0]
        status['hdw_mode'] = values[1]
        if status['hdw_mode'] == 'de':
            status["neutral_density_microsteps"] = values[2]
        else:
            status["neutral_density_microsteps"] = None
        status['lead_in_byte'] = values[-16]
        status['ttl_in'] = values[-15]
        status['ttl_out'] = values[-14]
        status['delay_timer'] = ''.join(values[-13:-8])
        status['exposure_timer'] = ''.join(values[-8:-3])
        status['free_run'] = ''.join(values[-3:])
        return status

    @property
    def is_open(self):
        status = self._get_status()
        if status['state'] == "aa":
            return True
        else:
            return False

    @property
    def state(self):
        """Get shutter state (open or closed)."""
        status = self._get_status()
        if status['state'] == 'aa':
            return "open"
        else:
            return "closed"

    @property
    def hdw_mode(self):
        """Hardware mode of operation of the SmartShutter.

        Return
        ------
        hdw_mode : str
            - 'fast': motion is optimized for the fastest open and
                      close times.
            - 'slow': less vibration at the expense of slower open
                      and close times (also called 'soft').
            - 'neutral density': the user selects the extent to which
                                 the shutter opens - the opening can be
                                 selected in steps from 1 (no opening)
                                 to 144 (complete opening).
        """
        status = self._get_status()
        if status['hdw_mode'] == 'db':
            return 'shutter not connected'
        elif status['hdw_mode'] == 'dc':
            return 'fast'
        elif status['hdw_mode'] == 'dd':
            return 'slow'
        elif status['hdw_mode'] == 'de':
            return 'neutral density'
        else:
            return 'unknown'

    @property
    def ttl_in_mode(self):
        """TTL IN (trigger) mode."""
        status = self._get_status()
        if status['ttl_in'] == 'a0':
            return 'off'
        elif status['ttl_in'] == 'a1':
            return 'high triggers open'
        elif status['ttl_in'] == 'a2':
            return 'low triggers close'
        elif status['ttl_in'] == 'a3':
            return 'toggle on rising edge'
        elif status['ttl_in'] == 'a4':
            return 'toggle on falling edge'
        else:
            return 'unknown'

    @property
    def ext_trig_mode(self):
        """External trigger mode."""
        return self.ttl_in_mode

    @property
    def ttl_out_mode(self):
        """TTL OUT mode."""
        status = self._get_status()
        if status['ttl_out'] == 'b0':
            return 'disabled'
        elif status['ttl_out'] == 'b1':
            return 'high on shutter open'
        elif status['ttl_out'] == 'b2':
            return 'low on shutter open'
        else:
            return 'unknown'

    @property
    def free_run_status(self):
        """Shutter free run status."""
        status = self._get_status()
        fr = status['free_run'][:2]
        if fr == 'f0':
            return 'off'
        elif fr == 'f1':
            return 'run on power on'
        elif fr == 'f2':
            return 'run on trigger pulse'
        elif fr == 'f3':
            return 'run now'
        else:
            return 'unknown'

    @property
    def nb_cycles(self):
        """Shutter number of cycles."""
        status = self._get_status()
        fr1 = status['free_run'][2:4]
        fr2 = status['free_run'][4:6]
        _nb_cycles = (int(fr1, 16) << 8) | int(fr2, 16)
        return _nb_cycles

    def power_on_motor(self):
        """Instruct the controller to power on the motor."""
        self.write_readline("CE")

    def power_off_motor(self):
        """Instruct the controller to power off the motor."""
        self.write_readline("CF")

    def fast_motion_on(self):
        """Set the shutter to fast mode."""
        self.write_readline("DC")

    def fast_motion_off(self):
        """Set the shutter to soft mode."""
        self.write_readline("DD")

    def _fa(self, cmd):
        self.comm.write_read(bytes.fromhex("FA"), timeout=self.timeout)
        self.write_readline(cmd)

    def ext_trig_off(self):
        """Disable external trigger."""
        self._fa("A0")

    def _ttl_in_high_triggers_open(self):
        """Enable external trigger. TTL IN high opens the shutter."""
        self._fa("A1")

    def _ttl_in_low_triggers_open(self):
        """Enable external trigger. TTL IN low opens the shutter."""
        self._fa("A2")

    def _ttl_in_rising_edge_toggles(self):
        """Enable external trigger. TTL IN rising edge toggles state."""
        self._fa("A3")

    def _ttl_in_falling_edge_toggles(self):
        """Enable external trigger. TTL IN falling edge toggles state."""
        self._fa("A4")

    def ext_trig_on(self):

        ttl_in_modes = ["high_triggers_open",
                        "low_triggers_open",
                        "rising_edge_toggles",
                        "falling_edge_toggles"]

        if self.ttlindefault not in ttl_in_modes:
            raise Exception("'ttlindefault' must be one of",
                            ttl_in_modes)

        if self.ttlindefault == 'high_triggers_open':
            self._ttl_in_high_triggers_open()
        elif self.ttlindefault == 'low_triggers_open':
            self._ttl_in_low_triggers_open()
        elif self.ttlindefault == 'rising_edge_toggles':
            self._ttl_in_rising_edge_toggles()
        elif self.ttlindefault == 'falling_edge_toggles':
            self._ttl_in_falling_edge_toggles()

    def ttl_out_off(self):
        """Disable TTL OUT signal."""
        self._fa("B0")

    def _ttl_out_high_on_shutter_open(self):
        """Enable TTL OUT signal. TTL OUT high when shutter is open."""
        self._fa("B1")

    def _ttl_out_low_on_shutter_open(self):
        """Enable TTL OUT signal. TTL OUT low when shutter is open."""
        self._fa("B2")

    def ttl_out_on(self):
        """Enable TTL OUT signal."""

        ttl_out_modes = ["high_on_shutter_open",
                         "low_on_shutter_open"]

        if self.ttloutdefault not in ttl_out_modes:
            raise Exception("'ttloutdefault' must be one of",
                            ttl_out_modes)

        if self.ttloutdefault == 'high_on_shutter_open':
            self._ttl_out_high_on_shutter_open()
        elif self.ttloutdefault == 'low_on_shutter_open':
            self._ttl_out_low_on_shutter_open()

    def config_restore_default(self):
        """Restore the controller factory default configuration."""
        self._fa("C0")

    # def config_save_current(self):
    #     """Save current configuration to the controller."""
    #     self._fa('C1')

    # def config_load_saved(self):
    #     """Load last saved controller configuration."""
    #     self._fa("FB")

    @property
    def model(self):
        """Get controller and shutter model.

        Return
        ------
        ctrl_type : str
            Has the form 'SC-vV.SS', where V is the firmware version and
            SS is the firmware subversion.
        shutter_type : str
            If 'S-IQ': SmartShutter
        """
        self.comm.write_read(bytes.fromhex("FD"))
        ctrl_type = self.comm.read(8, timeout=0.1).decode()
        shutter_type = self.comm.readline(eol=b'\r', timeout=0.1).decode()
        return ctrl_type, shutter_type

    def __info__(self):
        model = self.model
        out = "SUTTER LambdaSC\n"
        out += f"controller: {model[0]}\n"
        out += f"shutter: {model[1]}"
        return out
