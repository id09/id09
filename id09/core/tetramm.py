# -*- coding: utf-8 -*-
"""Module to communicate with Tetramm picoammeter."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "25/06/2024"
__version__ = "0.0.1"


import struct
import numpy as np
from bliss.comm.util import get_comm, TCP


class Tetramm:

    def __init__(self, config=None, url='id09tetramm1', port=10001,
                 timeout=0.005, debug=False, verbose=True):

        if debug:
            print("url:", url)
            print("port:", port)

        if config is not None:
            url = config.get("tcp")["url"]
            port = config.get("port")
            timeout = config.get("timeout", timeout)
            debug = config.get("debug", debug)
            verbose = config.get("verbose", verbose)

        conf = {'tcp': ''}
        opts = {'url': url, 'port': port}
        self.comm = get_comm(conf, ctype=TCP, **opts)

        self.url = url
        self.timeout = timeout
        self.debug = debug
        self.verbose = verbose

        # --- setup ---

        ans = self.write_readline("VER").split(":")
        self._firmware_version = ans[2]
        self.nch = ans[3].split()[0].strip("IV")
        rg0 = ans[3].split()[1][:-1].lower() + "A"
        rg1 = ans[3].split()[2][:-1].lower() + "A"
        self.all_ranges = {0: rg0, 1: rg1}
        bias_max = "+" if ans[4].split()[2] == "POS" else "-"
        bias_max += ans[4].split()[1]
        self._bias_max = bias_max
        self.is_bias = False if self.bias == -1 else True
        self.naq = 1
        self.ntrg = 1
        self.nrsamp = 100  # rate = 100e3/100
        self.nrsamp = 1000  # hc5719
        self.trg_off()  # sets 'is_trg' to False
        self.data_eol = ''

    def write_readline(self, cmd, eol='\r\n'):
        if not cmd.endswith('\r\n'):
            cmd += '\r\n'
        ans = self.comm.write_readline(cmd.encode(), eol=eol)
        ans = ans.decode()
        return ans

    def query(self, cmd):
        if not cmd.endswith(':?'):
            cmd += ':?'
        ans = self.write_readline(cmd)
        ans = ans.split(':')[-1]
        return ans

    @property
    def nch(self):
        self._nch = int(self.query("CHN"))
        return self._nch

    @nch.setter
    def nch(self, value):
        """Set number of active channels (1, 2 or 4)."""
        value = int(value)
        if value not in [1, 2, 4]:
            raise ValueError("'value' must be 1, 2 or 4")
        self.write_readline("CHN:%d" % value)
        self._nch = value

    @property
    def naq(self):
        self._naq = int(self.query("NAQ"))
        return self._naq

    @naq.setter
    def naq(self, value):
        if not isinstance(value, (int, np.int64)):
            raise TypeError("'naq' must be int")
        self.write_readline("NAQ:%d" % value)
        self._naq = value

    @property
    def nrsamp(self):
        """Number of averaged sampled data per single acquisition."""
        ans = self.query("NRSAMP")
        if self.debug:
            print("'nrsamp' = ", ans)
        self._nrsamp = int(ans)
        self._data_rate = 100e3 / self._nrsamp
        return self._nrsamp

    @nrsamp.setter
    def nrsamp(self, value):
        """Set the number of averaged sampled data."""
        if not isinstance(value, int):
            raise TypeError("'nrsamp' must be int.")
        self.write_readline("NRSAMP:%d" % value)
        self._nrsamp = value
        self._data_rate = 100e3 / self._nrsamp

    @property
    def data_rate(self):
        """Sampling rate (100 kHz) / number of averaged samples."""
        rate = 100e3 / self.nrsamp
        if self.debug:
            print("'data_rate' = 100e3 / %d = %g" % (self.nrsamp, rate))
        return rate

    @data_rate.setter
    def data_rate(self, value):
        """Sampling rate (100 kHz) / number of averaged samples.

        The max data rate transfer is:
        - 200 Hz when ASCII format is enabled (NRSAMP=500)
        - 20 kHz when ASCII format is disabled (NRSAMP=5)

        """
        self.nrsamp = int(100e3 / value)

    @property
    def ntrg(self):
        self._ntrg = int(self.query("NTRG"))
        return self._ntrg

    @ntrg.setter
    def ntrg(self, value):
        if not isinstance(value, (int, np.int64)):
            raise TypeError("'ntrg' must be int")
        self.write_readline("NTRG:%d" % value)
        self._ntrg = value

    def trg_on(self):
        self.write_readline("TRG:ON")
        self.is_trg = True

    def trg_off(self):
        self.write_readline("TRG:OFF")
        self.is_trg = False

    def bias_on(self):
        """Enable bias source and set voltage to zero."""
        self.write_readline("HVS:ON")
        self.is_bias = True

    def bias_off(self):
        """Disable bias source and set voltage to zero."""
        self.write_readline("HVS:OFF")
        self.is_bias = False

    @property
    def bias(self):
        """Get bias voltage set point (V) or -1 if OFF (needed for DS)."""
        ans = self.query("HVS")
        if self.debug:
            print(f"TETRAMM: bias --> {ans}")
        return -1 if ans == 'OFF' else float(ans)

    @bias.setter
    def bias(self, value):
        """Set bias voltage set point (V)."""

        if not isinstance(value, (int, float)):
            raise TypeError("'value' must int or float.")

        if self.debug:
            print(f"TETRAMM: {value} --> bias")

        if value == -1:
            self.bias_off()
            return
        elif value < 0:
            raise ValueError("'value' must be a positive value or -1 (OFF).")
        elif value > int(self._bias_max[:-1]):
            raise ValueError(f"'value' must be < {self._bias_max[:-1]}")
        elif not self.is_bias:
            self.bias_on()

        value = float(value)
        self.write_readline("HVS:%f" % value)

    def get_bias_current(self):
        """Get measured bias current (A)."""
        return float(self.query("HVI"))

    def get_bias_voltage(self):
        """Get measured bias voltage (V)."""
        return float(self.query("HVV"))

    def get_register_status(self):
        """Get TetrAMM register status (48 bits)."""
        ans = self.query("STATUS")
        bits = bin(int(ans, 16))[2:]
        bits = bits[::-1]
        if len(bits) < 47:
            bits += (47-len(bits))*'0'
        return bits

    def get_range(self, channel='all', raw=False):
        """Get current range of all channels or of a selected channel."""

        channels = ("all", "1", "2", "3", "4")
        if str(channel).lower() not in channels:
            raise ValueError("'channel' should be one of ", channels)

        if isinstance(channel, str) and channel.lower() != 'all':
            channel = int(channel)

        bits = self.get_register_status()
        ch_rawrng = [bits[24], bits[28], bits[32], bits[36]]
        ch_autorng = [bool(int(bit)) for bit in bits[16:20]]

        if raw:
            return ch_rawrng, ch_autorng

        ch_rng = []
        for autorng, rawrng in zip(ch_autorng, ch_rawrng):
            rng = self.all_ranges[int(rawrng)]
            if autorng:
                ch_rng.append(f"{rng} (AUTO ON)")
            else:
                ch_rng.append(rng)

        if isinstance(channel, str) and channel.lower() == "all":
            return ch_rng
        else:
            return ch_rng[channel-1]

    def set_range(self, value='auto', channel='all'):
        """Set current range of all channels or of a selected channel."""

        if not isinstance(value, (str, int, float)):
            raise TypeError("'value' must be str, int or float.")

        channels = ("all", "1", "2", "3", "4")
        if str(channel).lower() not in channels:
            raise ValueError("'channel' should be one of ", channels)

        if isinstance(value, float):
            all_values = [r[:-2] for r in self.all_ranges.values()]
            all_units = [r[-2] for r in self.all_ranges.values()]
            unit = {'n': 1e-9, 'u': 1e-6, 'm': 1e-3}
            all_ranges = [float(v)*unit[u]
                          for v, u in zip(all_values, all_units)]
            if not any(np.isclose(value, all_ranges, rtol=0.25)):
                raise ValueError("'value' must be close to one of", all_ranges)
            for k, r in enumerate(all_ranges):
                if np.isclose(value, r, rtol=0.25):
                    value = k
                    break

        if isinstance(value, int):
            if value not in [0, 1]:
                raise ValueError("'value' must be 0 or 1 if int.")

        if isinstance(value, str):
            all_ranges = [k for k in self.all_ranges.values()]
            all_ranges.append("0")
            all_ranges.append("1")
            if value not in all_ranges and value.lower() != "auto":
                raise ValueError("'value' must be 'auto' or one of ",
                                 all_ranges)
            if value == all_ranges[0] or value == '0':
                value = 0
            elif value == all_ranges[1] or value == '1':
                value = 1
            else:
                value = 'auto'

        # at this point 'value' can be only 'auto', 0 or 1

        if channel == "all":
            cmd = "RNG:%s" % str(value).upper()
            self.write_readline(cmd)
        else:
            initial_ranges, auto_ranges = self.get_range(raw=True)
            # if all channels are in "AUTO", they have to be set to manual
            # before a different setting for each channel can be used
            self.write_readline("RNG:0")
            # apply range to channel
            self.write_readline("RNG:CH%s:%s" % (channel, value))
            # restore all other channels
            for k, r in enumerate(initial_ranges):
                if (k+1) != int(channel):
                    if auto_ranges[k]:
                        self.write_readline("RNG:CH%d:AUTO" % (k+1))
                    else:
                        self.write_readline("RNG:CH%d:%s" % (k+1, r))

    def acq_on(self):
        self.comm.write("ACQ:ON\r\n".encode())

    def acq_off(self):
        # NOTE: the answer to ACQ:OFF might be undecodable
        self.comm.write_readline(b"ACQ:OFF\r\n", eol="ACK\r\n")

    @property
    def data_eol(self):
        return self._data_eol

    @data_eol.setter
    def data_eol(self, value):
        self._data_eol = value

    def empty_buffer(self):
        timeout = self._naq*self._ntrg/self._data_rate
        try:
            self.comm.readline(eol='\r\n', timeout=timeout)
        except Exception:
            pass

    def get_current(self, naq=1, ntrg=1, nch=4):
        """Used only for monitoring"""

        if nch != self._nch:
            self.nch = nch  # set nch

        self.naq = naq  # set naq

        if ntrg != self._ntrg:
            self.ntrg = ntrg  # set ntrg

        self.comm.write("ACQ:ON\r\n".encode())

        self.data_eol = b'\xff\xf4\x00\x03\xff\xff\xff\xff'*(nch+1)
        buf = self.comm.readline(eol=self.data_eol)
        data = self.parse_binary_buffer(buf).flatten()

        return data

    def parse_binary_buffer(self, buf, debug=False):

        nbytes = 8*(self._nch+1)  # number of bytes in a single acquisition

        if buf[-5:] == b'ACK\r\n':
            if debug:
                print("Removing termination 'ACK\\r\\n'.")
            buf = buf[:-5]

        if buf[-8:] == b'\xff\xf4\x00\x03\xff\xff\xff\xff':
            if debug:
                print("Removing termination FFF40003FFFFFFFF sequences.")
            buf = buf[:-nbytes]

        fmt = '>' + 'd'*(self._nch+1)

        data = [struct.unpack(fmt, buf[k:k+nbytes])[:-1]
                for k in range(0, len(buf), nbytes)]

        if self.is_trg:

            naq = self._naq

            if naq == 0:
                print("WARNING: unknown number of acquisitions per each " +
                      "trigger. Cannot remove headers and footers.")
                # although in good cases one can guess naq as:
                naq = len(data) // self._ntrg - 2
                # it is not sure that all acquisitions have the same length
                return data

            if debug:
                print("Removing header FFF40000???????? and footer" +
                      "FFF40001FFFFFFFF sequences from each acquisition.")

            data_range = range(0, len(data), naq+2)
            data = [data[k:k+naq+2][1:-1] for k in data_range]

        data = np.array(data)

        return data
