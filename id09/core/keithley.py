from bliss.comm.gpib import Gpib
from bliss.comm.scpi import SCPI

from id09.unit import convert_float2str


_not_implemented = "NOT IMPLEMENTED"


class Keithley:

    def __init__(self, config=None, url="enet://gpibid9b.esrf.fr", pad=4):
        """
        Parameters
        ----------
        url: str
            Keithley web address.
            OH GPIB controller: "enet://gpibid9a.esrf.fr"
            EH2 GPIB controller: "enet://gpibid9b.esrf.fr"
        pad: int
            Keithley address.
            PD0: 1; PD1: 1; PD2: 2; PD3: 3; PD4: 4.
        """

        if config is not None:
            url = config.get("url")
            pad = config.get("pad")

        self.scpi = SCPI(Gpib(url=url, pad=pad))
        self.idn = self.query("*IDN?")

        # check if Keithley 486, 487 or Keithley 6485, 6487, 6514
        if self.idn.split()[0] == 'KEITHLEY':
            self._old = False
        else:
            self._old = True

        self.model = self.get_model()

        if self.model == '6485':
            self.send("*RST")

        if self.model == '6514':
            self.send("CONF:CURR")
            self.send("INIT")

        if self.model == '486':
            self.send("G1\r\n")
        # self.set_analog_filter_off()
        # self.set_digital_filter_off()
        # self.set_rate_med()
        # self.set_display_digits(6)

    def __info__(self):
        return 'Keithley ' + self.model

    def send(self, cmd):
        self.scpi.interface.write(cmd)

    def query(self, cmd):
        ans = self.scpi.interface.write_readline(cmd)
        ans = ans.decode().strip("\r")
        return ans

    def get_model(self):
        model = None
        if self._old:
            ans = self.query("U2X\r\n")
            ans = self.query("U2X\r\n")
            model = ans[:3]
        else:
            for lbl in self.idn.split(","):
                if 'model' in lbl.lower():
                    model = lbl.split()[-1]
        return model

    def get_current(self):
        if self.model in ['486', '487']:
            try:
                ans = self.query("X\r\n")
                cur = float(ans)
            except Exception:
                cur = 0
        else:
            try:
                if self.model in ['6486', '6487']:
                    ans = self.query("MEAS:CURR:DC?")
                else:
                    ans = self.query("READ?")
                if self.model == '6514':
                    cur = float(ans.split(",")[0])
                else:
                    cur = float(ans.split(",")[0][:-1])
            except Exception:
                cur = 0
        return cur

    def get_range(self):
        """Get current range as string and display if in AUTO mode."""

        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            range_str = status.split("R")[1][:2]
            auto_range = bool(int(range_str[0]))
            all_ranges = self.get_all_ranges()
            cur_range = all_ranges[int(range_str[1])-1]
        else:
            ans = self.query("CURR:RANG:AUTO?")
            auto_range = bool(int(ans))
            ans = self.query("CURR:RANG?")
            cur_range = float(ans)

        cur_range = convert_float2str(cur_range, unit='A', digits=1,
                                      decimals=0, strip_spaces=True)

        if auto_range:
            cur_range += " (AUTO ON)"
        else:
            cur_range += " (AUTO OFF)"

        return cur_range

    def get_all_ranges(self):

        all_ranges = None

        if self.model in ['486', '487']:
            all_ranges = [2e-9, 20e-9, 200e-9,
                          2e-6, 20e-6, 200e-6,
                          2e-3]
        elif self.model in ['6485', '6487']:
            all_ranges = [2e-9, 20e-9, 200e-9,
                          2e-6, 20e-6, 200e-6,
                          2e-3, 20e-3]
        elif self.model == '6514':
            all_ranges = [20e-12, 200e-12, 2e-9,
                          20e-9, 200e-9, 2e-6,
                          20e-6, 200e-6, 2e-3, 20e-3]

        return all_ranges

    def set_range(self, cur_range):

        all_ranges = self.get_all_ranges()
        if cur_range not in all_ranges:
            raise ValueError("'cur_range' must be one of: ", all_ranges)

        if self.model in ['486', '487']:
            range_idx = all_ranges.index(cur_range)
            self.send("R%d\r\n" % (range_idx+1))
        else:
            cmd_str = "CURR:RANG %.1e" % cur_range
            # print(cmd_str)
            self.send(cmd_str)

    def set_range_auto(self):
        if self.model in ['486', '487']:
            self.send("R0\r\n")
        else:
            self.send("CURR:RANG:AUTO ON")

    def set_range_manual(self):
        if self.model in ['486', '487']:
            self.send("R10\r\n")
        else:
            self.send("CURR:RANG:AUTO OFF")

    def _get_nplc(self):
        """Get the A/D conversion rate as number of power-line cycles."""
        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            rate_str = status.split("S")[1][0]
            if rate_str == '0':
                nplc = 1/12
            else:
                nplc = 1
        else:
            ans = self.query("CURR:NPLC?")
            nplc = float(ans)
        return nplc

    def _set_nplc(self, nplc):
        """Set the A/D conversion rate in number of power line cycles."""
        if self.model in ['486', '487']:
            print(_not_implemented)
            print()
        else:
            if not isinstance(nplc, (float, int)):
                raise TypeError("'nplc' must be float.""")
            if (nplc < 0.01) or (nplc > 50):
                raise ValueError("'nplc' must be in the range (0.01, 50).""")
            self.send("CURR:NPLC %g" % nplc)

    def get_rate(self):
        """Get the A/D conversion rate setting (FAST, MED, SLOW)."""
        nplc = self._get_nplc()
        if nplc == 1:
            return "MED (IT=20ms)"
        elif nplc == 5:
            return "SLOW (IT=100ms)"
        elif self._old and nplc == 1/2:
            return "FAST (IT=1.6ms)"
        elif not self._old and nplc == 0.1:
            return "FAST (IT=2ms)"

    def set_rate_slow(self):
        if self.model in ['486', '487']:
            return "SLOW rate not available for models 486 and 487."
        else:
            self._set_nplc(5)

    def set_rate_med(self):
        if self.model in ['486', '487']:
            self.send("S1X\r\n")
        else:
            self._set_nplc(1)

    def set_rate_fast(self):
        if self.model in ['486', '487']:
            self.send("S0X\r\n")
        else:
            self._set_nplc(0.1)

    def get_analog_filter(self):
        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            filters = status.split("P")[1][0]
            if filters in ['2', '3']:
                return 'ON'
            else:
                return 'OFF'
        else:
            ans = self.query("CURR:DAMP?")
            damping_enable = bool(int(ans))
            if damping_enable:
                return 'ON'
            else:
                return 'OFF'

    def set_analog_filter_on(self):
        if self.model in ['486', '487']:
            self.send("P2\r\n")
        else:
            self.send("CURR:DAMP ON")

    def set_analog_filter_off(self):
        if self.model in ['486', '487']:
            self.send("P0")
        else:
            self.send("CURR:DAMP OFF")

    def get_digital_filter(self):
        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            filters = status.split("P")[1][0]
            if filters in ['1', '3']:
                return 'ON'
            else:
                return 'OFF'
        else:
            ans = self.query("AVER?")
            damping_enable = bool(int(ans))
            if damping_enable:
                return 'ON'
            else:
                return 'OFF'

    def set_digital_filter_off(self):
        if self.model in ['486', '487']:
            if self.get_analog_filter:
                self.send("P2\r\n")
            else:
                self.send("P0\r\n")
        else:
            self.send("MED OFF")
            self.send("AVER OFF")

    def set_remote_on(self):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send("REM")

    def set_remote_off(self):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send("GTL")

    def set_display_enable(self):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send(":DISP:ENAB ON")

    def set_display_disable(self):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send(":DISP:ENAB OFF")

    def get_display_intensity(self):
        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            dispint = status.split("A")[1][0]
            if dispint == '0':
                return 'NORMAL'
            elif dispint == '1':
                return 'DIM'
            elif dispint == '2':
                return 'OFF'

    def get_display_digits(self):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            ans = self.query(":DISP:DIG?")
            digits = int(ans)
            return digits

    def set_display_digits(self, digits):
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send("DISP:DIG %d" % digits)

    def get_zerocheck(self):
        """Check if zerocheck is enabled."""
        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            zerocheck = status.split("C")[1][0]
            if zerocheck == '1':
                return 'ON'
            else:
                return 'OFF'
        else:
            ans = self.query(":SYST:ZCH?")
            zerocheck_enable = bool(int(ans))
            if zerocheck_enable:
                return 'ON'
            else:
                return 'OFF'

    def set_zerocheck_on(self):
        if self.model in ['486', '487']:
            self.send("C1\r\n")
        else:
            self.send(":SYST:ZCH ON")

    def set_zerocheck_off(self):
        if self.model in ['486', '487']:
            self.send("C0\r\n")
        else:
            self.send(":SYST:ZCH OFF")

    def get_zerocorrect(self):
        """Check if zerocorrect is enabled."""
        if self.model in ['486', '487']:
            return self.get_zerocheck()
        else:
            ans = self.query(":SYST:ZCOR?")
            zerocorr_enable = bool(int(ans))
            if zerocorr_enable:
                return 'ON'
            else:
                return 'OFF'

    def set_zerocorrect_on(self):
        """Subtract voltage offset from measurement."""
        if self.model in ['486', '487']:
            self.get_zerocheck_on()
        else:
            self.send(":SYST:ZCOR ON")

    def set_zerocorrect_off(self):
        """Disable voltage offset subtraction from measurement."""
        if self.model in ['486', '487']:
            self.get_zerocheck_off()
        else:
            self.send(":SYST:ZCOR OFF")

    def get_autozero(self):
        """Check if autozero (offset and gain ref measurement) is enabled."""
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            ans = self.query(":SYST:AZER?")
            autozero_enable = bool(int(ans))
            if autozero_enable:
                return 'ON'
            else:
                return 'OFF'

    def set_autozero_on(self):
        """Enable autozero (offset and gain ref measurement)."""
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send(":SYST:AZER ON")

    def set_autozero_off(self):
        """Disable autozero (offset and gain ref measurement)."""
        if self.model in ['486', '487']:
            return _not_implemented
        else:
            self.send(":SYST:AZER OFF")

    def get_error_queue(self):
        """Get error queue and clear it."""
        if self.model in ['486', '487']:

            err_codes = {0: "IDDC",
                         1: "IDDCO",
                         2: "No Remote",
                         3: "Self-Test",
                         4: "Trigger Overryb",
                         5: "Conflict",
                         6: "Cal Locked",
                         7: "Zero Check",
                         8: "Calibration",
                         9: "E2PROM Defaults",
                         10: "E2PROM Cal Constants",
                         11: "V-Source Conflict",
                         12: "V-Source"}

            err_cond = self.query("U1X\r\n")[3:]

            errors = []

            for k in range(13):
                if err_cond[k] == '1':
                    errors.append(err_codes[k])

            if len(errors) > 0:
                return ", ".join(errors)
            else:
                return "NO ERRORS"

        else:

            ans1 = self.query(":SYST:ERR:COUN?")
            ans2 = self.query(":SYST:ERR:ALL?")

            print("%d ERROR(S) in queue." % int(ans1))

            return ans2

    def get_data_format(self):
        """Get the format of output data."""

        format_codes = {0: "ASCII readings with prefix",
                        1: "ASCII readings without prefix",
                        2: "ASCII readings and buffer location with prefix",
                        3: "ASCII readings and buffer location without prefix",
                        4: "Binary readings mode 1",
                        5: "Binary readings mode 1",
                        6: "Binary readings mode 1",
                        7: "Binary readings mode 1"}

        if self.model in ['486', '487']:
            status = self.query("U0X\r\n")
            format_str = status.split("G")[1][:1]
            code = int(format_str)
            return format_codes[code]
        else:
            return _not_implemented

    def set_data_format(self, code=1):
        """Set the format of output data."""
        if self.model in ['486', '487']:
            self.send("G%d\r\n" % code)
            out = "Data format is now: "
            out += self.get_data_format()
            return out
        else:
            return _not_implemented
