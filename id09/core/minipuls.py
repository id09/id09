import PyTango

dev = PyTango.DeviceProxy("id09/wcid09e/tg")


class MiniPuls():

    # NOTE
    # If you want to change the pump max speed:
    # - stop the pump by software
    # - change the pump speed manually to the max target value
    # - update the 'max_speed' attribute to the displayed value

    def __init__(self, max_speed=None, direction='cw', verbose=True):

        """
        Parameters
        ----------
        max_speed : float or None
            Max pump speed (RPM).

        """

        self.max_voltage = 5  # volts
        
        if max_speed is None:
            self.max_speed = 40
        else:
            self.max_speed = max_speed

        self.stop()
        
        if direction is not None:
            if direction == 'cw':
                self.cw()
            elif direction == 'ccw':
                self.ccw()

        self.verbose = verbose

    def __repr__(self):
        out = ["Gilson Minipuls-3"]
        out.append("-----------------")
        if self.running:
            out.append("status: RUNNING")
        else:
            out.append("status: STOPPED")
        out.append("direction: %s" % self.direction)
        out.append("max speed = %g RPM" % self.max_speed)
        out.append("max voltage = %g RPM" % self.max_voltage)
        return "\n".join(out)

    @property
    def speed(self):
        return dev.ao3*self.max_speed/self.max_voltage
    
    @speed.setter
    def speed(self, value):
        if value > self.max_speed:
            raise ValueError("'value' cannot be > %g RPM" % self.max_speed)
        dev.ao3 = value/self.max_speed*self.max_voltage
        if self.verbose:
            print("WARNING: the speed displayed on the pump screen will" +
                  "         change only after the pump is started.")


    def cw(self):
        dev.ao1 = 5
        self.direction = 'cw'

    def ccw(self):
        dev.ao1 = 0
        self.direction = 'ccw'

    def invert(self):
        """Invert the rotation direction."""
        if self.direction == 'cw':
            self.ccw()
        else:
            self.cw()

    def start(self):
        dev.ao2 = 0
        self.running = True

    def stop(self):
        dev.ao2 = 5
        self.running = False

