__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "11/12/2024"
__version__ = "0.0.1"

# import numpy as np
# import time

try:
    import bliss.comm.util
    from bliss import global_map
    get_comm = bliss.comm.util.get_comm
    is_bliss = True
except ImportError:
    is_bliss = False


class Micra:
    """Class to control the Coherent Synchrolock controller."""

    _fault_codes = {1: 'Laser Head Interlock Fault',
                    2: 'External Interlock Fault',
                    3: 'PS Cover Interlock Fault',
                    4: 'LBO Temperature Fault',
                    5: 'LBO Not Locked at Set Temp',
                    6: 'Vanadate Temp. Fault',
                    7: 'Etalon Temp. Fault',
                    8: 'Diode 1 Temp. Fault',
                    9: 'Diode 2 Temp. Fault',
                    10: 'Baseplate Temp. Fault',
                    11: 'Heatsink 1 Temp. Fault',
                    12: 'Heatsink 2 Temp. Fault',
                    16: 'Diode 1 Over Current Fault',
                    17: 'Diode 2 Over Current Fault',
                    18: 'Over Current Fault',
                    19: 'Diode 1 Under Volt Fault',
                    20: 'Diode 2 Under Volt Fault',
                    21: 'Diode 1 Over Volt Fault',
                    22: 'Diode 2 Over Volt Fault',
                    25: 'Diode 1 EEPROM Fault',
                    26: 'Diode 2 EEPROM Fault',
                    27: 'Laser Head EEPROM Fault',
                    28: 'Power Supply EEPROM Fault',
                    29: 'PS-Head Mismatch Fault',
                    30: 'LBO Battery Fault',
                    31: 'Shutter State Mismatch',
                    32: 'CPU PROM Checksum Fault',
                    33: 'Head PROM Checksum Fault',
                    34: 'Diode1 PROM Checksum Fault',
                    35: 'Diode2 PROM Checksum Fault',
                    36: 'CPU PROM Range Fault',
                    37: 'Head PROM Range Fault',
                    38: 'Diode1 PROM Range Fault',
                    39: 'Diode2 PROM Range Fault',
                    40: 'Lost Modelock Fault',
                    41: 'Lost Power Track Fault',
                    42: 'Exceeded CW Power Fault',
                    43: 'Below Q-Switch Power Fault',
                    44: 'Ti-Sapph Temp. Fault',
                    45: 'Lost UF Lasing Fault',
                    46: 'PZT X Fault',
                    47: 'PZT Y Fault',
                    48: 'Lost Power Track Fault',
                    51: 'T-Sapph Temp. Fault'}

    def __init__(self, config, url="id09brainbox02:9002", timeout=0.5,
                 verbose=True):

        if config is not None:
            url = config.get("url", url)
            timeout = config.get("timeout", timeout)
            verbose = config.get("verbose", verbose)

        conf = {"tcp-proxy": {"external": True, "tcp": {"url": url}}}
        opts = {"timeout": timeout}
        self.comm = get_comm(conf, **opts)

        self._url = url
        self.timeout = timeout
        self.verbose = verbose

        global_map.register(self, children_list=[self.comm])

        # self.init()
        self.verdi = self._Verdi(self)

    def write(self, cmd):

        if not cmd.endswith("\r\n"):
            cmd += "\r\n"

        self.comm.write(cmd.encode())

    def write_readline(self, cmd, timeout=None):

        if not cmd.endswith("\r\n"):
            cmd += "\r\n"

        if timeout is None:
            timeout = self.timeout

        ans = self.comm.write_readline(cmd.encode(), eol=b"\r\n",
                                       timeout=timeout)

        ans = ans.decode()

        return ans

    def send(self, cmd):
        self.write_readline(cmd=cmd)

    def query(self, cmd):
        ans = self.write_readline(cmd=cmd)
        try:
            float(ans)
        except Exception as e:
            print(e)
        return float(ans)

    def init(self):
        """Initialize Synchrolock controller."""
        self.send("ECHO=0")  # sent character are not echoed
        self.send("PROMPT=0")  # 'Verdi>' prompt is returned after a command

    class _Verdi:

        """
        The Verdi is composed by a laser head and a power supply.

        The power supply contains a diode laser fiber coupled to the laser head
        (FAP = fiber array package). The diode laser is mounted on a heatsink.

        The head contains:
        - the vanadate (Nd:YVO4) gain medium
        - a second harmonic crystal (type I LBO)
        - an etalon (for single frequency stabilization)

        The baseplate of the laser head is connected to a riser/heatsink.

        """

        def __init__(self, parent):
            self._parent = parent

        @property
        def power(self):
            """Verdi output power (W)."""
            return self._parent.query("?P")

        @property
        def power_setpoint(self):
            """Verdi power setpoint (W)."""
            return self._parent.query("?SP")

        @power_setpoint.setter
        def power_setpoint(self, value):
            self._parent.send(f"P={value:1.4f}")

        def power_level_low(self):
            self.power_setpoint = 0.1

        def power_level_high(self):
            self.power_setpoint = 5.48

        @property
        def baseplate_temperature(self):
            return self._parent.query("?BT")

        @property
        def baudrate(self):
            return int(self._parent.write_readline("?B"))

        @property
        def diode_current(self):
            """FAP laser diode current (A)."""
            return self._parent.query("?D1C")

        @diode_current.setter
        def diode_current(self, value):
            self._parent.send(f"D1C={value:.2f}")

        @property
        def diode_voltage(self):
            """FAP laser diode voltage (V)."""
            return self._parent.query("?D1V")

        @property
        def diode_photocell(self):
            """Silicon photodiode voltage (V)."""
            return self._parent.query("?D1PC")

        @property
        def diode_heatsink_temperature(self):
            """FAP laser diode heatsking temperature (degC)."""
            return self._parent.query("?D1HST")

        @property
        def diode_hours(self):
            """FAP laser diode operating hours."""
            return self._parent.query("?D1H")

        @property
        def diode_current_max(self):
            """FAP laser diode max allowed current."""
            return self._parent.query("?D1RCM")

        @property
        def diode_servo(self):
            """FAP laser diode temperature servo status."""
            state = self._parent.write_readline("?D1SS")
            if state == '0':
                return 'open'
            elif state == '1':
                return 'locked'
            elif state == '2':
                return 'seeking'
            elif state == '3':
                return 'fault'
            else:
                return 'unknown'

        @property
        def diode_temperature_setpoint(self):
            """FAP laser diode temperature setpoint (degC)."""
            return self._parent.query("?D1ST")

        @property
        def diode_temperature(self):
            """FAP laser diode measured temperature (degC)."""
            return self._parent.query("?D1T")

        @property
        def etalon_servo(self):
            """Etalon temperature servo status."""
            state = self._parent.write_readline("?ESS")
            if state == '0':
                return 'open'
            elif state == '1':
                return 'locked'
            elif state == '2':
                return 'seeking'
            elif state == '3':
                return 'fault'
            else:
                return 'unknown'

        @property
        def etalon_temperature_setpoint(self):
            """Etalon temperature setpoint (degC)."""
            return self._parent.query("?EST")

        @property
        def etalon_temperature(self):
            """Etaon measured temperature (degC)."""
            return self._parent.query("?ET")

        @property
        def head_hours(self):
            """Laser head operating hours."""
            return self._parent.query("?HH")

        @property
        def keyswitch(self):
            """Controller keyswitch status."""
            ans = self._parent.write_readline("?K")
            if ans == '0':
                return 'off'
            else:
                return 'on'

        @property
        def lbo_temperature_setpoint(self):
            """LBO crystal temperature setpoint (degC)."""
            return self._parent.query("?LBOST")

        @property
        def lbo_temperature(self):
            """LBO crystal measured temperature (degC)."""
            return self._parent.query("?LBOT")

        @property
        def lbo_servo(self):
            """LBO crystal temperature servo status."""
            state = self._parent.write_readline("?LBOSS")
            if state == '0':
                return 'open'
            elif state == '1':
                return 'locked'
            elif state == '2':
                return 'seeking'
            elif state == '3':
                return 'fault'
            elif state == '4':
                return 'optimizing'
            else:
                return 'unknown'

        @property
        def lbo_heater(self):
            """LBO crystal heater status."""
            state = self._parent.write_readline("?LBOH")
            if state == '0':
                return 'off'
            elif state == '1':
                return 'on'
            else:
                return 'unknown'

        def lbo_heater_off(self):
            self._parent.send("LBOH=0")

        def lbo_heater_on(self):
            self._parent.send("LBOH=1")

        @property
        def lightloop_servo(self):
            """Verdi light loop servo status."""
            state = self._parent.write_readline("?LRS")
            if state == '0':
                return 'open'
            elif state == '1':
                return 'locked'
            elif state == '2':
                return 'seeking'
            elif state == '3':
                return 'fault'
            else:
                return 'unknown'

        @property
        def powersupply_hours(self):
            """Power supply operating hours."""
            return self._parent.query("?PSH")

        @property
        def software_version(self):
            """Power supply software version."""
            return self._parent.query("?SV")

        @property
        def vanadate_temperature_setpoint(self):
            """Vanadate crystal temperature setpoint (degC)."""
            return self._parent.query("?VST")

        @property
        def shutter(self):
            """Verdi shutter status."""
            state = self._parent.write_readline("?S")
            if state == '0':
                return 'closed'
            elif state == '1':
                return 'open'
            else:
                return 'unknown'

        def shutter_close(self):
            self._parent.send("SHUTTER=0")

        def shutter_open(self):
            self._parent.send("SHUTTER=1")

        @property
        def vanadate_temperature(self):
            """Vanadate crystal measured temperature (degC)."""
            return self._parent.query("?VT")

        @property
        def vanadate_servo(self):
            """Vanadate temperature servo status."""
            state = self._parent.write_readline("?VSS")
            if state == '0':
                return 'open'
            elif state == '1':
                return 'locked'
            elif state == '2':
                return 'seeking'
            elif state == '3':
                return 'fault'
            else:
                return 'unknown'

        def standby(self):
            self._parent.send("L=0")

        def clear_fault(self):
            self._parent.send("L=1")

        def _get_all(self):
            vals = [v for v in self.__dir__() if not v.startswith('_')]
            pars = [v for v in vals if not callable(self.__getattribute__(v))]
            out = {}
            for par in pars:
                out[par] = self.__getattribute__(par)
            return out

        def _status(self):
            out = []
            out.append(f"Shutter = {self.shutter.upper()}")
            out.append(f"Power = {self.power} W")
            out.append(f"Diode current = {self.diode_current} A")
            out.append(f"Diode voltage = {self.diode_voltage} V")
            out.append(f"Diode photocell voltage = {self.diode_photocell} V")
            out.append("Diode heatsink temperature = " +
                       f"{self.diode_heatsink_temperature} C")
            out.append(f"Diode temperature = {self.diode_temperature} C" +
                       f" ({self.diode_servo})")
            out.append("Baseplate temperature = " +
                       f"{self.baseplate_temperature} C")
            out.append(f"Vanadate temperature = {self.vanadate_temperature} " +
                       f"C ({self.vanadate_servo})")
            out.append(f"LBO temperature = {self.lbo_temperature} C" +
                       f" ({self.lbo_servo})")
            out.append(f"Etalon temperature = {self.etalon_temperature} C" +
                       f" ({self.etalon_servo})")
            out.append(f"Diode hours = {self.diode_hours}")
            out.append(f"Power supply hours = {self.powersupply_hours}")
            return out

        def __info__(self):
            out = self._status()
            out.insert(0, "Coherent Verdi Bliss controller:")
            return '\n'.join(out)

    def standby(self):
        self.verdi.standby()

    def clear_fault(self):
        self.verdi.clear_fault()

    @property
    def is_fault(self):
        # not working correctly
        code = self.write_readline("?L")
        if code == '0' or code == '1':
            return False
        else:
            return True

    def get_fault(self):
        # not working correctly
        code = int(self.write_readline("?F"))
        if code == 0:
            # NOTE: 0 obtained also after powertrack fault!!
            print("System OK\n")
        elif code not in self._fault_codes.keys():
            print(f"ERROR: 'code'={code} not recognized.\n")
        else:
            print(f"System FAULT: {self._fault_codes[code]}\n")

    def get_fault_history(self):
        hist = self.write_readline("?FH")
        codes = [int(f) for f in hist.split(",")]
        for code in codes:
            if code not in self._fault_codes.keys():
                print(f"ERROR: 'code'={code} not recognized.")
            else:
                print(f"System FAULT: {self._fault_codes[code]}")
        print()

    @property
    def automodelock(self):
        """Micra automodelock state."""
        state = self.write_readline("?AMDLK")
        if state == '0':
            return "off"
        elif state == '1':
            return "on"
        else:
            return "uknown"

    def automodelock_off(self):
        self.send("AMDLK=0")

    def automodelock_on(self):
        self.send("AMDLK=1")

    @property
    def lightloop(self):
        """Micra light loop state."""
        state = self.write_readline("?VLL")
        if state == '0':
            return "Verdi Light Loop"
        elif state == '1':
            return "Micra Light Loop"
        else:
            return "unknown"

    @property
    def is_modelocked(self):
        """Micra modelock state."""
        state = self.write_readline("?MDLK")
        if state == '1':
            return True
        else:
            return False

    @property
    def peakhold(self):
        """Micra peakhold state."""
        state = self.write_readline("?PEAK HOLD")
        if state == '0':
            return 'off'
        elif state == '1':
            return 'on'
        else:
            return 'unknown'

    def peakhold_off(self):
        self.send("PEAK HOLD=0")

    def peakhold_on(self):
        self.send("PEAK HOLD=1")

    @property
    def power(self):
        """Micra output power (mW)."""
        return self.query("?UF")

    @property
    def powertrack(self):
        """Micra powertrack state."""
        state = self.write_readline("?PTRK")
        if state == '0':
            return 'off'
        elif state == '1':
            return 'on'
        else:
            return 'unknown'

    def powertrack_off(self):
        """Turn off voltages on PZT actuators."""
        self.send("PTRK=0")

    def powertrack_on(self):
        """Turn on voltages on PZT actuators."""
        self.send("PTRK=1")

    @property
    def pzt_mode(self):
        """Micra piezo control state."""
        state = self.write_readline("?PZTM")
        if state == '0':
            return 'auto'
        elif state == '1':
            return 'manual'
        else:
            return 'unknown'

    def pzt_mode_auto(self):
        """Enable automatic powertrack."""
        self.send("PZTM=0")

    def pzt_mode_manual(self):
        """Disable automatic powertrack and peakhold."""
        self.send("PZTM=1")

    @property
    def pztx(self):
        return self.query("?PZTX")

    @pztx.setter
    def pztx(self, value):
        return self.send(f"PZTX={value:.2f}")

    @property
    def pzty(self):
        return self.query("?PZTY")

    @pzty.setter
    def pzty(self, value):
        return self.send(f"PZTY={value:.2f}")

    def shutter_close(self):
        self.verdi.shutter_close()

    def shutter_open(self):
        self.verdi.shutter_open()

    @property
    def keyswitch(self):
        return self.verdi.keyswitch

    @property
    def head_hours(self):
        return self.verdi.head_hours

    @property
    def diode_hours(self):
        return self.verdi.diode_hours

    def read_data(self, cmd):
        if cmd.startswith('verdi_'):
            cmd = cmd.replace('verdi_', '')
            try:
                value = self.verdi.__getattribute__(cmd)
            except Exception:
                value = None
        else:
            try:
                value = self.__getattribute__(cmd)
            except Exception:
                value = None
        return value

    def _status(self):
        out = self.verdi._status()
        out = ["Verdi " + line for line in out]
        out.append(f"PZT mode = {self.pzt_mode.upper()}")
        out.append(f"PZT X = {self.pztx} V")
        out.append(f"PZT Y = {self.pzty} V")
        out.append(f"Peakhold = {self.peakhold.upper()}")
        out.append(f"Automodelock = {self.automodelock.upper()}")
        out.append(f"Is modelocked? = {self.is_modelocked}")
        out.append(f"Power = {self.power} mW")
        return out

    def __info__(self):
        out = self._status()
        out.insert(0, "Coherent Micra Bliss controller:")
        return '\n'.join(out)
