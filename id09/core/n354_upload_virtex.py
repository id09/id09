#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket
import sys
import array
import os
import time

folder = "/data/id09/archive/Electronic/N354/GUI_program/"

DEFAULT_VIRTEX = os.path.join(folder, "n354id09.bit")


class Data:

    def __init__(self, value):
        if isinstance(value, str) and value.startswith("0x"):
            value = int(value, 16)
        self._value = value

    def as_bytes(self, n=4):
        return self._value.to_bytes(n, "little")

    def as_16bits(self):
        return self.as_bytes(n=2)

    def as_32bits(self):
        return self.as_bytes(n=4)


class N354:

    def __init__(self, ip="id09n354a", port=5001):

        self.address = (ip, port)
        self._sock = socket.socket()
        self._sock.connect(self.address)

    def _send(self, cmd, add_endline=True, encode=True):
        if add_endline and not cmd.endswith("\n"):
            cmd += "\n"
        if encode:
            cmd = cmd.encode("ascii")
        self._sock.send(cmd)

    def _get(self, decode=True):
        ret = self._sock.recv(2048)
        if decode:
            ret = ret.decode("ascii")
        return ret

    def upload_virtex(self, fname):
        if not os.path.isfile(fname):
            raise ValueError("Could not read Virtex file '%s'" % fname)

        # read data (as 2-bytes array, needed for checksum)
        with open(fname, "rb") as f:
            data = f.read()

        # cast data as 2-bytes unsigned integer (needed for checksum)
        data_as_H = array.array("H")
        data_as_H.frombytes(data)
        checksum = sum(data_as_H) & 0xffffffff

        # send 'you are about to upload'
        self._send("*PROGBIT ")

        # from now on, send raw data, use little utility function
        def send(data, nbytes=None):
            if nbytes is not None:
                data = Data(data).as_bytes(nbytes)
            self._send(data, add_endline=False, encode=False)

        # send 'magic' number
        send("0xa5aa555a", nbytes=4)

        # size to send
        send_size = len(data_as_H) + 1
        send(send_size, nbytes=4)

        # send checksum
        send(checksum, nbytes=4)

        # send payload
        data_as_H.append(0)
        send(data_as_H)

    def upload_virtex2(self, fname):
        if not os.path.isfile(fname):
            raise ValueError("Could not read Virtex file '%s'" % fname)

        # read data (as 2-bytes array, needed for checksum)
        with open(fname, "rb") as f:
            fileSizeInBytes = os.path.getsize(fname)
            apdfw = array.array('H')
            apdfw.fromfile(f, int(fileSizeInBytes/2))
       
        progCmd = '*PROGBIT \n'
        self._sock.send(progCmd.encode())
        time.sleep(0.1)

        dataLong ='' ; sync = []
        sync.append( int('0xa5aa555a',  16) )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        dataLong ='' ; sync = [] ;
        arraySize = int(fileSizeInBytes/2) + 1
        sync.append( arraySize )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        dataLong ='' ; sync = [] ;
        checkSum = ( sum( apdfw ) & 0xffffffff )
        sync.append( checkSum )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        apdfw.append( int('0x0000',  16))
        bytesSent = 0
        bytesSent = self._sock.send(apdfw)


    def upload_virtex3(self, fname):
        if not os.path.isfile(fname):
            raise ValueError("Could not read Virtex file '%s'" % fname)

        # read data (as 2-bytes array, needed for checksum)
        with open(fname, "rb") as f:
            fileSizeInBytes = os.path.getsize(fname)
            apdfw = array.array('H')
            apdfw.fromfile(f, fileSizeInBytes/2)
       
        progCmd = '*PROGBIT \n'
        self._sock.send(progCmd)
        time.sleep(0.1)

        dataLong ='' ; sync = []
        sync.append( int('0xa5aa555a',  16) )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        dataLong ='' ; sync = [] ;
        arraySize = (fileSizeInBytes/2) + 1
        sync.append( arraySize )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        dataLong ='' ; sync = [] ;
        checkSum = ( sum( apdfw ) & 0xffffffff )
        sync.append( checkSum )
        dataLong = array.array('L', sync) ;
        self._sock.send(dataLong)
        time.sleep(0.1)

        apdfw.append( int('0x0000',  16))
        bytesSent = 0
        bytesSent = self._sock.send(apdfw)


if __name__ == "__main__":

    if len(sys.argv) == 1:
        print("Usage %s virtex_fname" % sys.argv[0])
        print(" hint: usually virtex files have .bit extension")
    else:
        n = N354()
        fname = sys.argv[1]
        n.upload_virtex3(fname)
