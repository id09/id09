__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "16/09/2024"
__version__ = "0.0.1"

import numpy as np
import time

try:
    import bliss.comm.util
    from bliss import global_map
    get_comm = bliss.comm.util.get_comm
    is_bliss = True
except ImportError:
    is_bliss = False


class ChemyxFusion4000:
    """Class to control the Chemyx Fusion 4000X syringe pump."""

    # syringe library
    _BD_luer_lock = {'1ml': 4.78, '3ml': 8.66, '5ml': 12.07, '10ml': 14.6,
                     '20ml': 19.13, '30ml': 21.69, '60ml': 26.72}

    _Hamilton = {'10ul': 0.48, '25ul': 0.73, '100ul': 1.46, '250ul': 2.3,
                 '1ml': 4.61, '2.5ml': 7.28, '5ml': 10.3, '10ml': 14.57,
                 '25ml': 23.03, '50ml': 32.57}

    def __init__(self, config, url="id09brainbox02:9002", timeout=5,
                 default_pump=1):

        if config is not None:
            url = config.get("url", url)
            timeout = config.get("timeout", timeout)
            default_pump = config.get("default_pump", default_pump)

            self._url = url
            self.timeout = timeout

        conf = {"tcp-proxy": {"external": True, "tcp": {"url": url}}}
        opts = {"timeout": timeout}
        self.comm = get_comm(conf, **opts)

        self.timeout = timeout
        self._default_pump = default_pump

        global_map.register(self, children_list=[self.comm])

    def __info__(self):
        out = ["Chemyx Fusion 4000X Bliss controller"]
        out_pars = self.show_pars(ret=True)
        out_status = self.show_status(ret=True)
        max_length = 0
        for k in range(len(out_pars)):
            if len(out_pars[k][0]) > max_length:
                max_length = len(out_pars[k][0])
        if len(out_status[0][0]) > max_length:
            max_length = len(out_status[0][0])
        max_length += 2
        out.append("")
        out.append("Pump 1".ljust(max_length) + "Pump 2")
        out.append("------".ljust(max_length) + "------")
        for k in range(len(out_pars)):
            out.append(out_pars[k][0].ljust(max_length) + out_pars[k][1])
        out.append(out_status[0][0].ljust(max_length) + out_status[0][1])
        return "\n".join(out)

    @property
    def default_pump(self):
        return self._default_pump

    @default_pump.setter
    def default_pump(self, value):
        if value not in [1, 2]:
            raise ValueError("'value' must be 1 or 2.")
        self._default_pump = value

    def write_readlines(self, cmd, timeout=None):

        if not cmd.endswith("\r"):
            cmd += "\r"

        if timeout is None:
            timeout = self.timeout

        ans = self.comm.write_readline(cmd.encode(), eol=b'>', timeout=timeout)

        ans = ans.decode()

        return ans

    def _get_pars(self):

        def get_dict(pars_str):
            pars_list = pars_str.split("\n\r")
            pars = dict()
            for par in pars_list:
                key, value = par.split("=")
                if 'unit' in key:
                    pars[key.strip()] = int(value.strip())
                else:
                    pars[key.strip()] = float(value.strip())
            return pars

        ans = self.write_readlines("1 view parameters")
        pump1_pars = ans.split("Pump 2:")[0].split("Pump 1:")[-1].strip()
        pump2_pars = ans.split("Pump 2:")[-1].strip()
        pump1_pars = get_dict(pump1_pars)
        pump2_pars = get_dict(pump2_pars)

        # patch needed as the firmare returns approx time (rounded as int)
        pump1_time = pump1_pars["volume"] / pump1_pars["rate"]
        if pump1_pars["unit"] in [1, 3]:  # hr
            pump1_time *= 60
        pump2_time = pump2_pars["volume"] / pump2_pars["rate"]
        if pump2_pars["unit"] in [1, 3]:  # hr
            pump2_time *= 60
        pump1_pars["time"] = pump1_time
        pump2_pars["time"] = pump2_time

        return pump1_pars, pump2_pars

    def show_pars(self, ret=False):
        unit = {0: 'ml/min', 1: 'ml/hr', 2: 'ul/min', 3: 'ul/hr'}
        pars = self._get_pars()
        val = []
        max_length = 0
        for k in range(2):
            val_list = [f"Pump {k+1}"]
            val_list.append("------")
            rate_unit = pars[k]['unit']
            rate_unit_str = unit[rate_unit]
            if rate_unit == 0 or rate_unit == 1:
                vol_unit = 'ml'
            else:
                vol_unit = 'ul'
            if rate_unit == 0 or rate_unit == 2:
                time_unit = 'min'
            else:
                time_unit = 'hr'
            val_list.append(f"rate unit: {rate_unit} ({rate_unit_str})")
            val_list.append(f"diameter: {pars[k]['dia']} mm")
            val_list.append(f"rate: {pars[k]['rate']} {rate_unit_str}")
            val_list.append(f"time: {pars[k]['time']} min")
            val_list.append(f"|volume|: {pars[k]['volume']} {vol_unit}")
            if pars[k]['delay'] == 0:
                val_list.append("delay: 0.0")
            else:
                val_list.append(f"delay: {pars[k]['delay']} {time_unit}")
            for v in val_list:
                if len(v) > max_length:
                    max_length = len(v)
            val.append(val_list)
        out = []
        for k in range(len(val_list)):
            val1, val2 = f"{val[0][k]}", f"{val[1][k]}"
            out.append([val1, val2])
        if ret:
            return out[2:]
        else:
            for k in range(len(val_list)):
                print(out[k][0].ljust(max_length+2), out[k][1])
            print()

    def _get_limits(self):

        def get_dict(lim):
            lim_list = lim.split()[-4:]
            lim = dict()
            lim["max_rate"] = float(lim_list[0])
            lim["min_rate"] = float(lim_list[1])
            lim["max_vol"] = float(lim_list[2])
            lim['min_vol'] = float(lim_list[3])
            return lim

        lim1 = get_dict(self.write_readlines("1 read limit parameter 0"))
        lim2 = get_dict(self.write_readlines("2 read limit parameter 0"))
        return lim1, lim2

    def _check_value(self, value, value_type, par_name, all_values=None):

        if isinstance(value, (tuple, list)):
            if len(value) != 2:
                raise ValueError("'value' must have len=2 if tuple or list")
        elif isinstance(value, value_type):
            value = [value]
        else:
            raise TypeError(f"'value' must be {value_type} or tuple or list")
        for v in value:
            if not isinstance(v, value_type):
                raise ValueError(f"{par_name} must be {value_type}.")
            if isinstance(v, str):
                v = v.lower()
            if all_values is not None and v not in all_values:
                raise ValueError(f"{par_name} must be one of {all_values}")

        return value

    def _set_check_par(self, value, par, verbose=True):
        """Set pump parameter and check if parameter was actually changed."""

        def _set(pump):
            par_changed = False
            if len(value) == 1:
                cmd = f"{pump} set {par} {value[0]}"
            else:
                cmd = f"{pump} set {par} {value[pump-1]}"
            # print(cmd)
            ans = self.write_readlines(cmd).split()
            # print(ans)
            if ans[-2] != '=':
                if verbose:
                    print(f"WARNING: pump{pump} {par} unchanged")
                return par_changed
            ans = ans[-1]
            for v in value:
                if isinstance(v, str) and ans != v:
                    # print(ans)
                    # print(value[pump-1])
                    # print()
                    if verbose:
                        print(f"WARNING: pump{pump} {par} unchanged " +
                              f"({ans})\n")
                    return par_changed
                else:
                    ans = float(ans)
                    # print(ans)
                    # print(value[pump-1])
                    # print()
                    if not np.isclose(ans, v, rtol=1e-5):
                        if verbose:
                            print(f"WARNING: pump{pump} {par} unchanged " +
                                  f"({ans})\n")
                        return par_changed
            return True

        par_changed = False
        if len(value) == 1:
            par_changed = _set(self.default_pump)
        else:
            par1_changed = _set(1)
            par2_changed = _set(2)
            par_changed = par1_changed & par2_changed
        return par_changed

    @property
    def diameter(self):
        """
        Syringes inner diameter (mm).

        min diameter = 0.103 mm
        max diameter = 60 mm

        """
        pump1_pars, pump2_pars = self._get_pars()
        return float(pump1_pars['dia']), float(pump2_pars['dia'])

    @diameter.setter
    def diameter(self, value, verbose=True):
        value = self._check_value(value, (int, float), 'diameter', None)
        self._set_check_par(value, 'diameter', verbose)

    @property
    def rate_unit(self):
        """Flow rate unit (ml/min, ml/hr, ul/min or ul/hr)."""
        unit = {0: 'ml/min', 1: 'ml/hr', 2: 'ul/min', 3: 'ul/hr'}
        pump1_pars, pump2_pars = self._get_pars()
        return unit[pump1_pars['unit']], unit[pump2_pars['unit']]

    @rate_unit.setter
    def rate_unit(self, value, verbose=True):
        unit = {'ml/min': 0, 'ml/hr': 1, 'ul/min': 2, 'ul/hr': 3}
        all_units = [*unit.keys()]
        value = self._check_value(value, str, 'rate unit', all_units)
        value = [unit[v.lower()] for v in value]
        self._set_check_par(value, 'units', verbose)

    @property
    def time_unit(self):
        """Run time unit (min or hr)."""
        rate_unit1, rate_unit2 = self.rate_unit
        time_unit1 = rate_unit1.split("/")[-1]
        time_unit2 = rate_unit2.split("/")[-1]
        return time_unit1, time_unit2

    @property
    def volume_unit(self):
        """Volume unit (ul or ml)."""
        rate_unit1, rate_unit2 = self.rate_unit
        vol_unit1 = rate_unit1.split("/")[0]
        vol_unit2 = rate_unit2.split("/")[0]
        return vol_unit1, vol_unit2

    @property
    def rate(self):
        """Flow rate."""
        pump1_pars, pump2_pars = self._get_pars()
        return float(pump1_pars['rate']), float(pump2_pars['rate'])

    @rate.setter
    def rate(self, value, verbose=True):
        value = self._check_value(value, (int, float), 'rate', None)
        lims = self._get_limits()
        pump1_max_rate = lims[0]["max_rate"]
        pump1_min_rate = lims[0]["min_rate"]
        pump2_max_rate = lims[1]["max_rate"]
        pump2_min_rate = lims[1]["min_rate"]
        if value[0] > pump1_max_rate or value[0] < pump1_min_rate:
            raise ValueError("pump1 rate must be in range " +
                             f"({pump1_min_rate}, {pump1_max_rate})")
        if len(value) > 1:
            if value[1] > pump2_max_rate or value[1] < pump2_min_rate:
                raise ValueError("pump2 rate must be in range " +
                                 f"({pump2_min_rate}, {pump2_max_rate})")
        value = [float(v) for v in value]
        self._set_check_par(value, 'rate', verbose)

    @property
    def volume(self):
        """Transfer volume (positive=inject, negative=withdrawal)."""
        pump1_pars, pump2_pars = self._get_pars()
        pump1_vol = float(pump1_pars['volume'])
        pump2_vol = float(pump2_pars['volume'])
        if hasattr(self, "_pump1_dir") and self._pump1_dir < 0:
            pump1_vol = -pump1_vol
        if hasattr(self, "_pump2_dir") and self._pump2_dir < 0:
            pump2_vol = -pump2_vol
        return pump1_vol, pump2_vol

    @volume.setter
    def volume(self, value, verbose=True):
        value = self._check_value(value, (int, float), 'volume', None)
        ret = self._set_check_par(value, 'volume', verbose)
        if ret:
            self._pump1_dir = -1 if value[0] < 0 else 1
            if len(value) > 1:
                self._pump2_dir = -1 if value[1] < 0 else 1

    @property
    def time(self):
        """Pump run duration (min)."""
        pump1_pars, pump2_pars = self._get_pars()
        pump1_time = float(pump1_pars['time'])
        pump2_time = float(pump2_pars['time'])
        return pump1_time, pump2_time

    @property
    def linear_speed(self):
        """Motor linear speed (mm/sec). Max speed = 2.97 mm/sec."""
        pars = self._get_pars()
        out = []
        for k in range(2):
            if pars[k]['unit'] == 0:  # 'ml/min'
                flow_rate = pars[k]['rate'] * 1e3 / 60
            elif pars[k]['unit'] == 1:  # 'ml/hr'
                flow_rate = pars[k]['rate'] * 1e3 / 3600
            elif pars[k]['unit'] == 2:  # 'ul/min'
                flow_rate = pars[k]['rate'] / 60
            elif pars[k]['unit'] == 3:  # 'ul/hr'
                flow_rate = pars[k]['rate'] / 3600
            v = flow_rate / (np.pi * pars[k]['dia']**2 / 4)
            v = np.round(v, 7)
            out.append(v)
        return out[0], out[1]

    def _get_property(self, prop):
        ans1 = self.write_readlines(f"1 {prop}").split()[-1]
        ans2 = self.write_readlines(f"2 {prop}").split()[-1]
        return ans1, ans2

    @property
    def _status(self):
        pump_status = {'0': 'stopped', '1': 'running', '2': 'paused',
                       '3': 'delayed', '4': 'stalled'}
        ans1, ans2 = self._get_property("status")
        pump1_status = pump_status[ans1]
        pump2_status = pump_status[ans2]
        return pump1_status, pump2_status

    def show_status(self, ret=False):
        max_length = 0
        val = []
        for k in range(2):
            val_list = [f"Pump {k+1}"]
            val_list.append("------")
            val_list.append(f"status: {self._status[k]}")
            for v in val_list:
                if len(v) > max_length:
                    max_length = len(v)
            val.append(val_list)
        out = []
        for k in range(len(val_list)):
            val1, val2 = f"{val[0][k]}", f"{val[1][k]}"
            out.append([val1, val2])
        if ret:
            return out[2:]
        else:
            for k in range(len(val_list)):
                print(out[k][0].ljust(max_length+2), out[k][1])
            print()

    @property
    def dispensed_volume(self):
        ans1, ans2 = self._get_property("dispensed volume")
        return float(ans1), float(ans2)

    @property
    def elapsed_time(self):
        ans1, ans2 = self._get_property("elapsed time")
        return float(ans1), float(ans2)

    def start(self, both=False):
        """Starts the pump with the currently set parameters."""
        if both:
            self.write_readlines("start 0")
        else:
            self.write_readlines(f"{self.default_pump} start 0")

    def start_show(self, sleep_time=1, both=False):
        if both:
            self.write_readlines("start 0")
        else:
            self.write_readlines(f"{self.default_pump} start 0")
            while self._status[self.default_pump-1] == 'running':
                print(f"{self.dispensed_volume[self.default_pump-1]}\r",
                      end="")
                time.sleep(sleep_time)

    def stop(self, both=False):
        """Ends the current run."""
        if both:
            self.write_readlines("stop")
        else:
            self.write_readlines(f"{self.default_pump} stop")

    def pause(self, both=False):
        """Pauses the current pump run."""
        if both:
            self.write_readlines("pause")
        else:
            self.write_readlines(f"{self.default_pump} pause")

    def reboot(self):
        self.comm.write("1 restart\r".encode())

    def _get_firmware(self, verbose=True):
        self.reboot()
        t0 = time.time()
        print("Waiting for reboot to finish...")
        while time.time() - t0 < 15:
            if time.time() - t0 > 12:
                try:
                    # print("elapsed time =", time.time() - t0)
                    ans = self.comm.raw_read(maxsize=2048, timeout=3)
                except Exception:
                    pass
        if verbose:
            print(ans.decode())
        else:
            return ans

    def help(self):
        ans = self.write_readlines("help", timeout=5)
        print(ans)

    def run(self, vol1, rate1, rate_unit1='ul/min',
            vol2=None, rate2=None, rate_unit2='ul/min'):
        """Run the defaul pump or both pumps."""

        if vol2 is None:
            self.rate_unit = rate_unit1
            self.volume = vol1
            self.rate = rate1
            self.start()
        else:
            self.rate_unit = rate_unit1, rate_unit2
            self.volume = vol1, vol2
            self.rate = rate1, rate2
            self.start(both=True)
