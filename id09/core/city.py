import numpy as np
from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.greenlet_utils import protect_from_kill
from bliss.common.logtools import log_debug
from id09.utils import yaml_storage
from id09.colors import bold_underline


OFFSETS_fname = "/data/id09/archive/configurations/city_offsets.txt"
OFFSETS = yaml_storage.Storage(filename=OFFSETS_fname)

ESRF_RF = dict(rf_f=352.374e6)
ESRF_RF["rf_t"] = 1. / ESRF_RF["rf_f"]
ESRF_RF["orbit_f"] = ESRF_RF["rf_f"] / 992.
ESRF_RF["orbit_t"] = 1 / ESRF_RF["orbit_f"]
ESRF_RF["hs_chopper_f"] = ESRF_RF["orbit_f"] / 360.
ESRF_RF["hs_chopper_t"] = 1 / ESRF_RF["hs_chopper_f"]


class City:

    def __init__(self, url="cityid091", port=5000,
                 lxt_laser_off_position=9999):

        self.address = (url, port)

        conf = {'tcp': {'url': 'cityid091:5000'}}
        self._cnx = get_comm(conf, timeout=3)
        global_map.register(self, children_list=[self._cnx])

        self.periods = {"orbit": ESRF_RF["orbit_t"],
                        "rf": ESRF_RF["rf_t"],
                        "hsc": ESRF_RF["hs_chopper_t"]}

        self.hsc = DelayChannel(self, "OA1")
        
        self.hlc = DelayChannel(self, "OB1")
        self.hlc_ref = DelayChannel(self, "OB2")
        # self.cshut = DelayChannel(self, "OB5")

        self.relay = DelayChannel(self, "OB3")  # ma6203
        self.jfin = DelayChannel(self, "OB4")  # ma6203

        self.ldc = DelayChannel(self, "OA4")  # dynamic compression
        self.vshut = DelayChannel(self, "OA5")  # dynamic compression
        self.vscope = DelayChannel(self, "OA6")  # dynamic compression

        self.delay_channels = ['ldc', 'vshut', 'vscope']

        self.is_off = False
        self.lxt_laser_off_position = lxt_laser_off_position

    def __info__(self):
        info = "CITY fpga synchronization board"
        info += " (IP=%s)\n" % self.address[0]
        info += self.monitor(printout=False)
        return info

    def __repr__(self):
        return self.__info__()

    @protect_from_kill
    def comm(self, cmd, timeout=None, text=True):
        self._cnx.open()
        with self._cnx._lock:
            self._cnx._write((cmd + "\r\n").encode())
            if cmd.startswith("?") or cmd.startswith("#"):
                msg = self._cnx._readline(timeout=timeout)
                cmd = cmd.strip("#").split(" ")[0]
                msg = msg.replace((cmd + " ").encode(), "".encode())
                if msg.startswith("$".encode()):
                    msg = self._cnx._readline(
                        # transaction=transaction,
                        # clear_transaction=False,
                        eol="$\n",
                        timeout=timeout,
                    )
                    return msg.strip("$\n".encode()).decode()
                elif msg.startswith("ERROR".encode()):
                    raise RuntimeError(msg.decode())
                if text:
                    return (msg.strip("\r\n".encode())).decode()
                else:
                    return msg.strip("\r\n".encode())

    def help(self):
        print(self.comm("?HELP")+"\n")

    def help_chcfg(self, print_out='parameters'):
        ans = self.comm("?HELP CHCFG")
        params = ans.split("Parameters description:")[-1].split("BURST/GATE mechanism chronogram:")[0]
        syntax = ans.split("Parameters description:")[-1].split("Syntax:")[-1].split("Example(s):")[0]
        examples = ans.split("Parameters description:")[-1].split("Syntax:")[-1].split("Example(s):")[-1]
        if 'syntax' in print_out:
            print("\nCHCFG Syntax:")
            print(syntax)
        if 'parameters' in print_out:
            print("\nCHCFG Parameters:")
            print(params)

    def monitor(self, printout=True):
        ans = self.comm("?MONITOR")
        if printout:
            ans += '\n'
            print(ans)
        else:
            return ans

    def delay_partitioner(self, delay, period, verbose=False):
        """Partition delay between xshut and laser."""
        coarse_delay = int(delay/period) * period
        fine_delay = delay - coarse_delay
        if verbose:
            print(coarse_delay, fine_delay)
        return coarse_delay, fine_delay

    def lxt_move(self, delay, ch_name="OA4"):
        self.is_off = False
        if delay == self.lxt_laser_off_position:
            self.is_off = True
            return
        # shut_delay, delay = self.delay_partitioner(delay, self.periods['hsc'])
        # laser_delay = -delay
        # channels = [self.vshut]
        # delay_values = [vshut_delay]
        # for channel, delay in zip(channels, delay_values):
        #     offset_value = channel.dial_offset
        #     dial_delay_value = delay + offset_value
        #     if dial_delay_value < 0:
        #         raise ValueError("Cannot move %s " % channel.name +
        #          "to %s. " % TimeDelay(delay) +
        #          "Dial delay would be negative " +
        #          "(%s)" % TimeDelay(dial_delay_value) +
        #          "since offset = %s." % TimeDelay(offset_value)
        #          )
        self.ldc.move_delay(-delay)

    def lxt_read(self, ch_name="OA4"):
        # temp solution (works only if offset were set correctly)
        if self.is_off:
            return self.lxt_laser_off_position
        return -self.ldc.read_delay()


class DelayChannel:
    """Class to control a CITY delay channel."""

    def __init__(self, city_instance, name, dial_offset="from_file"):

        self._city = city_instance
        self.name = name
        self.has_fine_delays = True if "OA" in self.name else False

        if isinstance(dial_offset, str):
            if dial_offset == "from_file":
                dial_offset = OFFSETS[self.name]
            else:
                raise ValueError("'dial_offset' must be float or 'from_file'.")

        self.coarse_step = ESRF_RF["rf_t"]

        if self.has_fine_delays:
            self.fine_step = 10e-12
        else:
            self.fine_step = 0

        global_map.register(self, parents_list=[self._city])

    def city_cmd(self, cmd):
        return self._city.comm(cmd)

    def get_city_config(self):
        return self._city.comm(f"?CHCFG {self.name}")

    def __call__(self, delay):
        self.move_delay(delay)

    def _read_coarse_delay_steps(self):
        ans = self.city_cmd(f"?CHCFG {self.name}")
        delay_steps = int(ans.split("CDELAY")[-1].split()[0])
        return delay_steps

    def _read_fine_delay_steps(self):
        ans = self.city_cmd(f"?CHCFG {self.name}")
        if "FDELAY" in ans:
            delay_steps = int(ans.split("FDELAY")[-1].split()[0])
        else:
            delay_steps = 0
        return delay_steps

    def _read_divider(self):
        ans = self.city_cmd(f"?CHCFG {self.name}")
        if 'DIVIDER' in ans:
            return int(ans.split("DIVIDER")[-1].split()[0])

    def _read_period(self):
        divider = self._read_divider()
        period = divider*self.coarse_step
        return period

    def _read_freq(self):
        divider = self._read_divider()
        if divider is not None:
            return 1/divider/self.coarse_step

    def _read_width(self):
        ans = self.city_cmd(f"?CHCFG {self.name}")
        width = ans.split("WIDTH")[-1].split()[0]
        if '%' in width:
            width = float(width.strip("'%'"))/100
            width *= self._read_period()
        else:
            width *= self.coarse_step
        return width

    def _move_coarse_delay(self, delay, as_steps=False,
                           raise_if_out_of_range=True):

        if not as_steps:
            delay_steps = int(delay / self.coarse_step)
        else:
            # int() is used in case delay is np.int ...
            delay_steps = int(delay)

        if raise_if_out_of_range:
            if (delay_steps < 0) or (delay_steps > 1410065408):
                raise ValueError(
                    "Coarse delay of '%s' cannot move to " % self.name +
                    "%s steps (it would be outside its limits)." % delay_steps
                )

        self.city_cmd(f"CHCFG {self.name} CDELAY {delay_steps}")

    def _move_fine_delay(self, delay, as_steps=False,
                         raise_if_out_of_range=True):

        if not self.has_fine_delays:
            print(f"ERROR: {self.name} does not have fine delays")
            return
        if not as_steps:
            delay_steps = int(delay / self.fine_step)
        else:
            # int() is used in case delay is np.int ...
            delay_steps = int(delay)

        if raise_if_out_of_range:
            if (delay_steps < 0) or (delay_steps > 1023):
                raise ValueError(
                    "Fine delay of '%s' cannot move to " % self.name +
                    "%s (it would be outside its limits)." % self.delay_steps
                )

        self.city_cmd(f"CHCFG {self.name} FDELAY {delay_steps}")

    def _read_delay(self):
        coarse_steps = self._read_coarse_delay_steps()
        fine_steps = self._read_fine_delay_steps()
        coarse_delay = coarse_steps * self.coarse_step
        fine_delay = fine_steps * self.fine_step
        delay_dial = coarse_delay + fine_delay
        delay = delay_dial - self.dial_offset
        return dict(
            coarse_steps=coarse_steps,
            fine_steps=fine_steps,
            coarse_delay=coarse_delay,
            fine_delay=fine_delay,
            delay_dial=delay_dial,
            delay=delay,
        )

    def read_delay(self, as_dial=False, as_steps=False):
        ret = self._read_delay()
        if as_steps:
            return ret["coarse_steps"], ret["fine_steps"]
        else:
            if as_dial:
                return ret["delay_dial"]
            else:
                return ret["delay"]

    @property
    def dial_offset(self):
        return OFFSETS[self.name]

    @dial_offset.setter
    def dial_offset(self, value):
        # this is used to automatically trigger the saving
        # when assigning a new offset using xshut.dial_offset=0
        OFFSETS[self.name] = float(value)

    def set_delay(self, value):
        """Set current position to value."""
        dial = self.read_delay(as_dial=True)
        offset = dial - value
        self.dial_offset = offset
#        OFFSETS[self.name] = float(offset)
        log_debug(self, "Resetting offset of '%s' from " % self.name +
                        str(self.dial_offset) + " to " + str(offset))
#        self.dial_offset = offset
        return self.read_delay(as_dial=True)

    def move_relative(self, delta_delay, as_steps=False,
                      raise_if_out_of_range=True):
        """Move delay relative by a delta delay."""
        current_pos = np.asarray(self.read_delay(as_steps=as_steps))
        if as_steps:
            delta_delay = np.asarray(delta_delay)
            new_pos = list(current_pos+delta_delay)
        else:
            new_pos = current_pos+delta_delay
        self.move_delay(new_pos, as_steps=as_steps,
                        raise_if_out_of_range=raise_if_out_of_range)

    def move_by_chopper_periods(self, nperiods, raise_if_out_of_range=True):
        """Move delay relative in steps of the chopper period."""
        # ML: 21-Jun-2023 (new function)
        if not isinstance(nperiods, int):
            raise TypeError("'nperiods' must be int.")
        delta_delay = ESRF_RF["hs_chopper_t"]*nperiods
        self.move_relative(delta_delay=delta_delay, as_steps=False,
                           raise_if_out_of_range=raise_if_out_of_range)

    def move_delay(self, delay, as_dial=False, as_steps=False,
                   raise_if_out_of_range=True):

        # if channel is heatt and using PIC, then delay *= 5/18

        if not as_steps:
            if not as_dial:
                if delay + self.dial_offset < 0:
                    raise ValueError(
                        "Cannot move '%s' delay " % self.name +
                        "to %s. Dial delay would be " % TimeDelay(delay) +
                        "negative (%s) " % TimeDelay(delay+self.dial_offset) +
                        "since offset = %s." % TimeDelay(self.dial_offset)
                    )
                delay = delay + self.dial_offset
            if self.has_fine_delays:
                coarse_steps, fine_delay = divmod(delay, self.coarse_step)
                coarse_steps = round(coarse_steps)
                fine_steps = round(fine_delay / self.fine_step)
            else:
                coarse_steps = round(delay / self.coarse_step)
                fine_steps = None
        else:
            if self.has_fine_delays:
                coarse_steps, fine_steps = delay
            else:
                if isinstance(delay, (tuple, list)):
                    delay = delay[0]
                coarse_steps = delay
                fine_steps = None

        if raise_if_out_of_range:
            check_delay = coarse_steps*self.coarse_step
            if fine_steps is not None:
                check_delay += fine_steps*self.fine_step
            # rep_rate = self.read_frequency()
            # if check_delay > 1/rep_rate:
            #     raise ValueError(
            #         "Cannot move '%s' dial delay " % self.name +
            #         "to %s " % TimeDelay(check_delay) +
            #         "(freq=%.3f Hz)." % rep_rate
            #     )

        log_debug(self, "Partitioning delay for motor '%s' " % self.name +
                        "(coarse_steps, fine_steps) = " +
                        "(%s, %s)" % (str(coarse_steps), str(fine_steps)))

        self._move_coarse_delay(coarse_steps, as_steps=True,
                                raise_if_out_of_range=raise_if_out_of_range)

        if fine_steps is not None:
            self._move_fine_delay(fine_steps, as_steps=True,
                                  raise_if_out_of_range=raise_if_out_of_range)

    def move_frequency(self, freq, as_divider=False):
        """Change channel divider in order to change frequency to `freq`."""
        if not as_divider:
            print("not implemented yet")
            return
            divider = round(self._read_freq()/freq*self._read_divider())
        else:
            divider = int(freq)
        if divider > 0:
            self.city_cmd(f"CHCFG {self.name} SOURCE DIVIDER {divider}")
        else:
            print("ERROR: cannot set channel divider to 0")

    def as_long_str(self, print_offsets=False, print_enhance=True):
        if print_enhance:
            s = bold_underline(self.name)
        else:
            s = self.name
        ret = self._read_delay()
        fine_delay = TimeDelay(ret["fine_delay"]).as_long_str()[-11:]
        fine_steps = ret["fine_steps"]
        coarse_delay = TimeDelay(ret["coarse_delay"]).as_long_str()
        coarse_steps = ret["coarse_steps"]
        s += "\n - Delay        "
        s += TimeDelay(ret["delay"]).as_long_str()
        if print_offsets:
            s += "\n - Offset       "
            s += TimeDelay(self.dial_offset).as_long_str()
        s += "\n - Dial Delay   "
        s += TimeDelay(ret["delay_dial"]).as_long_str()
        s += "\n   - coarse     %s (%10s steps" % (coarse_delay, coarse_steps)
        s += " @ %d RF)" % (int(self.coarse_step / ESRF_RF["rf_t"]))
        if self.has_fine_delays:
            s += "\n   - fine                        %s" % fine_delay
            s += " (%10s steps @ %.3f ps)" % (fine_steps, self.fine_step*1e12)
        div = self._read_divider()
        if div is not None:
            freq = self._read_freq()
            s += "\n - Frequency      %7.3f Hz " % freq
            s += "(channel div: %4d)" % div
        return s

    def as_short_str(self, as_dial=False):
        ret = self._read_delay()
        if as_dial:
            delay = ret["delay_dial"]
        else:
            delay = ret["delay"]
        coarse = ret["coarse_steps"]
        fine = ret["fine_steps"]
        s = self.name + " @ " + TimeDelay(delay).as_short_str()
        s += " (RF"
        if self.has_fine_delays:
            s += ", RF/256 = %d, %d steps)" % (coarse, fine)
        else:
            s += " = %d steps)" % coarse
        return s

    def __info__(self):
        return self.as_long_str(print_enhance=False)

    def __repr__(self):
        return self.as_long_str()


class TimeDelay:
    """
    Class to handle time delays string representation.

    Parameters
    ----------
    value : float
        Time delay value (s).

    Methods
    -------
    as_short_str, as_medium_str, as_long_str

    """

    def __init__(self, value):
        self.value = value

    def as_short_str(self):
        t = self.value
        if t == 0:
            return "0 s"
        elif abs(t) < 1e-9:
            return "%.3f ps" % (t * 1e12)
        elif abs(t) < 1e-6:
            return "%.3f ns" % (t * 1e9)
        elif abs(t) < 1e-3:
            # return "%.4g us" % (t * 1e6)
            return "%.3f us" % (t * 1e6)
        elif abs(t) < 1:
            return "%.3f ms" % (t * 1e3)
        else:
            return "%.3f s" % t

    def as_long_str(self):
        t = "%0.12f" % self.value
        s = t.split(".")
        sec = s[0]
        msec = s[1][0:3]
        usec = s[1][3:6]
        nsec = s[1][6:9]
        psec = s[1][9:12]
        return "%3ss %sms %sus %sns %sps" % (sec, msec, usec, nsec, psec)

    def as_medium_str(self):
        t = "%0.12f" % self.value
        s = t.split(".")
        sec = s[0]
        msec = s[1][0:3]
        usec = s[1][3:6]
        nsec = s[1][6:9]
        psec = s[1][9:12]
        ret = ""
        if int(sec) != 0:
            ret += "%3ss" % (sec)
        if int(msec) != 0:
            ret += " %3sms" % (msec)
        if int(usec) != 0:
            ret += " %3sus" % (usec)
        if int(nsec) != 0:
            ret += " %3sns" % (nsec)
        if int(psec) != 0:
            ret += " %3sps" % (psec)
        ret = ret.lstrip()
        return ret

    def __repr__(self):
        return self.as_medium_str()

    def __str__(self):
        return self.as_short_str()
