# -*- coding: utf-8 -*-
"""
Module to communicate with the MCS LA-2000 controller.

There are three ways to communicate with the controller:
    - direct USB connection
    - direct serial (RS-232) communication
    - ethernet connection (NOTE: some commands cannot be issued through eth)

If possible, BLISS communication modules are used to control the device.

The LA-2000 is a 4 quadrant velocity servo amplifier intended to control
3-phase permanent magnet synchronous motors.

The LA-2000 obtains rotor position information through:
    - a quadradure encoder (single-ended or differential)
    - Hall effect sensors
    - an external commutation source (Sine/Sine+120 operation)
Possible feedback (commutation) types are:
    - Encoder + Hall effect sensors (TYPE:06)
    - Encoder only (TYPE:07)
    - Hall effect sensors only --> Low-cost, analog torque/velocity control
    - Sine/Sine+120

Control Methods:
    - Frequency-Lock-Loop using the internal synthesizer
    - Frequency-Lock-Loop using an external frequency reference
    - Analog torque control
    - Analog velocity control
    - Sine/Sine+120 control
    - Position loop control and position capture input

Communication info:
    - Serial RS-232:
        - data bits = 8
        - stop bit = 1
        - no parity
        - default baud rate = 9600 baud
        - no handshaking
    - USB:
        - emulates a hardware RS-232 serial port
    - Ethernet:
        - speed = 10/100
        - command based Telnet interface

Command definition:
    - ASCII strings terminated by a carriage-return character ("\r")
    - Multiple commands are entered as one string ("CMD1:CMD2\r")
    - Max command length: 511 characters
    - If the command string is accepted, the system will respond with
      a line feed character ("\n"). Otherwise, an error code followed
      by a "?\n" is returned.

Notes:
    - POSITIONER SETUP configuration is not implemented

"""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "26/05/2021"
__version__ = "0.0.1"


import os
import time
import glob
import serial
import ipaddress

try:
    import bliss.comm.util
    get_comm = bliss.comm.util.get_comm
    SERIAL = bliss.comm.util.SERIAL
    is_bliss = True
except ImportError:
    import socket
    is_bliss = False


class ConfigModeError(Exception):
    pass


class LA2000:
    """
    Class to communicate with the MCS LA-2000 controller.

    If 'usb' is not None:
      - direct USB communication is used
    If BLISS communication is available:
      - ser2net is used by default
    If none of the above is available:
      - ethernet communication is used

    """

    def __init__(self, config=None, usb=None, url='id09pic2', is_bliss=True,
                 port=23, timeout=0.5, sleep=0.1, debug=False, verbose=True):

        """
        Initialize communication with LA2000.

        Parameters
        ----------
        usb : str or None.
            USB device port (e.g. "/dev/ttyUSB0"). If None,
            ethernet communication is used.
        host : str
            Hostname of the LA2000 controller. Ignored if 'usb' is not
            None.
        port : int
            Ethernet communication port. Ignored if 'usb' is None.
        sleep : float.
            Sleep time between consecutive commands.

        """

        if debug:
            print("usb:", usb)
            print("is_bliss:", is_bliss)
            print("url:", url)
            print("port:", port)

        self.usb = usb
        self.url = url
        self.port = port
        self.timeout = timeout
        if sleep >= 0.1:
            self.sleep = sleep
        else:
            print("WARNING: sleep < 0.1 s can lead to communication errors.")
            self.sleep = 0.1
        self.debug = debug
        self.verbose = verbose
        self.usb_ser_pars = dict(baudrate=9600, bytesize=8, stopbits=1,
                                 parity='N')
        self.errorcode = {"0?": "Unrecognized command",
                          "2?": "Invalid request (parameter out of range or " +
                                "option not valid in current configuration)",
                          "3?": "Unable to change setup (disable drive first)",
                          "4?": "EEPROM read failure",
                          "5?": "Decimal number length exceeds limit",
                          "6?": "Command string is too long",
                          "7?": "Reserved",
                          "8?": "Max commutation freq exceeded (TOPSPEED is " +
                                "too high for the encoder)"}

        if usb is not None:
            self.comm_module = 'usb'

        elif is_bliss:

            self.comm_module = 'bliss'

            if config is not None:
                self.url = config.get("url")
                self.timeout = config.get("timeout", timeout)
                self.sleep = config.get("sleep", sleep)
                self.debug = config.get("debug", debug)
                self.verbose = config.get("verbose", verbose)
            else:
                self.url = url
                self.timeout = timeout

        else:

            self.comm_module = 'socket'

        if debug:
            print("Attempting connection through:", self.comm_module)
        self.connect()

    def connect(self, eol=b"\r", timeout=None):
        """Open connection."""

        if timeout is None:
            timeout = self.timeout

        if self.comm_module == 'usb':

            if os.path.exists(self.usb):
                self.usb_ser_pars['timeout'] = timeout
                usb = serial.Serial(port=self.usb, **self.usb_ser_pars)
                if usb.is_open:
                    print("Connection OK.\n")
                    self.usb = usb
                else:
                    print("Error: port is not open.\n")
            else:
                print("Error: port %s does not exist. " % self.usb)
                usb_ports = glob.glob("/dev/ttyUSB*")
                print("Available USB ports are: %s" % usb_ports)

        elif self.comm_module == 'socket':

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((self.url, self.port))
            self.socket.settimeout(timeout)

        else:

            #conf = {"serial": {"url": self.url}}
            conf = {"tcp-proxy": {"external": True, "tcp":{"url": self.url}}}
            opts = {"timeout": timeout}
            self.bliss_socket = get_comm(conf, **opts)
#            self.bliss_socket = get_comm(conf, ctype=SERIAL, **opts)

    def close(self):
        """Close communication connection."""
        if self.usb is not None:
            pass
        elif self.socket is not None:
            self.socket.close()
        else:
            pass

    def send(self, cmd):
        """Send command through USB or ethernet port.

        LA2000 command format
        ---------------------
        - "AOT?\r".encode()

        """
        if len(cmd) > 511:
            raise ValueError("Length of 'cmd' should be < 511.")
        cmd = cmd.upper()
        cmd += "\r"
        cmd = cmd.encode()
        if self.debug:
            print(cmd)
        if self.comm_module == 'usb':
            self.usb.write(cmd)
        elif self.comm_module == 'bliss':
            self.bliss_socket.write(cmd)
        elif self.comm_module == 'socket':
            try:
                self.socket.send(cmd)
            except BlockingIOError:
                print("BlockingIOError...")

    def read(self, size=2048, raw=False):

        try:
            if self.comm_module == 'usb':
                ret = self.usb.read(size)
            elif self.comm_module == 'bliss':
                ret = self.bliss_socket.raw_read(maxsize=size)
            elif self.socket is not None:
                ret = self.socket.recv(size)
        except OSError:
            print("Timeout. No data available.")
            return
        else:
            if not ret and self.usb is None:
                raise RuntimeError("Peer closed.")

        if raw:
            return ret

        ret = ret.decode()
        ret = ret[:-1]  # remove last \n (should be always present...)

        if ret.split("\n")[0] in self.errorcode.keys():
            ret = self.errorcode[ret.split("\n")[0]]

        return ret

    def query(self, cmd, retans=False, sleep=None):
        if sleep is None:
            sleep = self.sleep
        self.send(cmd)
        time.sleep(sleep)
        if cmd.upper() in ["FLT?", "STAT?", "FLTQREAD?"]:
            ret = self.read(binary_output=True)
        else:
            ret = self.read()
        if retans:
            return ret
        else:
            print(ret)

    def get_id(self, sleep=1):
        out = self.query("ID?", sleep=sleep, retans=True).split(";")
        out = [o.strip() for o in out[:-1]]
        out[-3] = "-".join(out[-3:])
        out = out[:-2]
        return out

    def __str__(self):
        return "MCS LA-2000 controller obj"

    def __repr__(self):
        out = ["MCS LA-2000 controller obj:"]
        out += self.get_id()[1:]
        out = "\n".join(out)
        return out

    def get_config(self):
        """Get list of configuration parameters."""
        if self.comm_module == 'bliss':
            sleep = 2.5
        else:
            sleep = 1
        self.query("PLIST", sleep=sleep)

    def _get_short_config(self):
        """Get human readable subset of configuration parameters."""
        ret = self.query("CONFIG?", retans=True, sleep=1)
        return ret[:-1]

    def get_status(self):
        """Get drive parameters, faults and heatsink temperature."""
        print(self._get_short_config())
        print("\nSystem status:")
        self.query("STATQ?", sleep=1)
        if self.is_fault():
            print("\nSystem is in fault:")
            self.get_faults(sleep=1)
        print("\nPeak motor current: %g A" % self.get_current())
        print("Heatsink temperature: %g C" % self.get_heatsink_temperature())

    def start(self):
        self.stop()
        self.run()

    def is_over_temperature(self):
        """Queries the amplifier over temperature status."""
        ret = self.query("AOT?", retans=True)
        if ret.strip() == "OK":
            return False
        else:
            return True

    def get_ramp_acceleration(self):
        """Get the acceleration set point during a ramp (RPM/sec)."""
        ret = self.query("ACCEL?", retans=True)
        if ret.isnumeric():
            ret = int(ret)
            return ret

    def get_ramp_deceleration(self):
        """Get the deceleration set point during speed reduction (RPM/sec)."""
        ret = self.query("DECEL?", retans=True)
        if ret.isnumeric():
            ret = int(ret)
            return ret

    def _check_int_in_range(self, value, vmin=0, vmax=99999):
        if not isinstance(value, int):
            raise TypeError("'value' must be int.")
        if value not in range(vmin, vmax+1):
            raise ValueError("'value' must be an int betweem " +
                             "%d and %d." % (vmin, vmax))

    def set_ramp_acceleration(self, value):
        """Set the acceleration set point during a ramp (RPM/sec).

        Parameters
        ----------
        value : int
            Acceleration rate (RPM/sec). Must be a number between
            1 and 65535.

        """
        self._check_int_in_range(value, vmin=1, vmax=65535)
        self.query("ACCEL:%05d" % value, retans=False)

    def set_ramp_deceleration(self, value):
        """Set the deceleration set point during speed reduction (RPM/sec).

        Parameters
        ----------
        value : int
            Deceleration rate (RPM/sec). Must be a number between
            1 and 65535.

        """
        self._check_int_in_range(value, vmin=1, vmax=65535)
        self.query("DECEL:%05d" % value, retans=False)

    def is_at_speed(self):
        """Queries if motor is at the target speed."""
        ret = self.query("ATSPD?", retans=True)
        if ret == "YES":
            return True
        else:
            return False

    def ccw(self):
        """Set the counterclockwise rotation."""
        self.query("CCW", retans=True)

    def clamp(self):
        """Clamp the motor."""
        self.query("CLAMP", retans=True)

    def enable(self):
        """
        Enables the controller and commands the motor to accelerate to the
        requested speed.
        """
        self.query("ENABLE", retans=True)

    def get_current(self):
        """Get approximate peak motor current (A)."""
        ret = self.query("CURRENTQ", retans=True)
        ret = ret.strip()
        try:
            float(ret)
            return float(ret)
        except ValueError:
            return ret

    def cw(self):
        """Set the clockwise rotation."""
        self.query("CW", retans=True)

    def get_direction(self):
        """Get motor direction (CW or CCW)."""
        ret = self.query("DIR?", retans=True)
        ret = ret.strip()
        return ret

    def disable(self):
        """Coast motor to zero speed."""
        self.query("DISABLE", retans=True)

    def get_disfltq(self, sleep=1, retans=False):
        """Get which faults are enabled and which are disabled."""
        ret = self.query("DISFLTQ", sleep=sleep, retans=True)
        if not retans:
            print(ret)
        else:
            ret = ret.split("\r\n")
            ret = [r.strip() for r in ret]
            enabled = []
            disabled = []
            for r in ret:
                if 'ENABLED' in r:
                    enabled.append(r.replace("ENABLED", "").strip())
                else:
                    disabled.append(r.replace("DISABLED", "").strip())
            return enabled, disabled

    def get_faults(self, sleep=1, retans=False):
        """Get faults status."""
        ret = self.query("FLTQ?", sleep=sleep, retans=True)
        if not retans:
            print(ret)
        else:
            ret = ret.split("\r\n")
            ret = [r.strip() for r in ret]
            faults = []
            nofaults = []
            for r in ret:
                if 'OK' in r:
                    nofaults.append(r.replace("OK", "").strip())
                else:
                    faults.append(r.replace("FAULT", "").strip())
            return faults, nofaults

    def get_faults_queue(self, sleep=1, retans=False):
        """Get faults status."""
        ret = self.query("FLTQREADQ", sleep=sleep, retans=True)
        if not retans:
            print(ret)
        else:
            ret = ret.split("\r\n")
            ret = [r.strip() for r in ret]
            faults_queue = []
            for r in ret:
                faults_queue.append(r.replace("Fault", "").strip())
            return faults_queue

    def get_disabled_faults(self):
        """Get what faults are disabled."""
        enabled, disabled = self.get_disfltq(retans=True)
        return disabled

    def get_enabled_faults(self):
        """Get what faults are disabled."""
        enabled, disabled = self.get_disfltq(retans=True)
        return enabled

    def is_enabled(self):
        """Queries if drive is enabled."""
        ret = self.query("ENABLE?", retans=True)
        if ret.strip() == "ENABLED":
            return True
        else:
            return False

    def is_fault(self):
        """Queries if drive is in fault."""
        ret = self.query("FAULT?", retans=True)
        if ret.strip() == "FAULT":
            return True
        else:
            return False

    def fan_on(self):
        """Turn on the cooling fan."""
        self.query("FAN:ON", retans=True)

    def fan_off(self):
        """Turn off the cooling fan.

        For protection, the fan will turn on when the heatsink temperature
        reaches approx 50 degC.

        """
        self.query("FAN:OFF", retans=True)

    def get_heatsink_temperature(self):
        """Get the amplifier (controller) heat sink temperature (degC)."""
        ret = self.query("HEATSINKT", retans=True)
        ret = float(ret.strip())
        return ret

    def get_ip_configuration(self, sleep=1):
        """Get the network IP address and other network information."""
        self.query("IPLIST", sleep=sleep)

    def is_power_fault(self):
        """Queries the logic power fault status."""
        ret = self.query("LPR?", retans=True)
        if ret.strip() == "OK":
            return False
        else:
            return True

    # def goto_position(self, position):
    #     """Go to a specified position (counts)."""
    #     if not isinstance(position, int):
    #         raise TypeError("'position' must be int.")
    #     self.query("POS:%05d" % position, retans=True)

    # def goto_ref_position(self):
    #     """Go to the hardware ref position captured during installation."""
    #     self.query("POS:CAP")

    # def get_positioner_ref(self):
    #     """Get the hardware ref position captured during installation."""
    #     self.query("POSCAP?")

    # def get_positioner_config(self):
    #     """Lists all gain settings for the positioner option."""
    #     self.query("POSGAINS?")

    def get_position(self):
        """Get actual motor position (counts)."""
        ret = self.query("POSQ?", retans=True)
        ret = int(ret.strip())
        return ret

    def ramp_on(self):
        """Activates the controlled acceleration ramping.

        Use set_ramp_acceleration() and set_ramp_deceleration() to control
        the ramp acceleration/deceleration value.

        Ramp is on in the default power-up state.

        Ramping can be used with either internal or external reference FLL
        velocity control.
        """
        self.query("RAMPIN", retans=True)

    def ramp_off(self):
        """Deactivates the controlled acceleration ramping.

        If ramp is off, the motor will  accelerate at the maximum possible
        rate, dependent on inertia, load, etc. in an open loop manner.

        This mode should also be used if the drive is in external FLL mode
        and the command frequency is ramped in a controlled manner by customer
        supplied electronics, to provide a desired speed profile, such as a
        constant linear velocity spiral.
        """
        self.query("RAMPOUT", retans=True)

    def is_ready(self):
        """Queries if the drive can be enabled."""
        ret = self.query("READY?", retans=True)
        if ret.strip() == "YES":
            return True
        else:
            return False

    def run(self):
        """
        Enables the controller and commands the motor to accelerate to the
        requested speed.
        """
        self.query("RUN", retans=True)

    def get_speed(self):
        """Get the actual motor speed (RPM)."""
        ret = self.query("SPD?", retans=True)
        ret = ret.strip()
        if ret != '':
            ret = int(ret)
        else:
            ret = 0
        return ret

    def set_speed(self, value):
        """Set the target motor speed (RPM).

        Used only in FLL speed controlled systems using the internal
        synthesizer frequency reference.

        This target speed value can be checked either with get_status()
        or with get_config(). It will be displayed as "Speed".

        Parameters
        ----------
        value : int
            Motor set speed (RPM).

        """
        self._check_int_in_range(value, vmin=1, vmax=65535)
        self.query("SPEED:%05d" % value, retans=True)

    def stop(self):
        """
        Decelerate to zero speed and disable the controller when the motor
        speed is less than the stop speed.
        """
        self.query("STOP", retans=True)

    # def stop_and_lock(self):
    #     """
    #     Bring the motor to a controlled stop and then locks it in position.

    #     Requires using unlock() to resume normal operation.

    #     Used only if LOCKMODE option (Configuration Mode) is set to ‘1’.
    #     """
    #     self.query("STOPLOCK", retans=True)

    def set_max_torque(self, value):
        """Set the max torque to a given percent of full scale."""
        self._check_int_in_range(value, vmin=1, vmax=100)
        ret = self.query("TORQUE:%03d" % value, retans=True)
        return ret

    def get_max_torque(self):
        """Get the max torque (percent of full scale)."""
        ret = self.query("TORQUE?", retans=True)
        ret = ret.strip()
        if ret != "":
            ret = int(ret)
            return ret

    # def unclamp(self):
    #     """Unclamp the motor.

    #     Sending this command while the motor or spindle is rotating results
    #     in an error.
    #     """
    #     self.query("UNCLAMP")

    # def unlock(self):
    #     """
    #     Releases the lock on the motor.

    #     The motor will now be in a disabled state (Internal Enable systems)
    #     or resume external control (Torque or External Enable systems).

    #     unlock() must be used to clear a stop_and_lock() before
    #     normal operation may resume!
    #     """
    #     self.query("UNLOCK", retans=True)

    def is_zero_speed(self):
        """Queries the zero speed status."""
        ret = self.query("ZERP?", retans=True)
        if ret.strip() == "YES":
            return True
        else:
            return False

    def _change_config(self, cmd, save=False):
        """Enter configuration mode and change single parameter."""

        def exit_config_mode():
            print("Quitting 'Configuration Mode.'")
            ret = self.query("EXIT", retans=True)
            if ret == "":
                print("Controller is in 'Normal Mode'.")
            else:
                print("ERROR: " + ret)

        # enter Configuration Mode
        ret = self.query("CONFIGURE", retans=True)
        if "In configuration state" in ret:
            print("Controller is in 'Configuration Mode'.")
        else:
            print(ret)
            exit_config_mode()
            raise ConfigModeError

        # enter command
        if cmd != "":
            ret = self.query(cmd, retans=True)
            if "In configuration state" in ret:
                print("Command %s executed correctly." % cmd.upper())
            else:
                print(ret)
                exit_config_mode()
                raise ConfigModeError

        time.sleep(self.sleep)

        # save config
        if not save:
            store = input("Store new configuration permanently (yes/NO)? ")
            if store.lower() in ['yes', 'y']:
                save = True
        if save:
            ret = self.query("WRITE", retans=True)
            if "In configuration state" in ret:
                print("New configuration stored permanently.")
            else:
                print("ERROR: ", ret)

        # exit Configuration Mode
        exit_config_mode()

        # print configuration
        self.get_config()

    def change_config(self, cmd, save=False):
        """Enter configuration mode and change single parameter."""
        try:
            self._change_config(cmd=cmd, save=save)
        except ConfigModeError:
            return

    def save_config(self):
        """Store current configuration permanently."""
        self.change_config(cmd="", save=True)

    def _check_ip(self, address):
        try:
            ip = ipaddress.ip_address(address)
            ip = str(ip)
            ip = '.'.join(f'{int(n):03d}' for n in ip.split('.'))
            return ip
        except ValueError:
            print("Invalid address/netmask: %s" % address)

    def change_ip_address(self, address):
        """Change IP address in dotted decimal format."""
        address = self._check_ip(address)
        if address is not None:
            self.change_config("IPADDR:%s" % address)

    def change_gateway_address(self, address):
        """Change gateway address in dotted decimal format."""
        address = self._check_ip(address)
        if address is not None:
            self.change_config("GWADDR:%s" % address)

    def change_dns_address(self, address):
        """Change DNS address in dotted decimal format."""
        address = self._check_ip(address)
        if address is not None:
            self.change_config("DNSADDR:%s" % address)

    def change_subnet_address(self, address):
        """Change subnet mask address in dotted decimal format."""
        address = self._check_ip(address)
        if address is not None:
            self.change_config("SUBMASK:%s" % address)

    def _get_z_disfault(self):
        disabled = self.get_disabled_faults()
        cnt = 0
        if "Bearing Air Pressure Fault" in disabled:
            cnt += 1
        if "Clamp Air Pressure Fault" in disabled:
            cnt += 2
        if "Main Air Fault" in disabled:
            cnt += 4
        if "Over Speed Fault" in disabled:
            cnt += 8
        hex_cnt = hex(cnt).replace("0x", "").upper()
        return hex_cnt

    def disable_auxiliary_fault(self):
        if "Auxiliary #1 Fault" in self.get_disabled_faults():
            print("Auxiliary fault already disabled.")
        else:
            self.change_config("DISFAULTS:080%s" % self._get_z_disfault())

    def enable_auxiliary_fault(self):
        if "Auxiliary #1 Fault" in self.get_enabled_faults():
            print("Auxiliary fault already enabled.")
        else:
            self.change_config("DISFAULTS:000%s" % self._get_z_disfault())

    def _disable_fault(self, fault, weight):
        disabled = self.get_disabled_faults()
        if fault in disabled:
            print("%s already disabled.")
        else:
            if "Auxiliary #1 Fault" in disabled:
                aux = 8
            else:
                aux = 0
            z = int(self._get_z_disfault(), base=16) + weight
            z = hex(z).replace("0x", "")
            cmd = "DISFAULTS:0%s0%s" % (aux, z)
            print("Executing cmd: %s" % cmd)
            self.change_config(cmd)

    def _enable_fault(self, fault, weight):
        enabled = self.get_enabled_faults()
        if fault in enabled:
            print("%s already enabled.")
        else:
            if "Auxiliary #1 Fault" in enabled:
                aux = 0
            else:
                aux = 8
            z = int(self._get_z_disfault(), base=16) - weight
            z = hex(z).replace("0x", "")
            self.change_config("DISFAULTS:0%s0%s" % (aux, z))

    def disable_air_bearing_fault(self):
        self._disable_fault("Bearing Air Fault", 1)

    def enable_air_bearing_fault(self):
        self._enable_fault("Bearing Air Fault", 1)

    def disable_clamp_air_fault(self):
        self._disable_fault("Clamp Air Pressure Fault", 2)

    def enable_clamp_air_fault(self):
        self._enable_fault("Clamp Air Pressure Fault", 2)

    def disable_main_air_fault(self):
        self._disable_fault("Main Air Fult", 4)

    def enable_main_air_fault(self):
        self._enable_fault("Main Air Fult", 4)

    def disable_over_speed_fault(self):
        self._disable_fault("Over Speed Fult", 8)

    def enable_over_speed_fault(self):
        self._enable_fault("Over Speed Fult", 8)

    def set_encoder_resolution(self, value):
        """Set the encoder resolution (steps/rev)."""
        self._check_int_in_range(value, vmin=1, vmax=99999)
        self.change_config("ENCODERCOUNT:%05d" % value)

    def set_encoder_type_single_ended(self):
        """Set the encoder to single-ended type."""
        self.change_config("ENCODERTYPE:S")

    def set_encoder_type_differential(self):
        """Set the encoder to differential inputs type."""
        self.change_config("ENCODERTYPE:D")

    def ext_enable_on(self):
        """
        Set external enable mode on.

        Causes the drive to be enabled using the external enable signal on
        SC2, pin 6. A TTL low will enable the amplifier.

        If FLL speed control is used, the amplifier will also use the
        external direction input on SC2, pin 13 to determine what direction
        to run (TTL low = CW, TTL high/floating = CCW).
        """
        self.change_config("EXTEN")

    def set_ext_trig_divider(self, divider):
        """
        Set the divider of the external triggering signal (default=1).

        Used to add a divider to the FLL velocity control loop feedback when
        in external FLL mode. This is used if the maximum frequency available
        to the LA2000 is limited. Setting FEEDDIV to anything other than 1
        allows a higher speed for a given input frequency, but reduces
        performance, particularly low speed jitter.
        Choices are 1, 2, 4, and 8, with a default of 1.
        """
        if not isinstance(divider, int):
            raise TypeError("'divider' must be int.")
        if divider not in [1, 2, 4, 8]:
            raise ValueError("'divider' must be 1, 2, 4 or 8.")
        self.change_config("FEEDDIV:%d" % divider)

    def ext_trig_on(self):
        """
        Set external trigger mode on: FLL on external frequency reference.

        External reference must be connected to the optional BNC jack or the
        BDC compatibility connector.

        The relationship between input frequency and motor speed is given by:
        f_Hz = (speed_RPM * encoder_resolution) / (FEEDIV * 60Hz)
        speed_RPM = f_Hz/encoder_resolution * (FEEDEV * 60Hz)
        """
        self.change_config("FLLEXTERNAL")

    def set_ext_trig_gain(self, value):
        """
        Set the proportional gain of the digital portion of the FLL loop.

        Valid range is -3 to 3, expressed in powers of two to result in
        gain settings from 1/8 to 8. The default and recommended setting is 0
        for unity digital gain.
        """
        self._check_int_in_range(value, vmin=-3, vmax=3)
        self.change_config("FLLGAIN:%d" % value)

    def ext_trig_off(self):
        """
        Set external trigger mode off: FLL on internal frequency reference.
        """
        self.change_config("FLLINTERNAL")

    def ext_enable_off(self):
        """
        Set external enable mode off.

        Causes the system to accept enable and direction commands over the
        communication interface (or RS232) as opposed to the external TTL
        interface.
        """
        self.change_config("INTEN")

    def jerk(self, value):
        """
        Set jerk for s-curve positioning with the positioner option.
        Units are RPM / second squared.
        """
        self._check_int_in_range(value, vmin=0, vmax=9999)
        self.change_config("JERK:%04d" % value)

    # def enable_stop_and_lock(self):
    #     self.change_config("LOCKMODE1")

    # def disable_stop_and_lock(self):
    #     self.change_config("LOCKMODE0")

    def enable_min_speed_input(self):
        """
        Enable drive only if the external frequency input is above the
        speed set with set_min_speed().
        """
        self.change_config("MINSPDIN")

    def disable_min_speed_input(self):
        """Normal operation with external frequency input."""
        self.change_config("MINSPDOUT")

    def set_overspeed(self, value):
        """Set the speed (RPM) at which an over speed fault will occur."""
        if not isinstance(value, int):
            raise TypeError("'value' must be int.")
        if value not in range(0, 100000):
            raise ValueError("'value' must be an int betweem 0 and 99990.")
        self.change_config("OVERSPEED:%05d" % value)

    def set_min_speed(self, value):
        """Set the speed (RPM) at which the drive is enabled in MINSPD mode."""
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self.change_config("STARTS:%05d" % value)

    def set_stop_speed(self, value):
        """
        Set the speed (RPM) at which the drive is disabled after receiving a
        STOP command. Default is 30 RPM.
        """
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self.change_config("STOPS:%05d" % value)

    def set_max_speed(self, value):
        """Set the max speed (RPM) that can be set as a target."""
        self._check_int_in_range(value, vmin=0, vmax=99999)
        self.change_config("TOPSPEED:%05d" % value)
