import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks, peak_widths
from tetramm_exnovo import Tetramm

plt.ion()

t = Tetramm()

xray_freq = 986.71

def plot():
    plt.plot(t.t*1e3, t.data[:, 3]*1e3, '.-')
    plt.xlabel("time (ms)")
    plt.ylabel("current (mA)")
    plt.grid()
    plt.tight_layout()

def test_fastnaq(int_time=1, debug=True):
    # estimate noise and signal amplitude
    naq = int_time*100e3
    t.fastnaq(naq=naq, debug=debug)
    mean = t.data[:, 3].mean()
    for k, ti in enumerate(t.t):
        if t.data[k, 3] > mean:
            break
    t.t -= ti
    plot()
    distance = 100e3/xray_freq/2
    peaks = find_peaks(t.data[:,3], height=mean, distance=distance)[0]
    for p in peaks:
        plt.axvline(t.t[p]*1e3, 0, 0.2, color='r')
    t.peaks = peaks
    t.widths = peak_widths(t.data[:, 3], peaks=peaks)[0]*(t.t[1]-t.t[0])
    print("mean of maxima is:", np.mean(t.data[peaks, 3]))
    print("mean peak width is:", np.mean(t.widths))
    # plt.axhline(t.data[:, 3].mean()*1e3, 0, int_time*1e3, label='mean')
    # plt.axhline(t.data[:, 3].min()*1e3, 0, int_time*1e3, label='min')
    # plt.axhline(t.data[:, 3].max()*1e3, 0, int_time*1e3, label='max')
    # plt.legend()

# RESULT
#
# fastnaq
# -------
# PD4 reading, 100e3 acquisitions at 100 kHz (1 sec)
# background: mean=3.7e-11 A, std=1e-7 A (shutter closed) 
# peaks: width ~= 70us (both in chopper tunnel and chopper step)

def test_naq(int_time, data_rate=1000, debug=True):
    t.data_rate = data_rate
    naq = int_time*data_rate
    t.get_current(naq=naq, timetrace=True)
    mean = t.data[:, 3].mean()
    for k, ti in enumerate(t.t):
        if t.data[k, 3] > mean:
            break
    t.t -= ti
    plot()
