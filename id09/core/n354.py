# -*- coding: utf-8 -*-
"""N354 fpga synchronization board control."""

__author__ = "Marco Cammarata, Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "11/06/2020"
__version__ = "0.0.1"


import os
import time
import numpy as np
import math
import array
import gevent

from id09.colors import bold_underline, print_dim, print_red


try:
    import bliss.comm.util
    get_comm = bliss.comm.util.get_comm
    USE_BLISS_SOCKET = True
except ImportError:
    import socket
    USE_BLISS_SOCKET = False


try:
    from bliss import global_map
    bliss_global_map = True
except ImportError:
    bliss_global_map = False


try:
    from bliss.common.logtools import log_warning, log_debug, log_critical
except ImportError:

    def log_warning(self, s, *args):
        print(s)

    def log_debug(self, s, *args):
        print(s)

    def log_critical(self, s, *args):
        print(s)


try:
    from id09.utils import yaml_storage
    id09_yaml = True
except ImportError:
    id09_yaml = False
    print("Could not import id09_yaml: N354 offsets will not be saved on file")


CONFIG_folder = "/data/id09/archive/configurations/n354_configs/"

OFFSETS_fname = "/data/id09/archive/configurations/n354_offsets.txt"

BIT_fname = "/data/id09/archive/Electronic/N354/GUI_program/n354id09.bit"


if id09_yaml:
    OFFSETS = yaml_storage.Storage(filename=OFFSETS_fname)
else:
    # log_warning("You are not using yaml_storage to handle N354 offsets !")
    OFFSETS = {}
    with open(OFFSETS_fname, 'r') as f:
        lines = f.readlines()
    for ln in lines:
        if not ln.startswith("#"):
            ln = ln.strip().split(" ")
            OFFSETS[ln[0]] = float(ln[1])


N354_IPs = dict(n1="id09n354a", n2="id09n354b")


ESRF_RF = dict(rf_f=352.374e6)
ESRF_RF["rf_t"] = 1. / ESRF_RF["rf_f"]
ESRF_RF["orbit_f"] = ESRF_RF["rf_f"] / 992.
ESRF_RF["orbit_t"] = 1 / ESRF_RF["orbit_f"]
ESRF_RF["hs_chopper_f"] = ESRF_RF["orbit_f"] / 360.
ESRF_RF["hs_chopper_t"] = 1 / ESRF_RF["hs_chopper_f"]


FINE_DELAY_STEP = 11.008e-12


OSC_CALIB = {"id09n354a": dict(), "id09n354b": dict()}

RF256_CALIB = {"id09n354a": dict(), "id09n354b": dict()}


LXT_LASER_OFF_POSITION = 9999.9999


# CALIB FOR USUAL BOARD (id09n354a)
ip1 = N354_IPs["n1"]
OSC_CALIB[ip1]["polyfit_step_to_ps"] = [
    +9.952536e-14,
    -2.895597e-11,
    -8.341212e-07,
    -4.433167e-04,
    -1.739082,
    +1.461886e-13,
]

OSC_CALIB[ip1]["polyfit_d2s"] = [
    +8.681682e-16,
    +4.330384e-12,
    -9.804030e-09,
    -1.273244e-04,
    -5.842278e-01,
    -4.359207e-01,
]

# calibration done on June 22 2020, 10h30, T_fpga = 61, T_module=44
OSC_CALIB[ip1]["polyfit_step_to_ps"] = [
    -5.18244e-13,
    +1.40162e-09,
    -2.09016e-06,
    +8.13240e-05,
    -1.75155e+00,
    +0.00000e+00,
]

OSC_CALIB[ip1]["polyfit_d2s"] = [
    +2.618880e-15,
    +1.908961e-11,
    +3.391990e-08,
    -7.936818e-05,
    -5.816356e-01,
    -1.073921e-01,
]

# calibration done on June 22 2020, 13h00, T_fpga = 61, T_module=45
OSC_CALIB[ip1]["polyfit_step_to_ps"] = [
    +4.168449e-13,
    -4.164417e-10,
    -1.123826e-06,
    +7.024548e-05,
    -1.824626e+00,
    +0.000000e+00,
]

OSC_CALIB[ip1]["polyfit_d2s"] = [
    +4.167665e-15,
    +3.109820e-11,
    +6.718810e-08,
    -3.943516e-05,
    -5.622656e-01,
    -5.836211e-01,
]


ip2 = N354_IPs["n2"]
# calibration done on June 22 2020, 17h00, T_fpga = 58, T_module=41
OSC_CALIB[ip2]["polyfit_step_to_ps"] = [
    -1.471885e-12,
    +3.307554e-09,
    -2.981167e-06,
    -1.000598e-04,
    -1.819827e+00,
    +0.000000e+00,
]

OSC_CALIB[ip2]["polyfit_d2s"] = [
    -1.720438e-16,
    -5.642755e-12,
    -4.172530e-08,
    -1.609107e-04,
    -5.750239e-01,
    -9.511937e-01,
]

RF256_CALIB[ip2]["polyfit_step_to_ps"] = [
    +2.972954e-09,
    -1.940404e-06,
    +4.123553e-04,
    -2.836061e-02,
    +1.114833e+01,
    +0.000000e+00,
]

RF256_CALIB[ip2]["polyfit_d2s"] = [
    -1.455110e-15,
    +1.045726e-11,
    -2.419034e-08,
    +1.723302e-05,
    +9.117948e-02,
    -7.658382e-02,
]

# done on June 23rd 10h20
RF256_CALIB[ip2]["polyfit_d2s"] = [
    +2.728008e-16,
    -1.597640e-12,
    +3.304313e-09,
    -3.791235e-06,
    +9.319126e-02,
    +6.475239e-02,
]


def floor(x):
    if int(x) == x:
        return x
    elif x >= 0:
        return int(x)
    else:
        return int(x-1)


def round_next(x, step):
    return (floor(x/step+0.5)*step)


def signed_modulus(x, d):
    return x - round_next(x, d)


def _int_as_bits(value):
    return "{0:b}".format(value)


def _hex_as_bits(value):
    return _int_as_bits(int(value, 16))


def _as_bits(value):
    if isinstance(value, str) and value.startswith("0x"):
        value = int(value, 16)
    return _int_as_bits(value)


def _bits_as_int(value):
    return int(value, 2)


def delay_partitioner(delay, period, verbose=False):
    """Partition delay between xshut and laser."""
    coarse_delay = int(delay/period) * period
    fine_delay = delay - coarse_delay
    if verbose:
        print(coarse_delay, fine_delay)
    return coarse_delay, fine_delay


class TimeDelay:
    """
    Class to handle time delays string representation.

    Parameters
    ----------
    value : float
        Time delay value (s).

    Methods
    -------
    as_short_str, as_medium_str, as_long_str

    """

    def __init__(self, value):
        self.value = value

    def as_short_str(self):
        t = self.value
        if t == 0:
            return "0 s"
        elif abs(t) < 1e-9:
            return "%.3f ps" % (t * 1e12)
        elif abs(t) < 1e-6:
            return "%.3f ns" % (t * 1e9)
        elif abs(t) < 1e-3:
            # return "%.4g us" % (t * 1e6)
            return "%.3f us" % (t * 1e6)
        elif abs(t) < 1:
            return "%.3f ms" % (t * 1e3)
        else:
            return "%.3f s" % t

    def as_long_str(self):
        t = "%0.12f" % self.value
        s = t.split(".")
        sec = s[0]
        msec = s[1][0:3]
        usec = s[1][3:6]
        nsec = s[1][6:9]
        psec = s[1][9:12]
        return "%3ss %sms %sus %sns %sps" % (sec, msec, usec, nsec, psec)

    def as_medium_str(self):
        t = "%0.12f" % self.value
        s = t.split(".")
        sec = s[0]
        msec = s[1][0:3]
        usec = s[1][3:6]
        nsec = s[1][6:9]
        psec = s[1][9:12]
        ret = ""
        if int(sec) != 0:
            ret += "%3ss" % (sec)
        if int(msec) != 0:
            ret += " %3sms" % (msec)
        if int(usec) != 0:
            ret += " %3sus" % (usec)
        if int(nsec) != 0:
            ret += " %3sns" % (nsec)
        if int(psec) != 0:
            ret += " %3sps" % (psec)
        ret = ret.lstrip()
        return ret

    def __repr__(self):
        return self.as_medium_str()

    def __str__(self):
        return self.as_short_str()


class Number:
    """
    This class allows to:
    - automatically detect if input is binary, hex, int, or list of bool
    - convert input to binary, hex, int, or list of bool
    - handle operations on bits

    Parameters
    ----------
    value : int, str or list of bool
        Input number.
    nbits : int
        Number of bits.

    Methods
    -------
    as_int, as_hex, as_bits, as_bool, get_bits, change_bits

    Notes
    -----
    - bit position = 0 --> last string digit (self._bits_value[-1])

    """

    def __init__(self, value, nbits=32):
        if isinstance(value, Number):
            self._bits_value = value._bits_value
        elif isinstance(value, str):
            if set(value) == {"0", "1"}:
                self._bits_value = value
            elif value.startswith("0x"):
                self._bits_value = _hex_as_bits(value)
            elif value.isdigit():
                self._bits_value = _int_as_bits(int(value))
            else:
                raise ValueError("Cannot interpret 'value' as " +
                                 "bit, hex or int.")
        elif isinstance(value, (list, tuple)) and isinstance(value[0], bool):
            value = [str(int(el)) for el in value]
            self._bits_value = "".join(value)
        elif isinstance(value, int):
            self._bits_value = _int_as_bits(value)
        else:
            raise ValueError("'value' must be str or list/tuple of bool.")

        self._nbits = nbits

    def as_int(self):
        return int(self._bits_value, 2)

    def as_hex(self):
        return hex(self.as_int())

    def as_bits(self, nbits=None):
        r = self._bits_value
        if nbits is not None:
            r = "0" * (nbits - len(r)) + r
        return r

    def as_bool(self):
        """Convert to list of bool."""
        return [n == "1" for n in self._bits_value]

    def get_bits(self, first_bit, last_bit=None, nbits=1):
        if last_bit is not None:
            if last_bit < first_bit:
                raise ValueError("'last_bit' must be >= 'first_bit'.")
            nbits = last_bit - first_bit + 1
        if first_bit > self._nbits - 1:
            raise Exception("'first_bit' > number of available bits.")
        if first_bit + nbits - 1 > self._nbits - 1:
            raise Exception("last bit > number of available bits.")
        ret = (self.as_int() >> first_bit) & ((1 << nbits) - 1)
        return Number(ret)

    def change_bits(self, bit_pos, new_value, nbits=None):
        new_value = Number(new_value).as_bits(nbits=nbits)
        if not isinstance(bit_pos, int):
            raise ValueError("'bit_pos' must be int.")
        nbits = len(new_value)
        if bit_pos > self._nbits - 1:
            raise Exception("'bitpos' > number of available bits.")
        bits = self._bits_value[::-1]
        ret = bits[:bit_pos] + new_value[::-1] + bits[bit_pos+nbits:]
        ret = ret[::-1]
        ret = Number(ret)
        if ret.as_int() > (1 << self._nbits)-1:
            raise ValueError("overflown the number of available bits")
        return ret

    def __repr__(self):
        return self.as_bits()


class NonLinearDelay:
    """

    """

    def __init__(self, step_to_ps, n_steps=100):
        self.step_to_ps = step_to_ps
        self.n_steps = n_steps
        n = np.arange(n_steps)
        self.delays = np.polyval(step_to_ps, n) * 1e-12
        self.resolution = np.min(np.diff(self.delays))
        if self.delays[1] > self.delays[0]:
            self.is_increasing = True
        else:
            self.is_increasing = False

    def _check_range(self, delay, raise_if_out_of_range=False):
        delays = self.delays
        is_out_of_range = (delay < delays[0]) or (delay > delays[-1])
        if is_out_of_range:
            if raise_if_out_of_range:
                raise ValueError(
                    "Delay can go from",
                    delays.min(),
                    "to",
                    delays.max(),
                    "asked for",
                    delay,
                )
            else:
                delay = np.argmin(np.abs(delays - delay))
        return delay

    def delay(self, step, raise_if_out_of_range=False):
        step = int(round(step))
        if (step < 0) or (step > self.n_steps - 1):
            if raise_if_out_of_range:
                raise ValueError(
                    "Cannot move by %d steps. " % step +
                    "Max number of steps is %d." % (self.n_steps - 1)
                )
            else:
                if step > self.n_steps - 1:
                    step = self.n_steps - 1
                else:
                    step = 0
        return float(self.delays[step])

    def steps(self, delay, raise_if_out_of_range=False):
        delays = self.delays
        delay = self._check_range(delay)
        idx = np.argmin(np.abs(delays - delay))
        return idx


class DelayChannel:
    """
    Class to control a delay channel of the N354 synchronization board.

    Parameters
    ----------
    n354_instance : N354 obj
        N354 obj instance.
    name : str
        Channel name.
    control_register_address : str
        Address of the channel control register (hexadecimal).
    delay_register_address : str, optional
        Address of the channel delay register (hexadecimal).
        If 'auto' (default), the address is automatically guessed from the
        'control_register_address'.
    fine_delay_register_address : str or None, optional
        Address of the channel fine delay register (hexadecimal).
        If None (default), the channel is assumed to have coarse delay only.
    nbits : int
        Number of bits of the delay register.
    group : int
        Group identifier for the delay channel.
    steps_in_RF : int
        Coarse delay steps in unit of the RF period.

    Properties
    ----------
    coarse_step : float
        Coarse delay step (sec).
    fine_step : float
        Fine delay step (sec).

    Notes
    -----
    - A delay channel needs a delay register and a control register.
    - The control register is used to store information about:
      Soft Control (SC), Polarity Inversion (PI), Toggling Mode (TM) and
      Pulse width (Width).

    """

    def __init__(self, n354_instance, name, group=1, channel=1,
                 fine_delay_register_address=None, nbits=24,
                 dial_offset="from_file", steps_in_RF=1):

        self._n354 = n354_instance
        self.name = name

        if isinstance(dial_offset, str):
            if dial_offset == "from_file":
                dial_offset = OFFSETS[self.name]
            else:
                raise ValueError("'dial_offset' must be float or 'from_file'.")

        reg_offset = 16
        if group == 1:
            reg_offset = reg_offset + 8 * channel
        elif group == 2:
            # 2 outs in group1 (=2*8)
            reg_offset = reg_offset + 2 * 8 + 8 * channel
        elif group == 3:
            # 2 outs in group1 + 4 outs in group4 (=6*8)
            reg_offset = reg_offset + 6 * 8 + 8 * channel
        self._control_register_address = Number(reg_offset).as_hex()
        self._delay_register_address = Number(reg_offset + 4).as_hex()

        self._fine_delay_register_address = fine_delay_register_address
        self.has_fine_delays = fine_delay_register_address is not None
        self.nbits = nbits
        self.group = group
        self.channel = channel
        self.steps_in_RF = steps_in_RF
        self.coarse_step = ESRF_RF["rf_t"] * steps_in_RF
        if self._n354.firmware_variant == 'pic' and self.name == 'heatt':
            # for PIC coarse_step is 2.838 ns * 5/18 = 0.788 ns
            self.coarse_step *= 5/18
        self.fine_step = FINE_DELAY_STEP

        if bliss_global_map:
            global_map.register(self, parents_list=[self._n354])

    def __call__(self, delay):
        self.move_delay(delay)

    def _read_coarse_delay_steps(self):
        address = self._delay_register_address
        delay_steps = self._n354._register_read(address).as_int()
        return delay_steps

    def _read_fine_delay_steps(self):
        if not self.has_fine_delays:
            delay_steps = 0
        else:
            address = self._fine_delay_register_address
            delay_steps = self._n354._register_read(address).as_int()
        return delay_steps

    def _move_coarse_delay(self, delay, as_steps=False,
                           raise_if_out_of_range=True):

        if not as_steps:
            delay_steps = int(delay / self.coarse_step)
        else:
            # int() is used in case delay is np.int ...
            delay_steps = int(delay)

        new_delay = Number(delay_steps, nbits=self.nbits)
        new_steps = new_delay.as_int()
        # print(self.name, new_steps, new_delay,)

        if raise_if_out_of_range:
            if (new_steps < 0) or (new_steps > (1 << self.nbits)-1):
                raise ValueError(
                    "Coarse delay of '%s' cannot move to " % self.name +
                    "%s steps (it would be outside its limits)." % new_steps
                )

        self._n354._register_write(self._delay_register_address, new_delay)

    def _move_fine_delay(self, delay, as_steps=False,
                         raise_if_out_of_range=True):

        if not self.has_fine_delays:
            log_warning(self, "The motor %s does not have fine delays",
                        self.name)
            return
        if not as_steps:
            delay_steps = int(delay / self.fine_step)
        else:
            # int() is used in case delay is np.int ...
            delay_steps = int(delay)

        new_delay = Number(delay_steps, nbits=9)
        new_steps = new_delay.as_int()

        if raise_if_out_of_range:
            if (new_steps < 0) or (new_steps > (1 << 9) - 1):
                raise ValueError(
                    "Fine delay of '%s' cannot move to " % self.name +
                    "%s (it would be outside its limits)." % self.new_delay
                )

        self._n354._register_write(self._fine_delay_register_address,
                                   new_delay)

    def _read_delay(self):
        coarse_steps = self._read_coarse_delay_steps()
        fine_steps = self._read_fine_delay_steps()
        coarse_delay = coarse_steps * self.coarse_step
        fine_delay = fine_steps * self.fine_step
        delay_dial = coarse_delay + fine_delay
        delay = delay_dial - self.dial_offset
        return dict(
            coarse_steps=coarse_steps,
            fine_steps=fine_steps,
            coarse_delay=coarse_delay,
            fine_delay=fine_delay,
            delay_dial=delay_dial,
            delay=delay,
        )

    def read_delay(self, as_dial=False, as_steps=False):
        ret = self._read_delay()
        if as_steps:
            return ret["coarse_steps"], ret["fine_steps"]
        else:
            if as_dial:
                return ret["delay_dial"]
            else:
                return ret["delay"]

    @property
    def dial_offset(self):
        return OFFSETS[self.name]

    @dial_offset.setter
    def dial_offset(self, value):
        # this is used to automatically trigger the saving
        # when assigning a new offset using xshut.dial_offset=0
        OFFSETS[self.name] = float(value)

    def set_delay(self, value):
        """Set current position to value."""
        dial = self.read_delay(as_dial=True)
        offset = dial - value
        self.dial_offset = offset
#        OFFSETS[self.name] = float(offset)
        log_debug(self, "Resetting offset of '%s' from " % self.name +
                        str(self.dial_offset) + " to " + str(offset))
#        self.dial_offset = offset
        return self.read_delay(as_dial=True)

    def move_relative(self, delta_delay, as_steps=False,
                      raise_if_out_of_range=True):
        """Move delay relative by a delta delay."""
        current_pos = np.asarray(self.read_delay(as_steps=as_steps))
        if as_steps:
            delta_delay = np.asarray(delta_delay)
            new_pos = list(current_pos+delta_delay)
        else:
            new_pos = current_pos+delta_delay
        self.move_delay(new_pos, as_steps=as_steps,
                        raise_if_out_of_range=raise_if_out_of_range)

    def move_by_chopper_periods(self, nperiods, raise_if_out_of_range=True):
        """Move delay relative in steps of the chopper period."""
        if not isinstance(nperiods, int):
            raise TypeError("'nperiods' must be int.")
        delta_delay = ESRF_RF["hs_chopper_t"]*nperiods
        self.move_relative(delta_delay=delta_delay, as_steps=False,
                           raise_if_out_of_range=raise_if_out_of_range)

    def move_delay(self, delay, as_dial=False, as_steps=False,
                   raise_if_out_of_range=True):

        if not as_steps:
            if not as_dial:
                if delay + self.dial_offset < 0:
                    raise ValueError(
                        "Cannot move '%s' delay " % self.name +
                        "to %s. Dial delay would be " % TimeDelay(delay) +
                        "negative (%s) " % TimeDelay(delay+self.dial_offset) +
                        "since offset = %s." % TimeDelay(self.dial_offset)
                    )
                delay = delay + self.dial_offset
            if self.has_fine_delays:
                coarse_steps, fine_delay = divmod(delay, self.coarse_step)
                coarse_steps = round(coarse_steps)
                fine_steps = round(fine_delay / self.fine_step)
            else:
                coarse_steps = round(delay / self.coarse_step)
                fine_steps = None
        else:
            if self.has_fine_delays:
                coarse_steps, fine_steps = delay
            else:
                if isinstance(delay, (tuple, list)):
                    delay = delay[0]
                coarse_steps = delay
                fine_steps = None

        if raise_if_out_of_range and not self._n354.firmware_variant == 'pic':
            check_delay = coarse_steps*self.coarse_step
            if fine_steps is not None:
                check_delay += fine_steps*self.fine_step
            rep_rate = self.read_frequency()
            if check_delay > 1/rep_rate:
                raise ValueError(
                    "Cannot move '%s' dial delay " % self.name +
                    "to %s " % TimeDelay(check_delay) +
                    "(freq=%.3f Hz)." % rep_rate
                )

        log_debug(self, "Partitioning delay for motor '%s' " % self.name +
                        "(coarse_steps, fine_steps) = " +
                        "(%s, %s)" % (str(coarse_steps), str(fine_steps)))

        self._move_coarse_delay(coarse_steps, as_steps=True,
                                raise_if_out_of_range=raise_if_out_of_range)

        if fine_steps is not None:
            self._move_fine_delay(fine_steps, as_steps=True,
                                  raise_if_out_of_range=raise_if_out_of_range)

    def _read_control_register(self):

        ret = self._n354._register_read(self._control_register_address)

        ret = dict(
            freq_divider=ret.get_bits(0, 7).as_int(),
            width_selector=ret.get_bits(8, 9).as_int(),
            is_toggled=ret.get_bits(10).as_bool()[0],
            is_inverted=ret.get_bits(11).as_bool()[0],
            is_software_controlled=ret.get_bits(12).as_bool()[0],
        )

        return ret

    def _write_control_register(self, **kwargs):
        reg = self._read_control_register()
        names = ["freq_divider", "width_selector", "is_toggled", "is_inverted",
                 "is_software_controlled"]
        nbits = [8, 2, 1, 1, 1]
        names = names[::-1]
        nbits = nbits[::-1]
        for key in kwargs:
            if key not in names:
                raise ValueError(
                    "Control parameter '%s' is not defined. " % key +
                    "Available ones are: %s'" % str(names)
                )
        reg.update(kwargs)

        new_reg = ""
        for name, n in zip(names, nbits):
            new_reg += Number(reg[name]).as_bits(nbits=n)

        address = self._control_register_address
        return self._n354._register_write(address, new_reg)

    def change_shaper(self, width_selector=None, is_toggled=None,
                      is_inverted=None, is_software_controlled=None):

        tochange = dict()
        if width_selector is not None:
            width_selector = int(width_selector)
            if width_selector > 3:
                raise ValueError(
                    "Channel %s, width selector " % self.name + "must be 0-3"
                )
            tochange["width_selector"] = width_selector
        if is_toggled is not None:
            tochange["is_toggled"] = bool(is_toggled)
        if is_inverted is not None:
            tochange["is_inverted"] = bool(is_inverted)
        if is_software_controlled is not None:
            tochange["is_software_controlled"] = bool(is_software_controlled)

        self._write_control_register(**tochange)

    def read_pulse_width(self):
        control_register = self._read_control_register()
        selector = control_register["width_selector"]
        pulse_width = TimeDelay(0)
        if self.group == 1 or self.group == 2:
            if selector == 0:
                pulse_width = TimeDelay(190e-9)
            elif selector == 1:
                pulse_width = TimeDelay(370e-9)
            elif selector == 2:
                pulse_width = TimeDelay(1.5e-6)
            elif selector == 3:
                pulse_width = TimeDelay(6e-6)
        elif self.group == 3:
            if selector == 0:
                pulse_width = TimeDelay(190e-9)
            elif selector == 1:
                pulse_width = TimeDelay(2.9e-6)
            elif selector == 2:
                pulse_width = TimeDelay(50e-6)
            elif selector == 3:
                pulse_width = TimeDelay(3e-3)
        return pulse_width

    def read_frequency(self, as_dividers=False):
        div1, div2, div3 = self._n354._read_group_frequency_divider()
        if self.group == 1:
            div_group = div1
        elif self.group == 2:
            div_group = div1 * div2
        elif self.group == 3:
            div_group = div1 * div3
        control_reg = self._read_control_register()
        div_channel = control_reg["freq_divider"]
        if as_dividers:
            return div_group, div_channel
        else:
            if div_channel != 0:
                frequency = ESRF_RF["orbit_f"] / (div_group * div_channel)
            else:
                return 0
            delay = self.read_delay()
            if delay > 1/frequency:
                return 1/delay
            else:
                return frequency

    def move_frequency(self, freq, as_divider=False):
        """Change channel divider in order to change frequency to `freq`."""
        if as_divider:
            divider = int(freq)
        else:
            dividers = self.read_frequency(as_dividers=True)
            group_divider = dividers[0]
            divider = ESRF_RF["orbit_f"] / group_divider / freq
            divider = round(divider)
        if divider > 0:
            self._write_control_register(freq_divider=divider)
        else:
            print("ERROR: cannot set channel divider to 0")

    def as_long_str(self, print_offsets=False, print_enhance=True):
        if print_enhance:
            s = bold_underline(self.name)
        else:
            s = self.name
        ret = self._read_delay()
        fine_delay = TimeDelay(ret["fine_delay"]).as_long_str()[-11:]
        fine_steps = ret["fine_steps"]
        coarse_delay = TimeDelay(ret["coarse_delay"]).as_long_str()
        coarse_steps = ret["coarse_steps"]
        s += "\n - Delay        "
        s += TimeDelay(ret["delay"]).as_long_str()
        if print_offsets:
            s += "\n - Offset       "
            s += TimeDelay(self.dial_offset).as_long_str()
        s += "\n - Dial Delay   "
        s += TimeDelay(ret["delay_dial"]).as_long_str()
        s += "\n   - coarse     %s (%10s steps" % (coarse_delay, coarse_steps)
        s += " @ %d RF)" % (int(self.coarse_step / ESRF_RF["rf_t"]))
        if self.has_fine_delays:
            s += "\n   - fine                        %s" % fine_delay
            s += " (%10s steps @ %.3f ps)" % (fine_steps, self.fine_step*1e12)
        # else:
        #     s += "\n - Fine delay     Not available"
        freq = self.read_frequency()
        freq_div = self.read_frequency(as_dividers=True)
        s += "\n - Frequency      %7.3f Hz " % freq
        s += "(group div: %4d, channel div: %4d)" % (freq_div[0], freq_div[1])
        control = self._read_control_register()
        s += "\n - Width          %s" % self.read_pulse_width().as_short_str()
        s += "\n - Toggled?       "
        s += "yes" if control["is_toggled"] else "no"
        s += "\n - Inverted?      "
        s += "yes" if control["is_inverted"] else "no"
        s += "\n - Soft Control?  "
        s += "yes" if control["is_software_controlled"] else "no"
        return s

    def as_short_str(self, as_dial=False):
        ret = self._read_delay()
        if as_dial:
            delay = ret["delay_dial"]
        else:
            delay = ret["delay"]
        coarse = ret["coarse_steps"]
        fine = ret["fine_steps"]
        s = self.name + " @ " + TimeDelay(delay).as_short_str()
        if self.steps_in_RF == 1:
            s += " (RF"
        else:
            s += " (RF/%d" % self.steps_in_RF
        if self.has_fine_delays:
            s += ", RF/256 = %d, %d steps)" % (coarse, fine)
        else:
            s += " = %d steps)" % coarse
        # if self.has_fine_delays:
        #     s += " (%d %dxRF steps, %d fine steps" % (coarse,
        #                                               self.steps_in_RF,
        #                                               fine)
        # else:
        #     s += " (%d %dxRF steps" % (coarse, self.steps_in_RF)
        freq = self.read_frequency()
        s += ", freq=%.3f Hz" % (freq)
        return s

    def __info__(self):
        return self.as_long_str(print_enhance=False)

    def __repr__(self):
        return self.as_long_str()


class Oscillator:
    """
    Class to control the delay channel of the N354 used for synchronization
    of the ultrafast laser femtosecond oscillator.

    The N354 controls the OCK (sine wave) output (used to synchronize the
    ultrafast laser femtosecond oscillator) through 2 items:
    - Delay Line Locked (DLL)
    - Analog Phase Shifter

    The OCK output delay can be changed:
    - through the DLL in steps of:
      a) RF clock periods (from 0 to 3)
      a) RF/2 clock periods (from 1/2 to 7/2)
      b) ± RF/256 clock periods (~11 ps) (from ?? to ??)
    - through the Analog Phase Shifter in steps of:
      c) ...

    Max time delay = 3*RF + 1*RF/2 + 255*RF/256


    The OCK output is resynchronized with the prescaler when the command
    "RF DLL reset" is issued.
    It is not sensitive to the orbit synchronization input.

    Parameters
    ----------
    n354_instance : N354 obj
        N354 obj instance.
    dll_register address : str, optional
        Address of the OCK DLL register (hexadecimal).
    phase_shifter_register_address : str, optional
        Address of the OCK phase shifter register (hexadecimal).
    rf_t : str, optional
        Period of the RF signal (sec).
    step_to_ps_calib : array-like or None, optional
        Polynomial coefficients (from highest degree to the constant term)
        for delay calibration (conversion from step to delay).
        Delay are assumed to be in seconds.
    d2s_calib : array-like or None, optional
        Polynomial coefficients (from highest degree to the constant term)
        for step calibration (conversion from delay to step).
        Delay are assumed to be in seconds.
    use_rf2 : bool, optional
        If True (default), the RF/2 clock is used.
    use_rf256 : bool, optional
        If True (default), the RF/256 clock is used.

    """

    def __init__(self, n354_instance, name, dll_register_address="0x78",
                 phase_shifter_register_address="0x7c", rf_t=ESRF_RF["rf_t"],
                 phase_shifter_step_to_ps="auto", rf256_step_to_ps="auto",
                 dial_offset="from_file", use_rf2=False, use_rf256=True):

        self._n354 = n354_instance
        self.name = name
        if isinstance(dial_offset, str):
            if dial_offset == "from_file":
                dial_offset = OFFSETS[self.name]
            else:
                raise ValueError("'dial_offset' must be float or 'from_file'.")
        self.dll_register_address = dll_register_address
        self.phase_shifter_register_address = phase_shifter_register_address
        self.rf_t = rf_t
        self.dial_offset = dial_offset

        if phase_shifter_step_to_ps == "auto":
            key = "polyfit_step_to_ps"
            phase_shifter_step_to_ps = OSC_CALIB[self._n354.address[0]][key]
        elif phase_shifter_step_to_ps is None:
            phase_shifter_step_to_ps = [-2.5, 0]

        if rf256_step_to_ps == "auto":
            key = "polyfit_step_to_psv"
            rf256_step_to_ps = RF256_CALIB[self._n354.address[0]][key]
        elif rf256_step_to_ps is None:
            rf256_step_to_ps = [11.5, 0]

        self._phase_shifter_non_lin = NonLinearDelay(phase_shifter_step_to_ps,
                                                     1024)
        self._rf256_non_lin = NonLinearDelay(rf256_step_to_ps, 256)
        # self.phase_shifter_step_to_ps = phase_shifter_step_to_ps
        # self.rf256_step_to_ps = rf256_step_to_ps
        self.use_rf256 = use_rf256
        self.use_rf2 = use_rf2

    def _read_delay(self):

        # read dll register values
        address = self.dll_register_address
        rv = self._n354._register_read(address)
        dll_rf_steps = rv.get_bits(10, 11).as_int()  # could be (10, 11)
        dll_rf2_steps = rv.get_bits(9).as_int()
        dll_rf256_steps = rv.get_bits(16, 31).as_int()

        # read phase shifter register value
        address = self.phase_shifter_register_address
        phase_shifter_steps = self._n354._register_read(address).as_int()

        # convert delay from step to seconds
        rf_delay = dll_rf_steps * self.rf_t
        rf2_delay = dll_rf2_steps * self.rf_t / 2
        rf256_delay = self._rf256_non_lin.delay(dll_rf256_steps)
        psnld = self._phase_shifter_non_lin.delay(phase_shifter_steps)
        delay_dial = rf_delay + rf2_delay + rf256_delay + psnld
        delay = delay_dial - self.dial_offset

        res = dict(
            rf_steps=dll_rf_steps,
            rf2_steps=dll_rf2_steps,
            rf256_steps=dll_rf256_steps,
            phase_shifter_steps=phase_shifter_steps,
            rf_delay=rf_delay,
            rf2_delay=rf2_delay,
            rf256_delay=rf256_delay,
            phase_shifter_delay=psnld,
            delay_dial=delay_dial,
            delay=delay,
        )

        return res

    def read_delay(self, as_dial=False, as_steps=False):
        ret = self._read_delay()
        if as_steps:
            res = (
                ret["rf_steps"],
                ret["rf2_steps"],
                ret["rf256_steps"],
                ret["phase_shifter_steps"],
            )
        else:
            if as_dial:
                res = ret["delay_dial"]
            else:
                res = ret["delay"]
        return res

    def set_delay(self, value):
        """Set current position to value."""
        value = math.fmod(value, self.rf_t * 4)
        dial = self.read_delay(as_dial=True)
        offset = dial - value
        OFFSETS[self.name] = offset
        log_debug(
            self,
            "Resetting offset of 'oscillator' from"
            + str(self.dial_offset)
            + " to "
            + str(offset),
        )
        self.dial_offset = offset

    def _apply_rf_256(self):
        rv = self._n354._register_read("0x00")
        # reset to zero (manual is wrong, it says bits 2 and 3)
        rv_reset = rv.change_bits(1, 1)
        rv_reset = rv_reset.change_bits(2, 1)
        self._n354._register_write("0x00", rv_reset)
        # now start moving
        rv = self._n354._register_read("0x00")
        rv = rv.change_bits(1, 1)
        self._n354._register_write("0x00", rv)

    def move_delay(self, delay, as_dial=False, as_steps=False, debug=False):

        if not as_steps:
            if not as_dial:
                delay = delay + self.dial_offset

            # wrap delay in the 0 to RF/4 range
            _, delay = divmod(delay, self.rf_t * 4)

            # we can't do the last 255/256*RF_T without using the
            # RF2 steps; so set delay to a little less than 4*RF_T
            delay = min(delay, self.rf_t * 4 - 15e-12)

            dll_rf_steps, delay = divmod(delay, self.rf_t)

            if self.use_rf2:
                if self._rf256_non_lin is not None:
                    dll_rf2_steps, delay = divmod(delay, self.rf_t / 2)
            else:
                dll_rf2_steps = 0

            if self.use_rf256:
                dll_rf256_steps = self._rf256_non_lin.steps(delay)
            else:
                # we can't work with rf256_steps = 0 because we cannot
                # reach the more than 8.5ns
                dll_rf256_steps = 255
            delay -= self._rf256_non_lin.delay(dll_rf256_steps)

            # print("before while loop", delay, dll_rf_steps, dll_rf2_steps,
            #       dll_rf256_steps)
            while delay > 0 and delay > 5e-12:
                if self.use_rf256:
                    delay += self._rf256_non_lin.delay(dll_rf256_steps)
                    dll_rf256_steps += 1
                    delay -= self._rf256_non_lin.delay(dll_rf256_steps)
                    # print(dll_rf256_steps, delay)
                elif self.use_rf2:
                    dll_rf2_steps += 1
                    delay -= self.rf_t / 2
                else:
                    dll_rf_steps += 1
                    delay -= self.rf_t
            # print("after while loop", delay, dll_rf_steps, dll_rf2_steps,
            #       dll_rf256_steps)

            phase_shifter_steps = self._phase_shifter_non_lin.steps(delay)

        else:
            dll_rf_steps = delay[0]
            dll_rf2_steps = delay[1]
            dll_rf256_steps = delay[2]
            phase_shifter_steps = delay[3]

        if debug:
            print("dll_rf_steps ", dll_rf_steps)
            print("dll_rf2_steps: ", dll_rf2_steps)
            print("dll_rf256_steps: ", dll_rf256_steps)
            print("phase_shifter_steps: ", phase_shifter_steps)

        # int is needed because:
        # - round(np.float) returns int
        # - round(np.float64) returns float
        dll_rf_steps = int(round(dll_rf_steps))
        dll_rf2_steps = int(round(dll_rf2_steps))
        dll_rf256_steps = int(round(dll_rf256_steps))
        phase_shifter_steps = int(round(phase_shifter_steps))

        if dll_rf256_steps > 255 or dll_rf256_steps < 0:
            raise ValueError("RF/256 can do 0-255 steps")
        if dll_rf2_steps > 1 or dll_rf2_steps < 0:
            raise ValueError("RF/2 can do 0 or 1 steps")
        if dll_rf_steps > 3 or dll_rf_steps < 0:
            raise ValueError("RF can do 0, 1, 2 or 3 steps")
        if phase_shifter_steps > pow(2, 10):
            raise ValueError("phase shifter is 10 bits delay")

        # work on DLL register
        address = self.dll_register_address
        register = self._n354._register_read(address)
        # CAREFUL: parenthesys are essential for bit shifts !!
        register = dll_rf_steps << 10
        register += dll_rf2_steps << 9
        register += dll_rf256_steps
        register = Number(register)
        self._n354._register_write(address, register)

        self._apply_rf_256()

        # work on oscillator
        address = self.phase_shifter_register_address
        self._n354._register_write(address, phase_shifter_steps)
        return dict(
            dll_rf_steps=dll_rf_steps,
            dll_rf2_steps=dll_rf2_steps,
            dll_rf256_steps=dll_rf256_steps,
            phase_shifter_steps=phase_shifter_steps,
        )

    # def as_short_str(self):
    #     t = TimeDelay(self.read_delay()).as_short_str()
    #     return t

    def as_long_str(self, print_offsets=False, print_enhance=True):
        if print_enhance:
            s = bold_underline(self.name)
        else:
            s = self.name
        ret = self._read_delay()
        rf_delay = TimeDelay(ret["rf_delay"]).as_long_str()[-11:]
        rf2_delay = TimeDelay(ret["rf2_delay"]).as_long_str()[-11:]
        rf256_delay = TimeDelay(ret["rf256_delay"]).as_long_str()[-11:]
        ps_delay = TimeDelay(ret["phase_shifter_delay"]).as_long_str()[-11:]
        delay = TimeDelay(ret["delay"]).as_long_str()[-11:]
        delay_dial = TimeDelay(ret["delay_dial"]).as_long_str()[-11:]
        rf_steps = ret["rf_steps"]
        rf2_steps = ret["rf2_steps"]
        rf256_steps = ret["rf256_steps"]
        ps_steps = ret["phase_shifter_steps"]
        s += "\n - Delay            " + delay
        if print_offsets:
            s += "\n - Offset           "
            s += TimeDelay(self.dial_offset).as_long_str()[-11:]
        s += "\n - Dial Delay       " + delay_dial
        s += "\n   - RF             %s (%3s steps) @ RF" % (rf_delay,
                                                            rf_steps)
        s += "\n   - RF2            %s (%3s steps)" % (rf2_delay, rf2_steps)
        s += " @ RF/2"
        s += "\n   - RF256          %s (%3s steps)" % (rf256_delay,
                                                       rf256_steps)
        s += " @ RF/256"
        s += "\n   - Phase Shifter  %s (%3s steps)" % (ps_delay, ps_steps)
        return s

    def as_short_str(self, as_dial=False):
        ret = self._read_delay()
        if as_dial:
            delay = ret["delay"]
        else:
            delay = ret["delay_dial"]
        s = self.name + " @ " + TimeDelay(delay).as_short_str()
        s += " (RF, RF/2, RF/256, phase = "
        s += "%d, %d, " % (ret["rf_steps"], ret["rf2_steps"])
        s += "%d, %d" % (ret["rf256_steps"], ret["phase_shifter_steps"])
        s += " steps)"
        return s

    def __info__(self):
        return self.as_long_str(print_enhance=False)

    def __repr__(self):
        return self.as_long_str()


class N354_socket:

    def __init__(self, ip="id09n354a", port=5001):

        if USE_BLISS_SOCKET:
            conf = {'tcp': ''}
            opts = {'url': ip, 'port': port}
            self._socket = get_comm(conf, ctype='tcp', **opts)
        else:
            self.address = (ip, port)
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect(self.address)

#        self._socket = socket.socket()

    def _send_raw(self, cmd):
        if USE_BLISS_SOCKET:
            return self._socket.write(cmd)
        else:
            return self._socket.send(cmd)

    def _send(self, cmd):
        if not cmd.endswith("\n"):
            cmd += "\n"
        cmd = cmd.encode("ascii")
        return self._send_raw(cmd)

    def _get_raw(self, nbytes=2048):
        if USE_BLISS_SOCKET:
            return self._socket.raw_read(nbytes)
        else:
            return self._socket.recv(nbytes)

    def _get(self, nbytes=2048):
        ret = self._get_raw(nbytes=nbytes)
        ret = ret.decode("ascii")
        return ret

    def _register_read(self, offset):
        cmd = "?RD CS2 %s" % offset
        if USE_BLISS_SOCKET:
            # bliss uses gevent, make query atomic to avoid different
            # parts to write before reading the answer
            with self._socket.lock:
                self._send(cmd)
                ret = self._get()
        else:
            self._send(cmd)
            ret = self._get()
        return Number(ret)

    def _register_write(self, offset, value):
        value = Number(value).as_hex()

        # do not write if you already have the right value
        if value == self._register_read(offset):
            return value

        cmd = "WR CS2 %s %s" % (offset, value)
        if USE_BLISS_SOCKET:
            # bliss uses gevent, make query atomic to avoid different
            # parts to write before reading the answer
            with self._socket.lock:
                self._send(cmd)
                ret = self._get()
        else:
            self._send(cmd)
            ret = self._get()
        return ret


class N354:

    """ class for N354 board

    Notes
    -----
    - to read the value of a specific register ("0x00") bit (14):
      self._register_read("0x00").get_bits(13)
    - to read the value of a set of register ("0x68") bits (0, 19):
      self._register_read("0x68").get_bits(0, 19).as_int()

    Examples
    --------
    >> self._register_read("0x00").get_bits(13)
    >> self._register_read("0x68").get_bits(16, 21).as_int()

    """

    def __init__(self, ip="id09n354a", port=5001,
                 lxt_laser_off_position=LXT_LASER_OFF_POSITION):

        self.address = (ip, port)
        self._socket = N354_socket(ip=ip, port=port)
        self._register_read = self._socket._register_read
        self._register_write = self._socket._register_write
        self.orbit_t = ESRF_RF["orbit_t"]
        self.rf_t = ESRF_RF["rf_t"]
        self.hs_chopper_t = ESRF_RF["hs_chopper_t"]
        self.config_folder = CONFIG_folder
        self.config_fname_last = None
        self.offsets_fname = OFFSETS_fname
        self.bit_fname = BIT_fname

        self._cs0_mapping = {
            "RF/main_DLL_reset": (0, "w"),
            "RF/osc_rf256_increment": (1, "w"),
            "RF/osc_rf256_decrement": (2, "w"),
            "orbit/calibration_request": (3, "w"),
            "orbit/clear_sync_error": (4, "w"),
            # 5-6: reserved
            "burst/use_ext_trigger": (7, "rw"),
            "orbit/is_sync_monitor_enable": (8, "rw"),
            "is_group1_enable": (9, "rw"),
            "is_group2_enable": (10, "rw"),
            "is_group3_enable": (11, "rw"),
            "burst/is_running": (12, "r"),
            "RF/is_DLL_overflow": (13, "r"),
            "RF/is_DLL_locked": (14, "r"),
            "is_CPU_DLL_locked": (15, "r"),
            "orbit/delay_in_steps": ((16, 22), "r"),
            # 22-23: reserved
            "orbit/is_tuning_done": (24, "r"),
            "orbit/is_tuning_ready": (25, "r"),
            "orbit/is_tuning_error": (26, "r"),
            "orbit/is_wrong_waveform": (27, "r"),
            "pic/is_O12_sweep_running": (28, "r"),
            # 29: reserved
            "orbit/is_sync_error": (30, "r"),
            "RF/is_clock_error": (31, "r"),
        }
        self._info = dict()
        self._read_CS0()
        self._read_CS1()

        self.chopt = DelayChannel(
            self, "chopt", group=1, channel=1, nbits=24, steps_in_RF=1
        )

        self.heatt = DelayChannel(
            self, "heatt", group=1, channel=2, nbits=24, steps_in_RF=1
        )

        self.fspck = DelayChannel(
            self,
            "fspck",
            group=2,
            channel=1,
            nbits=24,
            steps_in_RF=1,
            fine_delay_register_address="0x0c",
        )

        self.nsl = DelayChannel(
            self,
            "nsl",
            group=2,
            channel=2,
            nbits=24,
            steps_in_RF=1,
            fine_delay_register_address="0x10",
        )

        self.fspump = DelayChannel(
            self, "fspump", group=2, channel=3, nbits=24, steps_in_RF=1
        )

        self.spare = DelayChannel(
            self, "spare", group=2, channel=4, nbits=24, steps_in_RF=1
        )

        self.xshut = DelayChannel(
            self, "xshut", group=3, channel=1, nbits=28, steps_in_RF=2
        )

        self.lshut = DelayChannel(
            self, "lshut", group=3, channel=2, nbits=28, steps_in_RF=2
        )

        self.xscope = DelayChannel(
            self, "xscope", group=3, channel=3, nbits=28, steps_in_RF=2
        )

        self.lscope = DelayChannel(
            self, "lscope", group=3, channel=4, nbits=28, steps_in_RF=2
        )

        # self.oscillator = Oscillator(self)
        # tmp fix:
        self.oscillator = Oscillator(self, rf256_step_to_ps=None,
                                     name="oscillator")

        delay_channels = [
            d for d in dir(self) if isinstance(getattr(self, d), DelayChannel)
        ]
        delay_channels = sorted(delay_channels)
        # delay_channels = [getattr(self, d) for d in delay_channels]
        self.delay_channels = [d for d in delay_channels]
        self.delay_channels = ['xshut', 'lshut', 'xscope', 'lscope', 'fspck',
                               'nsl', 'fspump', 'spare', 'chopt', 'heatt']

        delay_channels.append(self.oscillator.name)
        self.delay_channels_and_osc = delay_channels
        self.delay_channels_and_osc = ['xshut', 'lshut', 'xscope', 'lscope',
                                       'fspck', 'nsl', 'fspump', 'oscillator',
                                       'spare', 'chopt', 'heatt']

        self.chopper_channels = ['heatt', 'chopt']
        self.laser_channels = ['oscillator', 'fspump', 'fspck', 'nsl']

        self._prescaler_register_address = "0x14"
        self._burst_register_address = "0x68"

        # for laser off emulation
        self.is_off = False
        self.lxt_laser_off_position = lxt_laser_off_position

    def _get_delay_channel(self, group=1, channel=1):
        found = None
        for d in self.delay_channels:
            ch = getattr(self, d)
            if ch.group == group and ch.channel == channel:
                found = ch
                break
        return found

    def _read_CS0(self):
        log_debug(self, "read CS0 register")
        reg_value = self._register_read("0x00")

        cs0_map = self._cs0_mapping

        ret = dict()
        for key, (pos, rw) in cs0_map.items():
            if rw.find("r") < 0:
                continue
            else:
                if isinstance(pos, int):
                    ret[key] = reg_value.get_bits(pos).as_bool()[0]
                else:
                    p0, p1 = pos[0], pos[1]
                    ret[key] = reg_value.get_bits(p0, last_bit=p1).as_int()
        ret["orbit/delay_in_ps"] = ret["orbit/delay_in_steps"] * 75.
        self._info.update(ret)
        return ret

    def _write_CS0(self, **kw):
        log_debug(self, "Write CS0 register")
        cs0_map = self._cs0_mapping

        reg_value = self._register_read("0x00")
        for key in kw:
            if key not in cs0_map:
                raise ValueError(
                    "Trying to change '%s' in CS0 but " % key
                    + "its address is not defined in _cs0_mapping"
                )

        for key in kw:
            pos, rw = cs0_map[key]
            is_writable = rw.find("w") >= 0
            if not is_writable:
                log_warning(
                    self,
                    "Trying to write read-only attribute (%s)",
                    key + ", it will be ignored",
                )
            else:
                reg_value = reg_value.change_bits(pos, bool(kw[key]))

        return self._register_write("0x00", reg_value)

    # def write_CS0(self, **kw):
    #     cs0_map = self._cs0_mapping

    #     reg_value = self._register_read("0x00")
    #     for key in kw:
    #         if key not in cs0_map:
    #             raise ValueError(
    #                 "Trying to change '%s' in CS0 but " % key
    #                 + "its address is not defined in _cs0_mapping"
    #             )

    #     for key in kw:
    #         pos, rw = cs0_map[key]
    #         is_writable = rw.find("w") > 0
    #         if not is_writable:
    #             log_warning(
    #                 self,
    #                 "Trying to write read-only attribute (%s)",
    #                 key + ", it will be ignored",
    #             )
    #         else:
    #             reg_value = reg_value.change_bits(pos, bool(kw[key]))

    #     return self._register_write("0x00", reg_value)

    def _read_CS1(self):
        log_debug(self, "Reading CS1 register")
        reg_value = self._register_read("0x04")

        bit_positions = {
            "temp_fpga": (0, 7),
            "temp_module": (8, 15),
            "is_temp_alert": 16,
            "is_temp_panic": 17,
            "is_power_ok": 18,
            "is_firmware_ok": (19, 23),
            "is_id09_mezzanine": (24, 27),
            "firmware_variant": (28, 31)
        }

        ret = dict()
        for key, pos in bit_positions.items():
            if isinstance(pos, int):
                ret[key] = reg_value.get_bits(pos).as_bool()[0]
            else:
                ret[key] = reg_value.get_bits(pos[0], last_bit=pos[1]).as_int()
            if key == "is_firmware_ok":
                if ret[key] == 27:
                    ret[key] = True
                else:
                    ret[key] = False
            if key == "firmware_variant":
                if ret[key] == 1:
                    ret[key] = 'hlc'
                elif ret[key] == 3:
                    ret[key] = 'pic'
                else:
                    ret[key] = 'unknown'
            if key == "is_id09_mezzanine":
                if ret[key] == 1:
                    ret[key] = True
                else:
                    ret[key] = False

        self._info.update(ret)
        return ret

    @property
    def firmware_variant(self):
        return self._read_CS1()["firmware_variant"]

    def _read_BRST(self):
        """Read Burst Control registers."""
        reg_value = self._register_read("0x68")

        brst_map = {
            "burst_count_overflow_threshold": (0, 19),
            "group3_channel_source": (20, 21),
        }

        # group3_channel_source = 0 --> "xshut"
        # group3_channel_source = 1 --> "lshut"
        # ...

        ret = dict()
        for key, pos in brst_map.items():
            if isinstance(pos, int):
                ret[key] = reg_value.get_bits(pos).as_bool()[0]
            else:
                ret[key] = reg_value.get_bits(pos[0], last_bit=pos[1]).as_int()
        return ret

    def _read_group_frequency_divider(self, group=None):
        ret = self._register_read(self._prescaler_register_address)
        div1 = ret.get_bits(0, nbits=10).as_int()
        div2 = ret.get_bits(10, nbits=10).as_int()
        div3 = ret.get_bits(20, nbits=10).as_int()
        ret = [div1, div2, div3]

        for i in range(3):
            if ret[i] == 0:
                ret[i] = 1024

        if group is not None:
            ret = ret[group - 1]
        return ret

    def set_group_dividers(self, dividers, group=None):
        """
        If group is not None, values is tuple (group1, group2, group3)
        else a single value for a given group is expected
        use dividers zero to divide by 1024
        """
        ret = self._register_read(self._prescaler_register_address)
        div1 = ret.get_bits(0, nbits=10).as_int()
        div2 = ret.get_bits(10, nbits=10).as_int()
        div3 = ret.get_bits(20, nbits=10).as_int()

        if group is None:
            dividers = [int(d) for d in dividers]
            div1, div2, div3 = dividers
        elif group == 1:
            div1 = int(dividers)
        elif group == 2:
            div2 = int(dividers)
        elif group == 3:
            div3 = int(dividers)
        else:
            raise ValueError("group has to be None or 1|2|3")

        dividers = (div1, div2, div3)
        for i, divider in enumerate(dividers):
            if divider == 0:
                log_warning(
                    self,
                    "Group divider %d is zero, it will be",
                    (i + 1) + "interpreted as 1024",
                )
            elif divider == 1024:
                dividers[i] = 0
            elif divider > 1024:
                raise ValueError("Group divider %d cannot be > 1024" % (i + 1))
        div1, div2, div3 = dividers

        ret = ret.change_bits(0, div1, nbits=10)
        ret = ret.change_bits(10, div2, nbits=10)
        ret = ret.change_bits(20, div3, nbits=10)
        return self._register_write(self._prescaler_register_address, ret)

    def _reset_dll_main(self, sleep_time=0.5):
        reg_addr = "0x00"
        reg = self._register_read(reg_addr)
        new_reg = reg.change_bits(0, 1)
        self._register_write(reg_addr, new_reg)
        gevent.sleep(sleep_time)

    def _reset_dll_RFdiv4(self):
        reg_addr = "0x00"
        reg = self._register_read(reg_addr)
        new_reg = reg.change_bits(1, 1)
        new_reg = new_reg.change_bits(2, 1)
        self._register_write(reg_addr, new_reg)

    def synchronize_to_orbit(self, time_out=5):
        """
        Adjust the phase between the Orbit Ref Signal to RF/992 Signal.

        Notes
        -----
        The RF and Orbit signals are synchronous, but with some delay
        (phase shift). To compensate for this, a fine delay line (0-4.8ns)
        with 75ps resolution is available.

        When the sync_monitor option is enabled, this fine delay is left
        untouched, but an error flag is raised in case the Orbit and RF/992
        relative phase is not zero.

        When the sync_monitor option is disabled, it is possible to recalibrate
        the delay in order to impose zero phase. The 'synchronize_to_orbit'
        command takes care of temporary disabling the sync_monitor, performing
        a recalibration, checking if the recalibration has worked fine.

        """
        # reg_addr = "0x00"
        # disable orbit sync ebale
        self._write_CS0(**{"orbit/is_sync_monitor_enable": False})

        # clear orbit sync error (bit 4 to 1)
        self._write_CS0(**{"orbit/clear_sync_error": True})

        # stop clear sync error (bit 4 to 0)
        self._write_CS0(**{"orbit/clear_sync_error": False})

        gevent.sleep(0.2)

        # request orbit sync (bit 3 to 1)
        self._write_CS0(**{"orbit/calibration_request": True})

        gevent.sleep(0.5)

        # stop request orbit sync (bit 3 to 0)  ???

        done = False
        t0 = time.time()

        while not done:
            print("... trying to lock the N354 to the orbit clock", end="\r")
            done = self._read_CS0()["orbit/is_tuning_done"]
            gevent.sleep(0.1)
            if (time.time() - t0) > time_out:
                msg = "## TIMEOUT while trying to lock the N354 to orbit clock"
                print_red(msg)
                log_critical(self, msg)
                break

        print("WARNING: if choppers are spinning, wait for them to stabilize!")

        self._info["orbit/last_sync_attempt"] = time.asctime()
        done = self._read_CS0()["orbit/is_tuning_done"]

        # orbit sync monitor allows to get warning message on GUI
        # if synchronization failed
        self.enable_orbit_sync_monitor()

        # reset mail DLL to change status of orbit/is_sync_error
        self._reset_dll_main()

        if done:
            self._info["orbit/last_sync_done"] = time.asctime()
        else:
            self._info["orbit/last_sync_delay"] = "not done"

    def set_defaults(self):
        ans = input("Are you sure [y/n]")
        if ans.lower() != "y":
            print("Aborting set_defaults")
            return

        for delay in self.delay_channels:
            delay.move_delay((1, 1), as_steps=True)
            width_selector = 2 if delay.group == 3 else 3
            delay.change_shaper(
                width_selector=width_selector,
                is_inverted=False,
                is_software_controlled=False,
                is_toggled=False,
            )

        self.lshut.change_shaper(is_software_controlled=True)

        self.set_group_dividers((360, 1, 1))
        if self.firmware_variant != 'pic':
            self.heatt.move_frequency(14, as_divider=True)
        self.chopt.move_frequency(1, as_divider=True)

        self.enable_groups()

    def enable_groups(self, group=None):
        """
        group must be None (for all)
        or int
        """
        if group is None:
            self._write_CS0(
                is_group1_enable=True, is_group2_enable=True,
                is_group3_enable=True
            )
        elif group == 1:
            self._write_CS0(is_group1_enable=True)
        elif group == 2:
            self._write_CS0(is_group2_enable=True)
        elif group == 3:
            self._write_CS0(is_group3_enable=True)
        else:
            raise ValueError("group must be None|1|2|3")

    def enable_orbit_sync_monitor(self):
        r = {"orbit/is_sync_monitor_enable": True}
        self._write_CS0(**r)

    def reset(self, time_out=5):
        ans = input("Are you sure ? [y/n] ")
        if ans.lower() == "y":
            self._reset_dll_main()
            self._reset_dll_RFdiv4()
            self.synchronize_to_orbit()

    def _burst_read_register(self):
        log_debug(self, "Reading burst register")
        reg = self._register_read(self._burst_register_address)
        npulses = reg.get_bits(0, 19).as_int()
        source_channel = reg.get_bits(20, 21).as_int() + 1
        ret = {"burst/npulses": npulses,
               "burst/count_source_channel": source_channel}
        self._info.update(**ret)
        log_debug(self, "burst register read", self._info)
        return ret

    def _burst_write_register(self, npulses=1, count_source_channel=1):
        log_debug(self, "writing burst register")
        npulses = int(npulses)
        if npulses > pow(2, 20):
            raise ValueError("Maximum number of pulses for burst is 1048576")
        count_source_channel = int(count_source_channel)
        if count_source_channel > 4 or count_source_channel < 1:
            raise ValueError("Count channel number must 1-4")

        # careful, parenthesys are needed when bit shifting
        reg = npulses + ((count_source_channel - 1) << 20)

        _ = self._register_write(self._burst_register_address, reg)
        d = self._get_delay_channel(group=3, channel=count_source_channel)
        msg = "Burst set to channel %d (%s)" % (count_source_channel, d.name)
        msg += ", npulses=%d" % npulses
        log_debug(self, msg)
        return msg

    def burst_is_running(self):
        return self._read_CS0()["burst/is_running"]

    def burst_enable_ext_trig(self):
        """Enable burst external triggering (through INA)."""
        self._write_CS0(**{"burst/use_ext_trigger": True})

    def burst_disable_ext_trig(self):
        """Disable burst external triggering (through INA)."""
        self._write_CS0(**{"burst/use_ext_trigger": False})

    def burst_set_count(self, npulses):
        """Set the total number of pulses in the burst."""
        # Following two lines could make a ct(0.001) work in pulsed mode
        # if npulses == 0:
        #    npulses = 1
        channel = self._burst_read_register()["burst/count_source_channel"]
        return self._burst_write_register(npulses=npulses,
                                          count_source_channel=channel)

    def burst_get_count(self):
        """Return the number of remaining pulses in the burst."""
        return self._burst_read_register()["burst/npulses"]

    def burst_off(self):
        """
        Disable N354 burst mode.

        This restores free-running mode in all channels of group3 and in the
        O24 ("spare") channel.

        The total number of pulses in the burst is set to 0.

        """
        channel = self._burst_read_register()["burst/count_source_channel"]
        return self._burst_write_register(npulses=0,
                                          count_source_channel=channel)

    def burst_channels_inhibit(self):
        """Inhibit burst channel outputs (stop firing): group3 + O24 (spare)"""
        self.set_burst_count(1)

    def burst_select_count_channel(self, output=1):
        """Select what Group3 channel is used to monitor the burst."""
        if isinstance(output, str):
            output = getattr(self, output)
        if isinstance(output, DelayChannel):
            group = output.group
            output = output.channel
            if group != 3:
                raise ValueError("Output has to be a group 3 channel")
        npulses = self.burst_get_count()
        self._burst_write_register(npulses=npulses,
                                   count_source_channel=output)

    def set_all_delays_to_zero(self):
        """Set the delay user value of each channel to zero."""
        for delay in self.delay_channels_and_osc:
            channel = getattr(self, delay)
            channel.set_delay(0)

    def print_info(self, reset_main_dll=False):
        """Resetting main DLL allows to trust orbit/is_sync_error"""
        if reset_main_dll:
            self._reset_dll_main()
        self._read_CS0()
        self._read_CS1()
        self._burst_read_register()
        keys = list(self._info.keys())
        keys.sort()
        nchar = max([len(k) for k in keys]) + 1
        fmt = "%%-%ds: %%s" % nchar
        alert_keys = ["error", "alert", "panic", "wrong"]
        ok_keys = ["ready", "done", "enable", "locked"]
        for k in keys:
            v = self._info[k]
            string = fmt % (k, str(v))
            is_alert_key = any([k.find(s) > 0 and v for s in alert_keys])
            should_be_true = any([k.find(s) > 0 and not v for s in ok_keys])
            if k == "burst/count_source_channel":
                d = self._get_delay_channel(group=3, channel=v)
                string += " (%s)" % d.name
            if is_alert_key or should_be_true:
                if k == "orbit/is_sync_error" and not reset_main_dll:
                    print_dim(string)
                else:
                    print_red(string)
            else:
                print(string)
        print()

    def print_delays(self, as_long_str=True):
        for delay_name in self.delay_channels_and_osc:
            delay = getattr(self, delay_name)
            if as_long_str:
                print(delay.as_long_str())
            else:
                print(delay.as_short_str())
        print()

    def print_dial_delays(self, as_long_str=False):
        for delay_name in self.delay_channels_and_osc:
            delay = getattr(self, delay_name).dial_offset
            if as_long_str:
                delay = TimeDelay(delay).as_long_str()
            else:
                delay = TimeDelay(delay).as_short_str()
            print("{0:<12} {1:s}".format(delay_name, delay))
        print()

    def print_laser_delays(self, as_long_str=False, as_dial=True):
        for delay_name in self.laser_channels:
            delay = getattr(self, delay_name)
            if as_long_str:
                print(delay.as_long_str())
            else:
                print(delay.as_short_str(as_dial=as_dial))
        print()

    def print_chopper_delays(self, as_long_str=False, as_dial=True):
        for delay_name in self.chopper_channels:
            delay = getattr(self, delay_name)
            if as_long_str:
                print(delay.as_long_str())
            else:
                print(delay.as_short_str(as_dial=as_dial))
        print()

    def print_all(self):
        self.print_info()
        self.print_delays()

    def __info__(self):
        info = "N345 fpga synchronization board"
        info += " (IP=%s)" % self.address[0]
        return info

    def __repr__(self):
        return self.__info__()

    def lxt_move_ns(self, delay, move_scopes=True, verbose=False):

        self.is_off = False
        if delay == self.lxt_laser_off_position:
            self.is_off = True
            return

        hs_chopper_t = 1 / self.chopt.read_frequency()

        # partition delay between xshut and laser
        xshut_delay, delay = delay_partitioner(delay, hs_chopper_t)
        # xshut_delay, delay = divmod(delay, hs_chopper_t)
        # xshut_delay *= hs_chopper_t
        laser_delay = -delay
        if verbose:
            print("Partitioning lxt_move_ns to achive", delay)
            print(f"  xshut_delay: {xshut_delay}")
            print(f"  laser_delay: {laser_delay}")

        # ML: 21-Jun-2023:
        channels = [self.xshut, self.nsl, self.lshut]
        delay_values = [xshut_delay, laser_delay, laser_delay]
        for channel, delay in zip(channels, delay_values):
            offset_value = channel.dial_offset
            dial_delay_value = delay + offset_value
            if dial_delay_value < 0:
                raise ValueError("Cannot move %s " % channel.name +
                                 "to %s. " % TimeDelay(delay) +
                                 "Dial delay would be negative " +
                                 "(%s)" % TimeDelay(dial_delay_value) +
                                 "since offset = %s." % TimeDelay(offset_value)
                                 )
            # TO DO: add also check that delay is not larger than period

        self.xshut.move_delay(xshut_delay)
        self.nsl.move_delay(laser_delay)
        self.lshut.move_delay(laser_delay)

        if move_scopes:
            # ML: 15-Nov-2022 following line commented and replaced with
            # the following one
            # self.xscope.move_delay(xshut_delay + self.xshut.dial_offset)
            self.xscope.move_delay(xshut_delay)
            self.lscope.move_delay(laser_delay)

    def lxt_read_ns(self):
        if self.is_off:
            return self.lxt_laser_off_position
        xshut = self.xshut.read_delay()
        nsl = self.nsl.read_delay()
        return xshut - nsl

    def lxt_move_ps(self, delay, move_spare=True, move_scopes=True,
                    verbose=False):

        self.is_off = False
        if delay == self.lxt_laser_off_position:
            self.is_off = True
            return

        hs_chopper_t = 1 / self.chopt.read_frequency()

        # partition delay between xshut and laser
        xshut_delay, delay = delay_partitioner(delay, hs_chopper_t)
        # xshut_delay, delay = divmod(delay, hs_chopper_t)
        # xshut_delay *= hs_chopper_t
        laser_delay = -delay
        if verbose:
            print(xshut_delay, laser_delay)

        self.xshut.move_delay(xshut_delay)

        self.fspump.move_delay(laser_delay)
        self.fspck.move_delay(laser_delay)
        self.oscillator.move_delay(laser_delay)
        self.lshut.move_delay(laser_delay)

        if move_spare:  # celine 2022/07/13
            self.spare.move_delay(laser_delay)

        if move_scopes:
            self.xscope.move_delay(xshut_delay)
            self.lscope.move_delay(laser_delay)

    def lxt_read_ps(self):

        if self.is_off:
            return self.lxt_laser_off_position

        xshut = self.xshut.read_delay()
        fspck = self.fspck.read_delay()
        osc = self.oscillator.read_delay()

        rf_t = self.oscillator.rf_t
        # math.fmod behaves differently for negative numbers
        return xshut - (fspck + signed_modulus(osc - fspck, rf_t * 4))

    def save_offsets(self, fname):
        s = ""
        for delay in self.delay_channels_and_osc:
            s += "%15s %+18.15f\n" % (delay, getattr(self, delay).dial_offset)
        print(s)
        with open(fname, "w") as f:
            f.write("# " + time.asctime() + "\n")
            f.write(s)

    def read_offsets(self, fname):
        with open(fname, "r") as f:
            lines = f.readlines()
        lines = [ln.lstrip() for ln in lines]
        for line in lines:
            if line.startswith("#"):
                continue
            name, offset = line.split()
            offset = float(offset)
            delay = getattr(self, name)
            delay.dial_offset = offset

    def config_save(self, fname, move_lxt_to_zero=True):
        if not id09_yaml:
            raise Exception("Cannot save config: " +
                            "yaml_storage module not imported")
        data_dict = dict()
        for i in range(0, 128, 4):
            data_dict[i] = self._register_read(i).as_int()
        for motor_name in self.delay_channels_and_osc:
            data_dict[motor_name] = getattr(self, motor_name).dial_offset
        if fname is not None:
            out_fname = os.path.join(CONFIG_folder, fname)
            if os.path.exists(out_fname):
                print("Overwriting already existing file:")
                print(out_fname)
                ans = input("Are you sure (yes/NO)? ")
                if ans[:3].lower() != "yes":
                    print("Configuration NOT saved!")
                    return
            f = yaml_storage.Storage(filename=out_fname)
            f.update(data_dict)
            del f
            self.config_fname_last = fname
        return data_dict

    def _get_config(self, fname):
        fname = os.path.join(CONFIG_folder, fname)
        data_dict = yaml_storage.Storage(filename=fname)
        return data_dict

    def config_print(self, fname=None, data_dict=None, print_all=False,
                     as_long_str=False):
        if fname is None:
            if self.config_fname_last is not None:
                fname = self.config_fname_last
            else:
                raise ValueError("'fname' and 'self.config_fname_last' " +
                                 "cannot be both None.")
        if data_dict is None:
            data_dict = self._get_config(fname)
        print()
        if print_all:
            for i in range(0, 128, 4):
                config_value = data_dict[i]
                current_value = self._register_read(i).as_int()
                print("register %d: %d (current value: %d)"
                      % (i, config_value, current_value))
        for delay in self.delay_channels_and_osc:
            conf_val = data_dict[delay]
            curr_val = getattr(self, delay).dial_offset
            if as_long_str:
                conf_val = TimeDelay(conf_val).as_long_str()
                curr_val = TimeDelay(curr_val).as_long_str()
            else:
                conf_val = TimeDelay(conf_val).as_short_str()
                curr_val = TimeDelay(curr_val).as_short_str()
            outstr = "{0:<10}".format(delay)
            outstr += " {0:s}".format(conf_val)
            outstr += "   (current_value: {0:s})".format(curr_val)
            if conf_val == curr_val:
                print(outstr)
            else:
                print_red(outstr)
        print()

    def config_restore(self, fname):
        data_dict = self._get_config(fname)
        ans = input("Confirm N354 configuration restore from file (y/N)? ")
        if ans[:3].lower() not in ["y", "yes"]:
            print("--> N354 configuration UNCHANGED\n")
            return
        else:
            for i in range(0, 128, 4):
                if i in range(108, 119):
                    continue  # from 0x6c to 0x74 reserved
                self._register_write(i, data_dict[i])
            for motor_name in self.delay_channels_and_osc:
                motor = getattr(self, motor_name)
                motor.dial_offset = data_dict[motor_name]
                # OFFSETS[motor_name] = data_dict[motor_name]
        # rf256 has to be triggered manually (writing to register does not move
        #                                     to right position)
        OFFSETS.save()  # force saving
        self.oscillator._apply_rf_256()
        print("--> N354 configuration MODIFIED")
        self.config_print(fname, data_dict=data_dict)
        self.config_fname_last = fname

    def config_list(self, return_fnames=False):
        # list all configuration file in standard folder "CONFIG_folder"
        fnames = os.listdir(self.config_folder)
        full_fnames = [os.path.join(self.config_folder, f) for f in fnames]
        mtimes = [os.path.getmtime(f) for f in full_fnames]
        out = []
        mtimes, fnames = zip(*sorted(zip(mtimes, fnames)))
        if return_fnames:
            return fnames
        for f, t in zip(fnames, mtimes):
            out.append("%s:   %s" % (time.ctime(t), f))
        print("\n".join(out))

    def lshutopen(self):
        # check that burst count is 0
        if self.burst_get_count() != 0:
            # print("burst is running ... switch it off using " +
            #       "n354_burst_off and then try again\n")
            switchoff = input("burst is running... " +
                              "switch burst_off? (yes/no): \n")
            if switchoff == "yes":
                self.burst_off()
            else:
                print("laser shutter is still closed")
                return
        # else:
        # check if software controlled:
        if self.lshut._read_control_register()["is_software_controlled"]:
            self.lshut.change_shaper(is_inverted=True)
        else:  # pulsed mode
            self.lshut.change_shaper(is_inverted=False)

    def lshutclose(self):
        self.lshut.change_shaper(is_software_controlled=True,
                                 is_inverted=False)

    def xshut_prepare_for_pulsed(self):
        # we set xshut for fpga mode for pulsed triggering
        self.xshut.change_shaper(
            width_selector=2,  # 50 us
            is_software_controlled=False,  # pulsed mode
            is_inverted=False,  # pulses follow the burst
        )

    def xshut_prepare_for_gated(self):
        # we set xshut for soft. mode, just a gate
        self.xshut.change_shaper(
            width_selector=2,  # 50 us
            is_software_controlled=True,  # gate mode
            is_inverted=True,  #
        )

    def lshut_prepare_for_pulsed(self):
        # we set xshut for fpga mode for pulsed triggering
        self.lshut.change_shaper(
            width_selector=3,  # 3 ms
            is_software_controlled=False,  # channel follow the burst
            is_inverted=False,  # will open with the pulses
        )

    def lshut_prepare_for_gated(self):
        # we set xshut for soft. mode, just a gate
        self.lshut.change_shaper(
            width_selector=3,  # 3ms
            is_software_controlled=True,  # channel does not follow the burst
            is_inverted=True,  # should be "True" for gated mode in SC
        )

    def bit_upload(self, fname=None, folder=None, config_restore_last=False):
        """Upload BIT file (firmware/virtex) in N354 board."""

        if fname is None:
            fname = self.bit_fname
        else:
            basename = os.path.dirname(fname)
            if fname == basename:
                folder = os.path.dirname(self.bit_fname)
                fname = os.path.join(folder, fname)

        if not os.path.exists(fname):
            raise ValueError("File does not exist:\n%s\n" % fname)
        else:
            fname = os.path.realpath(fname)
            print("Uploading following BIT file in N354:\n%s\n" % fname)

        # read data (as 2-bytes array, needed for checksum)
        with open(fname, "rb") as f:
            data = f.read()

        # cast data as 2-bytes unsigned integer (needed for checksum)
        data_as_H = array.array("H")
        data_as_H.frombytes(data)
        checksum = sum(data_as_H) & 0xffffffff

        # send 'you are about to upload'
        self._socket._send("*PROGBIT ")

        class Data:

            def __init__(self, value):
                if isinstance(value, str) and value.startswith("0x"):
                    value = int(value, 16)
                self._value = value

            def as_bytes(self, n=4):
                return self._value.to_bytes(n, "little")

            def as_16bits(self):
                return self.as_bytes(n=2)

            def as_32bits(self):
                return self.as_bytes(n=4)

        # from now on, send raw data, use little utility function
        def send(data, nbytes=None):
            if nbytes is not None:
                data = Data(data).as_bytes(nbytes)
            self._socket._send_raw(data)

        # send 'magic' number
        send("0xa5aa555a", nbytes=4)

        # size to send
        send_size = len(data_as_H) + 1
        send(send_size, nbytes=4)

        # send checksum
        send(checksum, nbytes=4)

        # send payload
        data_as_H.append(0)
        send(data_as_H)

        if config_restore_last:
            self.config_restore(self.config_list(return_fnames=True)[-1])

    def bit_symbolic_link_swap(self):
        """Modify BIT file symbolic link from HLC to PIC (or viceversa)."""
        fname = os.path.realpath(self.bit_fname)
        folder = os.path.dirname(fname)
        bit_fnames = ["n354id09_pic_b.bit", "n354id09_eybert.bit"]
        bit_fnames = [os.path.join(folder, f) for f in bit_fnames]
        destination = os.path.join(folder, "n354id09.bit")
        if fname not in bit_fnames:
            raise Exception("Current BIT file should be one of:", bit_fnames)
        if fname == bit_fnames[0]:
            source = bit_fnames[1]
        elif fname == os.path.join(folder, bit_fnames[1]):
            source = bit_fnames[0]
        os.remove(destination)
        os.symlink(source, destination)
        print(f"New symbolic link points to following bit file: {source}")
