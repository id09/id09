import time
from bliss.comm import get_comm


class Chiller:
    """Class to communicate with the Termoteck P802 chiller."""

    def __init__(self, config=None, url='tango://id09/picctrl/serial_0',
                 baudrate=38400, timeout=0.2, sleep=0.2, debug=False,
                 verbose=True):

        """Initialize communication with LA2000."""

        if config is not None:
            url = config.get('url', url)
            baudrate = config.get('baudrate', baudrate)
            timeout = config.get('timeout', timeout)
            sleep = config.get('sleep', sleep)
            debug = config.get('debug', debug)
            verbose = config.get('verbose', verbose)

        conf = {'serial': {'url': url, 'baudrate': baudrate}}
        opts = {'timeout': timeout}
        self.comm = get_comm(conf, **opts)

        self.timeout = timeout
        self.sleep = sleep
        self.debug = debug
        self.verbose = verbose

    def hexlify(self, data, extra_space=False):
        if isinstance(data, int):
            hex_string = f'{data:0>2X}'
        elif isinstance(data, (list, bytes)):
            if extra_space:
                hex_string = ' '.join(f'{c:0>2X}' for c in data)
            else:
                hex_string = ''.join(f'{c:0>2X}' for c in data)
        else:
            raise TypeError("'data' must be int, bytes, or list of int")
        return hex_string

    def calc_chksum(self, data_len, dst, src, cmd1, cmd2, data=None,
                    return_cmd=False, verbose=True):
        cmd = [data_len, dst, src, cmd1, cmd2]
        if data is not None:
            for k in range(0, len(data), 2):
                cmd.append(data[k:k+2])
        cmd_int = [int(val, 16) for val in cmd]
        chksum_int = int('AA', 16) - sum(cmd_int)
        chksum = self.hexlify(chksum_int, extra_space=False)
        cmd = self.hexlify(cmd_int, extra_space=False)
        if verbose:
            print(f"Command without checksum : '{cmd}'")
            print(f"Command with checksum    : '{cmd+chksum}'")
        cmd += chksum
        if return_cmd:
            return cmd
        else:
            return chksum

    def split_cmd(self, cmd):
        cmd_dict = {'data_len': cmd[:2],
                    'dst': cmd[2:4],
                    'src': cmd[4:6],
                    'cmd1': cmd[6:8],
                    'cmd2': cmd[8:10],
                    'data': cmd[10:-2]}
        chksum = cmd[-2:]
        return cmd_dict, chksum

    def write(self, data_len, cmd1, cmd2, data=None,
              dst='01', src='00', verbose=True):

        cmd = self.calc_chksum(data_len, dst, src, cmd1, cmd2, data,
                               return_cmd=True, verbose=False)
        if verbose:
            print(f"Command: '{cmd}'")
        self.comm.write(bytes.fromhex(cmd))

    def read(self):
        ans = self.comm.raw_read()
        if self.hexlify(ans[0]) != 'AA':
            print("Chksum error: first byte im answer is not 0AAh.")
        ans = ans[1:-1]
        ans = self.hexlify(ans, extra_space=False)
        cmd_dict, chksum = self.split_cmd(ans)
        if chksum != self.calc_chksum(**cmd_dict, return_cmd=False,
                                      verbose=False):
            print("Chksum error: calculated value does not match last byte.")
        return cmd_dict

    def calc_data_from_offset(self, offset, length=2):
        offset_h = self.hexlify(offset).zfill(4)
        offset_h = offset_h[2:4] + offset_h[0:2]
        data = offset_h + self.hexlify(length)
        return data

    def get_par(self, offset, length, decimals=0, tt='01'):
        # tt='01': IO-data
        # tt='02': flag-data
        # tt='03': parameter data
        # tt='04': system data
        # tt='05': process-data
        data = self.calc_data_from_offset(offset, length)
        self.write('03', '13', tt, data, verbose=False)
        time.sleep(self.sleep)
        ans_dict = self.read()
        data = ans_dict['data']
        data = data[2:4] + data[0:2]
        par = int(data, 16)/10**decimals
        return par

    @property
    def fan_speed(self):
        """Fan speed (%)"""
        return self.get_par(offset=20, length=2, decimals=0)

    @property
    def exhaust_temperature(self):
        """Exhaust temperature Roh (degC)"""
        return self.get_par(offset=22, length=2, decimals=0)

    @property
    def outlet_temperature(self):
        """Outlet temperature Roh (degC)"""
        return self.get_par(offset=36, length=2, decimals=2)

    @property
    def inlet_temperature(self):
        """Inlet temperature Roh (degC)"""
        return self.get_par(offset=38, length=2, decimals=1)

    @property
    def flow(self):
        """Water flow (l/min)"""
        return self.get_par(offset=48, length=2, decimals=1)

    @property
    def firmware_ver(self):
        return self.get_par(offset=8, length=2, decimals=2, tt='02')

    def get_ver(self, return_int=False):
        self.write('0', '94', '0')
        time.sleep(self.sleep)
        ans = self.comm.raw_read()
        ans = ans[1:-1]
        dev_id = self.hexlify(ans[5:7])
        vers_soft = self.hexlify(ans[7:9])
        vers_kali = self.hexlify(ans[9:11])
        vers_sys = self.hexlify(ans[11:13])
        vers_par = self.hexlify(ans[13:15])
        vers_proc = self.hexlify(ans[15:17])
        if return_int:
            dev_id = int(dev_id, 16)
            vers_soft = int(vers_soft, 16)
            vers_kali = int(vers_kali, 16)
            vers_sys = int(vers_sys, 16)
            vers_par = int(vers_par, 16)
            vers_proc = int(vers_proc, 16)
        ver = {'dev_id': dev_id, 'vers_soft': vers_soft,
               'vers_kali': vers_kali, 'ver_sys': vers_sys,
               'vers_par': vers_par, 'vers_proc': vers_proc}
        return ver

    def get_status(self, retlist=False):
        """Get status."""
        status = []
        status.append(f"Flow: {self.flow} l/min")
        # status.append("Inlet temperature: " +
        #               f"{self.inlet_temperature} \u00b0C")
        # status.append("Outlet temperature: " +
        #               f"{self.outlet_temperature} \u00b0C")
        status.append("Exhaust temperature: " +
                      f"{self.exhaust_temperature} \u00b0C")
        status.append(f"Fan speed: {self.fan_speed}%")
        if not retlist:
            print('\n'.join(status)+'\n')
        else:
            return status

    def live_status(self, refresh_time=1):
        while True:
            try:
                status = self.get_status(retlist=True)
                if status is None:
                    break
                for line in status:
                    print(line)
                time.sleep(refresh_time)
                line_from_bottom = 0
                for line in status:
                    print("\033[1A", end="")  # line up
                    line_from_bottom += 1
                for line in status:
                    print("\x1b[2K")  # line clear
                    line_from_bottom += 1
                for line in status:
                    print("\033[1A", end="")  # line up
                    line_from_bottom -= 1
                time.sleep(0.001)
            except KeyboardInterrupt:
                for k in range(line_from_bottom):
                    print("\033[A", end="")  # line up
                for line in status:
                    print(line)
                print(end="\r")
                break


