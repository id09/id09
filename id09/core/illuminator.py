# -*- coding: utf-8 -*-
"""Module to communicate with sample illuminator."""

__author__ = "Matteo Levantino"
__contact__ = "matteo.levantino@esrf.fr"
__licence__ = "MIT"
__copyright__ = "ESRF - The European Synchrotron, Grenoble, France"
__data__ = "12/12/2020"
__version__ = "0.0.1"


from bliss.comm.util import get_comm, TCP
from bliss.comm.tcp import SocketTimeout


class Illuminator:
    """
    Connection is done through ser2net:
       - port 20100 corresponds to /dev/ttyUSB0
       - port 20101 corresponds to /dev/ttyUSB1
    """

    def __init__(self, config=None, url="id09raspi1", port=20100,
                 timeout=0.1, brightness=0, debug=False,
                 verbose=False):

        if config is not None:
            url = config.get("url")
            port = config.get("port")
            timeout = config.get("timeout", timeout)
            verbose = config.get("verbose", verbose)

        conf = {'tcp': ''}
        opts = {'url': url, 'port': port}
        self.comm = get_comm(conf, ctype=TCP, **opts)

        self.url = url
        self.timeout = timeout
        self.debug = debug
        self.verbose = verbose

        # --- setup ---
        if not debug:
            self.comm.connect()
            self._read_banner()

    def _read_banner(self, timeout=None):
        if timeout is None:
            timeout = self.timeout
        self.comm.readline(timeout=timeout, eol='\r\n\r\n')

    def write_readline(self, cmd, timeout=None):
        if timeout is None:
            timeout = self.timeout
        if not cmd.startswith("0"):
            cmd = "0" + cmd
        if not cmd.endswith(";"):
            cmd += ";"
        try:
            ans = self.comm.write_readline(cmd.encode(), eol=";",
                                           timeout=timeout)
            ans = ans.decode()
            return ans
        except SocketTimeout:
            print("Timeout error: check command syntax or increase timeout.")
            self._read_banner()

    def query(self, cmd):
        if not cmd.endswith("?;"):
            cmd += "?;"
        ans = self.write_readline(cmd)
        if ans is not None:
            ans = ans.replace(cmd[:-2], "")
            return ans

    def get_brightness(self):
        """Get lamp % brightness."""
        read = self.query("BR")
        hex_br = "0x" + read
        br = int(hex_br, 16)/10
        return br

    def set_brightness(self, value):
        """Set lamp % brightness."""
        if not 0 <= value <= 100:
            raise ValueError("Brightness should be 0-100. " +
                             "You asked for %.1f" % value)
        # lamps takes 0 to 1000
        value = int(value*10)
        hex_br = hex(value)[2:]  # [2:] removes '0x'
        npadding = 4-len(hex_br)
        hex_br = "0"*npadding + hex_br
        self.write_readline("0BR" + hex_br)
