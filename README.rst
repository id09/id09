============
ID09 project
============

[![build status](https://gitlab.esrf.fr/ID09/id09/badges/master/build.svg)](http://ID09.gitlab-pages.esrf.fr/ID09)
[![coverage report](https://gitlab.esrf.fr/ID09/id09/badges/master/coverage.svg)](http://ID09.gitlab-pages.esrf.fr/id09/htmlcov)

ID09 software & configuration

Latest documentation from master can be found [here](http://ID09.gitlab-pages.esrf.fr/id09)
